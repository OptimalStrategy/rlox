import iterators { iter as I, range };

/// A doubly linked list node.
class Node(val, prev, next);

/// An iterator for linked lists. Works with anything that has a `next` property. Must not contain cycles.
class LinkedListIterator {
    /// @param node: Node - anythig with a `next` property. Must not contain cycles.)
    init(node) {
        this.node = node;
    }

    iterator() {
        return this;
    }

    iternext() {
        if (this.node) {
            const node = this.node;
            this.node = this.node.next;
            return Ok(node);
        }
        return Err(nil);
    }
}

/// A doubly linked list.
class LinkedList {
    init() {
        this.head = nil;
        this.tail = nil;
        this._len = 0;
    }

    /// Returns the cached length of the list.
    /// Modifying the list manually may render this value invalid.
    ///
    /// Performance: O(1).
    len() {
        return this._len;
    }

    /// Counts the number of nodes in the list. Updates the `len()` property with the count.
    /// Performance: O(N).
    count() {
        this._len = I(this).count();
        return this._len;
    }

    /// Adds a new element to the end of the list.
    /// Performance: O(1).
    ///
    /// @param x: Any
    /// @return this: LinkedList
    push_back(x) {
        this._len += 1;
        match this.tail {
            nil => {
                this.tail = this.head = Node(x, nil, nil);
            }
            _ => {
                this.tail.next = Node(x, this.tail, nil);
                this.tail = this.tail.next;
            }
        }
        return this;
    }

    /// Adds a new element to the start of the list.
    /// Performance: O(1).
    ///
    /// @param x: Any
    /// @return this: LinkedList
    push_front(x) {
        this._len += 1;
        match this.head {
            nil => {
                this.head = this.tail = Node(x, nil, nil);
            }
            _ => {
                const n = Node(x, nil, this.head);
                this.head.prev = n;
                this.head = n;
            }
        }
        return this;
    }

    /// Appends the given list or node to the end of the list.
    /// Panics if the given object is not a list or a node.
    ///
    /// @param node: LinkedList | Node - the list or node to append.
    append_back(node) {
        match node {
            class Node => this.append_node_back(node),
            class LinkedList => this.append_list_back(node),
            _ => panic("Expected a Node or a LinkedList, but got `${type(node)}`"),
        }
    }

    /// Appends the given node to the end of the list.
    /// Performance: O(children(Node)) = O(N).
    ///
    /// @param node: Node - the node to append.
    append_node_back(node) {
        if (this.tail) {
            this.tail.next = node;
            node.prev = this.tail;
        } else {
            this.head = this.tail = node;
        }
        this._len += 1;

        while (node.next) {
            node = node.next;
            this._len += 1;
        }
        this.tail = node;
    }

    /// Appends the given list to the end of this list.
    /// Performance: O(1).
    ///
    /// @param list: LinkedList - the list to append.
    append_list_back(list) {
        if (this.tail) {
            this.tail.next = list.head;
            list.head.prev = this.tail;
            this.tail = list.tail;
        } else {
            this.head = list.head;
            this.tail = list.tail;
        }
        this._len += list._len;
    }

    /// Attempts to find the given element by linearly scanning the list. Returns `nil`
    /// if the element is not in the list.
    /// Performance: O(N).
    ///
    /// @param x: Any
    find(x) {
        for (n in this) {
            if (n.val == x) {
                return n;
            }
        }
        return nil;
    }

    /// Removes the given element from the list and returns the node holding that element. Returns `nil`
    /// if the element is not in the list.
    /// Performance: O(N).
    ///
    /// @param x: Any
    /// @return: Node | nil - the removed node or `nil`.
    remove(x) {
        for (n in this) {
            if (n.val != x) continue;
            if (n.prev) {
                n.prev.next = n.next;
            }
            if (n.next) {
                n.next.prev = n.prev;
            }

            this._len -= 1;
            if (n == this.head) this.head = n.next;
            if (n == this.tail) this.tail = n.prev;
            return n;
        }
        return nil;
    }

    /// Returns `true` if the element is in the list.
    /// Performance: O(N).
    ///
    /// @param x: Any
    /// @return: bool
    contains(x) {
        return this.find(x) != nil;
    }

    /// Compares this list to the given list.
    ///
    /// @param list: LinkedList | Node - the list or node to compare to.
    /// @return: bool
    equals(list) {
        var other_node = match list {
            class LinkedList => list.head,
            _ => list,
        };
        var node = this.head;

        while (node != nil and other_node != nil) {
            if (node.val != other_node.val) return false;
            node = node.next;
            other_node = other_node.next;
        }

        return !(node or other_node);
    }

    /// Detects whether the list has a loop using the Floyd's cycle-finding algorithm,
    /// also known as the "tortoise and hare algorithm".
    /// Performance: O(L + I), where
    ///     L is the length of the loop and
    ///     I is the index of the first element in the cycle.
    has_loop() {
        if (!this.head) return false;

        var tortoise = this.head;
        var hare = this.head.next;

        while (tortoise != hare and hare and tortoise) {
            tortoise = tortoise.next;
            hare = hare.next;
            if (hare) hare = hare.next;
        }

        return tortoise != nil and tortoise == hare;
    }

    /// Returns an iterator over the nodes in the list.
    /// The iterator will yield nodes indefinitely if the list contains a cycle.
    iterator() {
        return LinkedListIterator(this.head);
    }

    /// Serializes the list as a string.
    to_str() {
        var s = "LinkedList [ ";
        var node = this.head;
        while (node != nil) {
            s += "${node.val}";
            node = node.next;
            if (node != nil) s += " -> ";
        }
        return "${s} ]";
    }
}

