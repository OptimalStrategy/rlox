extern crate assert_cmd;

#[macro_use]
mod common;

#[cfg(test)]
mod lox_std_tests {
    use super::*;

    fuzz_err!(
        fuzz1_get_without_field,
        r#"==> File 'tests/lox/fuzz/fuzz1_get_without_field.lox':
--> [Line 2, at 1] ParseError at end: Expected a property name after the `.`.
|
|    
|    
==> File 'tests/lox/fuzz/fuzz1_get_without_field.lox':
--> [Line 2, at 1] ParseError at end: Expected a `;` after the expression
|
|    
|    
"#
    );

    fuzz_err!(
        fuzz2_nested_interpolation,
        // The byte array below is almost identical to the following, except for
        // some invisible characters that can't be copy-pasted:
        //
        // ==> File 'tests/lox/fuzz/fuzz2_nested_interpolation.lox':
        // --> [Line 1, at 1] LexError at `"`: Unterminated string
        // |
        // |    "rTr{/{{${{{{{{{{{
        // |    ^
        std::str::from_utf8(&[
            61, 61, 62, 32, 70, 105, 108, 101, 32, 39, 116, 101, 115, 116, 115, 47, 108, 111, 120,
            47, 102, 117, 122, 122, 47, 102, 117, 122, 122, 50, 95, 110, 101, 115, 116, 101, 100,
            95, 105, 110, 116, 101, 114, 112, 111, 108, 97, 116, 105, 111, 110, 46, 108, 111, 120,
            39, 58, 10, 45, 45, 62, 32, 91, 76, 105, 110, 101, 32, 49, 44, 32, 97, 116, 32, 49, 93,
            32, 76, 101, 120, 69, 114, 114, 111, 114, 32, 97, 116, 32, 96, 34, 96, 58, 32, 85, 110,
            116, 101, 114, 109, 105, 110, 97, 116, 101, 100, 32, 115, 116, 114, 105, 110, 103, 10,
            124, 10, 124, 32, 32, 32, 32, 34, 114, 84, 114, 123, 0, 0, 47, 123, 123, 36, 123, 123,
            123, 123, 123, 123, 123, 123, 123, 10, 124, 32, 32, 32, 32, 94, 10
        ])
        .unwrap()
    );

    fuzz_err!(nosegfault fuzz4_stack_overflow1);

    fuzz_ok!(fuzz3_string_scanning_slice_concat, "a + b == 10${}\n");
}
