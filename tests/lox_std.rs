extern crate assert_cmd;

#[macro_use]
mod common;

#[cfg(test)]
mod lox_std_tests {
    use super::*;

    test_ok!(tstd1_string, "a\n \ns\nt\nr\ni\nn\ng\n");
    test_ok!(tstd2_intrinsics, "UpValue(77)\nUpValue(77)\n");
    test_ok!(
        tstd3_type_casting,
        "5\n1031200\n1\n37\n37.35\n<@Object instance>\nnil\nnil\n<fn: [lambda@49:14]>\n"
    );
}
