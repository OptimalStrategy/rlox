// Type converesions
assert(int(5) == 5);
assert(int(5.32) == 5);
assert(float(4) == 4.0);
assert(bool(777));
assert(!bool(0));
assert(str(bool) == "bool");

// Type conversion overloading
class Test {
    // A generic cast method called by the int, float, and bool builtins.
    cast(ty) {
        return match ty {
            int => 42,
            float => Math.PI,
            bool => true,
        };
    }

    to_str() { return "test"; }
}
assert(int(Test()), 42);
assert(float(Test()), Math.PI);
assert(Test().cast(bool));
assert(Test().cast(str) == nil);
assert(str(Test()) == "test");

// More tests
fun cast(arg, cast) {
    return cast(arg);
}

print cast(5, int);
print cast(10.312e5, int);
print cast(true, int);

const obj = object();
obj.to_int = fun () => 37;
obj.cast = fun (ty) => match ty {
    float => 37.35,
    _ => print "what's this ${ty} ${type(ty)}";
};
print cast(obj, int);
print cast(obj, float);
print cast(obj, str);
print cast(obj, Test().cast);
print cast(obj, fun(_) => nil);

obj.to_str = fun() => str(obj.to_str);
print cast(obj, str);

