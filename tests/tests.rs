extern crate assert_cmd;

#[macro_use]
mod common;

#[cfg(test)]
mod lang_file_tests {
    use super::*;

    test_ok!(t1_addition, "4\n1.5\n1.5\n5.93\nrandom\n");
    test_ok!(t3_hello_world, "Hello, World!\n");
    test_ok!(t4_global_variables, "nil\n10\nbeignets with cafe au lait\n");
    test_ok!(
        t5_local_variables,
        "shadowing\nlocal 2\nlocal 3\nlocal\nglobal\n"
    );
    test_ok!(t9_const_variables, "10\n5\n10\n");
    test_ok!(t12_if_control_flow, "1\n0\n");
    test_ok!(t13_ternary_operator, "b is ten\nb is not ten\nb >= 3\n");
    test_ok!(t15_test_logical_operators, "true\ntrue\n0\n0\n1\n1\ntrue\n");
    test_ok!(t16_test_while_loop, "5\n4\n3\n2\n1\n0\n");
    test_ok!(t17_test_for_loop, "6\n6\n");
    test_ok!(t18_test_fibonacci_35, "9227465\n");
    test_ok!(
        t19_test_break_and_continue,
        "0\n1\n2\n3\n4\n4\n6\nwhile\nfor\n0\n2\n5\n4\n2\n1\n0\n"
    );
    test_ok!(t21_test_loop_statement, "0\n");
    test_ok!(
        t22_scopes_and_loops,
        r#"77
77
I am so frickin' big you better not mess with me
I am a fake small boy that pretends to be real big
what the hell are you talking about are you ok?
what the hell are you talking about are you ok?
I am so frickin' big you better not mess with me
"#
    );
    test_ok!(
        t23_break_and_continue_pop_only_available_values,
        "value\n5\n7\n12\n"
    );
    test_ok!(
        t24_increment_decrement,
        "0\n1\n1\n1\n2\n1\n1\n1\n0\n0\n1\n1\n1\n2\n1\n1\n1\n0\n"
    );
    test_ok!(t25_break_and_continue_pop_only_the_loop_scope, "0\n1\n2\n");
    test_ok!(t26_string_interpolation, "a + b == 10\n${}\n");
    test_ok!(t27_closures, "----- C1 -----\n<fn: [lambda@11:12]>\n<fn: getter>\n<fn: setter>\nc1: nil\nc1: 5\n\
                            ----- C2 -----\n<fn: [lambda@11:12]>\n<fn: getter>\n<fn: setter>\nc2: nil\nc2: 48\nc1: 5\n");
    test_ok!(t28_recursive_calls, "55\n6765\n120\n3628800\n");
    test_ok!(
        t29_test_logical_operators_are_lazy,
        "true\ntrue\ntrue\n0\n0\n0\n0\n1\n1\ntrue\n1\n1\n"
    );
    test_ok!(
        t31_test_class_objects,
        r#"Today's breakfast: Eggs a-fryin'!
Enjoy your breakfast, Yann LeCun.
Enjoy your breakfast, Ian Goodfellow.
I invented this breakfast 20 years ago!
"#
    );
    test_ok!(t32_test_this_property, "The Review GAN is awesome!\n");
    test_ok!(t34_test_empty_return_is_allowed_in_initializers, "42, 0\n");
    test_ok!(t35_test_class_initializers, "Test\nnew\n");
    test_ok!(t36_test_init_captures_variables, "5\n");
    test_ok!(ignore => t38_test_static_methods, "");
    test_ok!(ignore => t39_test_getters, "");
    test_ok!(t41_test_inheritance, "Fry until golden brown.\n");
    test_ok!(
        ignore => t44_test_inheritance_dynamic_superclass_access,
        "Fry until golden brown. Pipe full of custard and coat with chocolate.\n"
    );
    test_ok!(t45_test_inheritance_chain, "A method\n");
    test_ok!(
        t47_test_match_allows_to_omit_semicolons_in_expression_statements,
        ""
    );
    test_ok!(
        t48_test_match_expression_allows_select_statements_in_place_of_expressions,
        "nil\n2\nnil\n33\nnil\n"
    );
    test_ok!(
        t51_test_match_expression_type_matching,
        "true\ntrue\nfalse\ntrue\ntrue\nnil\ntrue\ntrue\nnil\nint\nstr\nbool\nbool\nfloat\n"
    );
    test_ok!(
        t52_test_const_constant_propagation,
        "56\n207885634\nnil\nf\n[g]\na, a\n10\n\
        00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000\n"
    );
    test_ok!(
        t56_test_rest_args,
        "[1, 2, 3]\n[1, 2, 3, 3, 2, 1]\n[1, 2, 3, 3, 2, 1, 123]\n[1, 2, 3, 4, 77]\n"
    );
    test_ok!(t57_test_extra_numeric_literals, "3735927486, 65535, 123\n");
    test_ok!(t59_records, "Node(5, Node(7, nil))\nNode(7, nil)\n");

    // Optimizations
    test_ok!(topt1_arithmetic_opts, "");
    test_ok!(topt2_tco_fibonacci, "55\n55\n55\n");
    test_ok!(
        topt3_tco_methods,
        "1, 1, 1\n<Empty instance>\n<Init instance>\n"
    );
    test_ok!(
        topt4_tco_with_native_calls,
        "[0]
[77]
[0]
[[[[0], [0]], [[0], [0]]], [[[0], [0]], [[0], [0]]]]
"
    );

    // Errors
    #[cfg(not(feature = "lox-64bit-ints"))]
    test_err!(
        t2_error_on_too_big_integer_literals,
        r#"==> File 'tests/lox/t2_error_on_too_big_integer_literals.lox':
--> [Line 2, at 1] ParseError at `170141183460469231731687303715...`: Integer literal is too big
|
|    170141183460469231731687303715884105728; // Error
|    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
"#
    );

    test_err!(
        t6_local_var_may_not_be_redeclared,
        "==> File 'tests/lox/t6_local_var_may_not_be_redeclared.lox':\n\
         --> [Line 3, at 9] ResolutionError at `a`: Variable `a` is already declared in this scope\n\
         |\n\
         |    var a;\n\
         |        ^\n\
         ==> File 'tests/lox/t6_local_var_may_not_be_redeclared.lox':\n\
         --> [Line 7, at 9] ResolutionError at `b`: Variable `b` is already declared in this scope\n\
         |\n\
         |    var b = 77;\n\
         |        ^\n"
    );

    test_err!(
        t7_cannot_read_local_var_in_its_own_initializer,
        "==> File 'tests/lox/t7_cannot_read_local_var_in_its_own_initializer.lox':\n\
         --> [Line 2, at 13] ResolutionError at `a`: Cannot read the variable `a` in its own initializer\n\
         |\n\
         |    var a = a;\n\
         |            ^\n"
    );

    test_err!(
        t8_undefined_variable,
        "==> File 'tests/lox/t8_undefined_variable.lox':\n\
         --> [Line 1, at 9] ResolutionError at `undefined`: Undefined variable `undefined`\n\
         |\n\
         |    var a = undefined;\n\
         |            ^^^^^^^^^\n"
    );

    test_err!(
        t10_const_variable_must_be_assigned,
        "==> File 'tests/lox/t10_const_variable_must_be_assigned.lox':\n\
         --> [Line 1, at 7] ResolutionError at `unassigned`: Constant variable `unassigned` must be assigned a value\n\
         |\n\
         |    const unassigned;\n\
         |          ^^^^^^^^^^\n"
    );

    test_err!(
        t11_const_variable_cannot_be_assigned_more_than_once,
        r#"==> File 'tests/lox/t11_const_variable_cannot_be_assigned_more_than_once.lox':
--> [Line 2, at 1] ResolutionError at `five`: Cannot assign twice to the const variable `five`
|
|    five = 4;
|    ^^^^
==> File 'tests/lox/t11_const_variable_cannot_be_assigned_more_than_once.lox':
--> [Line 4, at 5] ResolutionError at `five`: Cannot assign twice to the const variable `five`
|
|    five = 3;
|    ^^^^
==> File 'tests/lox/t11_const_variable_cannot_be_assigned_more_than_once.lox':
--> [Line 8, at 5] ResolutionError at `five`: Cannot assign twice to the const variable `five`
|
|    five = 7;
|    ^^^^
==> File 'tests/lox/t11_const_variable_cannot_be_assigned_more_than_once.lox':
--> [Line 15, at 28] ResolutionError at `i`: Cannot assign twice to the const variable `i`
|
|    for (const i = 0; i < 3; ++i) {}
|                               ^
==> File 'tests/lox/t11_const_variable_cannot_be_assigned_more_than_once.lox':
--> [Line 16, at 26] ResolutionError at `j`: Cannot assign twice to the const variable `j`
|
|    for (const j = 0; j < 3; j++) {}
|                             ^
==> File 'tests/lox/t11_const_variable_cannot_be_assigned_more_than_once.lox':
--> [Line 20, at 10] ResolutionError at `v`: Cannot assign twice to the const variable `v`
|
|    _ => v = 2,
|         ^
"#
    );

    test_err!(
        t14_nested_ternary_is_not_allowed,
        "==> File 'tests/lox/t14_nested_ternary_is_not_allowed.lox':\n\
         --> [Line 1, at 20] ParseError at `?`: Nested ternary operators are not allowed.\n\
         |\n\
         |    print true ? false ? 0 : 1 : 1;\n\
         |                 ^^^^^^^^^^^^^ help: consider wrapping the operator in a pair of parentheses `()`:\n\
         |\n\
         |    print true ? (false ? 0 : 1) : 1;\n\
         |                 ^             ^\n"
    );

    test_err!(
        t20_test_break_and_continue_are_not_allowed_outside_of_a_loop,
        "==> File 'tests/lox/t20_test_break_and_continue_are_not_allowed_outside_of_a_loop.lox':\n\
         --> [Line 6, at 1] ResolutionError at `break`: Cannot break outside of a loop.\n\
         |\n\
         |    break;\n\
         |    ^^^^^\n\
         ==> File 'tests/lox/t20_test_break_and_continue_are_not_allowed_outside_of_a_loop.lox':\n\
         --> [Line 7, at 1] ResolutionError at `continue`: Cannot continue outside of a loop.\n\
         |\n\
         |    continue;\n\
         |    ^^^^^^^^\n"
    );

    test_err!(
        t30_test_tracebacks_in_native_functions,
        if common::OPT_LEVEL.unwrap_or("1") == "1" {
            r#"Error traceback (most recent call last):
==> File 'tests/lox/t30_test_tracebacks_in_native_functions.lox':
--> [Line 66, at 6, in t30_test_tracebacks_in_native_functions.lox]  at `(`: 
|
|    )()
|     ^
==> File 'tests/lox/t30_test_tracebacks_in_native_functions.lox':
--> [Line 66, at 6, in array.map]  at `(`: 
|
|    )()
|     ^
==> File 'tests/lox/t30_test_tracebacks_in_native_functions.lox':
--> [Line 66, at 6, in array.getitem] RuntimeError: Array index is out of bounds: `3` >= `0`
|
|    )()
|     ^
"#
        } else {
            r#"Error traceback (most recent call last):
==> File 'tests/lox/t30_test_tracebacks_in_native_functions.lox':
--> [Line 66, at 6, in t30_test_tracebacks_in_native_functions.lox]  at `(`: 
|
|    )()
|     ^
==> File 'tests/lox/t30_test_tracebacks_in_native_functions.lox':
--> [Line 63, at 25, in [lambda@63:9]]  at `(`: 
|
|    fun() => [1].map(
|                    ^
==> File 'tests/lox/t30_test_tracebacks_in_native_functions.lox':
--> [Line 63, at 25, in array.map]  at `(`: 
|
|    fun() => [1].map(
|                    ^
==> File 'tests/lox/t30_test_tracebacks_in_native_functions.lox':
--> [Line 64, at 33, in [lambda@64:13]]  at `(`: 
|
|    fun(_) => [].getitem(3)
|                        ^
==> File 'tests/lox/t30_test_tracebacks_in_native_functions.lox':
--> [Line 64, at 33, in array.getitem] RuntimeError: Array index is out of bounds: `3` >= `0`
|
|    fun(_) => [].getitem(3)
|                        ^
"#
        }
    );
    test_err!(
        t33_test_return_is_prohibited_in_initializers,
        r#"==> File 'tests/lox/t33_test_return_is_prohibited_in_initializers.lox':
--> [Line 3, at 9] ResolutionError at `return 0`: Cannot return from an initializer.
|
|    return 0;
|    ^^^^^^^^
"#
    );
    test_err!(
        t37_test_this_not_in_classes_is_prohibited,
        r#"==> File 'tests/lox/t37_test_this_not_in_classes_is_prohibited.lox':
--> [Line 1, at 1] ResolutionError at `this`: Cannot use `this` outside of a class.
|
|    this;
|    ^^^^
==> File 'tests/lox/t37_test_this_not_in_classes_is_prohibited.lox':
--> [Line 3, at 18] ResolutionError at `this`: Cannot use `this` outside of a class.
|
|    fun f() { return this; }
|                     ^^^^
==> File 'tests/lox/t37_test_this_not_in_classes_is_prohibited.lox':
--> [Line 6, at 17] ResolutionError at `this`: Cannot use `this` outside of a class.
|
|    (fun () { print this; })();
|                    ^^^^
"#
    );
    test_err!(
        t40_test_only_classes_can_be_inherited,
        r#"Error traceback (most recent call last):
==> File 'tests/lox/t40_test_only_classes_can_be_inherited.lox':
--> [Line 2, at 18, in t40_test_only_classes_can_be_inherited.lox] RuntimeError: The superclass must be a class but was `str`
|
|    class Subclass < NotAClass {}
|                     ^
"#
    );
    test_err!(
        t42_test_super_not_in_subclasses_is_prohibited,
        r#"==> File 'tests/lox/t42_test_super_not_in_subclasses_is_prohibited.lox':
--> [Line 1, at 1] ResolutionError at `super.nonexistent`: Cannot use `super` outside of a class.
|
|    super.nonexistent;
|    ^^^^^^^^^^^^^^^^^
==> File 'tests/lox/t42_test_super_not_in_subclasses_is_prohibited.lox':
--> [Line 3, at 3] ResolutionError at `super.notEvenInAClass`: Cannot use `super` outside of a class.
|
|    { super.notEvenInAClass(); }
|      ^^^^^^^^^^^^^^^^^^^^^
==> File 'tests/lox/t42_test_super_not_in_subclasses_is_prohibited.lox':
--> [Line 5, at 11] ResolutionError at `super.f`: Cannot use `super` outside of a class.
|
|    fun f() { super.f; }
|              ^^^^^^^
==> File 'tests/lox/t42_test_super_not_in_subclasses_is_prohibited.lox':
--> [Line 10, at 9] ResolutionError at `super.cook`: Cannot use `super` in a class without a superclass.
|
|    super.cook();
|    ^^^^^^^^^^
"#
    );

    test_err!(
        t49_test_statements_in_match_expressions_require_semicolon_and_disallow_comma,
        r#"==> File 'tests/lox/t49_test_statements_in_match_expressions_require_semicolon_and_disallow_comma.lox':
--> [Line 2, at 18] ParseError at `,`: Statements must end with a `;` or `}` and therefore do not require a comma.
|
|    3 => print 3;,
|                 ^
"#
    );

    test_err!(
        t50_test_match_expression_class_match_requires_an_identifier,
        r#"==> File 'tests/lox/t50_test_match_expression_class_match_requires_an_identifier.lox':
--> [Line 4, at 18] ParseError at `5`: Expected an identifier, [], or {} in the class name pattern.
|
|    match 10 { class 5 => true }
|                     ^
==> File 'tests/lox/t50_test_match_expression_class_match_requires_an_identifier.lox':
--> [Line 4, at 18] ParseError at `5`: Expected a `=>` after a pattern
|
|    match 10 { class 5 => true }
|                     ^
"#
    );

    test_err!(
        t53_test_type_comparison_requires_a_type,
        r#"Error traceback (most recent call last):
==> File 'tests/lox/t53_test_type_comparison_requires_a_type.lox':
--> [Line 18, at 5, in t53_test_type_comparison_requires_a_type.lox] RuntimeError: `21` is not a type
|
|    class Bad => "boom",
|    ^
"#
    );
    test_err!(
        t54_test_modules,
        r#"SUBROOT PKG IMPORT
Running the module code.
true
2.718281828459045
22026.465794806703
Switch<on: false>
Switch<on: true>
Switch<on: false, S = `EMPTY`>
Switch<on: true, S = `INIT`>
"#,
        r#"Error traceback (most recent call last):
==> File 'tests/lox/t54_test_modules.lox':
--> [Line 37, at 12, in t54_test_modules.lox]  at `(`: 
|
|    switch.bomb();
|               ^
==> File 'tests/lox/mod/nested/switch.lox':
--> [Line 42, at 10, in bomb]  at `(`: 
|
|    panic("boom");
|         ^
==> File 'tests/lox/mod/nested/switch.lox':
--> [Line 42, at 10, in panic] Panic at `(`: boom
|
|    panic("boom");
|         ^
"#
    );
    test_err!(
        t55_test_rest_args_syntax,
        r#"==> File 'tests/lox/t55_test_rest_args_syntax.lox':
--> [Line 11, at 21] ParseError at `...[3, 2, 1]`: Rest arguments must be provided before normal arguments.
|
|    f(...[1, 2, 3], 42, ...[3, 2, 1]); // Error
|                        ^^^^^^^^^^^^
==> File 'tests/lox/t55_test_rest_args_syntax.lox':
--> [Line 13, at 6] ParseError at `...[3, 2, 1]`: Rest arguments must be provided before normal arguments.
|
|    f(5, ...[3, 2, 1], 123); // Error
|         ^^^^^^^^^^^^
"#
    );

    test_err!(
        t58_test_empty_extra_numeric_literals,
        r#"==> File 'tests/lox/t58_test_empty_extra_numeric_literals.lox':
--> [Line 1, at 15] ParseError at `0x`: The literal must be followed by at least one digit
|
|    const empty = 0x;
|                  ^^
==> File 'tests/lox/t58_test_empty_extra_numeric_literals.lox':
--> [Line 2, at 15] ParseError at `0b`: The literal must be followed by at least one digit
|
|    const empty = 0b;
|                  ^^
"#
    );

    test_err!(
        t60_records_disallow_inheritance,
        r#"==> File 'tests/lox/t60_records_disallow_inheritance.lox':
--> [Line 2, at 17] ParseError at `<`: Record syntax doesn't support inheritance.
|
|    class Record(x) < A;
|                    ^
"#
    );
}
