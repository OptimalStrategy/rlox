#![allow(unused)]
extern crate assert_cmd;

pub use assert_cmd::{assert::Assert, prelude::*};
pub use std::process::Command;

pub const OPT_LEVEL: Option<&'static str> = option_env!("LOX_TEST_OPT_LEVEL");

pub fn run_file(filename: &str) -> Assert {
    Command::cargo_bin("rlox")
        .unwrap()
        .arg(filename)
        .arg(&format!("-O{}", OPT_LEVEL.unwrap_or("1")))
        .assert()
}

pub fn run_and_assert_ok(
    filename: &str,
    ok_output: &'static str,
    err_output: &'static str,
) -> Assert {
    run_file(filename)
        .success()
        .code(0)
        .stdout(ok_output)
        .stderr(err_output)
}

pub fn run_and_assert_err(
    filename: &str,
    ok_output: &'static str,
    err_output: &'static str,
) -> Assert {
    run_file(filename)
        .failure()
        .code(65)
        .stdout(ok_output)
        .stderr(err_output)
}

pub fn check_error_without_segfault(filename: &str) -> Assert {
    run_file(filename).failure().code(65)
}

#[macro_export]
macro_rules! test_ok {
    (ignore => $test_name:ident, $output:expr) => {
        test_ok!($test_name, $output, "" => ignore,test);
    };
    ($test_name:ident, $output:expr) => {
        test_ok!($test_name, $output, "" => test);
    };
    ($test_name:ident, $ok_output:expr, $err_output:expr => $( $attr:ident ),*) => {
        test_output!(__internal $test_name, run_and_assert_ok, "tests/lox/{}.lox",  $ok_output, "" => $( $attr ),*);
    };
}

#[macro_export]
macro_rules! test_err {
    (ignore => $test_name:ident, $output:expr) => {
        test_err!($test_name, $output => ignore,test);
    };
    ($test_name:ident, $output:expr) => {
        test_err!($test_name, "", $output => test);
    };
    ($test_name:ident, $ok_output:expr, $err_output:expr) => {
        test_err!($test_name, $ok_output, $err_output => test);
    };
    ($test_name:ident, $ok_output:expr, $err_output:expr => $( $attr:ident ),*) => {
        test_output!(__internal $test_name, run_and_assert_err, "tests/lox/{}.lox",  $ok_output, $err_output => $( $attr ),*);
    };
}

#[macro_export]
macro_rules! test_output {
    (__internal $test_name:ident, $tester:ident, $path:tt, $ok_output:expr, $err_output:expr => $( $attr:ident ),*) => {
        $(#[$attr])*
        fn $test_name() {
            $crate::common::$tester(
                &format!($path, stringify!($test_name))[..],
                $ok_output,
                $err_output,
            );
        }
    };
    (__internal nosegfault $test_name:ident, $tester:ident, $path:tt => $( $attr:ident ),*) => {
        $(#[$attr])*
        fn $test_name() {
            $crate::common::$tester(&format!($path, stringify!($test_name))[..]);
        }
    };
}

#[macro_export]
macro_rules! fuzz_ok {
    (ignore => $test_name:ident, $output:expr, $tester:ident) => {
        test_fuzz!(ignore => $test_name, $output, "", run_and_assert_ok);
    };
    ($test_name:ident, $output:expr) => {
        test_fuzz!($test_name, $output, "", run_and_assert_ok);
    };
}

#[macro_export]
macro_rules! fuzz_err {
   (nosegfault $test_name:ident) => {
        test_fuzz!($test_name, check_error_without_segfault);
    };
    (ignore => $test_name:ident, $output:expr, $tester:ident) => {
        test_fuzz!(ignore => $test_name, "", $output, run_and_assert_err);
    };
    ($test_name:ident, $output:expr) => {
        test_fuzz!($test_name, "", $output, run_and_assert_err);
    };
}

#[macro_export]
macro_rules! test_fuzz {
    (ignore => $test_name:ident, $ok_output:expr, $err_output:expr, $tester:ident) => {
        test_fuzz!(__internal $test_name, $tester, $ok_output:expr, $err_output:expr => ignore,test);
    };
    ($test_name:ident, $ok_output:expr, $err_output:expr, $tester:ident) => {
        test_fuzz!(__internal $test_name, $tester, $ok_output, $err_output => test);
    };
    ($test_name:ident, $tester:ident) => {
        test_fuzz!(__internal $test_name, $tester);
    };
    (__internal $test_name:ident, $tester:ident) => {
        test_output!(__internal nosegfault $test_name, $tester, "tests/lox/fuzz/{}.lox" => test);
    };
    (__internal $test_name:ident, $tester:ident, $ok_output:expr, $err_output:expr => $( $attr:ident ),*) => {
        test_output!(__internal $test_name, $tester, "tests/lox/fuzz/{}.lox",  $ok_output, $err_output => test);
    };
}
