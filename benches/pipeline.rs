#[macro_use]
extern crate criterion;
extern crate rlox;

use criterion::Criterion;
use rlox::bench_utils::compile;
use rlox::*;

fn pipeline(c: &mut Criterion) {
    let donut_source = get_donut_source(100);
    let line_count = donut_source.lines().count();
    let global_ctx = Context::default().into_shared();

    let mut group = c.benchmark_group("pipeline");

    group
        .bench_with_input(
            format!("<donut.lox ({} LOC, O0, no cache)>", line_count),
            &(&global_ctx, &donut_source),
            move |b, (ctx, donut_source)| {
                ctx.write_shared().config.disable_opts_in_place();
                ctx.write_shared().config.caching = false;

                assert!(!ctx.read_shared().config.caching);

                b.iter(|| compile(&donut_source[..], (*ctx).clone()).unwrap());
                ctx.write_shared().config.enable_opts_in_place();
            },
        )
        .bench_with_input(
            format!("<donut.lox ({} LOC, O1, no cache)>", line_count),
            &(&global_ctx, &donut_source),
            move |b, (ctx, donut_source)| {
                ctx.write_shared().config.caching = false;
                assert!(!ctx.write_shared().config.caching);
                assert!(ctx.write_shared().config.optimize);

                b.iter(|| compile(&donut_source[..], (*ctx).clone()).unwrap());
                ctx.write_shared().trigger_gc(std::iter::empty());
            },
        )
        .bench_with_input(
            format!("<donut.lox ({} LOC, O0, cache)>", line_count),
            &(&global_ctx, &donut_source),
            move |b, (ctx, donut_source)| {
                ctx.write_shared().config.caching = true;
                ctx.write_shared().config.disable_opts_in_place();

                assert!(ctx.write_shared().config.caching);
                assert!(!ctx.write_shared().config.optimize);

                b.iter(|| compile(&donut_source[..], (*ctx).clone()).unwrap());
                ctx.write_shared().config.enable_opts_in_place();
            },
        )
        .bench_with_input(
            format!("<donut.lox ({} LOC, O1, cache)>", line_count),
            &(&global_ctx, &donut_source),
            move |b, (ctx, donut_source)| {
                ctx.write_shared().config.caching = true;
                assert!(ctx.write_shared().config.caching);
                assert!(ctx.write_shared().config.optimize);

                b.iter(|| compile(&donut_source[..], (*ctx).clone()).unwrap());
                ctx.write_shared().trigger_gc(std::iter::empty());
            },
        );
    group.finish();
}

criterion_group!(benches, pipeline);
criterion_main!(benches);

fn get_donut_source(n: usize) -> String {
    format!(
        r#"
    /*
        Adapted from https://github.com/slightknack/rusty-donut.
    */
    const THETA_SPACING = 0.07;
    const PHI_SPACING = 0.02;
    const LUMINESCENCE = "▁▂▂▃▄▄▅▆▆▇██";
    const MISSING_LUMI =  "▁";
    
    fun render_frame(a, b) {{
        const g = Math.sin(a),
              m = Math.cos(a);
        const p = Math.sin(b),
              r = Math.cos(b);
    
        const output = [].fill(1760, " ");
        const zbuf   = [].fill(1760,   0.0);
    
        for (var j = 0.0; j <= Math.PI * 2.0; j += THETA_SPACING) {{
            const u = Math.sin(j),
                  v = Math.cos(j);
    
            for (var i = 0.0; i <= Math.PI * 2.0; i += PHI_SPACING) {{
                const w = Math.sin(i),
                      c = Math.cos(i);
                const h = v + 2.0;
    
                const d = 1.0 / (w * h * g + u * m + 5.0);
                const t = w * h * m - u * g;
    
                const x = to_int(40.0 + 30.0 * d * (c * h * r - t * p));
                const y = to_int(12.0 + 15.0 * d * (c * h * p + t * r));
    
                const o = x + 80 * y;
                const n = to_int(8.0 * ((u * g - w * v * m) * r - w * v * g - u * m - c * v * p));
    
    
                if (y < 22 and x < 79 and d > zbuf..o) {{
                    zbuf..o = d;
                    if (n < LUMINESCENCE.length and n >= 0) {{
                        output..o = LUMINESCENCE..n;
                    }} else {{
                        output..o = MISSING_LUMI;
                    }}
                }}
            }}
        }}
    
        for (var y = 0; y < 22; ++y) {{
            const row = y * 80;
            for (var x = 0; x < 80; ++x) {{
                const _ = output..(row + x);
            }}
        }}
    }}
    
    fun render_loop(its) {{
        var a = 1.0;
        var b = 1.0;
        for (var i = 0; i < its; ++i) {{
            a += 0.07;
            b += 0.03;
            render_frame(a, b);
        }}
    }}
    
    render_loop({});    
    "#,
        n
    )
}
