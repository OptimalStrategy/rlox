#[macro_use]
extern crate criterion;
extern crate rlox;

use criterion::{measurement::Measurement, Bencher, Criterion};
use rlox::bench_utils::compile;
use rlox::vm::Vm;
use rlox::*;

fn get_array_push_source(n: usize, expr: &str) -> String {
    format!(
        r#"
        var arr = [];
        for (var i = 0; i < {}; ++i) {{
            arr.push({});
        }}
    "#,
        n, expr
    )
}

fn bench_array<M: Measurement>(b: &mut Bencher<'_, M>, ctx: &SharedCtx, source: &str) {
    let cco = compile(source, ctx.clone()).unwrap();
    b.iter_with_setup(
        || ctx.clone(),
        |ctx| {
            let mut vm = Vm::new(ctx);
            vm.run(cco).unwrap()
        },
    );
    ctx.write_shared().trigger_gc(std::iter::empty());
}

fn array(c: &mut Criterion) {
    let array_push_const_10k = get_array_push_source(10_000, "3.141592");
    let array_push_obj_10k = get_array_push_source(10_000, "object()");
    let global_ctx = Context::default().with_disabled_cache().into_shared();

    let mut group = c.benchmark_group("array");
    group
        .bench_with_input(
            "<array.push (const, 10k)>",
            &(global_ctx.clone(), &array_push_const_10k),
            move |b, (ctx, source)| bench_array(b, ctx, source),
        )
        .bench_with_input(
            "<array.push (const, 10k, no opts)>",
            &(global_ctx.clone(), &array_push_const_10k),
            move |b, (ctx, source)| {
                ctx.write_shared().config.opt_config.peephole_optimizations = false;
                bench_array(b, ctx, source);
                ctx.write_shared().config.opt_config.peephole_optimizations = true;
            },
        )
        .bench_with_input(
            "<array.push (object(), 10k)>",
            &(global_ctx.clone(), array_push_obj_10k),
            move |b, (ctx, source)| bench_array(b, ctx, source),
        )
        .bench_with_input(
            "<{rust} Vec::push (object(), 10k)>",
            &global_ctx.read_shared().builtins().object_class(),
            move |b, class| {
                b.iter_with_setup(
                    || *class,
                    |class| {
                        let mut v = vec![];
                        for _ in 0..10_000 {
                            v.push(rlox::vm::Object::with_class(class));
                        }
                        v
                    },
                );
            },
        );
    group.finish();
}

criterion_group!(benches, array);
criterion_main!(benches);
