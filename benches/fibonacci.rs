#[macro_use]
extern crate criterion;
extern crate rlox;

use criterion::{measurement::Measurement, Bencher, Criterion};
use rlox::bench_utils::compile;
use rlox::vm::Vm;
use rlox::*;

fn bench_fibonacci<M: Measurement>(b: &mut Bencher<'_, M>, ctx: &SharedCtx, source: &str) {
    let cco = compile(source, ctx.clone()).unwrap();
    b.iter_with_setup(
        || ctx.clone(),
        |ctx| {
            let mut vm = Vm::new(ctx);
            vm.run(cco).unwrap()
        },
    );
    ctx.write_shared().trigger_gc(std::iter::empty());
}

fn fibonacci(c: &mut Criterion) {
    let fib_35 = get_iterative_fib_source(35);
    let fib_46 = get_iterative_fib_source(46);
    let fib_rec_20 = get_recursive_fib_source(20);
    let fib_tco_20 = get_tco_fib_source(20);
    let fib_memo_20 = get_memo_fib_source(20);
    let global_ctx = Context::default().with_disabled_cache().into_shared();

    let mut group = c.benchmark_group("fibonacci");
    group
        .bench_with_input(
            "<fib (iter): 35>",
            &(&global_ctx, &fib_35),
            move |b, (ctx, local_donut)| bench_fibonacci(b, ctx, local_donut),
        )
        .bench_with_input(
            "<fib (iter): 46>",
            &(&global_ctx, &fib_46),
            move |b, (ctx, local_donut)| bench_fibonacci(b, ctx, local_donut),
        )
        .bench_with_input(
            "<fib (rec): 20>",
            &(&global_ctx, &fib_rec_20),
            move |b, (ctx, local_donut)| bench_fibonacci(b, ctx, local_donut),
        )
        .bench_with_input(
            "<fib (rec + tco): 20>",
            &(&global_ctx, &fib_tco_20),
            move |b, (ctx, local_donut)| bench_fibonacci(b, ctx, local_donut),
        )
        .bench_with_input(
            "<fib (rec + memo): 20>",
            &(&global_ctx, &fib_memo_20),
            move |b, (ctx, local_donut)| bench_fibonacci(b, ctx, local_donut),
        );
    group.finish();
}

criterion_group!(benches, fibonacci);
criterion_main!(benches);

fn get_iterative_fib_source(n: usize) -> String {
    format!(
        r#"
    var a = 0;
    var b = 1;

    for (var n = {}; n > 0; --n) {{
    	var temp = a;
    	a = b;
    	b = temp + b;
    }}
    var result = a;
    "#,
        n
    )
}

fn get_recursive_fib_source(n: usize) -> String {
    format!(
        r#"
        fun fib(n) {{
            if (n > 2) {{
                return fib(n - 1) + fib(n - 2);
            }}
            return match n {{
                0 => 0,
                1 => 1,
                2 => 1,
            }};
        }}
        var result = fib({});
    "#,
        n
    )
}

fn get_tco_fib_source(n: usize) -> String {
    format!(
        r#"
        fun fib_tco_help(a, b, n) {{
            if (n > 0) {{
                return fib_tco_help(b, a + b, n - 1);
            }}
            return a;
        }}
        fun fib_tco(n) {{
            return fib_tco_help(0, 1, n);
        }}
        var result = fib_tco({});
    "#,
        n
    )
}

fn get_memo_fib_source(n: usize) -> String {
    format!(
        r#"
        fun fib_memo_help(n, memory) {{
            if (memory.get(n, nil) == nil) {{
                memory.push(fib_memo_help(n - 1, memory) + fib_memo_help(n - 2, memory));
            }}
            return memory.get(n);
        }}
        
        fun fib_memo(n) {{
            // Prevent TCO by wrapping the call in parentheses
            return (fib_memo_help(n, [0, 1]));
        }}
        var result = fib_memo({});
    "#,
        n
    )
}
