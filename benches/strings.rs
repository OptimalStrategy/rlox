#[macro_use]
extern crate criterion;
extern crate rlox;

use criterion::{measurement::Measurement, Bencher, Criterion};
use rlox::bench_utils::compile;
use rlox::vm::Vm;
use rlox::*;

fn get_string_addition_source(n: usize) -> String {
    format!(
        r#"
    var s = "";

    for (var i = 0; i < {}; i = i + 1) {{
        s = s + "chr";
    }}"#,
        n
    )
}

fn bench_strings<M: Measurement>(b: &mut Bencher<'_, M>, ctx: &SharedCtx, source: &str) {
    let cco = compile(source, ctx.clone()).unwrap();
    b.iter_with_setup(
        || ctx.clone(),
        |ctx| {
            let mut vm = Vm::new(ctx);
            vm.run(cco).unwrap()
        },
    );
    ctx.write_shared().trigger_gc(std::iter::empty());
}

fn strings(c: &mut Criterion) {
    let add_128 = get_string_addition_source(128);
    let add_2056 = get_string_addition_source(2056);
    let global_ctx = Context::default().with_disabled_cache().into_shared();

    let mut group = c.benchmark_group("strings");
    group
        .bench_with_input(
            "<add (128)>",
            &(&global_ctx, &add_128[..]),
            |b, (ctx, source)| bench_strings(b, ctx, source),
        )
        .bench_with_input(
            "<add (2056)>",
            &(&global_ctx, &add_2056[..]),
            |b, (ctx, source)| bench_strings(b, ctx, source),
        );
    group.finish();
}

criterion_group!(benches, strings);
criterion_main!(benches);
