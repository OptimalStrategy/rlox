#[macro_use]
extern crate criterion;
extern crate rlox;

use criterion::Criterion;
use rlox::bench_utils::compile;
use rlox::vm::Vm;
use rlox::*;

fn regression(c: &mut Criterion) {
    let reg_source = get_regression_source(100);

    // Fit the regression for 100 iterations
    let mut group = c.benchmark_group("regression");
    group
        .bench_with_input(
            "<reg.lox (100 samples x 100 epochs, no output, all opts)>",
            &reg_source[..],
            move |b, reg_source| {
                let ctx = Context::default().into_shared();
                let cco = compile(reg_source, ctx.clone()).unwrap();
                b.iter(|| {
                    let mut vm = Vm::new(ctx.clone());
                    vm.run(cco).unwrap()
                });
            },
        )
        .sample_size(10)
        .bench_with_input(
            "<reg.lox (100 samples x 100 epochs, no output, no opts)>",
            &reg_source[..],
            move |b, reg_source| {
                let ctx = Context::default().with_disabled_cache().into_shared();
                ctx.write_shared().config.disable_opts_in_place();
                let cco = compile(reg_source, ctx.clone()).unwrap();
                b.iter(|| {
                    let mut vm = Vm::new(ctx.clone());
                    vm.run(cco).unwrap()
                });
                ctx.write_shared().trigger_gc(std::iter::once(cco));
            },
        )
        .sample_size(10);
    group.finish();
}

criterion_group!(benches, regression);
criterion_main!(benches);

fn get_regression_source(iters: usize) -> String {
    format!(
        r#"
import stdlib.array {{ Array }};
import stdlib.iterators {{ range }};

import stdlib.ml.reg {{ Regression }};
import stdlib.ml.metrics {{ r2score }};

fun gen_data(n, f) {{
    const x_train = [];
    const y_train = [];
    const x_test = [];
    const y_test = [];

    for (_ in range(n)) {{
        var v = Math.random();
        x_train.push(Array([v, 1 / v]));
        y_train.push(f(v, 1 / v));

        v = Math.random();
        x_test.push(Array([v, 1 / v]));
        y_test.push(f(v, 1 / v));
    }}

    return [x_train, y_train, x_test, y_test].map(Array);
}}

// Pin the seed:
Math.seed(0);

const data = gen_data(100, fun(x1, x2) => 8 * x1 + x2);
const X_train = data .. 0;
const y_train = data .. 1;
const X_test = data .. 2;
const y_test = data .. 3;

const reg = Regression(2);
reg.log_training(false);
const _loss = reg.fit({}, X_train, y_train, 0.001);
const _r2 = r2score(reg.predict(X_test), y_test);
"#,
        iters
    )
}
