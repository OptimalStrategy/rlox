#[macro_use]
extern crate criterion;
extern crate rlox;

use criterion::{measurement::Measurement, Bencher, Criterion};
use rlox::bench_utils::compile;
use rlox::vm::Vm;
use rlox::*;

fn bench_lang<M: Measurement>(b: &mut Bencher<'_, M>, ctx: &SharedCtx, source: &str) {
    let cco = compile(source, ctx.clone()).unwrap();
    b.iter_with_setup(
        || ctx.clone(),
        |ctx| {
            let mut vm = Vm::new(ctx);
            vm.run(cco).unwrap()
        },
    );
    ctx.write_shared().trigger_gc(std::iter::empty());
}

fn lang(c: &mut Criterion) {
    let source_this_looped = get_bench_setup(10_000, "test.test();");
    let source_this_cached = get_bench_setup(10_000, "test.test_cached();");
    let source_this_closure = get_bench_setup(10_000, "test.test_closure();");
    let global_ctx = Context::default().with_disabled_cache().into_shared();

    let mut group = c.benchmark_group("lang");
    group
        // Access this directly
        .bench_with_input(
            "<this access: direct (x 10k)>",
            &(&global_ctx, &source_this_looped),
            move |b, (ctx, local_donut)| bench_lang(b, ctx, local_donut),
        )
        // Access this via a cached variable
        .bench_with_input(
            "<this access: cached (x 10k)>",
            &(&global_ctx, &source_this_cached),
            move |b, (ctx, local_donut)| bench_lang(b, ctx, local_donut),
        )
        // Access this via a closure
        .bench_with_input(
            "<this access: closure (x 10k)>",
            &(&global_ctx, &source_this_closure),
            move |b, (ctx, local_donut)| bench_lang(b, ctx, local_donut),
        );
    group.finish();
}

fn get_bench_setup(n: usize, exec: &str) -> String {
    format!(
        r#"
        class Test {{
            init() {{
                this.value = 4;
            }}

            test() {{
                var sum = 0;
                for (var i = 0; i < {0}; ++i) {{
                    sum += this.value;
                }}
                return sum;
            }}

          test_cached() {{
                const value = this.value;
                var sum = 0;
                for (var i = 0; i < {0}; ++i) {{
                    sum += value;
                }}
                return sum;
            }}

            test_closure() {{
                const get = fun() => this.value;
                var sum = 0;
                for (var i = 0; i < {0}; ++i) {{
                    sum += get();
                }}
                return sum;
            }}
        }}

        const test = Test();
        {1}
    "#,
        n, exec
    )
}

criterion_group!(benches, lang);
criterion_main!(benches);
