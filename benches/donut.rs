#[macro_use]
extern crate criterion;
extern crate rlox;

use criterion::{measurement::Measurement, Bencher, Criterion};
use rlox::bench_utils::compile;
use rlox::vm::Vm;
use rlox::*;

fn bench_donut<M: Measurement>(b: &mut Bencher<'_, M>, ctx: &SharedCtx, source: &str) {
    let cco = compile(source, ctx.clone()).unwrap();
    b.iter_with_setup(
        || ctx.clone(),
        |ctx| {
            let mut vm = Vm::new(ctx);
            vm.run(cco).unwrap()
        },
    );
    ctx.write_shared().trigger_gc(std::iter::empty());
}

fn donut(c: &mut Criterion) {
    let donut_frame = get_donut_source(1);
    let global_ctx = Context::default().with_disabled_cache().into_shared();

    // Render 1 frame of the doughnut
    let mut group = c.benchmark_group("donut_runtime");
    group
        .bench_with_input(
            "<donut.lox (1 frame, no output, all opts)>",
            &(&global_ctx, &donut_frame),
            move |b, (ctx, local_donut)| bench_donut(b, ctx, local_donut),
        )
        .sample_size(25)
        .bench_with_input(
            "<donut.lox (1 frame, no output, no opts)>",
            &(global_ctx, donut_frame),
            move |b, (ctx, local_donut)| {
                ctx.write_shared().config.disable_opts_in_place();
                bench_donut(b, ctx, local_donut);
                ctx.write_shared().config.enable_opts_in_place();
            },
        )
        .sample_size(25);
    group.finish();
}

criterion_group!(benches, donut);
criterion_main!(benches);

fn get_donut_source(n: usize) -> String {
    format!(
        r#"
    /*
        Adapted from https://github.com/slightknack/rusty-donut.
    */
    const THETA_SPACING = 0.07;
    const PHI_SPACING = 0.02;
    const LUMINESCENCE = "▁▂▂▃▄▄▅▆▆▇██";
    const MISSING_LUMI =  "▁";
    
    fun render_frame(a, b) {{
        const g = Math.sin(a),
              m = Math.cos(a);
        const p = Math.sin(b),
              r = Math.cos(b);
    
        const output = [].fill(1760, " ");
        const zbuf   = [].fill(1760,   0.0);
    
        for (var j = 0.0; j <= Math.PI * 2.0; j += THETA_SPACING) {{
            const u = Math.sin(j),
                  v = Math.cos(j);
    
            for (var i = 0.0; i <= Math.PI * 2.0; i += PHI_SPACING) {{
                const w = Math.sin(i),
                      c = Math.cos(i);
                const h = v + 2.0;
    
                const d = 1.0 / (w * h * g + u * m + 5.0);
                const t = w * h * m - u * g;
    
                const x = to_int(40.0 + 30.0 * d * (c * h * r - t * p));
                const y = to_int(12.0 + 15.0 * d * (c * h * p + t * r));
    
                const o = x + 80 * y;
                const n = to_int(8.0 * ((u * g - w * v * m) * r - w * v * g - u * m - c * v * p));
    
    
                if (y < 22 and x < 79 and d > zbuf..o) {{
                    zbuf..o = d;
                    if (n < LUMINESCENCE.length and n >= 0) {{
                        output..o = LUMINESCENCE..n;
                    }} else {{
                        output..o = MISSING_LUMI;
                    }}
                }}
            }}
        }}
    
        for (var y = 0; y < 22; ++y) {{
            const row = y * 80;
            for (var x = 0; x < 80; ++x) {{
                const _ = output..(row + x);
            }}
        }}
    }}
    
    fun render_loop(its) {{
        var a = 1.0;
        var b = 1.0;
        for (var i = 0; i < its; ++i) {{
            a += 0.07;
            b += 0.03;
            render_frame(a, b);
        }}
    }}
    
    render_loop({});    
    "#,
        n
    )
}
