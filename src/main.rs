extern crate clap;
extern crate rlox;

use std::{path::PathBuf, str::FromStr};

use clap::{App, Arg};

use rlox::{compiler::version as bytecode_version, pipeline::IRPass, *};
use rlox::{
    test_runner::{run_lox_tests, TestDiscoveryArgs},
    vm::builtin::Builtins,
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = App::new("rlox")
        .version(
            &format!(
                "{} (bytecode {})",
                env!("CARGO_PKG_VERSION"),
                bytecode_version::CURRENT_VERSION
            )[..],
        )
        .about("A Rust implementation of the Lox programming language.")
        .subcommand(
            App::new("test")
                .version(env!("CARGO_PKG_VERSION"))
                .about("A test runner for rlox.")
                .arg(
                    Arg::with_name("tests")
                        .required(true)
                        .takes_value(true)
                        .default_value(".")
                        .value_name("DIR")
                        .help("Path to the root tests directory."),
                )
                .arg(
                    Arg::with_name("test-prefix")
                        .short("p")
                        .long("test-prefix")
                        .required(false)
                        .takes_value(true)
                        .default_value("test")
                        .help("The prefix for the test modules and functions. \
                        All modules that start with the prefix will be collected for test discovery. \
                        Inside those modules, all functions that start with the prefix will be executed as tests. \
                        Note that the modules containing the functions will be executed as well.
                        "
                    ),
                ),
        )
        .arg(
            Arg::with_name("script")
                .required(false)
                .takes_value(true)
                .value_name("FILE")
                .help("Path to a .lox script."),
        )
        .arg(
            Arg::with_name("blob")
                .long("emit-blob")
                .short("b")
                .help("Serializes the compiled script into an RBSF blob (.rloxb).")
                .required(false),
        )
        .arg(
            Arg::with_name("print-pipeline")
                .short("p")
                .help("Print the pipeline description and configuration.")
                .required(false),
        )
        .arg(
            Arg::with_name("ast")
                .long("ast")
                .short("a")
                .help("Print the AST after all passes that modify the AST.")
                .required(false),
        )
        .arg(
            Arg::with_name("code")
                .long("--print-code")
                .short("c")
                .help("Convert the AST to code after all passes that modify the AST.")
                .required(false),
        )
        .arg(
            Arg::with_name("absolute-paths")
                .long("--absolute-paths")
                .help("Print absolute paths when reporting errors.")
                .required(false),
        )
        .arg(
            Arg::with_name("debug")
                .long("debug")
                .short("d")
                .help("Enable debug prints")
                .required(false),
        )
        .arg(
            Arg::with_name("optimize")
                .short("O")
                .takes_value(true)
                .default_value("1")
                .possible_values(&["0", "1"])
                .help("Enable or disable all optimizations"),
        )
        .arg(
            Arg::with_name("C")
                .short("C")
                .required(false)
                .multiple(true)
                .number_of_values(1)
                .takes_value(true)
                .possible_values(&[
                    "constant-folding",
                    "no-constant-folding",
                    "tco",
                    "no-tco",
                    "peephole-opts",
                    "no-peephole-opts",
                    "dce",
                    "no-dce",
                    "unchecked-ig",
                    "export-cfg",
                    "export-ig",
                    "caching",
                    "no-caching",
                ])
                .help("Compiler flags"),
        )
        .arg(
            Arg::with_name("R")
                .short("R")
                .required(false)
                .multiple(true)
                .number_of_values(1)
                .takes_value(true)
                .help(
                    "Runtime flags [possible values: reclimit=N, interning_threshold=N, \
                                    max_compiler_threads=N, thread_spawn_threshold=N, timeout=MS]",
                ),
        )
        .arg(
            Arg::with_name("no-runtime")
                .long("no-runtime")
                .short("n")
                .help("Do not run the program after compilation.")
                .required(false),
        )
        .get_matches();

    let mut config = CompilerConfig {
        print_ast: matches.is_present("ast"),
        print_code: matches.is_present("code"),
        dis: matches.is_present("debug"),
        print_full_path: matches.is_present("absolute-paths"),
        ..CompilerConfig::default()
    };
    if matches.value_of("optimize").unwrap() == "0" {
        config.opt_config = OptConfig::all_disabled()
    }

    if let Some(values) = matches.values_of("C") {
        for opt in values {
            match opt {
                "constant-folding" => config.opt_config.constant_folding = true,
                "no-constant-folding" => config.opt_config.constant_folding = false,
                "tco" => config.opt_config.tail_call_optimization = true,
                "no-tco" => config.opt_config.tail_call_optimization = false,
                "peephole-opts" => config.opt_config.peephole_optimizations = true,
                "no-peephole-opts" => config.opt_config.peephole_optimizations = false,
                "dce" => config.opt_config.dead_code_elimination = true,
                "no-dce" => config.opt_config.dead_code_elimination = false,
                "export-cfg" => config.export_cfg = true,
                "export-ig" => config.export_ig = true,
                "unchecked-ig" => config.check_ig = false,
                "caching" => config.caching = true,
                "no-caching" => config.caching = false,
                _ => unreachable!("Unknown compiler flag: {}", opt),
            }
        }
    }

    if let Some(values) = matches.values_of("R") {
        fn parse_arg_value<T: FromStr>(
            arg: &str,
            message: &str,
        ) -> Result<T, Box<dyn std::error::Error>> {
            let parts = arg.split('=').collect::<Vec<_>>();
            let value = parts.last().unwrap().parse::<T>();
            if parts.len() != 2 || value.is_err() {
                return Err(Box::from(message));
            }
            Ok(value.ok().unwrap())
        }

        for arg in values {
            if arg.starts_with("reclimit") {
                let r_limit = parse_arg_value(
                    arg,
                    &format!(
                        "The flag `reclimit` expects one positive integer argument -- the maximum number of recursive function calls. \
                         Providing 0 is equivalent to the default limit value of {}.", rlox::config::DEFAULT_LOX_CALL_STACK_SIZE
                    )
                )?;
                config.max_call_stack_size = match r_limit {
                    0 => rlox::config::DEFAULT_LOX_CALL_STACK_SIZE,
                    value => value,
                };
            } else if arg.starts_with("interning_threshold") {
                let it = parse_arg_value(
                    arg,
                    &format!(
                        "The flag `interning_threshold` expects one positive 8-bit integer argument -- the maximum number of graphemes in a string after which it will not be interned. \
                         Providing 0 is equivalent to the default value of {}.", rlox::config::DEFAULT_LOX_INTERNING_THRESHOLD
                    )
                )?;
                config.interning_threshold = match it {
                    0 => rlox::config::DEFAULT_LOX_INTERNING_THRESHOLD,
                    value => value,
                };
            } else if arg.starts_with("max_compiler_threads") {
                let it = parse_arg_value(
                    arg,
                    &format!(
                        "The flag `max_compiler_threads` expects one positive integer argument -- the maximum number of compiler threads that can be spawned. \
                         Providing 0 is equivalent to the default value of {}.", rlox::config::DEFAULT_MAX_COMPILER_THREADS
                    )
                )?;
                config.max_compiler_threads = match it {
                    0 => rlox::config::DEFAULT_MAX_COMPILER_THREADS,
                    value => value,
                };
            } else if arg.starts_with("thread_spawn_threshold") {
                let it = parse_arg_value(
                    arg,
                    &format!(
                        "The flag `thread_spawn_threshold` expects one positive integer argument -- the minimum number of files in the compiler queue to spawn an additional thread. \
                         The maximum number of threads will not exceed the max_compiler_threads value. \
                         Providing 0 is equivalent to the default value of {}.", rlox::config::DEFAULT_Q_SIZE_THREAD_SPAWN_THRESHOLD,
                    )
                )?;
                config.compiler_queue_size_thread_spawn_threshold = match it {
                    0 => rlox::config::DEFAULT_Q_SIZE_THREAD_SPAWN_THRESHOLD,
                    value => value,
                };
            } else if arg.starts_with("timeout") {
                let it = parse_arg_value(
                    arg,
                    "The flag `timeout` expects one positive integer argument -- the number of seconds the vm may run for. The timeout is enforced by \
                        instrumenting the compiler program with timeout checks (loops, function calls and returns). \
                        Providing 0 is equivalent to omitting the flag."
                )?;
                config.execution_timeout = if it > 0 {
                    Some(std::time::Duration::from_millis(it))
                } else {
                    None
                };
            } else {
                return Err(Box::from(format!(
                    "Unknown Runtime flag: `{}`. See --help for the possible values.",
                    arg
                )));
            }
        }
    }

    // We have to make sure that the context is created before everything else
    // and destroyed after everything else.
    let ctx = Context::default().set_config(config).into_shared();
    let builtins = Builtins::default(ctx.clone());
    ctx.write_shared().set_builtins(builtins);

    let pipeline = make_pipeline(ctx.clone());

    if matches.is_present("print-pipeline") {
        println!("{}", ctx.read_shared().config.description());
        println!("{}", pipeline.description());
    }

    let (module, blob) = match matches.value_of("script") {
        None if matches.subcommand_matches("test").is_some() => {
            let sub = matches.subcommand_matches("test").unwrap();
            let root_directory =
                std::fs::canonicalize(PathBuf::from(sub.value_of("tests").unwrap()))?;

            if !root_directory.is_dir() {
                return Err(Box::from(format!(
                    "Given `tests` path is not a directory: `{}`",
                    root_directory.to_string_lossy()
                )));
            }

            let test_prefix = sub.value_of("test-prefix").unwrap();
            let args = TestDiscoveryArgs::with_default_ignored_dirs(root_directory, test_prefix);
            return run_lox_tests(ctx, args);
        }
        None => {
            std::mem::drop(pipeline);
            return rlox::repl::repl(ctx).map_err(|code| std::process::exit(code));
        }
        Some(filename) => {
            let mut path = std::fs::canonicalize(PathBuf::from(filename))?;
            if !path.is_file() {
                return Err(Box::from(format!(
                    "Given script is not a file: `{}`",
                    path.to_string_lossy()
                )));
            }
            let module = path
                .file_name()
                .unwrap()
                .to_str()
                .expect("Expected the file name to be a valid unicode string.")
                .trim_end_matches(".lox")
                .trim_end_matches(".rloxb")
                .to_owned();
            let (source, blob) = if let Some("rloxb") = path.extension().and_then(|s| s.to_str()) {
                let blob = std::fs::read(&path)?;
                path.set_extension("lox");
                let source = std::fs::read_to_string(&path).unwrap_or_else(|e| {
                    eprintln!(
                        "[Warning] Failed to read the source file associated with the blob at `{}`: {};\n          \
                         reported errors won't contain source code excerpts.",
                        path.display(), e
                    );
                    String::new()
                });
                (source, Some(blob))
            } else {
                (std::fs::read_to_string(&path)?, None)
            };
            // Change the file extension to .lox because module hashes care about that
            path.set_extension("lox");
            let filename = path.display().to_string();
            let info = SourceInfo::new(filename, source, module);
            (ctx.write_shared().add_module(info), blob)
        }
    };

    let cco = if let Some(blob) = blob {
        match ctx.deserialize(module, &blob) {
            Ok(cco) => cco,
            Err(e) => {
                eprintln!("[Error] Failed to decode the blob: {:?}", e);
                std::process::exit(65);
            }
        }
    } else {
        match pipeline.apply(module) {
            Ok(res) => {
                ctx.read_shared().ex.report_all(ctx.clone());
                res
            }
            Err(ex) => {
                ctx.write_shared().ex.extend_from_another_ex(ex);
                ctx.read_shared().ex.report_all(ctx.clone());
                std::process::exit(65);
            }
        }
    };
    Context::terminate_workers(ctx.clone())?;
    if !Context::check_worker_errors_and_import_graph(ctx.clone()) {
        std::process::exit(65);
    }

    if ctx.read_shared().config.dis {
        cco.as_ref().as_cco().dis_print();
    }

    if matches.is_present("blob") {
        let mut path = PathBuf::from(
            ctx.read_shared()
                .get_module(module)
                .unwrap()
                .source_info
                .module
                .clone(),
        );
        path.set_extension("rloxb");
        let blob = ctx.serialize(module);
        std::fs::write(path, blob).unwrap_or_else(|e| {
            eprintln!("{}", e);
            std::process::exit(65);
        })
    }

    if matches.is_present("no-runtime") {
        return Ok(());
    }

    let mut vm = rlox::vm::Vm::new(ctx.clone());
    match vm.run(cco) {
        Ok(_) => (),
        Err(tb) => {
            ctx.read_shared().ex.report_all(ctx.clone());
            tb.display(ctx);
            std::process::exit(65);
        }
    };

    Ok(())
}
