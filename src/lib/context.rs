use std::{
    collections::HashMap,
    path::PathBuf,
    sync::{Arc, RwLock, RwLockReadGuard, RwLockWriteGuard},
};

use crate::rt_hashmap::FastHashMap;
use crossbeam_channel::{bounded, unbounded, Receiver};
use rlox_graph::DiGraph;

use crate::{
    allocator::{Allocator, LoxAllocator},
    err_context::ErrCtx,
    module::{Module, ModuleId},
    pipeline_worker::{self, *},
    vm::{
        builtin::Builtins,
        immutable_string::hash,
        module::LoxModule,
        object::{object::ClassObject, LoxObject, ObjectPointer},
    },
    CompilerConfig, SourceInfo,
};

pub type SharedCtx = Arc<SharedContext>;

#[derive(Debug)]
pub struct SharedContext {
    ctx: RwLock<Context<Allocator>>,
}
/// SAFETY: Context doesn't use any thread-local storage, while the SharedContext
/// wrapper prevents us from moving out of the lock.
unsafe impl Send for SharedContext {}
unsafe impl Sync for SharedContext {}

impl SharedContext {
    pub fn new(ctx: Context) -> Self {
        Self {
            ctx: RwLock::new(ctx),
        }
    }

    /// Blocks the thread until the lock can be acquired for reading.
    /// Panics if the lock is poisoned.
    pub fn read_shared(&self) -> RwLockReadGuard<Context> {
        self.ctx
            .read()
            .expect("Attempted to acquire a poisoned lock")
    }

    /// Blocks the thread until the lock can be acquired for writing.
    /// Panics if the lock is poisoned.
    pub fn write_shared(&self) -> RwLockWriteGuard<Context> {
        self.ctx
            .write()
            .expect("Attempted to acquire a poisoned lock")
    }

    /// Blocks the thread until the lock can be acquired for reading, then retrieves `CompilerConfig::max_compiler_threads`.
    /// Panics if the lock is poisoned.
    #[inline]
    pub fn max_compiler_threads(&self) -> usize {
        self.read_shared().config.max_compiler_threads
    }
}

#[derive(Debug)]
pub struct Context<L = Allocator>
where
    L: LoxAllocator<LoxObject>,
{
    /// The pipeline configuration.
    pub config: CompilerConfig,
    pub ex: ErrCtx,
    /// Path to the root.
    pub root: PathBuf,
    builtins: Builtins,
    allocator: L,
    modules: HashMap<ModuleId, Module>,
    root_module: Option<ModuleId>,
    string_pool: FastHashMap<u64, ObjectPointer>,
    import_graph: DiGraph<ModuleId>,
    tokens: Arc<()>,
    queue: Option<(PipelineTx, PipelineRx)>,
    workers: Vec<std::thread::JoinHandle<Result<(), &'static str>>>,
}

impl<L> Default for Context<L>
where
    L: LoxAllocator<LoxObject>,
{
    fn default() -> Context<L> {
        let mut allocator = L::new();
        let object_class = allocator.alloc_without_gc_trigger(LoxObject::new(
            ClassObject::empty_with_name("@Object", ModuleId::internal()),
        ));
        Self {
            config: CompilerConfig::default(),
            root: std::fs::canonicalize(PathBuf::from("."))
                .expect("Something went wrong while resolving the path to `.`"),
            ex: ErrCtx::default(),
            builtins: Builtins::new(object_class),
            allocator,
            modules: HashMap::new(),
            root_module: None,
            string_pool: FastHashMap::default(),
            import_graph: DiGraph::new(),
            tokens: Arc::new(()),
            queue: Some(unbounded()),
            workers: Vec::with_capacity(1),
        }
        .with_default_string_pool()
    }
}

impl Context<Allocator> {
    pub fn with_disabled_cache(mut self) -> Self {
        self.config.caching = false;
        self
    }

    pub fn into_shared(self) -> SharedCtx {
        Arc::new(SharedContext::new(self))
    }
}

impl<L> Context<L>
where
    L: LoxAllocator<LoxObject>,
{
    #[allow(clippy::map_entry)]
    pub fn with_default_string_pool(mut self) -> Self {
        for s in unicode_segmentation::UnicodeSegmentation::graphemes(DEFAULT_STRING_POOL, true) {
            let hash = hash(s);
            if !self.string_pool.contains_key(&hash) {
                let ptr = *self.intern_ptr(s.to_owned());
                self.string_pool.insert(hash, ptr);
            }
        }
        self
    }

    pub fn from_source<S: Into<String>>(s: S) -> Self {
        let mut ctx = Context::default();
        ctx.add_module(SourceInfo::test_info(s));
        ctx
    }

    pub fn set_builtins(&mut self, builtins: Builtins) {
        self.builtins = builtins;
    }

    pub fn drop_modules(&mut self) {
        self.modules.clear();
        self.import_graph.clear();
        self.trigger_gc(std::iter::empty());
    }

    fn restore_queue(&mut self) {
        if self.queue.is_none() {
            self.queue = Some(unbounded());
        }
    }

    #[inline(always)]
    pub fn builtins(&self) -> &Builtins {
        &self.builtins
    }

    #[inline(always)]
    pub fn import_graph(&self) -> &DiGraph<ModuleId> {
        &self.import_graph
    }

    #[inline]
    pub fn copy_builtins_map(&self) -> FastHashMap<u8, ObjectPointer> {
        self.builtins.get_map().clone()
    }

    pub fn set_config(mut self, config: CompilerConfig) -> Self {
        self.config = config;
        self
    }

    pub fn intern<'s, 'c: 's>(&'c mut self, s: String) -> &'s str {
        self.intern_ptr(s).as_ref().as_immutable_string().as_ref()
    }

    pub fn intern_ptr(&mut self, s: String) -> &ObjectPointer {
        let hash = hash(&s);
        if self.string_pool.get(&hash).is_none() {
            let ptr: ObjectPointer = self.allocator.alloc_without_gc_trigger(LoxObject::new(s));
            self.string_pool.insert(hash, ptr);
        }
        self.string_pool.get(&hash).unwrap()
    }

    pub fn try_intern_ptr(&mut self, s: String) -> Option<ObjectPointer> {
        if s.len() <= self.config.interning_threshold as usize {
            Some(*self.intern_ptr(s))
        } else {
            None
        }
    }

    #[inline]
    pub fn get_interned(&mut self, s: &str) -> Option<&ObjectPointer> {
        self.string_pool.get(&hash(s))
    }

    #[inline]
    pub fn get_interned_by_hash(&self, hash: u64) -> Option<&ObjectPointer> {
        self.string_pool.get(&hash)
    }

    #[inline(always)]
    pub fn alloc(
        &mut self,
        obj: LoxObject,
        roots: impl Iterator<Item = ObjectPointer>,
    ) -> ObjectPointer {
        self.allocator.alloc(
            obj,
            roots
                .chain(self.string_pool.values().cloned())
                .chain(self.builtins.contained_pointers())
                .chain(self.modules.values().flat_map(Module::contained_pointers)),
        )
    }

    pub fn trigger_gc(&mut self, roots: impl Iterator<Item = ObjectPointer>) {
        crate::gc_debug!(stats | "[GC     ] Manually triggering the GC");
        self.allocator.collect_garbage(
            roots
                .chain(self.string_pool.values().cloned())
                .chain(self.builtins.contained_pointers())
                .chain(self.modules.values().flat_map(Module::contained_pointers)),
        );
    }

    #[inline(always)]
    pub(crate) fn alloc_without_gc_trigger(&mut self, obj: LoxObject) -> ObjectPointer {
        self.allocator.alloc_without_gc_trigger(obj)
    }

    #[inline(always)]
    pub(crate) fn bump_allocated_bytes(&mut self, amount: isize) {
        self.allocator.bump_allocated_bytes(amount)
    }

    pub fn init_module_cache(&mut self, module_id: ModuleId) -> Option<PathBuf> {
        let module_name = self.get_module(module_id).map(|m| m.name().to_owned());
        self.try_init_module_cache(module_id).and_then(|r| {
            r.map_err(|e| {
                eprintln!(
                    "[WARNING] Could not initialize the module cache directory for {}: {}",
                    module_name.unwrap(),
                    e
                );
            })
            .ok()
        })
    }

    pub fn add_module(&mut self, info: SourceInfo) -> ModuleId {
        let module_id = ModuleId(hash(&info.filename));
        if self.root_module.is_none() {
            self.root_module = Some(module_id);
        }
        match self.modules.get_mut(&module_id) {
            Some(module) => {
                module.source_info = info;
            }
            None => self.create_module_object(module_id, info, false),
        }
        module_id
    }

    pub fn set_module_info(&mut self, info: SourceInfo) -> ModuleId {
        let module_id = ModuleId(hash(&info.filename));
        if let Some(module) = self.get_module_mut(module_id) {
            module.update_source_info(info)
        } else {
            self.add_module(info);
        }
        module_id
    }

    pub fn try_init_module_cache(
        &mut self,
        module_id: ModuleId,
    ) -> Option<Result<PathBuf, std::io::Error>> {
        let build_dir = self.config.build_dir.clone();
        if let Some(module) = self.get_module_mut(module_id) {
            if module.cache_initialized() {
                Some(Ok(module.cache.clone()))
            } else {
                let path = module.set_cache(&build_dir);
                Some(std::fs::create_dir_all(path).map(|_| path.to_owned()))
            }
        } else {
            None
        }
    }

    pub fn verify_import_graph(&mut self) -> bool {
        if self.config.export_ig {
            if let Some(path) = self.init_module_cache(self.root_module.unwrap()) {
                let dot = self
                    .import_graph
                    .export_dot(|node, _| self.get_module(*node.data()).unwrap().name().to_owned());
                let path = path.join(&format!(
                    "import_graph_{}.dot",
                    self.get_root_module_name().unwrap()
                ));
                if let Err(e) = std::fs::write(&path, dot) {
                    eprintln!(
                        "[WARNING] Could not export the IG for {}: {}",
                        self.get_root_module_name().unwrap(),
                        e
                    );
                };
            }
        }
        let cycles = self.import_graph.find_cycles().collect::<Vec<_>>();
        if cycles.is_empty() {
            return true;
        }

        eprintln!(
            "Error: Imports in the program create {} cycle(s):",
            cycles.len()
        );
        for (i, cycle) in cycles.into_iter().enumerate() {
            let cycle_vec = cycle.collect::<Vec<_>>();
            let mut cycle = cycle_vec.clone();
            cycle.sort_by_key(|n| std::cmp::Reverse(n.id()));

            let mut ordered = Vec::with_capacity(cycle.len());
            let mut current = cycle.pop();
            'outer: while let Some(top) = current {
                ordered.push(top);
                let edges = self.import_graph.adjacent_set(top.id());
                for (i, node) in cycle.iter().copied().enumerate() {
                    if edges.contains(&node.id()) {
                        current = Some(node);
                        cycle.remove(i);
                        continue 'outer;
                    }
                }
                current = cycle.pop();
            }
            // Connect the cycle
            if let Some(node) = ordered.last() {
                let edges = self.import_graph.adjacent_set(node.id());
                for node in cycle_vec.into_iter() {
                    if edges.contains(&node.id()) {
                        ordered.push(node);
                        break;
                    }
                }
            }
            eprintln!(
                "[{}] {}",
                i + 1,
                ordered
                    .into_iter()
                    .map(|n| {
                        let module = *n.data();
                        format!("{}.lox", self.get_module(module).unwrap().name().to_owned())
                    })
                    .collect::<Vec<_>>()
                    .join(" -> ")
            );
        }
        false
    }

    #[must_use]
    pub(crate) fn queue_module(
        &mut self,
        ctx: SharedCtx,
        parent: ModuleId,
        filename: PathBuf,
        was_root_imported: bool,
    ) -> QueueGuard {
        let module_id = ModuleId(hash(filename.display().to_string()));
        let rx = if self.modules.get(&module_id).is_none() {
            self.create_module_object(module_id, SourceInfo::empty(), was_root_imported);
            self.queue_new_module(ctx, filename)
        } else {
            None
        };
        let from = self.import_graph.add_node(parent);
        let to = self.import_graph.add_node(module_id);
        self.import_graph.add_edge(from, to);
        QueueGuard {
            module_id,
            rx,
            _token: self.take_token(),
        }
    }

    #[must_use]
    pub(crate) fn queue_rogue_module(&mut self, ctx: SharedCtx, filename: PathBuf) -> QueueGuard {
        let module_id = ModuleId(hash(filename.display().to_string()));
        let rx = if self.modules.get(&module_id).is_none() {
            self.create_module_object(module_id, SourceInfo::empty(), false);
            self.queue_new_module(ctx, filename)
        } else {
            None
        };
        self.import_graph.add_node(module_id);
        QueueGuard {
            module_id,
            rx,
            _token: self.take_token(),
        }
    }

    fn create_module_object(
        &mut self,
        module_id: ModuleId,
        info: SourceInfo,
        was_root_imported: bool,
    ) {
        let mut module = Module::new(module_id, info, was_root_imported);
        let lox_module = LoxModule::new(module.source_info.module.clone(), module_id);
        let lox_module_ptr = self.alloc_without_gc_trigger(LoxObject::new(
            crate::vm::object::HeapValue::ObjNative(Box::new(lox_module)),
        ));
        module.set_lox_module(lox_module_ptr);
        self.modules.insert(module_id, module);
        self.import_graph.add_node(module_id);
    }

    fn queue_new_module(
        &mut self,
        ctx: SharedCtx,
        message: PipelineMessage,
    ) -> Option<Receiver<()>> {
        let rx = if self.workers.is_empty()
            || (self.workers.len() < self.config.max_compiler_threads
                && self.queue.as_ref().map(|(_, q)| q.len()).unwrap_or(0)
                    >= self.config.compiler_queue_size_thread_spawn_threshold)
        {
            // We need a one-shot channel to make sure that the thread acquires the queue before
            // some external code decides to terminate the threads.
            let (tx, rx) = bounded(1);
            self.workers.push(std::thread::spawn(move || {
                pipeline_worker::spawn_worker(ctx, tx)
            }));
            Some(rx)
        } else {
            None
        };
        if let Some((tx, _)) = self.queue.as_ref() {
            tx.send(message).unwrap()
        }
        rx
    }

    pub fn add_module_and_get_source(&mut self, info: SourceInfo) -> &'static str {
        let module = self.add_module(info);
        self.get_module_source(module).unwrap()
    }

    // TODO: constrain this lifetime
    #[inline]
    pub fn get_module(&self, id: ModuleId) -> Option<&'static Module> {
        let module: Option<&Module> = self.modules.get(&id);
        let module: Option<&'static Module> = unsafe { std::mem::transmute(module) };
        module
    }

    #[inline]
    pub fn get_module_mut(&mut self, id: ModuleId) -> Option<&mut Module> {
        self.modules.get_mut(&id)
    }

    #[inline]
    pub fn get_module_by_name(&self, name: &str) -> Option<&Module> {
        self.modules.get(&ModuleId(hash(name)))
    }

    #[inline]
    pub fn get_module_mut_by_name(&mut self, name: &str) -> Option<&mut Module> {
        self.modules.get_mut(&ModuleId(hash(name)))
    }

    pub fn set_module_source(&mut self, id: ModuleId, source: &str) -> bool {
        self.modules
            .get_mut(&id)
            .map(|module| {
                module.source_info.source.clear();
                module.source_info.source.push_str(source);
                true
            })
            .unwrap_or(false)
    }

    pub fn set_module_hash(&mut self, id: ModuleId, hash: u64) -> bool {
        self.modules
            .get_mut(&id)
            .map(|module| {
                module.source_info.source_hash = hash;
                true
            })
            .unwrap_or(false)
    }

    pub fn set_root_module_hash(&mut self, hash: u64) -> bool {
        self.root_module
            .map(|id| self.set_module_hash(id, hash))
            .unwrap_or(false)
    }

    pub fn get_module_source(&self, id: ModuleId) -> Option<&'static str> {
        self.modules
            .get(&id)
            .map(|m| unsafe { std::mem::transmute(&m.source_info.source[..]) })
    }

    pub fn get_module_source_info(&self, id: ModuleId) -> Option<&SourceInfo> {
        self.modules.get(&id).map(|m| &m.source_info)
    }

    pub fn get_root_module_source_info(&self) -> Option<&SourceInfo> {
        self.root_module
            .and_then(|name| self.get_module(name).map(|m| &m.source_info))
    }

    #[inline(always)]
    pub fn get_root_module_id(&self) -> Option<ModuleId> {
        self.root_module
    }

    pub fn get_root_module_name(&self) -> Option<&'static str> {
        self.root_module
            .and_then(|name| self.get_module(name).map(|m| m.name()))
    }

    pub fn get_root_module_source(&self) -> Option<&'static str> {
        self.root_module
            .and_then(|name| self.get_module_source(name))
    }

    #[inline]
    pub fn get_queue(&self) -> Option<PipelineRx> {
        self.queue.as_ref().map(|(_, rx)| rx.clone())
    }

    /// Blocks the thread until the queue is empty.
    pub fn wait_for_workers(ctx: SharedCtx) -> Result<(), WorkerError> {
        thread_debug!("wait_for_workers(): Waiting for the worker to finish");
        Self::terminate_workers(ctx.clone())?;
        ctx.write_shared().queue = Some(unbounded());
        Ok(())
    }

    /// Blocks the thread until the queue is empty; then suspends the workers.
    pub fn terminate_workers(ctx: SharedCtx) -> Result<(), WorkerError> {
        thread_debug!("terminate_workers(): Terminating the workers...");
        let (queue, token_count) = {
            let guard = ctx.read_shared();
            (guard.queue.clone(), guard.tokens.clone())
        };
        let result = if let Some(queue) = queue {
            // The token inside the context struct + the cloned value used for checks give us a count of 2.
            const MAIN_THREAD_COUNT: usize = 2;

            thread_debug!(
                "terminate_workers(): waiting until the queue is empty and all threads are idling"
            );
            // Wait until the queue is empty and the workers are idle
            while !queue.1.is_empty() || Arc::strong_count(&token_count) > MAIN_THREAD_COUNT {
                std::hint::spin_loop();
            }
            thread_debug!(
                "terminate_workers(): looks like the queue is empty and the workers are idling"
            );

            let workers = {
                thread_debug!("terminate_workers(): Dropping the queue...");
                let mut guard = ctx.write_shared();

                // Drops the queue to trigger the workers to
                // stop listening for new messages.
                std::mem::drop(queue);
                std::mem::drop(guard.queue.take());

                std::mem::take(&mut guard.workers)
            };

            // After the queue is dead, terminate the workers using their thread handles.
            wait_for_workers(workers)
        } else {
            Ok(())
        };
        // Create a new queue since the code above drops it.
        ctx.write_shared().restore_queue();
        result
    }

    /// Returns false if any of the imported modules had an error or if the IG is wrong.
    pub fn check_worker_errors_and_import_graph(ctx: SharedCtx) -> bool {
        let mut guard = ctx.write_shared();
        let workers_had_error = guard.ex.had_error;
        let ig_is_bad = guard.config.check_ig && !guard.verify_import_graph();
        !(workers_had_error || ig_is_bad)
    }

    #[inline]
    pub(crate) fn take_token(&mut self) -> Arc<()> {
        self.tokens.clone()
    }
}

pub(crate) struct QueueGuard {
    module_id: ModuleId,
    rx: Option<Receiver<()>>,
    _token: Arc<()>,
}

impl QueueGuard {
    pub fn get(self) -> ModuleId {
        if let Some(rx) = self.rx {
            rx.recv().unwrap();
        }
        self.module_id
    }
}

fn wait_for_workers(
    workers: Vec<std::thread::JoinHandle<Result<(), &'static str>>>,
) -> Result<(), WorkerError> {
    let mut had_error = false;
    for worker in workers {
        had_error |= worker
            .join()
            .expect("Failed to terminate a worker thread.")
            .map_err(|e| {
                eprintln!("{}", e);
            })
            .is_err();
    }
    thread_debug!("All workers should have terminated at this point.");
    if had_error {
        Err(WorkerError)
    } else {
        Ok(())
    }
}

static DEFAULT_STRING_POOL: &str =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ0123456789!@#$%^&*_+-=(){}[],.:;/\\\'\"\n";
