use std::path::{Path, PathBuf};

use crate::{
    compiler::cco::CompiledCodeObject, vm::module::LoxModule, vm::native::NativeObject,
    ObjectPointer, SourceInfo,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct ModuleId(pub(crate) u64);
known_deep_size_fast!(0, ModuleId);

impl ModuleId {
    pub const fn anon() -> Self {
        ModuleId(0)
    }

    pub const fn internal() -> Self {
        ModuleId(1)
    }
}

#[derive(Debug)]
pub struct Module {
    pub id: ModuleId,
    pub source_info: SourceInfo,
    pub cache: PathBuf,
    pub was_root_imported: bool,
    /// Whether the module has been loaded from cache.
    pub loaded_from_cache: bool,
    /// Whether the module has been evaluated.
    evaluated: bool,
    /// A pointer to the module's code object
    cco: Option<ObjectPointer>,
    /// The native object wrapping this module.
    obj: Option<ObjectPointer>,
}

impl Module {
    pub fn new(id: ModuleId, source_info: SourceInfo, was_root_imported: bool) -> Self {
        Self {
            id,
            source_info,
            cache: PathBuf::new(),
            was_root_imported,
            evaluated: false,
            loaded_from_cache: false,
            cco: None,
            obj: None,
        }
    }

    #[inline]
    pub fn cco(&self) -> Option<&CompiledCodeObject> {
        self.cco.as_ref().map(|ptr| ptr.as_ref().as_cco())
    }

    #[inline]
    pub fn set_cco(&mut self, cco: ObjectPointer) {
        debug_assert!(cco.as_ref().as_heap_value().as_cco().is_some());
        self.cco = Some(cco);
    }

    #[inline]
    pub fn cco_ptr(&self) -> Option<ObjectPointer> {
        self.cco
    }

    #[inline]
    pub fn lox_module(&self) -> Option<ObjectPointer> {
        self.obj
    }

    #[inline]
    pub(crate) fn set_lox_module(&mut self, ptr: ObjectPointer) {
        self.obj = Some(ptr);
    }

    #[inline]
    pub fn evaluated(&self) -> bool {
        self.evaluated
    }

    #[inline]
    pub fn set_evaluated(&mut self) {
        self.evaluated = true;
    }

    #[inline]
    pub(crate) unsafe fn lox_module_object(&self) -> Option<&LoxModule> {
        self.obj
            .as_ref()
            .map(|ptr| <dyn NativeObject>::downcast::<LoxModule>(ptr))
    }

    #[allow(unused)]
    #[inline]
    pub(crate) unsafe fn lox_module_object_mut(&mut self) -> Option<&mut LoxModule> {
        self.obj
            .as_mut()
            .map(|ptr| <dyn NativeObject>::downcast_mut::<LoxModule>(ptr))
    }

    #[inline]
    pub fn name(&self) -> &str {
        &self.source_info.module[..]
    }

    #[inline]
    pub fn cache(&self) -> &Path {
        &self.cache
    }

    #[inline]
    pub fn bin_cache(&self) -> PathBuf {
        self.cache.join(format!("{}.rloxb", self.name()))
    }

    #[inline]
    pub fn cache_initialized(&self) -> bool {
        !self.cache.as_os_str().is_empty()
    }

    pub fn set_cache(&mut self, build_dir: &Path) -> &Path {
        if !self.cache_initialized() {
            self.cache = build_dir
                .join(&self.source_info.module)
                .join(format!("{:016x}", self.source_info.source_hash));
        }
        &self.cache
    }

    pub fn update_source_info(&mut self, info: SourceInfo) {
        self.source_info = info;
        let name = self.name().to_owned();
        if let Some(m) = unsafe { self.lox_module_object_mut() } {
            m.set_name(name)
        };
    }

    #[inline]
    pub fn contained_pointers(&self) -> impl Iterator<Item = ObjectPointer> + '_ {
        self.cco.into_iter().chain(self.obj.into_iter())
    }
}
