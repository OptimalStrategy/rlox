use crate::{ast::*, env::Env, pipeline::IRPass, token::Span};
use std::marker::PhantomData;

#[derive(Debug)]
pub struct ConstantFolder<'a> {
    enabled: bool,
    // We only propagate the strings that can be interned
    interning_threshold: usize,
    propagated_const_vars: Env<&'a str, Option<Lit<'a>>>,
    _marker: PhantomData<&'a ()>,
}

impl<'a, E> IRPass<E> for ConstantFolder<'a> {
    type Input = AST<'a>;
    type Output = AST<'a>;

    fn apply(mut self, mut ir: Self::Input) -> Result<Self::Output, E> {
        if self.enabled {
            self.fold(&mut ir);
        }
        Ok(ir)
    }

    fn description(&self) -> String {
        format!(
            "<ConstantFolder: Performs constant folding on the AST [enabled = {}]>",
            self.enabled,
        )
    }
}

impl<'a> ConstantFolder<'a> {
    pub fn new(enabled: bool, interning_threshold: u8) -> Self {
        Self {
            enabled,
            interning_threshold: interning_threshold as usize,
            propagated_const_vars: Env::new(),
            _marker: PhantomData,
        }
    }

    pub fn fold(&mut self, ast: &mut AST<'a>) {
        for ref mut stmt in ast.body.iter_mut() {
            self.fold_stmt(stmt);
        }
    }

    pub fn fold_stmt(&mut self, stmt: &mut Stmt<'a>) {
        match &mut stmt.kind {
            StmtKind::ExprStmt(ref mut expr) => self.fold_expr(expr),
            StmtKind::Print(ref mut expr) => self.fold_expr(expr),
            StmtKind::Var(vars) => {
                for v in vars.iter_mut().map(|v| &mut v.node) {
                    match v.initializer {
                        Some(ref mut init) => {
                            self.fold_expr(init);
                            if let ExprKind::Lit(lit @ box Lit { value, .. }) = &init.kind {
                                let value = if v.is_const {
                                    match value {
                                        // TODO: bench this grapheme counter
                                        // XXX: counting graphemes here is not great
                                        LitValue::Str(s)
                                            if crate::frontend::scanner::number_of_graphemes(
                                                &s[..],
                                            ) <= self.interning_threshold =>
                                        {
                                            Some(*lit.clone())
                                        }
                                        LitValue::Str(_) => None,
                                        _ => Some(*lit.clone()),
                                    }
                                } else {
                                    None
                                };
                                self.propagated_const_vars.insert(v.name.lexeme, value);
                            }
                        }
                        None => {
                            self.propagated_const_vars.insert(v.name.lexeme, None);
                        }
                    }
                }
            }
            StmtKind::Block(stmts) => {
                self.propagated_const_vars.push();
                stmts.iter_mut().for_each(|stmt| self.fold_stmt(stmt));
                self.propagated_const_vars.pop();
            }
            StmtKind::For(r#for) => {
                if let Some(ref mut init) = r#for.initializer {
                    self.fold_stmt(init);
                }

                if let Some(ref mut cond) = r#for.condition {
                    self.fold_expr(cond);
                }
                if let Some(ref mut inc) = r#for.increment {
                    self.fold_expr(inc);
                }
                self.fold_stmt(&mut r#for.body);
            }
            StmtKind::ForEach(r#for) => {
                self.fold_expr(&mut r#for.iterator);
                self.fold_stmt(&mut r#for.body);
            }
            StmtKind::While(condition, body) => {
                self.fold_expr(condition);
                self.fold_stmt(body);
                if let Some(false) = self.is_truthy(condition) {
                    stmt.kind = StmtKind::_Empty;
                }
            }
            StmtKind::If(r#if) => {
                self.fold_expr(&mut r#if.condition);
                self.fold_stmt(&mut r#if.then);

                if let Some(ref mut r#else) = r#if.otherwise {
                    self.fold_stmt(r#else);
                }

                match self.is_truthy(&r#if.condition) {
                    Some(true) => {
                        let _empty = Stmt {
                            kind: StmtKind::_Empty,
                            span: Span::builtin(),
                        };
                        *stmt = std::mem::replace(&mut r#if.then, _empty);
                    }
                    Some(false) => {
                        if let Some(r#else) = r#if.otherwise.take() {
                            *stmt = r#else;
                        } else {
                            stmt.kind = StmtKind::_Empty;
                        }
                    }
                    None => {}
                }
            }
            StmtKind::Return(value) => {
                if let Some(ref mut expr) = value {
                    self.fold_expr(expr);
                }
            }
            StmtKind::Break => (),
            StmtKind::Continue => (),
            StmtKind::Function(info) => self.fold_function(info),
            StmtKind::Class(info) => {
                self.propagated_const_vars.insert(info.name.lexeme, None);
                info.methods.iter_mut().for_each(|m| self.fold_function(m));
            }
            StmtKind::Import(_) => (),
            StmtKind::_Empty => {}
        }
    }

    pub fn fold_expr(&mut self, expr: &mut Expr<'a>) {
        match &mut expr.kind {
            ExprKind::Binary(op, left, right) => {
                self.fold_expr(left);
                self.fold_expr(right);
                if let (ExprKind::Lit(ref mut left), ExprKind::Lit(ref mut right)) =
                    (&mut left.kind, &mut right.kind)
                {
                    match op.node {
                        BinOpKind::Add => fold_binary_op!(expr, left, right, +),
                        BinOpKind::Sub => fold_binary_op!(expr, left, right, -),
                        BinOpKind::Mul => fold_binary_op!(expr, left, right, *),
                        BinOpKind::Rem => {
                            if !right.value.is_zero() {
                                fold_binary_op!(expr, left, right, %)
                            }
                        }
                        BinOpKind::Div => {
                            if !right.value.is_zero() {
                                fold_binary_op!(expr, left, right, /)
                            }
                        }
                        BinOpKind::Pow => assign_lit_node_if_some!(
                            expr,
                            left,
                            right,
                            match (&left.value, &right.value) {
                                (LitValue::Integer(l), LitValue::Integer(r)) =>
                                    Some(LitValue::from(l.saturating_pow(*r as u32))),
                                (LitValue::Float(l), LitValue::Float(r)) =>
                                    Some(LitValue::from(l.powf(*r))),
                                (LitValue::Integer(l), LitValue::Float(r)) =>
                                    Some(LitValue::from((*l as crate::types::LoxFloat).powf(*r))),
                                (LitValue::Float(l), LitValue::Integer(r)) =>
                                    Some(LitValue::from(l.powf(*r as crate::types::LoxFloat))),
                                _ => None,
                            }
                        ),
                        BinOpKind::And => assign_lit_node_if_some!(
                            expr,
                            left,
                            right,
                            Some(LitValue::from(
                                left.value.is_truthy() && right.value.is_truthy(),
                            ))
                        ),
                        BinOpKind::Or => assign_lit_node_if_some!(
                            expr,
                            left,
                            right,
                            Some(LitValue::from(
                                left.value.is_truthy() || right.value.is_truthy(),
                            ))
                        ),
                        BinOpKind::Eq => assign_lit_node_if_some!(
                            expr,
                            left,
                            right,
                            Some(LitValue::from(left.value == right.value))
                        ),
                        BinOpKind::Ne => assign_lit_node_if_some!(
                            expr,
                            left,
                            right,
                            Some(LitValue::from(left.value != right.value))
                        ),
                        BinOpKind::Lt => {
                            fold_binary_op!(ord => expr, left.clone(), right.clone(), <)
                        }
                        BinOpKind::Le => {
                            fold_binary_op!(ord => expr, left.clone(), right.clone(), <=)
                        }
                        BinOpKind::Gt => {
                            fold_binary_op!(ord => expr, left.clone(), right.clone(), >)
                        }
                        BinOpKind::Ge => {
                            fold_binary_op!(ord => expr, left.clone(), right.clone(), >=)
                        }
                    }
                }
            }
            ExprKind::Unary(op, operand) => {
                self.fold_expr(operand);
                if let ExprKind::Lit(ref mut lit) = &mut operand.kind {
                    match op.node {
                        UnOpKind::Neg => {
                            let mut new = LitValue::Nil;
                            std::mem::swap(&mut lit.value, &mut new);
                            assign_lit_node_if_some!(expr, lit, Some(unary_num_map!(new, -)));
                        }
                        UnOpKind::Not => {
                            assign_lit_node_if_some!(
                                expr,
                                lit,
                                Some(LitValue::from(!lit.value.is_truthy()))
                            );
                        }
                        UnOpKind::Plus => (),
                    }
                }
            }
            ExprKind::Lit(_lit) => (),
            ExprKind::ArgForwarding(expr) => self.fold_expr(expr),
            ExprKind::Comma(left, right) => {
                self.fold_expr(left);
                self.fold_expr(right);
            }
            ExprKind::Call { callee, args, .. } => {
                self.fold_expr(callee);
                for arg in &mut args.node {
                    self.fold_expr(arg);
                }
            }
            ExprKind::GetAttr(get) => self.fold_expr(&mut get.node),
            ExprKind::SetAttr(set) => {
                self.fold_expr(&mut set.node);
                self.fold_expr(&mut set.value);
            }
            ExprKind::GetItem(obj, item) => {
                self.fold_expr(obj);
                self.fold_expr(item);
            }
            ExprKind::SetItem(set) => {
                self.fold_expr(&mut set.node);
                self.fold_expr(&mut set.key);
                self.fold_expr(&mut set.value);
            }
            ExprKind::InterStr(inter) => {
                for ref mut frag in &mut inter.fragments {
                    match frag {
                        InterFrag::Expr(expr) => self.fold_expr(expr),
                        InterFrag::Str(_) => (),
                    }
                }
            }
            ExprKind::Array(exprs) => {
                for expr in exprs {
                    self.fold_expr(expr);
                }
            }
            ExprKind::Dict(pairs) => {
                for (k, v) in pairs {
                    self.fold_expr(k);
                    self.fold_expr(v);
                }
            }
            ExprKind::This => (),
            ExprKind::Super(_) => (),
            ExprKind::Ternary(ternary) => {
                self.fold_expr(&mut ternary.predicate);
                self.fold_expr(&mut ternary.then);
                self.fold_expr(&mut ternary.otherwise);
            }
            ExprKind::Match(r#match) => {
                self.propagated_const_vars.push();
                if let Some(ref name) = r#match.bound_var {
                    self.propagated_const_vars.insert(name.lexeme, None);
                }
                self.fold_expr(&mut r#match.value);
                r#match.arms.iter_mut().for_each(|arm| {
                    if !arm.is_placeholder {
                        self.fold_expr(&mut arm.pat);
                    }
                    self.fold_stmt(&mut arm.body);
                });
                self.propagated_const_vars.pop();
            }
            ExprKind::Variable(v) => {
                if let Some(const_expr) = self
                    .propagated_const_vars
                    .get(&v.lexeme)
                    .and_then(|v| v.as_ref())
                {
                    *expr = Expr {
                        kind: ExprKind::Lit(Box::new(const_expr.clone())),
                        span: expr.span,
                    };
                }
            }
            ExprKind::PrefixIncDec(_) => (),
            ExprKind::PostfixIncDec(_) => (),
            ExprKind::Assignment(asn) => self.fold_expr(&mut asn.value),
            ExprKind::Lambda(info) => self.fold_function(info),
            ExprKind::Grouping(data) => {
                self.fold_expr(data);
                // The kind doesn't matter. `This` is just easier to construct.
                let mut new = Expr {
                    kind: ExprKind::This,
                    span: expr.span,
                };
                std::mem::swap(&mut **data, &mut new);
                *expr = new;
            }
        }
    }

    fn is_truthy(&self, expr: &Expr<'a>) -> Option<bool> {
        match &expr.kind {
            ExprKind::Lit(lit) => Some(match lit.value {
                LitValue::Integer(i) => i != 0,
                LitValue::Float(f) => f != 0.0,
                LitValue::Bool(b) => b,
                LitValue::Nil => false,
                LitValue::Str(ref s) => !s.is_empty(),
            }),
            _ => None,
        }
    }

    #[inline]
    fn fold_function(&mut self, info: &mut FnInfo<'a>) {
        self.propagated_const_vars.push();
        info.params.into_iter().for_each(|param| {
            self.propagated_const_vars.insert(param.lexeme, None);
        });
        info.body.iter_mut().for_each(|stmt| self.fold_stmt(stmt));
        self.propagated_const_vars.pop();
    }
}
