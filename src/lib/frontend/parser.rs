use std::{num::IntErrorKind, path::PathBuf};

use super::stack::ensure_sufficient_stack;
use crate::{
    ast::*, err_context::ErrCtx, module::ModuleId, pipeline::IRPass, token::*, types::*, LoxError,
    SharedCtx,
};

type StmtResult<'a> = Result<Stmt<'a>, LoxError>;
type ExprResult<'a> = Result<Expr<'a>, LoxError>;

#[derive(Debug)]
pub struct Parser<'a> {
    tokens: Vec<Token<'a>>,
    /// The token the parser is currently at.
    current: usize,
    is_in_ternary: bool,
    ex: ErrCtx,
    ctx: SharedCtx,
}

impl<'a> IRPass<ErrCtx> for Parser<'a> {
    type Input = (u64, Vec<Token<'a>>);
    type Output = AST<'a>;

    fn apply(self, ir: Self::Input) -> Result<Self::Output, ErrCtx> {
        let (hash, tokens) = ir;
        if let Some(tok) = tokens.first() {
            self.ctx.write_shared().set_module_hash(tok.module, hash);
        }
        self.parse(tokens)
    }

    fn description(&self) -> String {
        String::from("<Parser: Builds an AST from the received sequence of tokens>")
    }
}

impl<'a> Parser<'a> {
    pub fn new(ctx: SharedCtx) -> Self {
        Self {
            tokens: vec![],
            current: 0,
            is_in_ternary: false,
            ctx,
            ex: ErrCtx::new(),
        }
    }

    #[allow(clippy::or_fun_call)]
    pub fn parse(mut self, tokens: Vec<Token<'a>>) -> Result<AST<'a>, ErrCtx> {
        let module = tokens
            .first()
            .map(|tok| tok.module)
            .unwrap_or(ModuleId::anon());
        self.tokens = tokens;
        let mut statements: Vec<Stmt> = vec![];

        while !self.is_at_end() {
            let stmt = self.declaration();
            match stmt {
                Ok(stmt) => statements.push(stmt),
                Err(e) => {
                    self.ex.record(e);
                    self.synchronize();
                }
            }
        }

        if self.ex.had_error {
            Err(self.ex)
        } else {
            Ok(AST::new(statements, module))
        }
    }

    fn declaration(&mut self) -> StmtResult<'a> {
        if self.r#match(TokenKind::Class) {
            self.class_declaration()
        } else if self.match_any(&[TokenKind::Var, TokenKind::Const]) {
            self.var_declaration(true)
        } else if self.r#match(TokenKind::Fun) {
            self.function()
        } else if self.r#match(TokenKind::Import) {
            self.import_statement()
        } else {
            self.statement()
        }
    }

    fn class_declaration(&mut self) -> StmtResult<'a> {
        let start = self.previous().span;
        let name = self.consume_identifier("Expected a class name after the keyword", "cls-name");
        let mut superclass = None;
        let mut methods = vec![];

        if self.r#match(TokenKind::LeftParen) {
            return self.record_declaration(name);
        }

        if self.r#match(TokenKind::Less) {
            let name =
                self.consume_identifier("Expected a superclass name after the `<`.", "superclass");
            let span = name.span;
            superclass = Some(expr!(ExprKind::Variable(name), span));

            let mut attrs = vec![];
            while self.r#match(TokenKind::Dot) {
                attrs.push(self.consume_identifier(
                    "Expected an identifier after the `.`.",
                    "superclass-attr-name",
                ));
            }
            attrs.reverse();
            while let Some(attr) = attrs.pop() {
                let span = combspan!(span => superclass.as_ref().unwrap().span, span => attr.span);
                superclass = Some(expr!(
                    ExprKind::GetAttr(Box::new(GetAttr {
                        node: superclass.unwrap(),
                        field: attr,
                    })),
                    span
                ));
            }
        }
        let opener = self
            .consume(
                TokenKind::LeftBrace,
                "Expected a `{` before the class body.",
            )?
            .span;

        while !self.check(TokenKind::RightBrace) && !self.is_at_end() {
            let mut kind = FnKind::Method;
            if self.r#match(TokenKind::Class) {
                kind = FnKind::Static;
            }
            let name = self.consume_identifier("Expected a method name.", "method-name");
            methods.push(self.finish_function_parsing(kind, name.clone())?);
        }

        let closer = self
            .consume_paired(
                TokenKind::RightBrace,
                opener,
                "Expected a `}` after the class body.",
            )?
            .span;

        let span = combspan!(span => start, span => closer);
        stmt_ok!(
            StmtKind::Class(Box::new(ClsInfo {
                name,
                superclass,
                methods
            })),
            span
        )
    }

    fn record_declaration(&mut self, name: Token<'a>) -> StmtResult<'a> {
        let opener = self.previous().span;
        let fields = self.parse_identifier_list(
            "Expected a field name",
            "record-field",
            TokenKind::RightParen,
        );
        self.consume_paired(
            TokenKind::RightParen,
            opener,
            "Expected a `)` after the record fields",
        )?;

        if self.r#match(TokenKind::Less) {
            let tok = self.previous().clone();
            self.ex.record(
                LoxError::parse("Record syntax doesn't support inheritance.", tok.span)
                    .with_module(tok.module),
            );
            // Try to consume the name that's like to follow after the <
            self.r#match(TokenKind::Identifier);
        }

        self.consume(
            TokenKind::Semicolon,
            "Expected a semicolon after a record declaration",
        )?;
        let span = combspan!(name, self.previous());

        let init = FnInfo {
            name: Token::with_span(TokenKind::Identifier, "init", name.span),
            params: Params::Params(fields.clone()),
            body: fields
                .into_iter()
                .map(|f| {
                    let span = f.span;
                    let expr = expr!(
                        ExprKind::SetAttr(Box::new(SettAttr {
                            node: expr!(ExprKind::This, span),
                            field: f.clone(),
                            value: expr!(ExprKind::Variable(f), span),
                        })),
                        span
                    );
                    stmt!(StmtKind::ExprStmt(Box::new(expr)), span)
                })
                .collect(),
            kind: FnKind::Method,
        };

        let cls = ClsInfo {
            name,
            superclass: None,
            methods: vec![init],
        };
        stmt_ok!(StmtKind::Class(Box::new(cls)), span)
    }

    fn var_declaration(&mut self, consume_semicolon: bool) -> StmtResult<'a> {
        let (span, vars) = capture_span!(self, {
            let mut vars = vec![self.identifier_expression()?];
            let is_const = vars[0].node.is_const;
            while self.r#match(TokenKind::Comma) {
                if self.check(TokenKind::Semicolon) {
                    break;
                }
                let mut var = self.identifier_expression()?;
                var.node.is_const = is_const;
                vars.push(var);
            }
            vars
        });
        let fmt_end = if vars.len() == 1 { "." } else { " list." };
        if consume_semicolon {
            self.consume_semicolon(format!(
                "Expected a `;` after the variable declaration{}",
                fmt_end
            ))?;
        }
        stmt_ok!(StmtKind::Var(vars), span.unwrap())
    }

    fn function(&mut self) -> StmtResult<'a> {
        let keyword = self.previous().span;
        let name = self.consume_identifier("Expected a function name after the keyword.", "fn");
        let info = self.finish_function_parsing(FnKind::Function, name.clone())?;
        let span = combspan!(span => keyword, self.previous());
        stmt_ok!(StmtKind::Function(Box::new(info)), span)
    }

    fn finish_function_parsing(
        &mut self,
        mut kind: FnKind,
        name: Token<'a>,
    ) -> Result<FnInfo<'a>, LoxError> {
        let params = if kind == FnKind::Method && !self.check(TokenKind::LeftParen) {
            kind = FnKind::Getter;
            Params::Params(vec![])
        } else {
            let opener = self
                .consume(
                    TokenKind::LeftParen,
                    format!(
                        "Expected a `(` after the {}.",
                        match name.kind {
                            TokenKind::Fun => "keyword",
                            TokenKind::Identifier => "name",
                            _ => unreachable!(),
                        }
                    ),
                )?
                .span;

            let params = if self.r#match(TokenKind::Tilde) {
                Params::VarArgs(self.consume_identifier("Expected the parameter name.", "vararg"))
            } else {
                let pack = self.parse_identifier_list(
                    "Expected a parameter name",
                    "param",
                    TokenKind::RightParen,
                );

                if pack.len() >= crate::MAX_PARAMETERS_COUNT {
                    for param in pack[crate::MAX_PARAMETERS_COUNT - 1..].iter() {
                        self.ex.record(
                            LoxError::parse(
                                format!(
                                    "Cannot have more than {} parameters.",
                                    crate::MAX_PARAMETERS_COUNT
                                ),
                                param.span,
                            )
                            .with_module(param.module),
                        );
                    }
                }

                Params::Params(pack)
            };

            self.consume_paired(
                TokenKind::RightParen,
                opener,
                "Expected a `)` after the parameter list.",
            )?;
            params
        };

        let body = if self.r#match(TokenKind::FatArrow) {
            let arrow = self.previous().span;
            let body = vec![stmt!(
                StmtKind::Return(Some(Box::new(self.expression()?))),
                arrow
            )];
            if kind != FnKind::Lambda {
                self.consume_semicolon("Expected a `;` after the function expression.")?;
            }
            body
        } else {
            self.consume(
                TokenKind::LeftBrace,
                "Expected a `{` or `=>` after the parameter pack.",
            )?;
            match self.block()?.kind {
                StmtKind::Block(stmts) => stmts,
                _ => unreachable!(),
            }
        };

        Ok(FnInfo {
            name,
            params,
            body,
            kind,
        })
    }

    fn identifier_expression(&mut self) -> Result<Spanned<Var<'a>>, LoxError> {
        let (span, node) = capture_span!(self, {
            let is_const: bool = self.previous().kind == TokenKind::Const;
            let name = self.consume_identifier("Expected a variable name", "var/const-name");
            let initializer = if self.r#match(TokenKind::Equal) {
                Some(self.expression()?)
            } else {
                None
            };
            Var {
                name,
                initializer,
                is_const,
            }
        });
        Ok(Spanned {
            node,
            span: span.unwrap(),
        })
    }

    fn import_statement(&mut self) -> StmtResult<'a> {
        let keyword = self.previous().span;
        let mut import_from_root = false;

        if self.check(TokenKind::Identifier) && self.peek().lexeme == "root" {
            self.advance();

            if self.r#match(TokenKind::Colon) {
                self.consume(
                    TokenKind::Colon,
                    "Expected a `::` after the root qualifier.",
                )?;
                import_from_root = true;
            } else {
                self.rollback();
            }
        }

        let mut package = vec![self.consume_identifier(
            "Expected a package name after the keyword.",
            "import-pkg-name",
        )];
        while self.r#match(TokenKind::Dot) {
            package.push(self.consume_identifier(
                "Expected a subpackage name after the dot.",
                "import-subpkg-name",
            ));
        }

        let mut bindings = vec![];
        if self.r#match(TokenKind::LeftBrace) {
            let opener = self.previous().span;
            bindings = self.parse_import_binding_list("import-binding");
            self.consume_paired(
                TokenKind::RightBrace,
                opener,
                "Expected a `}` after the export list.",
            )?;
        }

        let alias = if self.r#match(TokenKind::As) {
            Some(self.consume_identifier(
                "Expected an alias name after the `as`.",
                "import-alias-name",
            ))
        } else {
            None
        };
        let span = combspan!(span => keyword, span => self.previous().span);
        self.consume_semicolon("Expected a `;` after the import statement.")?;

        stmt_ok!(
            StmtKind::Import(Box::new(Import {
                package,
                alias,
                // Path and module are set in the resolver
                path: PathBuf::new(),
                module: ModuleId::anon(),
                import_from_root,
                bindings,
            })),
            span
        )
    }

    fn statement(&mut self) -> StmtResult<'a> {
        if self.r#match(TokenKind::If) {
            self.if_statement()
        } else if self.r#match(TokenKind::Break) {
            self.break_statement(true)
        } else if self.r#match(TokenKind::Continue) {
            self.continue_statement(true)
        } else if self.r#match(TokenKind::LeftBrace) {
            self.block()
        } else if self.r#match(TokenKind::For) {
            self.for_statement()
        } else if self.r#match(TokenKind::Loop) {
            self.loop_statement()
        } else if self.r#match(TokenKind::While) {
            self.while_statement()
        } else if self.r#match(TokenKind::Print) {
            self.print_statement(true)
        } else if self.r#match(TokenKind::Return) {
            self.return_statement(true, true)
        } else {
            self.expression_statement()
        }
    }

    fn if_statement(&mut self) -> StmtResult<'a> {
        let (span, stmt) = capture_span!(self, {
            let opener = self
                .consume(TokenKind::LeftParen, "Expected a `(` after the `if`.")?
                .span;
            let condition = self.expression()?;
            self.consume_paired(
                TokenKind::RightParen,
                opener,
                "Expected a `)` after the if condition",
            )?;
            let then = self.statement()?;
            let otherwise = if self.r#match(TokenKind::Else) {
                Some(self.statement()?)
            } else {
                None
            };
            StmtKind::If(Box::new(If {
                condition,
                then,
                otherwise,
            }))
        });

        stmt_ok!(stmt, span.unwrap())
    }

    fn break_statement(&mut self, semicolon_required: bool) -> StmtResult<'a> {
        let span = self.previous().span;
        if semicolon_required {
            self.consume_semicolon("Expected a `;` after the break statement.")?;
        }
        stmt_ok!(StmtKind::Break, span)
    }

    fn continue_statement(&mut self, semicolon_required: bool) -> StmtResult<'a> {
        let span = self.previous().span;
        if semicolon_required {
            self.consume_semicolon("Expected a `;` after the continue statement.")?;
        }
        stmt_ok!(StmtKind::Continue, span)
    }

    fn block(&mut self) -> StmtResult<'a> {
        let span;
        let mut statements = vec![];
        capture_span!(self, span, {
            let opener = self.previous().span;
            while !self.check(TokenKind::RightBrace) && !self.is_at_end() {
                let decl = self.declaration();
                match decl {
                    Ok(stmt) => statements.push(stmt),
                    Err(e) => {
                        self.ex.record(e);
                        self.synchronize();
                    }
                }
            }
            self.consume_paired(
                TokenKind::RightBrace,
                opener,
                "Expected a `}` after the block.",
            )?;
        });
        stmt_ok!(StmtKind::Block(statements), span.unwrap())
    }

    fn for_statement(&mut self) -> StmtResult<'a> {
        let (span, r#for) = capture_span!(self, {
            let keyword = self.previous().span;
            let opener = self
                .consume(TokenKind::LeftParen, "Expected a `(` before the `for`.")?
                .span;
            let mut initializer = None;
            if !self.r#match(TokenKind::Semicolon) {
                if self.match_any(&[TokenKind::Var, TokenKind::Const]) {
                    initializer = Some(self.var_declaration(false)?);
                } else if !self.r#match(TokenKind::Semicolon) {
                    if self.r#match(TokenKind::Identifier) {
                        if self.check(TokenKind::In) {
                            return self.for_each_statement(keyword, opener);
                        }

                        // Restore the identifier token
                        self.rollback();
                    }
                    let expr = self.comma_expression()?;
                    let span = expr.span;
                    initializer = Some(stmt!(StmtKind::ExprStmt(Box::new(expr)), span));
                }
                self.consume(
                    TokenKind::Semicolon,
                    "Expected a `;` after the for variable",
                )?;
            }

            let condition = if self.check(TokenKind::Semicolon) {
                None
            } else {
                Some(self.expression()?)
            };
            self.consume_semicolon("Expected a `;` after the for condition")?;

            let increment = if self.check(TokenKind::RightParen) {
                None
            } else {
                Some(self.comma_expression()?)
            };
            self.consume_paired(
                TokenKind::RightParen,
                opener,
                "Expected a `)` after the for clauses.",
            )?;

            let body = self.statement()?.into_block();

            StmtKind::For(Box::new(For {
                initializer,
                condition,
                increment,
                body,
            }))
        });

        stmt_ok!(r#for, span.unwrap())
    }

    fn for_each_statement(&mut self, start: Span, opener: Span) -> StmtResult<'a> {
        let variable = self.previous().clone();
        self.consume(TokenKind::In, "Expected an `in` after the variable name.")?;
        let iterator = self.expression()?;
        self.consume_paired(
            TokenKind::RightParen,
            opener,
            "Expected a `)` after the for each clauses.",
        )?;
        let body = self.statement()?.into_block();
        let end = self.previous().span;
        let span = combine_spans(start, end).unwrap();
        stmt_ok!(
            StmtKind::ForEach(Box::new(ForEach {
                variable,
                iterator,
                body
            })),
            span
        )
    }

    fn while_statement(&mut self) -> StmtResult<'a> {
        let (span, stmt) = capture_span!(self, {
            let opener = self
                .consume(
                    TokenKind::LeftParen,
                    "Expected a `(` before the while condition.",
                )?
                .span;
            let condition = self.expression()?;
            self.consume_paired(
                TokenKind::RightParen,
                opener,
                "Expected a `)` after the while condition.",
            )?;
            let body = self.statement()?.into_block();
            StmtKind::While(Box::new(condition), Box::new(body))
        });

        stmt_ok!(stmt, span.unwrap())
    }

    fn loop_statement(&mut self) -> StmtResult<'a> {
        let keyword = self.previous();
        let (span, stmt) = capture_span!(self, {
            let condition = expr!(
                ExprKind::Lit(Box::new(Lit {
                    token: keyword.clone(),
                    value: LitValue::Bool(true)
                })),
                keyword.span
            );
            let body = self.statement()?.into_block();
            StmtKind::While(Box::new(condition), Box::new(body))
        });

        stmt_ok!(stmt, span.unwrap())
    }

    fn print_statement(&mut self, semicolon_required: bool) -> StmtResult<'a> {
        let (span, expr) = capture_span!(self, self.comma_expression()?);
        if semicolon_required {
            self.consume_semicolon("Expected a `;` after the print statement.")?;
        }
        stmt_ok!(StmtKind::Print(Box::new(expr)), span.unwrap())
    }

    fn return_statement(
        &mut self,
        semicolon_required: bool,
        comma_expr_allowed: bool,
    ) -> StmtResult<'a> {
        let (span, expr) = capture_span!(self, {
            if self.check(TokenKind::Semicolon) {
                None
            } else {
                Some(Box::new(match comma_expr_allowed {
                    true => self.comma_expression()?,
                    false => self.expression()?,
                }))
            }
        });
        if semicolon_required {
            self.consume_semicolon("Expected a `;` after the return value.")?;
        }
        stmt_ok!(StmtKind::Return(expr), span.unwrap())
    }

    fn expression_statement(&mut self) -> StmtResult<'a> {
        let start = self.previous().span;
        let expr = self.comma_expression()?;
        let end = if let ExprKind::Match(_) = &expr.kind {
            let span = self.previous().span;
            while self.r#match(TokenKind::Semicolon) {}
            span
        } else {
            let span = self.previous().span;
            self.consume_semicolon("Expected a `;` after the expression")?;
            span
        };
        let span = combine_spans(start, end).unwrap();
        stmt_ok!(StmtKind::ExprStmt(Box::new(expr)), span)
    }

    fn comma_expression(&mut self) -> ExprResult<'a> {
        let mut expr = self.expression()?;
        // XXX: shouldn't this be an if?
        while self.r#match(TokenKind::Comma) {
            let right = self.comma_expression()?;
            let span = combine_spans(expr.span, right.span).unwrap();
            expr = expr!(ExprKind::Comma(Box::new(expr), Box::new(right)), span);
        }

        Ok(expr)
    }

    fn expression(&mut self) -> ExprResult<'a> {
        if self.r#match(TokenKind::Fun) {
            self.lambda()
        } else {
            self.assignment()
        }
    }

    fn lambda(&mut self) -> ExprResult<'a> {
        let keyword = self.previous().span;
        let mut name = self.previous().clone();
        name.lexeme = self.intern(format!("[lambda@{}:{}]", keyword.line, keyword.column));
        let info = self.finish_function_parsing(FnKind::Lambda, name)?;
        let span = combspan!(span => keyword, self.previous());
        expr_ok!(ExprKind::Lambda(Box::new(info)), span)
    }

    fn assignment(&mut self) -> ExprResult<'a> {
        let expr = self.logic_or()?;

        if self.match_any(&[
            TokenKind::Equal,
            TokenKind::PlusEqual,
            TokenKind::MinusEqual,
            TokenKind::StarEqual,
            TokenKind::SlashEqual,
            TokenKind::PowEqual,
            TokenKind::PercentEqual,
        ]) {
            let equal = self.previous().clone();
            let mut value = self.expression()?;
            if equal.kind != TokenKind::Equal {
                let span = combine_spans(expr.span, value.span).unwrap();
                // XXX: maybe make a macro for this?
                let kind: Option<BinOpKind> = equal.kind.into();
                let binop = BinOp {
                    node: kind.unwrap(),
                    span: equal.span,
                };
                value = expr!(
                    ExprKind::Binary(binop, Box::new(expr.clone()), Box::new(value)),
                    span
                )
            }
            let span = combspan!(expr, value);
            return match expr.kind {
                ExprKind::Variable(name) => {
                    expr_ok!(
                        ExprKind::Assignment(Box::new(Assignment { name, value })),
                        span
                    )
                }
                ExprKind::GetAttr(attr) => expr_ok!(
                    ExprKind::SetAttr(Box::new(SettAttr {
                        node: attr.node,
                        field: attr.field,
                        value,
                    })),
                    span
                ),
                ExprKind::GetItem(box node, box key) => {
                    expr_ok!(
                        ExprKind::SetItem(Box::new(SetItem { node, key, value })),
                        span
                    )
                }
                _ => {
                    return Err(LoxError::parse("Invalid assignment target", span)
                        .with_module(self.peek().module))
                }
            };
        } else if self.r#match(TokenKind::Query) {
            let query = self.previous().span;
            let was_in_ternary = self.is_in_ternary;
            self.is_in_ternary = true;

            let left = self.expression()?;
            self.consume(TokenKind::Colon, "Expected a `:` in a ternary operator.")?;
            let right = self.expression()?;

            self.is_in_ternary = was_in_ternary;
            let span = combspan!(expr, right);

            if was_in_ternary {
                self.ex.record(
                    LoxError::parse_nested_ternary(
                        "Nested ternary operators are not allowed.",
                        query,
                        span,
                    )
                    .with_module(self.peek().module),
                );
            }

            return expr_ok!(
                ExprKind::Ternary(Box::new(Ternary {
                    predicate: expr,
                    then: left,
                    otherwise: right
                })),
                span
            );
        }

        Ok(expr)
    }

    fn logic_or(&mut self) -> ExprResult<'a> {
        let mut expr = self.logic_and()?;

        while self.r#match(TokenKind::Or) {
            let binop = BinOp {
                span: self.previous().span,
                node: BinOpKind::Or,
            };
            let right = self.logic_and()?;
            let span = combspan!(expr, right);
            expr = expr!(
                ExprKind::Binary(binop, Box::new(expr), Box::new(right)),
                span
            );
        }

        Ok(expr)
    }

    fn logic_and(&mut self) -> ExprResult<'a> {
        let mut expr = self.equality()?;

        while self.r#match(TokenKind::And) {
            let binop = BinOp {
                span: self.previous().span,
                node: BinOpKind::And,
            };
            let right = self.equality()?;
            let span = combspan!(expr, right);
            expr = expr!(
                ExprKind::Binary(binop, Box::new(expr), Box::new(right)),
                span
            );
        }

        Ok(expr)
    }

    fn equality(&mut self) -> ExprResult<'a> {
        let mut expr = self.comparison()?;

        while self.match_any(&[TokenKind::EqualEqual, TokenKind::BangEqual]) {
            expr = parse_rhs_operators!(self, expr, self.comparison());
        }

        Ok(expr)
    }

    fn comparison(&mut self) -> ExprResult<'a> {
        let mut expr = self.addition()?;

        while self.match_any(&[
            TokenKind::Greater,
            TokenKind::Less,
            TokenKind::GreaterEqual,
            TokenKind::LessEqual,
        ]) {
            expr = parse_rhs_operators!(self, expr, self.addition());
        }

        Ok(expr)
    }

    fn addition(&mut self) -> ExprResult<'a> {
        let mut expr = self.multiplication()?;

        while self.match_any(&[TokenKind::Plus, TokenKind::Minus]) {
            expr = parse_rhs_operators!(self, expr, self.multiplication());
        }

        Ok(expr)
    }

    fn multiplication(&mut self) -> ExprResult<'a> {
        let mut expr = self.exponentiation()?;

        while self.match_any(&[TokenKind::Star, TokenKind::Slash, TokenKind::Percent]) {
            expr = parse_rhs_operators!(self, expr, self.exponentiation());
        }

        Ok(expr)
    }

    fn exponentiation(&mut self) -> ExprResult<'a> {
        let mut expr = self.unary()?;

        while self.r#match(TokenKind::Pow) {
            expr = parse_rhs_operators!(self, expr, self.exponentiation());
        }

        Ok(expr)
    }

    fn unary(&mut self) -> ExprResult<'a> {
        match self.peek().kind {
            TokenKind::Match => {
                self.advance();
                self.match_expression()
            }
            TokenKind::Plus | TokenKind::Minus | TokenKind::Bang => {
                let op = self.advance().clone();
                let kind: Option<UnOpKind> = op.kind.into();
                let expr = self.unary()?;
                let unop = UnOp {
                    node: kind.unwrap(),
                    span: op.span,
                };
                let span = combspan!(op, expr);
                expr_ok!(ExprKind::Unary(unop, Box::new(expr)), span)
            }
            _ => self.call(),
        }
    }

    fn match_expression(&mut self) -> ExprResult<'a> {
        let keyword = self.previous().span;
        let value = self.expression()?;
        let mut bound_var = None;

        if self.r#match(TokenKind::As) {
            bound_var =
                Some(self.consume_identifier(
                    "Expected an identifier after the `as`.",
                    "match-bound-var",
                ))
                .map(|mut v| {
                    v.kind = TokenKind::MatchBoundVar;
                    v
                });
        }

        let opener = self
            .consume(
                TokenKind::LeftBrace,
                "Expected a `{` after the match value.",
            )?
            .span;

        let arms = self.parse_match_arms()?;

        let closer = self.consume_paired(
            TokenKind::RightBrace,
            opener,
            "Expected a `}` after a match expression",
        )?;
        let span = combspan!(span => keyword, closer);
        expr_ok!(
            ExprKind::Match(Box::new(Match {
                value,
                arms,
                bound_var,
            })),
            span
        )
    }

    fn parse_match_arms(&mut self) -> Result<Vec<Arm<'a>>, LoxError> {
        let mut placeholder = None;
        let mut last_arm_is_stmt = false;
        let mut arms = vec![];

        loop {
            if self.r#match(TokenKind::Comma) && last_arm_is_stmt {
                let tok = self.previous().clone();
                self.ex.record(LoxError::parse(
                    "Statements must end with a `;` or `}` and therefore do not require a comma.",
                    tok.span,
                ).with_module(tok.module))
            }
            if self.check(TokenKind::RightBrace) {
                break;
            }

            let arm = self.match_arm()?;
            last_arm_is_stmt = !matches!(arm.body.kind, StmtKind::ExprStmt(_));

            if arm.is_placeholder {
                if placeholder.is_some() {
                    self.ex.record(
                        LoxError::parse(
                            "Multiple wildcard `_` branches are not allowed.",
                            arm.body.span,
                        )
                        .with_module(self.peek().module),
                    );
                } else {
                    placeholder = Some(arm);
                    continue;
                }
            }
            arms.push(arm);
        }

        if placeholder.is_none() {
            let span = self.peek().span;
            let nil = expr!(
                ExprKind::Lit(Box::new(Lit {
                    value: LitValue::Nil,
                    token: Token::with_span(TokenKind::Identifier, "nil", span),
                })),
                span
            );
            let pat = expr!(
                ExprKind::Variable(Token::with_span(TokenKind::Identifier, "_", span)),
                span
            );
            placeholder = Some(Arm {
                pat,
                pat_kind: MatchPatKind::Equality,
                body: stmt!(StmtKind::ExprStmt(Box::new(nil)), span),
                is_placeholder: true,
            });
        }

        arms.push(placeholder.unwrap());

        Ok(arms)
    }

    fn match_arm(&mut self) -> Result<Arm<'a>, LoxError> {
        let pat;
        let mut kind = MatchPatKind::Equality;
        let mut is_placeholder = false;

        // Check if this arm is a wildcard arm (`_` => ...)
        if self.check(TokenKind::Identifier) && self.peek().lexeme == "_" {
            let name = self.advance();
            pat = expr!(ExprKind::Variable(name.clone()), name.span);
            is_placeholder = true;
        } else if self.r#match(TokenKind::Class) {
            let kw = self.previous().span;
            let name = match self.peek().kind {
                TokenKind::LeftBrace => {
                    let opener = self.advance().span;
                    let closer = self
                        .consume_paired(
                            TokenKind::RightBrace,
                            opener,
                            "Expected a `}` after the dict class pattern.",
                        )?
                        .span;
                    let span = combspan!(span => opener, span => closer);
                    Token::with_span(TokenKind::Identifier, "{}", span)
                }
                TokenKind::LeftBracket => {
                    let opener = self.advance().span;
                    let closer = self
                        .consume_paired(
                            TokenKind::RightBracket,
                            opener,
                            "Expected a `]` after the array class pattern.",
                        )?
                        .span;
                    let span = combspan!(span => opener, span => closer);
                    Token::with_span(TokenKind::Identifier, "[]", span)
                }
                _ => self.consume_identifier(
                    "Expected an identifier, [], or {} in the class name pattern.",
                    "match/cls-pat-ident",
                ),
            };
            let span = combspan!(span => kw, name);
            pat = expr!(ExprKind::Variable(name), span);
            kind = MatchPatKind::TypeEquality;
        } else {
            pat = self.expression()?;
        }

        self.consume(TokenKind::FatArrow, "Expected a `=>` after a pattern")?;

        let token = self.advance().clone();
        let body = match token.kind {
            TokenKind::Return => self.return_statement(true, false)?,
            TokenKind::Continue => self.continue_statement(true)?,
            TokenKind::Break => self.break_statement(true)?,
            TokenKind::Print => self.print_statement(true)?,
            TokenKind::LeftBrace => self.block()?,
            _ => {
                self.rollback();
                let expr = self.expression()?;
                let span = expr.span;
                stmt!(StmtKind::ExprStmt(Box::new(expr)), span)
            }
        };

        Ok(Arm {
            pat,
            pat_kind: kind,
            body,
            is_placeholder,
        })
    }

    fn call(&mut self) -> ExprResult<'a> {
        let mut expr = self.increment_or_variable()?;

        while match self.advance().kind {
            TokenKind::LeftParen => {
                expr = self.finish_call(expr)?;
                true
            }
            TokenKind::DoubleDot => {
                let item = self.unary()?;
                let span = combspan!(expr, item);
                expr = expr!(ExprKind::GetItem(Box::new(expr), Box::new(item)), span);
                true
            }
            TokenKind::Dot => {
                let field = self
                    .consume_identifier("Expected a property name after the `.`.", "field-name");
                let span = combspan!(expr, field);
                expr = expr!(
                    ExprKind::GetAttr(Box::new(GetAttr { node: expr, field })),
                    span
                );
                true
            }
            TokenKind::EOF => false,
            _ => {
                self.rollback(); // TODO: profile this vs .peek()
                false
            }
        } {}

        Ok(expr)
    }

    fn increment_or_variable(&mut self) -> ExprResult<'a> {
        if self.match_any(&[TokenKind::Increment, TokenKind::Decrement]) {
            let token = self.previous().clone();
            let name = self.consume_identifier(
                "Expected a variable name after the operator.",
                "inc/dec-prefix",
            );
            let kind = if token.kind == TokenKind::Increment {
                IncDecKind::Increment
            } else {
                IncDecKind::Decrement
            };
            let span = combspan!(token, name);
            expr_ok!(
                ExprKind::PrefixIncDec(Box::new(IncDec { name, kind })),
                span
            )
        } else if self.r#match(TokenKind::Identifier) {
            let name = self.previous().clone();
            if self.match_any(&[TokenKind::Increment, TokenKind::Decrement]) {
                let token = self.previous();
                let kind = if token.kind == TokenKind::Increment {
                    IncDecKind::Increment
                } else {
                    IncDecKind::Decrement
                };
                let span = combspan!(name, token);
                expr_ok!(
                    ExprKind::PostfixIncDec(Box::new(IncDec { name, kind })),
                    span
                )
            } else {
                let span = name.span;
                expr_ok!(ExprKind::Variable(name), span)
            }
        } else {
            self.primary()
        }
    }

    fn primary(&mut self) -> ExprResult<'a> {
        let token = self.advance().clone();
        let t_span = token.span;
        let expr = match token.kind {
            TokenKind::True => expr!(
                ExprKind::Lit(Box::new(Lit {
                    token,
                    value: LitValue::Bool(true)
                })),
                t_span
            ),
            TokenKind::False => expr!(
                ExprKind::Lit(Box::new(Lit {
                    token,
                    value: LitValue::Bool(false)
                })),
                t_span
            ),
            TokenKind::Nil => expr!(
                ExprKind::Lit(Box::new(Lit {
                    token,
                    value: LitValue::Nil
                })),
                t_span
            ),
            TokenKind::IntegerLiteral
            | TokenKind::HexadecimalLiteral
            | TokenKind::BinaryLiteral => {
                let parse_result = match token.kind {
                    TokenKind::BinaryLiteral => from_str_radix(&token.lexeme[2..], 2),
                    TokenKind::IntegerLiteral => from_str_radix(token.lexeme, 10),
                    TokenKind::HexadecimalLiteral => from_str_radix(&token.lexeme[2..], 16),
                    _ => unreachable!(),
                };
                expr!(
                    ExprKind::Lit(Box::new(Lit {
                        token: token.clone(),
                        value: LitValue::Integer(parse_result.map_err(|e| {
                            LoxError::parse(
                                match e {
                                    IntErrorKind::PosOverflow => "Integer literal is too big",
                                    IntErrorKind::NegOverflow => "Integer literal is too small",
                                    IntErrorKind::Empty => {
                                        "The literal must be followed by at least one digit"
                                    }
                                    kind => unreachable!(
                                        "Scanned an invalid integer literal: {}, e = {:?}",
                                        token.lexeme, kind
                                    ),
                                }
                                .to_string(),
                                token.span,
                            )
                            .with_module(token.module)
                        })?),
                    })),
                    t_span
                )
            }
            TokenKind::FloatLiteral => expr!(
                ExprKind::Lit(Box::new(Lit {
                    token: token.clone(),
                    value: LitValue::Float(token.lexeme.parse::<LoxFloat>().map_err(|e| {
                        LoxError::parse(format!("Invalid float: {}", e), token.span)
                            .with_module(token.module)
                    })?),
                })),
                t_span
            ),
            TokenKind::StringLiteral => expr!(
                ExprKind::Lit(Box::new(Lit {
                    token: token.clone(),
                    // Strip the quotes
                    value: LitValue::Str(token.lexeme[1..token.lexeme.len() - 1].to_owned()),
                })),
                t_span
            ),
            TokenKind::This => expr!(ExprKind::This, token.span),
            TokenKind::Super => {
                self.consume(TokenKind::Dot, "Expected a `.` after the `super`.")?;
                let method =
                    self.consume_identifier("Expected a superclass method name", "super/method");
                let span = combspan!(token, method);
                expr!(
                    ExprKind::Super(Box::new(Super {
                        method,
                        span: token.span
                    })),
                    span
                )
            }
            TokenKind::LeftParen => {
                let is_in_ternary = self.is_in_ternary;
                self.is_in_ternary = false;
                let expr = ensure_sufficient_stack(|| self.comma_expression());
                self.is_in_ternary = is_in_ternary;

                let expr = expr?;
                let closer = self.consume_paired(
                    TokenKind::RightParen,
                    token.span,
                    "Expected a `)` after the expression.",
                )?;
                let span = combspan!(token, closer);
                expr!(ExprKind::Grouping(Box::new(expr)), span)
            }
            TokenKind::InterpolatedString => self.interpolated_string()?,
            TokenKind::LeftBracket => ensure_sufficient_stack(|| self.array_literal())?,
            TokenKind::LeftBrace => ensure_sufficient_stack(|| self.dict_literal())?,
            _ => {
                return Err(if token.kind.into_binop().is_some() {
                    LoxError::parse(
                        format!(
                            "Expected a left operand for the operator `{}`.",
                            token.lexeme
                        ),
                        token.span,
                    )
                } else {
                    LoxError::parse("Expected an expression.", token.span)
                }
                .with_module(token.module))
            }
        };
        Ok(expr)
    }

    fn interpolated_string(&mut self) -> ExprResult<'a> {
        let start = self.previous().span;
        let mut fragments = vec![];

        while !self.check(TokenKind::InterpolatedStringEnd) && !self.is_at_end() {
            if self.r#match(TokenKind::InterpFragmentStart) {
                let start = self.current;
                while !self.is_at_end() && !self.check(TokenKind::InterpFragmentEnd) {
                    self.advance();
                }
                if self.r#match(TokenKind::InterpFragmentEnd) {
                    let mut tokens = self.tokens[start..self.current - 1].to_vec();
                    tokens.push(Token::eof_from_span(tokens.last().unwrap().span));

                    let mut sub_parser = Parser::new(self.ctx.clone());
                    sub_parser.tokens = tokens;
                    match sub_parser.expression() {
                        Ok(expr) => fragments.push(InterFrag::Expr(expr)),
                        Err(e) => self.ex.record(e),
                    }
                }
            } else {
                let token = self
                    .consume(TokenKind::StringLiteral, "Expected a string literal.")
                    .unwrap()
                    .clone();
                if !token.lexeme.is_empty() {
                    fragments.push(InterFrag::Str(Lit {
                        token: token.clone(),
                        value: LitValue::Str(token.lexeme.to_owned()),
                    }));
                }
            }
        }

        self.consume(
            TokenKind::InterpolatedStringEnd,
            "Expected the end of an interpolated string",
        )?;

        if let Some(InterFrag::Str(s)) = fragments.first_mut() {
            match s.value {
                LitValue::Str(ref mut s) => *s = String::from(&s[1..]),
                _ => unreachable!(),
            }
        }

        if fragments.len() > 1 {
            match &mut fragments.last_mut().unwrap() {
                InterFrag::Str(s) => match s.value {
                    LitValue::Str(ref mut s) => *s = String::from(&s[..s.len() - 1]),
                    _ => unreachable!(),
                },
                _ => unreachable!(),
            }
        }

        let fragments = fragments
            .into_iter()
            .filter(|f| match f {
                InterFrag::Str(s) => match s.value {
                    LitValue::Str(ref s) => !s.is_empty(),
                    _ => unreachable!(),
                },
                _ => true,
            })
            .collect();

        let span = combspan!(span => start, self.previous());
        expr_ok!(ExprKind::InterStr(InterStr { fragments }), span)
    }

    fn array_literal(&mut self) -> ExprResult<'a> {
        let opener = self.previous().span;
        let array = self.parse_expr_list(TokenKind::RightBracket)?;
        let closer = self.consume_paired(
            TokenKind::RightBracket,
            opener,
            "Expected a `]` at the end of an array literal.",
        )?;
        let span = combspan!(span => opener, closer);
        expr_ok!(ExprKind::Array(array), span)
    }

    fn dict_literal(&mut self) -> ExprResult<'a> {
        let opener = self.previous().span;
        let mut dict = vec![];

        if !self.check(TokenKind::RightBrace) {
            dict.push(self.mapping()?);
        }

        while self.r#match(TokenKind::Comma) {
            // Allow trailing comma
            if self.check(TokenKind::RightBrace) {
                break;
            }
            dict.push(self.mapping()?);
        }
        let closer = self.consume_paired(
            TokenKind::RightBrace,
            opener,
            "Expected a `}` at the end of a dict literal.",
        )?;
        let span = combspan!(span => opener, closer);
        expr_ok!(ExprKind::Dict(dict), span)
    }

    fn mapping(&mut self) -> Result<(Expr<'a>, Expr<'a>), LoxError> {
        let key = self.expression()?;
        self.consume(TokenKind::Colon, "Expected a `:` after a dict key")?;
        let value = self.expression()?;
        Ok((key, value))
    }

    fn finish_call(&mut self, callee: Expr<'a>) -> ExprResult<'a> {
        let opener = self.previous().span;
        let (args, fwd) = if self.check(TokenKind::RightParen) {
            self.advance();
            (
                Args {
                    node: vec![],
                    span: Span::new(opener.line, opener.column, 0),
                },
                false,
            )
        } else {
            let (span, (fwd, args)) = capture_span!(self, self.arguments()?);
            self.consume_paired(
                TokenKind::RightParen,
                opener,
                "Expected a `)` after the argument list.",
            )?;
            (
                Args {
                    node: args,
                    span: span.unwrap(),
                },
                fwd,
            )
        };
        let span = combspan!(callee, args);
        expr_ok!(ExprKind::call(Box::new(callee), args, fwd), span)
    }

    /// Parses a call expression's argument list. Does not consume the closing parenthesis.
    fn arguments(&mut self) -> Result<(bool, Vec<Expr<'a>>), LoxError> {
        let mut args = vec![];
        let mut can_use_fwd = true;
        let mut used_fwd = false;

        if !self.check(TokenKind::RightParen) {
            if can_use_fwd && self.r#match(TokenKind::Ellipsis) {
                used_fwd = true;
                args.push(self.parse_fwd_arg()?);
            } else {
                can_use_fwd = false;
                args.push(self.expression()?);
            }
        }

        while self.r#match(TokenKind::Comma) {
            // Allow trailing comma
            if self.check(TokenKind::RightParen) {
                break;
            }
            if self.r#match(TokenKind::Ellipsis) {
                args.push(self.parse_fwd_arg()?);
                if !can_use_fwd {
                    let span = args.last().unwrap().span;
                    self.ex.record(
                        LoxError::parse(
                            "Rest arguments must be provided before normal arguments.",
                            span,
                        )
                        .with_module(self.peek().module),
                    );
                }
            } else {
                can_use_fwd = false;
                args.push(self.expression()?);
            }
        }

        Ok((used_fwd, args))
    }

    /// Parses an expression preceded by an ellipsis (forwarded argument)
    fn parse_fwd_arg(&mut self) -> Result<Expr<'a>, LoxError> {
        let op = self.previous().span;
        let arg = self.expression()?;
        let span = combspan!(span => op, arg);
        expr_ok!(ExprKind::ArgForwarding(Box::new(arg)), span)
    }

    /// Parses a comma-delimited sequence of expressions. Does not consume the stop token.
    fn parse_expr_list(&mut self, stop_token: TokenKind) -> Result<Vec<Expr<'a>>, LoxError> {
        let mut args = vec![];

        if !self.check(stop_token) {
            args.push(self.expression()?);
        }

        while self.r#match(TokenKind::Comma) {
            // Allow trailing comma
            if self.check(stop_token) {
                break;
            }
            args.push(self.expression()?);
        }

        Ok(args)
    }

    /// Parses a comma-delimited sequence of identifiers. Does not consume the stop token.
    fn parse_identifier_list(
        &mut self,
        msg: &str,
        label: &str,
        stop_token: TokenKind,
    ) -> Vec<Token<'a>> {
        let mut args = vec![];

        if !self.check(stop_token) {
            args.push(self.consume_identifier(msg, &format!("{}-{}", label, args.len())));
        }

        while self.r#match(TokenKind::Comma) {
            // Allow trailing comma
            if self.check(stop_token) {
                break;
            }
            args.push(self.consume_identifier(msg, &format!("{}-{}", label, args.len())));
        }

        args
    }

    /// Parse a comma-delimited sequence of import bindings.
    fn parse_import_binding_list(&mut self, label: &str) -> Vec<ImportBinding<'a>> {
        const EXPORT_MSG: &str = "Expected an export name";
        const BNDING_MSG: &str = "Expected a binding name after the `as`.";

        let mut args = vec![];

        if !self.check(TokenKind::RightBrace) {
            let export = self.consume_identifier(EXPORT_MSG, &format!("{}-{}", label, args.len()));
            let bind_to = if self.r#match(TokenKind::As) {
                self.consume_identifier(BNDING_MSG, &format!("{}-{}", label, args.len()))
            } else {
                export.clone()
            };
            args.push(ImportBinding { export, bind_to });
        }

        while self.r#match(TokenKind::Comma) {
            // Allow trailing comma
            if self.check(TokenKind::RightBrace) {
                break;
            }
            let export = self.consume_identifier(EXPORT_MSG, &format!("{}-{}", label, args.len()));
            let bind_to = if self.r#match(TokenKind::As) {
                self.consume_identifier(BNDING_MSG, &format!("{}-{}", label, args.len()))
            } else {
                export.clone()
            };
            args.push(ImportBinding { export, bind_to });
        }

        args
    }

    #[inline]
    /// Returns a reference to the current, yet to be consumed token.
    fn peek(&self) -> &Token<'a> {
        &self.tokens[self.current]
    }

    /// Returns a reference to the previous token.
    fn previous(&self) -> &Token<'a> {
        if self.current == 0 {
            &self.tokens[self.current]
        } else {
            &self.tokens[self.current - 1]
        }
    }

    /// Advances to the next token if the parser has not reached the end yet.
    /// Otherwise, returns the last token (EOF).
    fn advance(&mut self) -> &Token<'a> {
        if self.is_at_end() {
            self.peek()
        } else {
            self.current += 1;
            self.previous()
        }
    }

    /// Rollbacks the token pointer to the previous value.
    fn rollback(&mut self) {
        if self.current > 0 {
            self.current -= 1;
        }
    }

    /// Attempts to consume the current token, verifying that it matches the given `kind`.
    /// If the kinds do not match, returns an [`parse`] error with the given message and the span of the current token.
    fn consume<S: Into<String>>(
        &mut self,
        kind: TokenKind,
        msg: S,
    ) -> Result<&Token<'a>, LoxError> {
        if self.check(kind) {
            Ok(self.advance())
        } else {
            let tok = self.peek();
            Err(LoxError::parse(msg, tok.span).with_module(tok.module))
        }
    }

    /// Attempts to consume a semicolon. If there are several semicolons one after another, consumes them as well.
    /// Otherwise, fails with the given message.
    fn consume_semicolon<S: Into<String>>(&mut self, msg: S) -> Result<&Token<'a>, LoxError> {
        if self.check(TokenKind::Semicolon) {
            let token = self.current;

            // allow multiple semicolons
            while self.r#match(TokenKind::Semicolon) {}

            Ok(&self.tokens[token])
        } else {
            let tok = self.peek();
            Err(LoxError::parse(msg, tok.span).with_module(tok.module))
        }
    }

    /// Consumes the second token in a pair.
    /// The given `opener` token will be used to construct an error highlighting the unmatched parts.
    fn consume_paired<S: Into<String>>(
        &mut self,
        kind: TokenKind,
        opener: Span,
        msg: S,
    ) -> Result<&Token<'a>, LoxError> {
        if self.check(kind) {
            Ok(self.advance())
        } else {
            let tok = self.peek();
            Err(LoxError::unclosed_delimiter(msg.into(), tok.span, opener).with_module(tok.module))
        }
    }

    /// Attempts to consume an identifier. If the current token is not one,
    /// records the error and creates a fake identifier token
    /// using the span of the current token and the given label.
    #[allow(clippy::extra_unused_lifetimes)]
    fn consume_identifier<'c: 'a, S: Into<String>>(&mut self, msg: S, label: S) -> Token<'a> {
        let token = self.consume(TokenKind::Identifier, msg);
        match token {
            Ok(token) => token.clone(),
            Err(e) => {
                self.ex.record(e);
                if self.check(TokenKind::This) {
                    let tok = self.peek().clone();
                    self.ex.record(
                        LoxError::parse(
                            "`this` is only allowed inside methods, getters, and initializers.",
                            tok.span,
                        )
                        .with_module(tok.module),
                    );
                }
                let span = self.peek().span;
                let lexeme: &'c str =
                    self.intern(format!("#{}@{}:{}", label.into(), span.line, span.column));
                Token::with_span(TokenKind::Identifier, lexeme, span).set_module(self.peek().module)
            }
        }
    }

    fn intern<'c: 'a>(&self, s: String) -> &'c str {
        // SAFETY: I'm not sure why this is invalid without unsafe. The context is constrained to live longer than 'a,
        //         so reducing 'c to 'a should be OK. The unsafe block below assumes that the string pool inside
        //         the context lives for as long as the context itself, in addition to the ['c: 'a] required on the Parser struct.
        unsafe { std::mem::transmute(self.ctx.write_shared().intern(s)) }
    }

    /// Checks if the current token matches the given kind.
    /// Return false if the current token is an EOF.
    fn check(&self, kind: TokenKind) -> bool {
        if self.is_at_end() {
            return false;
        }

        self.peek().kind == kind
    }

    /// Returns true and advances to the next token if the current token's type matches
    /// any of the given token kinds.
    fn match_any(&mut self, kinds: &[TokenKind]) -> bool {
        for kind in kinds {
            if self.check(*kind) {
                self.advance();
                return true;
            }
        }

        false
    }

    /// Just like `match_any()`, but matches a signgle kind.
    fn r#match(&mut self, kind: TokenKind) -> bool {
        self.match_any(&[kind])
    }

    /// Returns true if the parsed has reached the end and there is no tokens left.
    fn is_at_end(&self) -> bool {
        self.peek().is_eof()
    }

    fn synchronize(&mut self) {
        self.advance();
        while !self.is_at_end() {
            if self.previous().kind == TokenKind::Semicolon {
                return;
            }
            if [
                TokenKind::Class,
                TokenKind::Fun,
                TokenKind::Var,
                TokenKind::For,
                TokenKind::If,
                TokenKind::While,
                TokenKind::Print,
                TokenKind::Return,
            ]
            .contains(&self.peek().kind)
            {
                return;
            }

            self.advance();
        }
    }
}

/// Combines two spans in a single span that covers
/// the entire range between the spans.
pub fn combine_spans(start: Span, end: Span) -> Option<Span> {
    if start == end {
        return Some(start);
    }

    if end.column < start.column && end.line == start.line {
        return None;
    }

    Some(Span::combined(
        Span::new(
            start.line,
            start.column,
            if start.line == end.line {
                end.end() - start.column
            } else {
                start.length
            },
        ),
        end,
    ))
}

/// Stolen from the standard library and modified to parse only `LoxInt` and ignore underscores.
fn from_str_radix(src: &str, radix: u32) -> Result<LoxInt, IntErrorKind> {
    use std::num::IntErrorKind::*;

    assert!(
        (2..=36).contains(&radix),
        "from_str_radix_int: must lie in the range `[2, 36]` - found {}",
        radix
    );

    if src.is_empty() {
        return Err(Empty);
    }

    let is_signed_ty = true; // Lox integers are signed

    // all valid digits are ascii, so we will just iterate over the utf8 bytes
    // and cast them to chars. .to_digit() will safely return None for anything
    // other than a valid ascii digit for the given radix, including the first-byte
    // of multi-byte sequences
    let src = src.as_bytes();

    let (is_positive, digits) = match src[0] {
        b'+' | b'-' if src[1..].is_empty() => {
            return Err(InvalidDigit);
        }
        b'+' => (true, &src[1..]),
        b'-' if is_signed_ty => (false, &src[1..]),
        _ => (true, src),
    };

    let mut result = LoxInt::from(0);
    if is_positive {
        // The number is positive
        for &c in digits {
            if c == b'_' {
                continue;
            }
            let x = match (c as char).to_digit(radix) {
                Some(x) => x as LoxInt,
                None => return Err(InvalidDigit),
            };
            result = match result.checked_mul(radix as LoxInt) {
                Some(result) => result,
                None => return Err(PosOverflow),
            };
            result = match result.checked_add(x) {
                Some(result) => result,
                None => return Err(PosOverflow),
            };
        }
    } else {
        // The number is negative
        for &c in digits {
            if c == b'_' {
                continue;
            }
            let x = match (c as char).to_digit(radix) {
                Some(x) => x as LoxInt,
                None => return Err(InvalidDigit),
            };
            result = match result.checked_mul(radix as LoxInt) {
                Some(result) => result,
                None => return Err(NegOverflow),
            };
            result = match result.checked_sub(x) {
                Some(result) => result,
                None => return Err(NegOverflow),
            };
        }
    }
    Ok(result)
}
