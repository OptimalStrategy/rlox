use crate::{ast::*, SourceInfo};

pub fn ast_to_code(ast: &AST<'_>, info: Option<&SourceInfo>) -> String {
    let header = info
        .map(|i| {
            vec![
                format!("// Module: {}", i.module),
                format!("// Script: {}", i.filename),
                format!("// Hash:   {:x}", i.source_hash),
            ]
        })
        .unwrap_or_else(|| vec!["// Script: <stream>".to_owned()])
        .join("\n");

    let mut buf = header;
    buf.push_str("\n\n");
    for stmt in &ast.body {
        buf.push_str(&format!("{}\n", stmt_to_code(stmt, 0)));
    }
    buf
}

pub(crate) fn expr_to_code(expr: &Expr<'_>, depth: usize) -> String {
    match &expr.kind {
        ExprKind::Binary(op, left, right) => format!(
            "{} {} {}",
            expr_to_code(left, depth),
            match op.node {
                BinOpKind::Add => "+",
                BinOpKind::Sub => "-",
                BinOpKind::Mul => "*",
                BinOpKind::Pow => "**",
                BinOpKind::Div => "/",
                BinOpKind::Rem => "%",
                BinOpKind::And => "and",
                BinOpKind::Or => "or",
                BinOpKind::Eq => "==",
                BinOpKind::Ne => "!=",
                BinOpKind::Lt => "<",
                BinOpKind::Le => "<=",
                BinOpKind::Ge => ">=",
                BinOpKind::Gt => ">",
            },
            expr_to_code(right, depth)
        ),
        ExprKind::Unary(op, operand) => format!(
            "{}{}",
            match op.node {
                UnOpKind::Not => "!",
                UnOpKind::Neg => "-",
                UnOpKind::Plus => "+",
            },
            expr_to_code(operand, depth)
        ),
        ExprKind::ArgForwarding(expr) => format!("...{}", expr_to_code(expr, depth)),
        ExprKind::Lit(lit) => match &lit.value {
            LitValue::Integer(i) => i.to_string(),
            LitValue::Float(f) => f.to_string(),
            LitValue::Bool(b) => b.to_string(),
            LitValue::Str(s) => format!("\"{}\"", s.to_string()),
            LitValue::Nil => String::from("nil"),
        },
        ExprKind::Comma(left, right) => format!(
            "({}, {})",
            expr_to_code(left, depth),
            expr_to_code(right, depth)
        ),
        ExprKind::Call { callee, args, .. } => format!(
            "{}({})",
            expr_to_code(callee, depth),
            args.node
                .iter()
                .map(|expr| expr_to_code(expr, depth))
                .collect::<Vec<_>>()
                .join(", ")
        ),

        ExprKind::GetAttr(get) => {
            format!("{}.{}", expr_to_code(&get.node, depth), get.field.lexeme)
        }
        ExprKind::SetAttr(set) => format!(
            "{}.{} = {}",
            expr_to_code(&set.node, depth),
            set.field.lexeme,
            expr_to_code(&set.value, depth)
        ),
        ExprKind::GetItem(obj, item) => format!(
            "{} .. {}",
            expr_to_code(obj, depth),
            expr_to_code(item, depth)
        ),
        ExprKind::SetItem(set) => format!(
            "{} .. {} = {}",
            expr_to_code(&set.node, depth),
            expr_to_code(&set.key, depth),
            expr_to_code(&set.value, depth)
        ),
        ExprKind::InterStr(inter) => format!(
            "\"{}\"",
            inter
                .fragments
                .iter()
                .map(|frag| match frag {
                    InterFrag::Str(s) => {
                        s.token.lexeme.to_owned()
                    }
                    InterFrag::Expr(e) => format!("${{{}}}", expr_to_code(e, depth)),
                })
                .collect::<String>()
                .trim_matches('\"')
        ),
        ExprKind::Array(exprs) => format!(
            "[{}]",
            exprs
                .iter()
                .map(|expr| expr_to_code(expr, depth))
                .collect::<Vec<_>>()
                .join(", ")
        ),
        ExprKind::Dict(pairs) => format!(
            "{{{}}}",
            pairs
                .iter()
                .map(|(k, v)| format!("{}: {}", expr_to_code(k, depth), expr_to_code(v, depth)))
                .collect::<Vec<_>>()
                .join(", ")
        ),
        ExprKind::This => String::from("this"),
        ExprKind::Super(sup) => format!("super.{}", sup.method.lexeme),
        ExprKind::Ternary(t) => format!(
            "({} ? {} : {})",
            expr_to_code(&t.predicate, depth),
            expr_to_code(&t.then, depth),
            expr_to_code(&t.otherwise, depth)
        ),
        ExprKind::Match(r#match) => {
            let mut buf = format!("match {}", expr_to_code(&r#match.value, depth));
            let indent = " ".repeat(4 * depth.max(1));

            if let Some(bound) = &r#match.bound_var {
                buf.push_str(&format!(" as {}", bound.lexeme));
            }

            buf.push_str(" {\n");

            for arm in &r#match.arms {
                buf.push_str(&indent);
                if matches!(arm.pat_kind, MatchPatKind::TypeEquality) {
                    buf.push_str("class ");
                }
                buf.push_str(&format!("{} => ", expr_to_code(&arm.pat, depth)));
                let mut body = stmt_to_code(&arm.body, depth);
                if matches!(&arm.body.kind, StmtKind::ExprStmt(_)) {
                    body.pop();
                    body.push(',');
                }
                buf.push_str(&format!("{}\n", body));
            }

            buf.push_str(&format!("{}}}", " ".repeat(depth.saturating_sub(1) * 4)));
            buf
        }
        ExprKind::Variable(name) => name.lexeme.to_owned(),
        ExprKind::PrefixIncDec(inc) => format!(
            "{}{}",
            match inc.kind {
                IncDecKind::Increment => "++",
                IncDecKind::Decrement => "--",
            },
            inc.name.lexeme
        ),
        ExprKind::PostfixIncDec(inc) => format!(
            "{}{}",
            inc.name.lexeme,
            match inc.kind {
                IncDecKind::Increment => "++",
                IncDecKind::Decrement => "--",
            }
        ),
        ExprKind::Assignment(asn) => {
            format!("{} = {}", asn.name.lexeme, expr_to_code(&asn.value, depth))
        }
        ExprKind::Lambda(info) => fn_to_code(info, depth.saturating_sub(1)),
        ExprKind::Grouping(expr) => format!("({})", expr_to_code(expr, depth)),
    }
}

fn fn_to_code(meta: &FnInfo, depth: usize) -> String {
    format!(
        "{}({}){}{}",
        match meta.kind {
            FnKind::Function => {
                format!("fun {}", meta.name.lexeme)
            }
            FnKind::Method | FnKind::Getter | FnKind::Static => {
                meta.name.lexeme.to_owned()
            }
            FnKind::Lambda => String::from("fun"),
        },
        match &meta.params {
            Params::Params(ref params) => params
                .iter()
                .map(|p| p.lexeme.to_owned())
                .collect::<Vec<_>>()
                .join(", "),
            Params::VarArgs(param) => format!("~{}", param.lexeme.to_owned()),
        },
        if matches!(meta.kind, FnKind::Lambda) {
            format!(" /* {} */ ", meta.name.lexeme)
        } else {
            String::from(" ")
        },
        stmt_to_code(
            &Stmt {
                kind: StmtKind::Block(meta.body.clone()),
                span: Span::builtin()
            },
            depth + 1
        )
    )
}

pub(crate) fn stmt_to_code(statement: &Stmt, depth: usize) -> String {
    match &statement.kind {
        StmtKind::ExprStmt(expr) => format!("{};", expr_to_code(expr, depth)),
        StmtKind::Print(expr) => format!("print {};", expr_to_code(expr, depth)),
        StmtKind::Var(vars) => vars
            .iter()
            .enumerate()
            .map(|(i, v)| {
                format!(
                    "{}{} {} = {};",
                    if i == 0 {
                        String::new()
                    } else {
                        " ".repeat((depth - 1) * 4)
                    },
                    if v.node.is_const { "const" } else { "var" },
                    v.node.name.lexeme,
                    v.node
                        .initializer
                        .as_ref()
                        .map(|e| expr_to_code(e, depth))
                        .unwrap_or_else(|| String::from("nil"))
                )
            })
            .collect::<Vec<_>>()
            .join("\n"),
        StmtKind::Block(stmts) => {
            let mut buf = vec!["{".to_owned()];
            let indent = " ".repeat(depth.max(1) * 4);
            for stmt in stmts {
                buf.push(format!("{}{}", indent, stmt_to_code(stmt, depth + 1)));
            }
            buf.push(format!("{}}}", " ".repeat((depth.saturating_sub(1)) * 4)));
            buf.join("\n")
        }
        StmtKind::For(r#for) => format!(
            "for ({} {}; {}) {}",
            r#for
                .initializer
                .as_ref()
                .map(|init| stmt_to_code(init, 0))
                .unwrap_or_else(|| String::from(";")),
            r#for
                .condition
                .as_ref()
                .map(|cond| expr_to_code(cond, 0))
                .unwrap_or_else(String::new),
            r#for
                .increment
                .as_ref()
                .map(|inc| expr_to_code(inc, 0))
                .unwrap_or_else(String::new),
            stmt_to_code(&r#for.body, depth),
        ),
        StmtKind::ForEach(r#for) => format!(
            " for ({} in {}) {}",
            r#for.variable.lexeme,
            expr_to_code(&r#for.iterator, depth),
            stmt_to_code(&r#for.body, depth)
        ),
        StmtKind::While(condition, body) => format!(
            "while ({}) {}",
            expr_to_code(condition, depth),
            stmt_to_code(body, depth)
        ),
        StmtKind::If(r#if) => {
            let mut buf = format!(
                "if ({}) {}",
                expr_to_code(&r#if.condition, depth),
                stmt_to_code(&r#if.then, depth)
            );
            if let Some(otherwise) = &r#if.otherwise {
                buf.push_str(&format!(" else {}", stmt_to_code(otherwise, depth)));
            }
            buf
        }
        StmtKind::Return(value) => format!(
            "return {};",
            value
                .as_ref()
                .map(|v| expr_to_code(v, depth))
                .unwrap_or_else(|| String::from("nil"))
        ),
        StmtKind::Break => String::from("break;"),
        StmtKind::Continue => String::from("continue;"),
        StmtKind::Function(info) => fn_to_code(info, depth),
        StmtKind::Class(info) => {
            let mut buf = format!("class {}", info.name.lexeme);
            let indent = " ".repeat((depth + 1) * 4);
            if let Some(superclass) = &info.superclass {
                buf.push_str(&format!(" < {}", expr_to_code(superclass, depth)));
            }
            buf.push_str(" {\n");
            for method in &info.methods {
                buf.push_str(&format!("{}{}\n", indent, fn_to_code(method, depth + 1)));
            }
            buf.push_str("}\n");
            buf
        }
        StmtKind::Import(import) => {
            let mut buf = format!(
                "import {}",
                import
                    .package
                    .iter()
                    .map(|t| t.lexeme)
                    .collect::<Vec<_>>()
                    .join("."),
            );

            if !import.bindings.is_empty() {
                buf.push_str(&format!(
                    "{{ {} }}",
                    import
                        .bindings
                        .iter()
                        .map(|b| format!("{} as {}", b.export.lexeme, b.bind_to.lexeme))
                        .collect::<Vec<_>>()
                        .join(", ")
                ))
            }

            if let Some(alias) = import.alias.as_ref() {
                buf.push_str(&format!(" as {}", alias.lexeme));
            }

            buf.push_str(";\n");
            buf
        }
        StmtKind::_Empty => String::new(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::context::Context;
    use crate::parser::Parser;
    use crate::pipeline::Pipeline;
    use crate::scanner::Scanner;
    use crate::source_info::SourceInfo;

    #[test]
    fn test_serialization() {
        let ctx = Context::default().into_shared();
        let id = ctx
            .write_shared()
            .add_module(SourceInfo::test_info("1 + 2;"));

        let ast = Pipeline::with_pass(Scanner::new(ctx.clone()))
            .add_pass(Parser::new(ctx))
            .execute(id)
            .unwrap();
        assert_eq!(
            ast_to_str(&ast, None),
            "\
╭──────────────────╮\n\
│ Script: <stream> │\n\
<program>──────────╯\n\
╰─Stmt::ExprStmt\n  \
╰─Expr::Binary\n    \
├─left: Expr::Literal\n    \
│ ╰─value: Integer(1)\n    \
├─operator: Add\n    \
╰─right: Expr::Literal\n      \
╰─value: Integer(2)"
        );
    }
}
