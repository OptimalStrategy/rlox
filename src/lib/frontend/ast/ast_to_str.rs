use crate::{ast::*, SourceInfo};

enum TreeIndent {
    Trunk,
    Branch,
}

pub fn generate_message_box(rows: &[String], last_row_caption: Option<&str>) -> String {
    let longest = rows
        .iter()
        .map(|r| r.len())
        .max()
        .expect("Expected at least one row.");
    let longest = last_row_caption.map(|r| r.len()).unwrap_or(0).max(longest);

    let mut result =
        vec![LEFT_UPPER_CORNER.to_owned() + &NODE.repeat(longest + 2) + RIGHT_UPPER_CORNER];

    for row in rows {
        let spaces = " ".repeat(longest - row.len());
        result.push(format!("{} {}{} {}", TRUNK, row, spaces, TRUNK));
    }

    if let Some(caption) = last_row_caption {
        result.push(format!(
            "<{}>{}{}",
            caption,
            NODE.repeat(longest - caption.len() + 1),
            RIGHT_BOTTOM_CORNER
        ));
    } else {
        result
            .push(LEFT_BOTTOM_CORNER.to_owned() + &NODE.repeat(longest + 2) + RIGHT_BOTTOM_CORNER);
    }

    result.join("\n")
}

pub fn ast_to_str(ast: &AST<'_>, info: Option<&SourceInfo>) -> String {
    let rows = info
        .map(|i| {
            vec![
                format!("Module: {}", i.module),
                format!("Script: {}", i.filename),
                format!("Hash:   {:x}", i.source_hash),
            ]
        })
        .unwrap_or_else(|| vec!["Script: <stream>".to_owned()]);

    let root = generate_message_box(&rows, Some("program"));
    format(&root, ast.body.iter().map(serialize).collect())
}

fn serialize_list_generic<T>(
    child: &str,
    collection: impl Iterator<Item = T>,
    f: impl Fn(&T) -> String,
) -> String {
    let mut collection = collection.peekable();
    let symbol = if collection.peek().is_none() {
        "✕"
    } else {
        "↓"
    };
    format(
        &format!("{}={}", child, symbol),
        collection.map(|ref item| f(item)).collect(),
    )
}

fn serialize_list(child: &str, statements: &[Stmt<'_>]) -> String {
    serialize_list_generic(child, statements.iter(), |stmt| serialize(stmt))
}

fn serialize_expr_list(child: &str, expressions: &[Expr<'_>]) -> String {
    serialize_list_generic(child, expressions.iter(), |expr| serialize_expr(expr))
}

fn serialize_string_list(child: &str, strings: Vec<String>) -> String {
    serialize_list_generic(child, strings.iter(), |s| s.to_string())
}

pub(crate) fn serialize_expr(expr: &Expr<'_>) -> String {
    match &expr.kind {
        ExprKind::Binary(op, left, right) => format(
            "Expr::Binary",
            vec![
                format!("left: {}", serialize_expr(left)),
                format!("operator: {:?}", op.node),
                format!("right: {}", serialize_expr(right)),
            ],
        ),
        ExprKind::Unary(op, operand) => format(
            "Expr::Unary",
            vec![
                format!("operator: {:?}", op),
                format!("operand: {}", serialize_expr(operand)),
            ],
        ),
        ExprKind::ArgForwarding(expr) => format(
            "Expr::ArgForwarding",
            vec![format!("operand: {}", serialize_expr(expr))],
        ),
        ExprKind::Lit(lit) => format("Expr::Literal", vec![format!("value: {:?}", lit.value)]),
        ExprKind::Comma(left, right) => format(
            "Expr::Comma",
            vec![
                format!("left: {}", serialize_expr(left)),
                format!("right: {}", serialize_expr(right)),
            ],
        ),
        ExprKind::Call {
            callee,
            args,
            tco,
            fwd,
        } => format(
            "Expr::Call",
            vec![
                format!("callee: {}", serialize_expr(callee)),
                format!("tco_eligible: {}", tco),
                format!("has_fwd_args: {}", fwd),
                serialize_expr_list("args", &args.node),
            ],
        ),
        ExprKind::GetAttr(get) => format(
            "Expr::GetAttr",
            vec![
                format!("obj: {}", serialize_expr(&get.node)),
                format!("attr: {}", get.field.lexeme),
            ],
        ),
        ExprKind::SetAttr(set) => format(
            "Expr::SetAttr",
            vec![
                format!("obj: {}", serialize_expr(&set.node)),
                format!("attr: {}", set.field.lexeme),
                format!("value: {}", serialize_expr(&set.value)),
            ],
        ),
        ExprKind::GetItem(obj, item) => format(
            "Expr::GetItem",
            vec![
                format!("obj: {}", serialize_expr(obj)),
                format!("item: {}", serialize_expr(item)),
            ],
        ),
        ExprKind::SetItem(set) => format(
            "Expr::SetItem",
            vec![
                format!("obj: {}", serialize_expr(&set.node)),
                format!("key: {}", serialize_expr(&set.key)),
                format!("value: {}", serialize_expr(&set.value)),
            ],
        ),
        ExprKind::InterStr(inter) => format(
            "Expr::InterStr",
            vec![serialize_list_generic(
                "fragments",
                inter.fragments.iter(),
                |frag| match frag {
                    InterFrag::Str(s) => {
                        format("Expr::StrFrag", vec![format!("value: {:?}", s.value)])
                    }
                    InterFrag::Expr(e) => format(
                        "Expr::ExprFrag",
                        vec![format!("value: {}", serialize_expr(e))],
                    ),
                },
            )],
        ),
        ExprKind::Array(exprs) => format(
            "Expr::Array",
            vec![serialize_expr_list("initializer", exprs)],
        ),
        ExprKind::Dict(pairs) => format(
            "Expr::Dict",
            vec![serialize_list_generic(
                "initializers",
                pairs.iter(),
                |pair| {
                    format(
                        "Expr::KV_Pair",
                        vec![
                            format!("key: {}", serialize_expr(&pair.0)),
                            format!("value: {}", serialize_expr(&pair.1)),
                        ],
                    )
                },
            )],
        ),
        ExprKind::This => "Expr::This".to_string(),
        ExprKind::Super(sup) => format(
            "Expr::Super",
            vec![format!("method:: {}", sup.method.lexeme)],
        ),
        ExprKind::Ternary(ternary) => format(
            "ExprKind::Ternary",
            vec![
                format!("predicate: {}", serialize_expr(&ternary.predicate)),
                format!("then: {}", serialize_expr(&ternary.then)),
                format!("otherwise: {}", serialize_expr(&ternary.otherwise)),
            ],
        ),
        ExprKind::Match(r#match) => format(
            "Expr::Match",
            vec![
                format!("value: {}", serialize_expr(&r#match.value)),
                serialize_list_generic("arms", r#match.arms.iter(), |arm| {
                    format(
                        "Expr::Arm",
                        vec![
                            format!("pat: {}", serialize_expr(&arm.pat)),
                            format!("is_placeholder: {}", arm.is_placeholder),
                            format!("pat_kind: {:?}", arm.pat_kind),
                            format!("body: {}", serialize(&arm.body)),
                        ],
                    )
                }),
                format!(
                    "bound_var: {}",
                    r#match
                        .bound_var
                        .as_ref()
                        .map(|v| format!("'{}'", v.lexeme))
                        .unwrap_or_else(|| "-----".to_owned())
                ),
            ],
        ),
        ExprKind::Variable(name) => {
            format("Expr::Variable", vec![format!("name: '{}'", name.lexeme)])
        }
        ExprKind::PrefixIncDec(inc) => format(
            "Expr::PrefixIncDec",
            vec![
                format!("name: '{}'", inc.name.lexeme),
                format!("kind: {:?}", inc.kind),
            ],
        ),
        ExprKind::PostfixIncDec(inc) => format(
            "Expr::PostfixIncDec",
            vec![
                format!("name: '{}'", inc.name.lexeme),
                format!("kind: {:?}", inc.kind),
            ],
        ),
        ExprKind::Assignment(asn) => format(
            "Expr::Assignment",
            vec![
                format!("name: '{}'", asn.name.lexeme),
                format!("value: {}", serialize_expr(&asn.value)),
            ],
        ),
        ExprKind::Lambda(info) => serialize_fn("Expr::Lambda", info),
        ExprKind::Grouping(expr) => format(
            "Expr::Grouping",
            vec![format!("expr: {}", serialize_expr(expr))],
        ),
    }
}

fn serialize_fn(node: &str, meta: &FnInfo) -> String {
    format(
        node,
        vec![
            format!("name: `{}`", meta.name.lexeme),
            format!("kind: {:?}", meta.kind),
            serialize_string_list(
                "params",
                match &meta.params {
                    Params::Params(ref params) => params
                        .iter()
                        .map(|p| p.lexeme.to_owned())
                        .collect::<Vec<_>>(),
                    Params::VarArgs(param) => vec![format!("{} (vararg)", param.lexeme)],
                },
            ),
            serialize_list("body", &meta.body),
        ],
    )
}

pub(crate) fn serialize(statement: &Stmt) -> String {
    match &statement.kind {
        StmtKind::ExprStmt(expr) => format("Stmt::ExprStmt", vec![serialize_expr(expr)]),
        StmtKind::Print(expr) => format("Stmt::Print", vec![serialize_expr(expr)]),
        StmtKind::Var(vars) => format(
            "Stmt::Var",
            vec![serialize_list_generic("declarations", vars.iter(), |var| {
                format(
                    "Stmt::VarDecl",
                    vec![
                        format!("name: {}", var.node.name.lexeme),
                        format!(
                            "initializer: {}",
                            var.node
                                .initializer
                                .as_ref()
                                .map(serialize_expr)
                                .unwrap_or_else(|| "nil".to_string())
                        ),
                    ],
                )
            })],
        ),
        StmtKind::Block(stmts) => format("Stmt::Block", vec![serialize_list("statements", stmts)]),
        StmtKind::For(r#for) => format(
            "Stmt::For",
            vec![
                format!(
                    "initializer: `{}`",
                    r#for
                        .initializer
                        .as_ref()
                        .map(serialize)
                        .unwrap_or_else(|| "-----".to_owned())
                ),
                format!(
                    "condition: {}",
                    r#for
                        .condition
                        .as_ref()
                        .map(serialize_expr)
                        .unwrap_or_else(|| "True".to_owned())
                ),
                format!(
                    "increment: {}",
                    r#for
                        .increment
                        .as_ref()
                        .map(serialize_expr)
                        .unwrap_or_else(|| "-----".to_owned())
                ),
                format!("body: {}", serialize(&r#for.body)),
            ],
        ),
        StmtKind::ForEach(r#for) => format(
            "Stmt::ForEach",
            vec![
                format!("variable: `{}`", r#for.variable.lexeme),
                format!("iterator: {}", serialize_expr(&r#for.iterator)),
                format!("body: {}", serialize(&r#for.body)),
            ],
        ),
        StmtKind::While(condition, body) => format(
            "Stmt::While",
            vec![
                format!("condition: {}", serialize_expr(condition)),
                format!("body: {}", serialize(body)),
            ],
        ),
        StmtKind::If(r#if) => format(
            "StmtKind::If",
            vec![
                format!("condition: {}", serialize_expr(&r#if.condition)),
                format!("then: {}", serialize(&r#if.then)),
                format!(
                    "otherwise: {}",
                    r#if.otherwise
                        .as_ref()
                        .map(serialize)
                        .unwrap_or_else(|| "-----".to_owned())
                ),
            ],
        ),
        StmtKind::Return(value) => format(
            "Stmt::Return",
            vec![format!(
                "value: {}",
                value
                    .as_ref()
                    .map(|expr| serialize_expr(expr))
                    .unwrap_or_else(|| "-----".to_owned())
            )],
        ),
        StmtKind::Break => format("Stmt::Break", vec![]),
        StmtKind::Continue => format("Stmt::Continue", vec![]),
        StmtKind::Function(info) => serialize_fn("Stmt::FnDecl", info),
        StmtKind::Class(info) => format(
            "Stmt::Class",
            vec![
                format!("name: `{}`", info.name.lexeme),
                format!(
                    "superclass: {}",
                    info.superclass
                        .as_ref()
                        .map(|s| format!("`{}`", serialize_expr(s)))
                        .unwrap_or_else(|| "-----".to_owned())
                ),
                serialize_string_list(
                    "methods",
                    info.methods
                        .iter()
                        .map(|meth| serialize_fn("Stmt::FnDecl", meth))
                        .collect(),
                ),
            ],
        ),
        StmtKind::Import(import) => format(
            "Stmt::Import",
            vec![
                format!(
                    "package: `{}`",
                    import
                        .package
                        .iter()
                        .map(|t| t.lexeme)
                        .collect::<Vec<_>>()
                        .join(".")
                ),
                format!(
                    "alias: {}",
                    import
                        .alias
                        .as_ref()
                        .map(|s| format!("`{}`", s.lexeme))
                        .unwrap_or_else(|| "-----".to_owned())
                ),
                format!("path: `{}`", import.path.to_string_lossy()),
                format!("module: {:?}", import.module),
                format!("import_from_root: {}", import.import_from_root),
                serialize_list_generic("bindings", import.bindings.iter(), |binding| {
                    format(
                        "ImportBinding",
                        vec![
                            format!("export: {}", binding.export.lexeme),
                            format!("bind_to: {}", binding.bind_to.lexeme),
                        ],
                    )
                }),
            ],
        ),
        StmtKind::_Empty => String::from("{ stmt: Eliminated }"),
    }
}

fn format(tree: &str, children: Vec<String>) -> String {
    let mut new_tree = vec![tree.to_string()];

    if !children.is_empty() {
        for child in &children[0..children.len() - 1] {
            new_tree.push(indent(child, TreeIndent::Trunk));
        }

        new_tree.push(indent(children.last().unwrap(), TreeIndent::Branch));
    }

    new_tree.join("\n")
}

fn indent(tree: &str, kind: TreeIndent) -> String {
    let tree = tree.split('\n').collect::<Vec<&str>>();
    let mut new_tree = vec![];

    // Handle the root first
    let wood = if kind.is_trunk() {
        format!("{}{}", BRANCH, NODE)
    } else {
        format!("{}{}", FINAL_LEAF, NODE)
    };
    new_tree.push(format!("{}{}", wood, tree[0]));

    if tree.len() > 1 {
        for child in tree[1..tree.len()].iter() {
            let wood = if kind.is_trunk() {
                format!("{}{}", TRUNK, INDENT)
            } else {
                INDENT.repeat(2)
            };

            new_tree.push(format!("{}{}", wood, child));
        }
    }

    new_tree.join("\n")
}

impl TreeIndent {
    pub fn is_trunk(&self) -> bool {
        match &self {
            TreeIndent::Trunk => true,
            TreeIndent::Branch => false,
        }
    }
}

pub static NODE: &str = "─";
pub static TRUNK: &str = "│";
pub static BRANCH: &str = "├";
pub static INDENT: &str = " ";
pub static FINAL_LEAF: &str = "╰";
pub static LEFT_UPPER_CORNER: &str = "╭";
pub static LEFT_BOTTOM_CORNER: &str = FINAL_LEAF;
pub static RIGHT_UPPER_CORNER: &str = "╮";
pub static RIGHT_BOTTOM_CORNER: &str = "╯";

#[cfg(test)]
mod tests {
    use super::*;
    use crate::context::Context;
    use crate::parser::Parser;
    use crate::pipeline::Pipeline;
    use crate::scanner::Scanner;
    use crate::source_info::SourceInfo;

    #[test]
    fn test_serialization() {
        let ctx = Context::default().into_shared();
        let id = ctx
            .write_shared()
            .add_module(SourceInfo::test_info("1 + 2;"));

        let ast = Pipeline::with_pass(Scanner::new(ctx.clone()))
            .add_pass(Parser::new(ctx))
            .execute(id)
            .unwrap();
        assert_eq!(
            ast_to_str(&ast, None),
            "\
            ╭──────────────────╮\n\
            │ Script: <stream> │\n\
            <program>──────────╯\n\
             ╰─Stmt::ExprStmt\n  \
             ╰─Expr::Binary\n    \
             ├─left: Expr::Literal\n    \
             │ ╰─value: Integer(1)\n    \
             ├─operator: Add\n    \
             ╰─right: Expr::Literal\n      \
             ╰─value: Integer(2)"
        );
    }
}
