pub mod ast_to_code;
pub mod ast_to_str;

use std::path::PathBuf;

use crate::token::{Span, Spanned, Token};
use crate::{module::ModuleId, types::*};

pub use crate::{
    ast::ast_to_str::{ast_to_str, generate_message_box},
    vm::object::Value,
};

pub type Ptr<T> = Box<T>;
pub type ExprPtr<'a> = Ptr<Expr<'a>>;
pub type StmtPtr<'a> = Ptr<Stmt<'a>>;
pub type BinOp = Spanned<BinOpKind>;
pub type UnOp = Spanned<UnOpKind>;
pub type Args<'a> = Spanned<Vec<Expr<'a>>>;

#[derive(Debug)]
pub struct AST<'a> {
    pub body: Vec<Stmt<'a>>,
    pub module: ModuleId,
}

impl<'a> AST<'a> {
    pub fn new(body: Vec<Stmt<'a>>, module: ModuleId) -> Self {
        Self { body, module }
    }
}

/// The possible binary operators.
#[derive(Clone, PartialEq, Debug, Copy)]
pub enum BinOpKind {
    /// The `+` operator (addition)
    Add,
    /// The `-` operator (subtraction)
    Sub,
    /// The `*` operator (multiplication)
    Mul,
    /// The `**` operator (exponentiation)
    Pow,
    /// The `/` operator (division)
    Div,
    /// The `%` operator (modulus)
    Rem,
    /// The `and` operator (logical and)
    And,
    /// The `or` operator (logical or)
    Or,
    /// The `==` operator (equality)
    Eq,
    /// The `!=` operator (not equal to)
    Ne,
    /// The `<` operator (less than)
    Lt,
    /// The `<=` operator (less than or equal to)
    Le,
    /// The `>=` operator (greater than or equal to)
    Ge,
    /// The `>` operator (greater than)
    Gt,
}

/// The possible unary operators.
#[derive(Clone, PartialEq, Debug, Copy)]
pub enum UnOpKind {
    /// The `!` operator for logical inversion
    Not,
    /// The `-` operator for negation
    Neg,
    /// The `+` unary operator (I regret this decision)
    Plus,
}

/// Represents a primitive Lox literal.
#[derive(Debug, Clone)]
pub enum LitValue {
    /// A 128-bit integer.
    Integer(LoxInt),
    /// A 64-bit float.
    Float(LoxFloat),
    /// A boolean.
    Bool(bool),
    /// A `nil`.
    Nil,
    /// A utf-8 string.
    Str(String),
}

impl LitValue {
    pub fn into_value(self) -> Value {
        match self {
            LitValue::Integer(v) => Value::from(v),
            LitValue::Float(v) => Value::from(v),
            LitValue::Bool(v) => Value::from(v),
            LitValue::Nil => Value::Nil,
            LitValue::Str(_) => panic!("Strings must be heap allocated"),
        }
    }
    pub fn into_string(self) -> String {
        match self {
            LitValue::Str(v) => v,
            _ => panic!("Cannot convert the literal into a string: {:?}", self),
        }
    }
}

impl PartialEq for LitValue {
    fn eq(&self, other: &LitValue) -> bool {
        match (self, other) {
            (LitValue::Integer(l), LitValue::Integer(r)) => l == r,
            (LitValue::Integer(l), LitValue::Float(r)) => (*l as crate::types::LoxFloat) == *r,
            (LitValue::Float(l), LitValue::Integer(r)) => *l == (*r as crate::types::LoxFloat),
            (LitValue::Float(l), LitValue::Float(r)) => l == r,
            (LitValue::Bool(l), LitValue::Bool(r)) => l == r,
            (LitValue::Nil, LitValue::Nil) => true,
            (LitValue::Str(l), LitValue::Str(r)) => l == r,
            _ => false,
        }
    }
}

/// Contains a literal and its token.
#[derive(Debug, Clone)]
pub struct Lit<'a> {
    /// The token that produced the literal.
    pub token: Token<'a>,
    /// A literal value.
    pub value: LitValue,
}

/// An attribute access expression.
#[derive(Debug, Clone)]
pub struct GetAttr<'a> {
    /// The expression to access the field on, e.g. a class instance.
    pub node: Expr<'a>,
    /// The field to access.
    pub field: Token<'a>,
}

/// An attribute assignment expression.
#[derive(Debug, Clone)]
pub struct SettAttr<'a> {
    /// The object to assign the field on.
    pub node: Expr<'a>,
    /// The field to assign to.
    pub field: Token<'a>,
    /// The value to assign.
    pub value: Expr<'a>,
}

#[derive(Debug, Clone)]
pub struct SetItem<'a> {
    pub node: Expr<'a>,
    pub key: Expr<'a>,
    pub value: Expr<'a>,
}

/// A fragment of an interpolated string.
#[derive(Debug, Clone)]
pub enum InterFrag<'a> {
    /// A string literal fragment.
    Str(Lit<'a>),
    /// An expression fragment.
    Expr(Expr<'a>),
}

/// An interpolated string literal.
#[derive(Debug, Clone)]
pub struct InterStr<'a> {
    /// The fragments of an interpolated string.
    pub fragments: Vec<InterFrag<'a>>,
}

/// A `super` expression used to access methods defined on the parent class.,
/// e.g. `super.method()`
#[derive(Debug, Clone)]
pub struct Super<'a> {
    /// The superclass method to access.
    pub method: Token<'a>,
    /// The span of the `super` keyword.
    pub span: Span,
}

/// A ternary operator expression.
#[derive(Debug, Clone)]
pub struct Ternary<'a> {
    /// The ternary  operator condition.
    pub predicate: Expr<'a>,
    /// The code to execute if the condition is true.
    pub then: Expr<'a>,
    /// The code to execute if the condition is false.
    pub otherwise: Expr<'a>,
}

/// The kind of a match pattern.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum MatchPatKind {
    /// Match by comparing the value to the pattern
    Equality,
    /// Match by comparing the type of the value to the pattern
    TypeEquality,
}

/// An arm of a match expression.
#[derive(Debug, Clone)]
pub struct Arm<'a> {
    /// The pattern value, e.g. `2` or `class Ok`.
    pub pat: Expr<'a>,
    /// The kind of the pattern.
    pub pat_kind: MatchPatKind,
    /// The code to execute if the pattern has been matched on.
    pub body: Stmt<'a>,
    /// Set to true if the arm is the any arm, i.e. `_ => ...`.
    /// The placeholder arm is guaranteed to be the last in the match.
    pub is_placeholder: bool,
}

/// A match expression.
#[derive(Debug, Clone)]
pub struct Match<'a> {
    /// The value to match on.
    pub value: Expr<'a>,
    /// The match arms.
    pub arms: Vec<Arm<'a>>,
    /// An optional variable bound to the expression's value.
    pub bound_var: Option<Token<'a>>,
}

/// The kind of a prefix- or postfix- increment or decrement.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum IncDecKind {
    /// A `++`
    Increment,
    /// A `--`
    Decrement,
}

/// An increment or decrement expression.
/// May operate only on local variables.
#[derive(Debug, Clone)]
pub struct IncDec<'a> {
    pub name: Token<'a>,
    pub kind: IncDecKind,
}

/// A variable assignment expression.
#[derive(Debug, Clone)]
pub struct Assignment<'a> {
    /// The name of the variable to assign to.
    pub name: Token<'a>,
    /// The value to assign.
    pub value: Expr<'a>,
}

/// The possible kinds of parameters a function can take.
#[derive(Debug, Clone)]
pub enum Params<'a> {
    /// A list of regular parameters.
    /// Example: `fun f(a, b, c) { print a, b, c; }`
    Params(Vec<Token<'a>>),
    /// A parameter that represents a variable number of arguments
    /// that are packed into an array with the same name at runtime.
    /// Example: `fun (~args) { print args; }`
    VarArgs(Token<'a>),
}

/// The possible kinds of a function.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum FnKind {
    /// A regular function
    /// Example: `fun test() {}`
    Function,
    /// An anonymous function (lambda).
    /// Example: `fun(x) => x`
    Lambda,
    /// A class method.
    /// Example: `method() { return this; }`
    Method,
    /// A class getter.
    /// Example: `getter { return 42; }`
    Getter,
    /// A static method.
    /// Example: `class static() { return 3.141592; }`
    Static,
}

/// A function, lambda, method, or getter declaration.
#[derive(Debug, Clone)]
pub struct FnInfo<'a> {
    /// The name of the function.
    /// Contains the lambda keyword or an auto-generated name for lambdas.
    /// (names are generated during the compilation pass).
    pub name: Token<'a>,
    /// The parameters this function defines.
    pub params: Params<'a>,
    /// The body of the function.
    pub body: Vec<Stmt<'a>>,
    /// The kind of the function.
    pub kind: FnKind,
}

impl<'a> FnInfo<'a> {
    #[inline]
    pub fn is_method(&self) -> bool {
        match self.kind {
            FnKind::Function | FnKind::Lambda => false,
            FnKind::Method | FnKind::Getter | FnKind::Static => true,
        }
    }
}

/// The kind of an expression.
#[derive(Debug, Clone)]
pub enum ExprKind<'a> {
    /// A binary operator, e.g. `a + b`
    Binary(BinOp, ExprPtr<'a>, ExprPtr<'a>),
    /// A unary operator, e.g. `!a`
    Unary(UnOp, ExprPtr<'a>),
    /// A literal, e.g. a number, a string, an array
    Lit(Ptr<Lit<'a>>),
    /// A C++-like comma operator
    Comma(ExprPtr<'a>, ExprPtr<'a>),
    /// A function call or class constructor.
    Call {
        callee: ExprPtr<'a>,
        args: Args<'a>,
        tco: bool,
        // Whether the call has forwarded arguments
        fwd: bool,
    },
    /// An attribute access expression, e.g. `a.b`
    GetAttr(Ptr<GetAttr<'a>>),
    /// An attribute assignment expression, e.g. `a.b = c`
    SetAttr(Ptr<SettAttr<'a>>),
    /// An item access expression, e.g. `a..0` (I regret this decision)
    GetItem(ExprPtr<'a>, ExprPtr<'a>),
    /// An item assignment expression, e.g. `a..0 = 3`
    SetItem(Ptr<SetItem<'a>>),
    /// A utf-8 string that contains one or more interpolation fragments.
    InterStr(InterStr<'a>),
    /// An array literal.
    Array(Vec<Expr<'a>>),
    /// A dictionary literal.
    Dict(Vec<(Expr<'a>, Expr<'a>)>),
    /// A `this` expression inside a method.
    This,
    /// A `super.method` expression inside a method.
    Super(Ptr<Super<'a>>),
    /// A ternary operator expression.
    Ternary(Ptr<Ternary<'a>>),
    /// A match expression.
    Match(Ptr<Match<'a>>),
    /// A variable access expression.
    Variable(Token<'a>),
    /// A prefix increment or decrement
    PrefixIncDec(Ptr<IncDec<'a>>),
    /// A postfix increment or decrement
    PostfixIncDec(Ptr<IncDec<'a>>),
    /// An assignment expression
    Assignment(Ptr<Assignment<'a>>),
    /// An anonymous function expression, e.g. `fun(x) => x * 2; fun() { return 3; }`
    Lambda(Ptr<FnInfo<'a>>),
    /// A grouping expression, e.g. `(a + b)`.
    Grouping(ExprPtr<'a>),
    /// An argument forwarding/spread expression, e.g. `f(...arr)`.
    ArgForwarding(ExprPtr<'a>),
}

/// An expression.
// TODO: directives
#[derive(Debug, Clone)]
pub struct Expr<'a> {
    /// The kind of the expression.
    pub kind: ExprKind<'a>,
    /// The span of the whole expression.
    pub span: Span,
}

/// A variable declaration statement.
#[derive(Debug, Clone)]
pub struct Var<'a> {
    /// The name of the variable being declared.
    pub name: Token<'a>,
    /// The optional initializer of the variable.
    pub initializer: Option<Expr<'a>>,
    /// Whether the variable is a `const` variable.
    pub is_const: bool,
}

/// A for loop statement.
#[derive(Debug, Clone)]
pub struct For<'a> {
    /// The list of the loop initializers.
    pub initializer: Option<Stmt<'a>>,
    /// The loop condition.
    pub condition: Option<Expr<'a>>,
    /// The loop increment (update) expression.
    pub increment: Option<Expr<'a>>,
    /// The body of the loop.
    pub body: Stmt<'a>,
}

/// A foreach loop statement, e.g. `for (a in "abc") {}`
#[derive(Debug, Clone)]
pub struct ForEach<'a> {
    /// The variable to bind the elements to.
    pub variable: Token<'a>,
    /// The iterator to iterate over.
    pub iterator: Expr<'a>,
    /// The loop body.
    pub body: Stmt<'a>,
}

/// An if statement.
#[derive(Debug, Clone)]
pub struct If<'a> {
    /// The if condition.
    pub condition: Expr<'a>,
    /// The code to execute if the condition is true.
    pub then: Stmt<'a>,
    /// The code to execute if the condition is false.
    pub otherwise: Option<Stmt<'a>>,
}

/// A class declaration statement.
#[derive(Debug, Clone)]
pub struct ClsInfo<'a> {
    /// The name of the class.
    pub name: Token<'a>,
    /// The optional superclass.
    pub superclass: Option<Expr<'a>>,
    /// The methods defined on the class.
    pub methods: Vec<FnInfo<'a>>,
}

#[derive(Debug, Clone)]
pub struct ImportBinding<'a> {
    pub export: Token<'a>,
    pub bind_to: Token<'a>,
}

/// An import statement.
#[derive(Debug, Clone)]
pub struct Import<'a> {
    /// The list of the tokens comprising the import, e.g.
    /// `import package.sub.mod;` will produce the tokens `[package, sub, mod]`.
    pub package: Vec<Token<'a>>,
    /// The optional alias to bind the imported module to.
    pub alias: Option<Token<'a>>,
    /// The path to the module file.
    pub path: PathBuf,
    // The ID of the import's module
    pub module: ModuleId,
    /// Whether the import is interpreter-root relative.
    /// A regular import will search in the directory
    /// the current module is stored in. A root-relative import
    /// will search from where the interpreter was invoked.
    pub import_from_root: bool,
    /// The members of the module to bring to the scope.
    pub bindings: Vec<ImportBinding<'a>>,
}

/// A statement kind.
#[derive(Debug, Clone)]
pub enum StmtKind<'a> {
    /// An expression statement.
    ExprStmt(ExprPtr<'a>),
    /// A print statement.
    Print(ExprPtr<'a>),
    /// A one or more variable declarations, e.g. `var a, b = 3, c`.
    /// All variables are guaranteed to be either all const or all non-const.
    Var(Vec<Spanned<Var<'a>>>),
    /// A block statement.
    Block(Vec<Stmt<'a>>),
    /// A for loop statement.
    For(Ptr<For<'a>>),
    /// A foreach loop statement.
    ForEach(Ptr<ForEach<'a>>),
    /// A while loop statement.
    While(ExprPtr<'a>, StmtPtr<'a>),
    /// An if statement.
    If(Ptr<If<'a>>),
    /// A return statement.
    Return(Option<ExprPtr<'a>>),
    /// A break statement.
    Break,
    /// A continue statement.
    Continue,
    /// A function declaration.
    Function(Ptr<FnInfo<'a>>),
    /// A class declaration.
    Class(Ptr<ClsInfo<'a>>),
    /// A module import.
    Import(Ptr<Import<'a>>),
    /// An empty node that compiles to nothing.
    /// Used by the constant folder to remove dead code.
    _Empty,
}

// TODO: directives
/// A Lox statement.
#[derive(Debug, Clone)]
pub struct Stmt<'a> {
    /// The kind of the statement.
    pub kind: StmtKind<'a>,
    /// The span of the whole statement.
    pub span: Span,
}

impl<'a> Stmt<'a> {
    /// Wraps the statement in a block if it is not already one.
    pub fn into_block(self) -> Stmt<'a> {
        match self.kind {
            StmtKind::Block(_) => self,
            _ => Stmt {
                span: self.span,
                kind: StmtKind::Block(vec![self]),
            },
        }
    }
}

impl<'a> ExprKind<'a> {
    pub fn call(callee: ExprPtr<'a>, args: Args<'a>, fwd: bool) -> Self {
        Self::Call {
            callee,
            args,
            tco: false,
            fwd,
        }
    }
}

impl<'a, 'p> IntoIterator for &'p Params<'a> {
    type Item = &'p Token<'a>;
    type IntoIter = ParamsIterator<'a, 'p>;

    fn into_iter(self) -> Self::IntoIter {
        ParamsIterator {
            p: self,
            current: 0,
        }
    }
}

pub struct ParamsIterator<'a, 'p> {
    p: &'p Params<'a>,
    current: usize,
}

impl<'a, 'p> Iterator for ParamsIterator<'a, 'p> {
    type Item = &'p Token<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.p {
            Params::Params(ref p) => {
                self.current += 1;
                p.get(self.current - 1)
            }
            Params::VarArgs(ref p) => {
                if self.current > 0 {
                    None
                } else {
                    self.current += 1;
                    Some(p)
                }
            }
        }
    }
}

impl LitValue {
    pub fn is_truthy(&self) -> bool {
        match self {
            LitValue::Integer(_) | LitValue::Float(_) => !self.is_zero(),
            LitValue::Bool(b) => *b,
            LitValue::Str(s) => !s.is_empty(),
            LitValue::Nil => false,
        }
    }

    pub fn is_numeric(&self) -> bool {
        matches!(self, LitValue::Integer(_) | LitValue::Float(_))
    }

    pub fn is_boolean(&self) -> bool {
        matches!(self, LitValue::Bool(_))
    }

    pub fn is_zero(&self) -> bool {
        match self {
            LitValue::Integer(i) => *i == 0,
            LitValue::Float(f) => *f == 0.,
            _ => false,
        }
    }
}

gen_from_for_enum!(LoxInt, LitValue, Integer);
gen_from_for_enum!(LoxFloat, LitValue, Float);
gen_from_for_enum!(bool, LitValue, Bool);
gen_from_for_enum!(String, LitValue, Str);
