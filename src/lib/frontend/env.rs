use std::{collections::HashMap, hash::Hash};

pub type Scope<K, T> = HashMap<K, T>;

#[derive(Debug)]
pub struct Env<K, T>
where
    K: Hash + Eq,
{
    scopes: Vec<Scope<K, T>>,
}

impl<K, T> Env<K, T>
where
    K: Hash + Eq,
{
    pub fn new() -> Self {
        Self {
            scopes: vec![Scope::new()],
        }
    }

    #[inline(always)]
    pub fn is_global(&self) -> bool {
        self.scopes.len() == 1
    }

    #[inline(always)]
    pub fn push(&mut self) {
        self.scopes.push(Scope::new());
    }

    #[inline(always)]
    pub fn pop(&mut self) -> Option<Scope<K, T>> {
        self.scopes.pop()
    }

    #[inline(always)]
    pub fn get_scope(&self) -> Option<&Scope<K, T>> {
        self.scopes.last()
    }

    #[inline]
    pub fn present_in_current_scope(&self, name: &K) -> bool {
        self.scopes.last().unwrap().contains_key(name)
    }

    pub fn get(&self, name: &K) -> Option<&T> {
        for scope in self.scopes.iter().rev() {
            if scope.contains_key(name) {
                return Some(&scope[name]);
            }
        }
        None
    }

    pub fn get_mut(&mut self, name: &K) -> Option<&mut T> {
        for scope in self.scopes.iter_mut().rev() {
            if scope.contains_key(name) {
                return Some(scope.get_mut(name).unwrap());
            }
        }
        None
    }

    pub fn insert(&mut self, name: K, value: T) -> usize {
        self.scopes.last_mut().unwrap().insert(name, value);
        self.scopes.len() - 1
    }

    pub fn update(&mut self, name: &K, value: T) -> Option<T> {
        for i in (0..self.scopes.len()).rev() {
            let scope: &mut Scope<K, T> = &mut self.scopes[i];
            if scope.contains_key(name) {
                return Some(std::mem::replace(scope.get_mut(name).unwrap(), value));
            }
        }

        None
    }

    #[inline(always)]
    pub fn depth(&self) -> usize {
        self.scopes.len()
    }
}
