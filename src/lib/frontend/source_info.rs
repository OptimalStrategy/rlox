//! This module contains the definition of the `SourceInfo` struct used to describe
//! a module's source code.

/// This struct contains the information about a particular module's source code.
#[derive(Debug)]
pub struct SourceInfo {
    /// The filename of the module, usually a full path to the source file.
    pub filename: String,
    /// The source code of the module.
    pub source: String,
    /// The name of the module, usually the name of the file without the extension.
    pub module: String,
    /// The token hash of the module (see [`Scanner`] for more info).
    pub source_hash: u64,
}

impl SourceInfo {
    /// Creates a new [`SourceInfo`] instance with the given filename, source, and module.
    /// Initializes [`source_hash`] to 0.
    pub fn new(filename: String, source: String, module: String) -> Self {
        Self {
            filename,
            source,
            module,
            source_hash: 0,
        }
    }

    /// Create a new [`SourceInfo`] instance with the given source code.
    /// Initializes the filename and module to `<stream>`.
    pub fn test_info<S: Into<String>>(source: S) -> Self {
        Self {
            filename: "<stream>".into(),
            source: source.into(),
            module: "<stream>".into(),
            source_hash: 0,
        }
    }

    /// Creates a completely empty [`SourceInfo`] instance.
    pub(crate) fn empty() -> Self {
        Self {
            filename: String::new(),
            source: String::new(),
            module: String::new(),
            source_hash: 0,
        }
    }
}
