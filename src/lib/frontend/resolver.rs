//! This module contains the implementation of the Resolver used to resolve variables and imports.
use std::collections::HashSet;
use std::path::PathBuf;

use crate::{
    ast::*, env::Env, err_context::ErrCtx, module::ModuleId, pipeline::IRPass, token::*, LoxError,
    SharedCtx,
};

/// The kind of the loop currently being resolved.
#[derive(Debug, Clone, Copy, PartialEq)]
enum LoopKind {
    None,
    While,
    For,
    ForEach,
}

/// The kind of the scope currently being resolved.
#[derive(Debug, Clone, Copy, PartialEq)]
enum ScopeKind {
    /// The global scope.
    Global,
    /// A local scope like a block.
    Local,
    /// A function scope.
    Function,
    /// An initializer `init` function.
    Initializer,
    /// A static method scope.
    AssocFunction,
    /// A method scope.
    Method,
}

/// The kind of the name currently being resolved.
#[derive(Debug, Clone, Copy, PartialEq)]
enum NameKind {
    /// A mutable variable.
    Var,
    /// A constant variable.
    Const,
    /// A function declaration.
    Func,
    /// A class declaration
    Class,
    /// An import.
    Import,
}

/// The information about a variable's usage.
#[derive(Debug, Clone)]
struct VarUsage {
    /// Whether the variable has been defined.
    pub defined: bool,
    /// Whether the variable has been assigned after its declaration.
    pub assigned: bool,
    /// Whether the variable has been used.
    pub used: bool,
    /// Whether the variable is a match statement alias.
    pub is_match_var: bool,
    /// The `NameKind` of the variable.
    pub kind: NameKind,
    /// The span of the variable; used for error reporting.
    pub span: Span,
}

impl VarUsage {
    /// Returns true if the variable is a `var` or `const` variable.
    pub fn is_var(&self) -> bool {
        matches!(self.kind, NameKind::Var | NameKind::Const)
    }

    /// Returns true if variable is a `const`.
    pub fn is_const(&self) -> bool {
        matches!(self.kind, NameKind::Const)
    }

    /// Returns true if the variable is an import.
    pub fn is_import(&self) -> bool {
        matches!(self.kind, NameKind::Import)
    }
}

/// A variable and import resolver. Checks for unused variables, duplicated, declarations,
/// const variable constraints, and queues imports for compilation.
#[derive(Debug)]
pub struct Resolver<'a> {
    /// A reference to the compilation and execution context.
    ctx: SharedCtx,
    /// The set of builtin types' names.
    builtin_names: HashSet<String>,
    /// An environment mapping variables to their usage.
    env: Env<&'a str, VarUsage>,
    /// Whether a subclass is currently being resolved.
    is_in_subclass: bool,
    /// Whether a class is currently being resolved.
    is_in_class: bool,
    /// The kind of the loop being resolved.
    loop_kind: LoopKind,
    /// THe kind of the scope being resolved.
    scope_kind: ScopeKind,
    /// The module being resovled.
    module: ModuleId,
}

impl<'a> Resolver<'a> {
    /// Creates a new [`Resolver`] instance using the given [`SharedCtx`].
    pub fn new(ctx: SharedCtx) -> Self {
        let builtin_names = ctx
            .read_shared()
            .builtins()
            .name_to_hash()
            .keys()
            .cloned()
            .collect();
        Self {
            ctx,
            builtin_names,
            env: Env::new(),
            is_in_class: false,
            is_in_subclass: false,
            loop_kind: LoopKind::None,
            scope_kind: ScopeKind::Global,
            module: ModuleId::anon(),
        }
    }

    /// Resolves the given ast.
    pub fn resolve(mut self, ast: &mut AST<'a>) -> Result<(), ErrCtx> {
        self.module = ast.module;
        let mut ex = ErrCtx::with_local_module(self.module);

        for stmt in &mut ast.body {
            self.resolve_stmt(stmt, &mut ex);
        }
        self.check_variable_usage(&mut ex);

        if ex.had_error {
            Err(ex)
        } else {
            // Move the warnings (if any) to the shared error context
            self.ctx.write_shared().ex.extend_from_another_ex(ex);
            Ok(())
        }
    }

    /// Resolves the given statement. Writes errors and warnings, if any, to `ex`.
    fn resolve_stmt(&mut self, stmt: &mut Stmt<'a>, ex: &mut ErrCtx) {
        match &mut stmt.kind {
            StmtKind::ExprStmt(expr) => self.resolve_expr(expr, ex),
            StmtKind::Print(expr) => self.resolve_expr(expr, ex),
            StmtKind::Var(vars) => {
                for var in vars {
                    // Declare the variable first without defining it.
                    // This is needed to detect referencing variable in its own initializer.
                    self.declare(
                        &var.node.name,
                        if var.node.is_const {
                            NameKind::Const
                        } else {
                            NameKind::Var
                        },
                        ex,
                    );

                    // If the variable has an initializer, resolve it.
                    if let Some(ref mut init) = var.node.initializer {
                        self.resolve_expr(init, ex);
                        self.assign(&var.node.name, ex);
                    // If the variable is `const` and doesn't have an initializer, return an error.
                    } else if var.node.is_const {
                        ex.record(LoxError::resolution(
                            format!(
                                "Constant variable `{}` must be assigned a value",
                                var.node.name.lexeme
                            ),
                            var.node.name.span,
                        ))
                    }

                    // Define the variable after it has been resolved.
                    self.define(&var.node.name, ex);
                }
            }
            StmtKind::Block(stmts) => {
                let prev_scope = self.scope_kind;
                if self.scope_kind == ScopeKind::Global {
                    self.scope_kind = ScopeKind::Local;
                }
                self.push();
                self.resolve_block(stmts, ex);
                self.pop(ex);
                self.scope_kind = prev_scope;
            }
            StmtKind::For(r#for) => {
                self.push();
                if let Some(ref mut init) = r#for.initializer {
                    self.resolve_stmt(init, ex);
                }

                let prev_loop = self.loop_kind;
                self.loop_kind = LoopKind::For;
                if let Some(ref mut cond) = r#for.condition {
                    self.resolve_expr(cond, ex);
                }
                if let Some(ref mut inc) = r#for.increment {
                    self.resolve_expr(inc, ex);
                }
                self.resolve_stmt(&mut r#for.body, ex);
                self.loop_kind = prev_loop;
                self.pop(ex);
            }
            StmtKind::ForEach(r#for) => {
                self.resolve_expr(&mut r#for.iterator, ex);
                self.declare(&r#for.variable, NameKind::Var, ex);
                self.assign(&r#for.variable, ex);
                let prev_loop = self.loop_kind;
                self.loop_kind = LoopKind::ForEach;
                self.resolve_stmt(&mut r#for.body, ex);
                self.loop_kind = prev_loop;
            }
            StmtKind::While(condition, body) => {
                self.resolve_expr(condition, ex);
                let prev_loop = self.loop_kind;
                self.loop_kind = LoopKind::While;
                self.resolve_stmt(body, ex);
                self.loop_kind = prev_loop;
            }
            StmtKind::If(r#if) => {
                self.resolve_expr(&mut r#if.condition, ex);
                self.resolve_stmt(&mut r#if.then, ex);
                if let Some(ref mut r#else) = r#if.otherwise {
                    self.resolve_stmt(r#else, ex);
                }
            }
            StmtKind::Return(value) => {
                if let Some(ref mut expr) = value {
                    self.resolve_expr(expr, ex);
                }
                match self.scope_kind {
                    ScopeKind::Global | ScopeKind::Local => ex.record(LoxError::resolution(
                        "Cannot return outside of a function.",
                        stmt.span,
                    )),
                    ScopeKind::Initializer if value.is_some() => ex.record(LoxError::resolution(
                        "Cannot return from an initializer.",
                        stmt.span,
                    )),
                    ScopeKind::Initializer => {
                        *value = Some(Box::new(expr!(ExprKind::This, stmt.span)));
                    }
                    ScopeKind::Function | ScopeKind::Method | ScopeKind::AssocFunction => (),
                };
            }
            StmtKind::Break => {
                if let LoopKind::None = self.loop_kind {
                    ex.record(LoxError::resolution(
                        "Cannot break outside of a loop.",
                        stmt.span,
                    ))
                }
            }
            StmtKind::Continue => {
                if let LoopKind::None = self.loop_kind {
                    ex.record(LoxError::resolution(
                        "Cannot continue outside of a loop.",
                        stmt.span,
                    ))
                }
            }
            StmtKind::Function(info) => {
                self.declare(&info.name, NameKind::Func, ex);
                self.assign(&info.name, ex);
                self.resolve_function(info, ScopeKind::Function, ex);
            }
            StmtKind::Class(info) => {
                let scope = self.scope_kind;
                let sub = self.is_in_subclass;
                let cls = self.is_in_class;
                self.is_in_class = true;

                self.declare(&info.name, NameKind::Class, ex);
                if let Some(ref mut superclass) = info.superclass {
                    self.resolve_expr(superclass, ex);
                    self.is_in_subclass = true;
                }
                self.assign(&info.name, ex);

                self.push();
                for method in &mut info.methods {
                    self.resolve_function(
                        method,
                        match method.kind {
                            FnKind::Method if method.name.lexeme == "init" => {
                                ScopeKind::Initializer
                            }
                            FnKind::Method | FnKind::Getter => ScopeKind::Method,
                            FnKind::Static => ScopeKind::AssocFunction,
                            FnKind::Function | FnKind::Lambda => unreachable!(),
                        },
                        ex,
                    );
                }
                self.pop(ex);

                self.is_in_subclass = sub;
                self.is_in_class = cls;
                self.scope_kind = scope;
            }
            StmtKind::Import(import) => self.resolve_import(import, ex),
            StmtKind::_Empty => {}
        }
    }

    /// Resolves the given expression. Writes errors and warnings, if any, to `ex`.
    fn resolve_expr(&mut self, expr: &mut Expr<'a>, ex: &mut ErrCtx) {
        match &mut expr.kind {
            ExprKind::Binary(_, left, right) => {
                self.resolve_expr(left, ex);
                self.resolve_expr(right, ex);
            }
            ExprKind::Unary(_, operand) => {
                self.resolve_expr(operand, ex);
            }
            ExprKind::ArgForwarding(expr) => {
                self.resolve_expr(expr, ex);
            }
            ExprKind::Lit(lit) => {
                if let LitValue::Str(s) = &mut lit.value {
                    crate::frontend::scanner::unescape_in_place(s).unwrap_or_else(|| {
                        ex.record(LoxError::parse(
                            "Invalid unicode escape sequence",
                            lit.token.span,
                        ))
                    });
                }
            }
            ExprKind::Comma(left, right) => {
                self.resolve_expr(left, ex);
                self.resolve_expr(right, ex);
            }
            ExprKind::Call { callee, args, .. } => {
                self.resolve_expr(callee, ex);
                for arg in &mut args.node {
                    self.resolve_expr(arg, ex);
                }
                if let ExprKind::GetAttr(box GetAttr {
                    field: Token { lexeme: "init", .. },
                    ..
                }) = &callee.kind
                {
                    if self.scope_kind == ScopeKind::Initializer {
                        ex.record(LoxError::resolution(
                            "Cannot recursively call an initializer.",
                            callee.span,
                        ));
                    }
                }
            }
            ExprKind::GetAttr(get) => self.resolve_expr(&mut get.node, ex),
            ExprKind::SetAttr(set) => {
                self.resolve_expr(&mut set.node, ex);
                self.resolve_expr(&mut set.value, ex);
            }
            ExprKind::GetItem(obj, item) => {
                self.resolve_expr(obj, ex);
                self.resolve_expr(item, ex);
            }
            ExprKind::SetItem(set) => {
                self.resolve_expr(&mut set.node, ex);
                self.resolve_expr(&mut set.key, ex);
                self.resolve_expr(&mut set.value, ex);
            }
            ExprKind::InterStr(inter) => {
                for ref mut frag in &mut inter.fragments {
                    match frag {
                        InterFrag::Expr(expr) => self.resolve_expr(expr, ex),
                        InterFrag::Str(lit) => match &mut lit.value {
                            LitValue::Str(s) => {
                                crate::frontend::scanner::unescape_in_place(s).unwrap_or_else(
                                    || {
                                        ex.record(LoxError::parse(
                                            "Invalid unicode escape sequence",
                                            lit.token.span,
                                        ))
                                    },
                                );
                            }
                            _ => unreachable!(),
                        },
                    }
                }
            }
            ExprKind::Array(exprs) => {
                for expr in exprs {
                    self.resolve_expr(expr, ex);
                }
            }
            ExprKind::Dict(pairs) => {
                for (k, v) in pairs {
                    self.resolve_expr(k, ex);
                    self.resolve_expr(v, ex);
                }
            }
            ExprKind::This => match self.scope_kind {
                ScopeKind::Global | ScopeKind::Function | ScopeKind::Local => {
                    if !self.is_in_class {
                        ex.record(LoxError::resolution(
                            "Cannot use `this` outside of a class.",
                            expr.span,
                        ));
                    }
                }
                ScopeKind::Initializer | ScopeKind::AssocFunction | ScopeKind::Method => (),
            },
            ExprKind::Super(_) => match self.scope_kind {
                ScopeKind::Global | ScopeKind::Function | ScopeKind::Local => ex.record(
                    LoxError::resolution("Cannot use `super` outside of a class.", expr.span),
                ),
                ScopeKind::Initializer | ScopeKind::AssocFunction | ScopeKind::Method
                    if !self.is_in_subclass =>
                {
                    ex.record(LoxError::resolution(
                        "Cannot use `super` in a class without a superclass.",
                        expr.span,
                    ))
                }
                _ => (),
            },
            ExprKind::Ternary(ternary) => {
                self.resolve_expr(&mut ternary.predicate, ex);
                self.resolve_expr(&mut ternary.then, ex);
                self.resolve_expr(&mut ternary.otherwise, ex);
            }
            ExprKind::Match(r#match) => {
                self.resolve_expr(&mut r#match.value, ex);
                self.push();

                if let Some(ref var) = r#match.bound_var {
                    self.declare(var, NameKind::Const, ex);
                    self.assign(var, ex);
                }

                for arm in &mut r#match.arms {
                    if !arm.is_placeholder {
                        self.resolve_expr(&mut arm.pat, ex);
                    }
                    self.resolve_stmt(&mut arm.body, ex);
                }

                self.pop(ex);
            }
            ExprKind::Variable(name) => {
                if !self.is_defined(name) {
                    ex.record(LoxError::resolution(
                        format!(
                            "Cannot read the variable `{}` in its own initializer",
                            name.lexeme,
                        ),
                        name.span,
                    ));
                } else if let Some(VarUsage {
                    is_match_var: true, ..
                }) = self.env.get(&name.lexeme)
                {
                    name.kind = TokenKind::MatchBoundVar;
                }
                self.r#use(name, ex)
            }
            ExprKind::PrefixIncDec(inc) => {
                self.assign(&inc.name, ex);
                self.r#use(&inc.name, ex)
            }
            ExprKind::PostfixIncDec(inc) => {
                self.assign(&inc.name, ex);
                self.r#use(&inc.name, ex)
            }
            ExprKind::Assignment(asn) => {
                self.resolve_expr(&mut asn.value, ex);
                self.assign(&asn.name, ex);
            }
            ExprKind::Lambda(info) => self.resolve_function(info, ScopeKind::Function, ex),
            ExprKind::Grouping(expr) => self.resolve_expr(expr, ex),
        }
    }

    /// Resolves the given import statement and queues the imported module for compilation.
    fn resolve_import(&mut self, import: &mut Import<'a>, ex: &mut ErrCtx) {
        // if the import has an alias, declare and define it instead of package name.
        match import.alias {
            Some(ref alias) => {
                self.declare(alias, NameKind::Import, ex);
                self.assign(alias, ex);
            }
            None => {
                let name = import.package.last().unwrap();
                self.declare(name, NameKind::Import, ex);
                self.assign(name, ex);
            }
        }

        // Declare and define the import's bindings, e.g. `import stdlib.array { Array, range }`.
        for binding in &import.bindings {
            self.declare(&binding.bind_to, NameKind::Const, ex);
            self.assign(&binding.bind_to, ex);
        }

        // Depending on the configuration, use the interpreter's root or the parent directory
        // of the script.
        let root = if import.import_from_root {
            self.ctx.read_shared().root.clone()
        } else {
            PathBuf::from(
                self.ctx
                    .read_shared()
                    .get_module(self.module)
                    .unwrap()
                    .source_info
                    .filename
                    .clone(),
            )
            .parent()
            .unwrap()
            .to_owned()
        };
        // Construct the script path by appending the import's packages to the root.
        let mut path = root.join(
            import
                .package
                .iter()
                .map(|t| t.lexeme)
                .collect::<Vec<_>>()
                .join("/"),
        );
        path.set_extension("lox");
        let package_span = combspan!(import.package[0], import.package.last().unwrap());

        // Attempt to resolve the path
        import.path = match std::fs::canonicalize(path.clone()) {
            Ok(canonical) => canonical,
            Err(e) => {
                ex.record(LoxError::resolution(
                    format!(
                        "Failed to resolve a package import with the path `{}`: {}",
                        path.to_string_lossy(),
                        e,
                    ),
                    package_span,
                ));
                path
            }
        };
        if !import.path.is_file() {
            ex.record(LoxError::resolution(
                format!(
                    "Package at the path is not a file: `{}`",
                    import.path.display()
                ),
                package_span,
            ));
        } else {
            // Queue the resolved module for compilation.
            let guard = self.ctx.write_shared().queue_module(
                self.ctx.clone(),
                self.module,
                import.path.clone(),
                import.import_from_root,
            );
            import.module = guard.get();
        }
    }

    /// Resolves the given function.
    fn resolve_function(&mut self, info: &mut FnInfo<'a>, kind: ScopeKind, ex: &mut ErrCtx) {
        let scope_kind = self.scope_kind;
        let loop_kind = self.loop_kind;
        self.scope_kind = kind;
        self.loop_kind = LoopKind::None;

        self.push();
        for param in info.params.into_iter() {
            self.declare(param, NameKind::Var, ex);
            self.assign(param, ex);
        }
        self.resolve_block(&mut info.body, ex);
        self.pop(ex);

        if info.kind == FnKind::Method && info.name.lexeme == "init" {
            info.body.push(stmt!(
                StmtKind::Return(Some(Box::new(expr!(ExprKind::This, info.name.span)))),
                info.name.span
            ));
        } else if info.kind == FnKind::Static && info.name.lexeme == "init" {
            ex.record(LoxError::parse(
                "Cannot have a static initializer",
                info.name.span,
            ));
        }

        self.scope_kind = scope_kind;
        self.loop_kind = loop_kind;
    }

    /// Resolves the given block. If the last statement in the block is a return statement with a call,
    /// e.g. `return func()`, marks it as TCO-eligible.
    #[allow(clippy::single_match)]
    fn resolve_block(&mut self, statements: &mut Vec<Stmt<'a>>, ex: &mut ErrCtx) {
        if let Some(StmtKind::Return(Some(ref mut expr))) =
            statements.last_mut().map(|s| &mut s.kind)
        {
            // TODO: Better TCO detection
            match &mut expr.kind {
                ExprKind::Call { tco, .. } => *tco = true,
                _ => (),
            }
        }
        for stmt in statements {
            self.resolve_stmt(stmt, ex);
        }
    }

    /// Pushes a new scope onto the environment stack.
    #[inline(always)]
    fn push(&mut self) {
        self.env.push();
    }

    /// Checks the variable usages in the current scope and pops it off the environment stack.
    fn pop(&mut self, ex: &mut ErrCtx) {
        self.check_variable_usage(ex);
        let _ = self.env.pop();
    }

    /// Checks the variable usage in the current scope.
    #[allow(clippy::match_bool)]
    fn check_variable_usage(&self, ex: &mut ErrCtx) {
        let is_global = self.env.is_global();
        for (name, vu) in self.env.get_scope().unwrap() {
            let mut should_report = false;
            let mut should_suggest_wildcard = false;
            let mut report_as_warning = false;
            let noun = match vu.kind {
                NameKind::Func => "Function",
                NameKind::Class => "Class",
                NameKind::Import => "Import",
                NameKind::Var | NameKind::Const => "Variable",
            };

            let is_prefixed = name.starts_with('_');

            // Report an error for unused local classes/functions.A
            // While having an unused local variable makes sense in some cases,
            // Having an unused local class, function, or import doesn't.
            if !vu.used && !is_global && !vu.is_var() && !is_prefixed {
                should_report = true;
            // Issue an error for unused local variables.
            } else if !vu.used && !is_global && !is_prefixed && (vu.is_var() || vu.is_import()) {
                should_report = true;
                should_suggest_wildcard = true;
            // Issue a warning for unused global imports
            } else if !vu.used && !is_prefixed && vu.is_import() {
                should_report = false;
                report_as_warning = true;
                should_suggest_wildcard = true;
            }

            if !should_report {
                continue;
            }

            let msg = format!("{} `{}` is never used", noun, name);
            ex.record(if report_as_warning {
                LoxError::warning(msg, vu.span)
            } else {
                match should_suggest_wildcard {
                    true => LoxError::resolution_wildcard(msg, vu.span),
                    false => LoxError::resolution(msg, vu.span),
                }
            })
        }
    }

    /// Inserts the given name into the current scope, initializing it with the given [`NameKind`],
    /// and marking as `declared`, but not `defined`, `assigned`, or `used`.
    fn declare(&mut self, name: &Token<'a>, kind: NameKind, ex: &mut ErrCtx) {
        if let Some(vu) = self.env.get_scope().and_then(|s| s.get(&name.lexeme)) {
            match vu.kind {
                NameKind::Func | NameKind::Class => (),
                _ if self.scope_kind != ScopeKind::Global || vu.kind == NameKind::Const => ex
                    .record(LoxError::resolution(
                        format!(
                            "Variable `{}` is already declared in this scope",
                            name.lexeme
                        ),
                        name.span,
                    )),
                _ => (),
            }
        }

        self.env.insert(
            name.lexeme,
            VarUsage {
                kind,
                defined: false,
                assigned: false,
                used: false,
                is_match_var: name.kind == TokenKind::MatchBoundVar,
                span: name.span,
            },
        );
    }

    /// Returns true the given variable has been defined.
    #[inline]
    fn is_defined(&self, name: &Token<'a>) -> bool {
        self.env
            .get(&name.lexeme)
            .map(|v| v.defined)
            .unwrap_or(true)
    }

    /// Marks the given variable as defined.
    fn define(&mut self, name: &Token<'a>, ex: &mut ErrCtx) {
        match self.env.get_mut(&name.lexeme) {
            Some(v) => v.defined = true,
            None => {
                self.undefined_variable_lint(name.lexeme, name.span, ex);
            }
        }
    }

    /// Makes the given variable as assigned if it isn't `const`.
    fn assign(&mut self, name: &Token<'a>, ex: &mut ErrCtx) {
        match self.env.get_mut(&name.lexeme) {
            Some(v) => {
                if v.is_const() && v.assigned {
                    ex.record(LoxError::resolution(
                        format!(
                            "Cannot assign twice to the const variable `{}`",
                            name.lexeme
                        ),
                        name.span,
                    ));
                }
                v.defined = true;
                v.assigned = true;
            }
            None => self.undefined_variable_lint(name.lexeme, name.span, ex),
        }
    }

    /// Marks the given variable as used.
    #[inline]
    fn r#use(&mut self, name: &Token<'a>, ex: &mut ErrCtx) {
        match self.env.get_mut(&name.lexeme) {
            Some(v) => v.used = true,
            None => self.undefined_variable_lint(name.lexeme, name.span, ex),
        }
    }

    /// Issues an undefined variable lint if the given variable isn't a builtin.
    fn undefined_variable_lint(&mut self, name: &str, span: Span, ex: &mut ErrCtx) {
        if !self.builtin_names.contains(name) {
            ex.record(LoxError::resolution(
                format!("Undefined variable `{}`", name),
                span,
            ));
        }
    }
}

impl<'a> IRPass<ErrCtx> for Resolver<'a> {
    type Input = AST<'a>;
    type Output = AST<'a>;

    fn apply(self, mut ir: Self::Input) -> Result<Self::Output, ErrCtx> {
        self.resolve(&mut ir).map(|_| ir)
    }

    fn description(&self) -> String {
        String::from("<Resolver: Resolves variable usage and initialization>")
    }
}
