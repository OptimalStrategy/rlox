//! This module contains the definitions for [`Token`] and its related types such as [`TokenKind`]
//! and [`Span`].
//!
//! [`Token`]: crate::token::Token
//! [`TokenKind`]: crate::token::TokenKind
//! [`Span`]: crate::token::Span
use crate::{
    ast::{BinOpKind, UnOpKind},
    module::ModuleId,
};

/// The kind of a particular token, e.g. `+` (Plus) or `name` (Identifier).
/// Note that some of the variants, such as `InterpFragmentStart`, do not directly correspond to lexemes
/// and are used as markers.
#[derive(Debug, Clone, Copy, Hash, Eq, PartialEq)]
pub enum TokenKind {
    /// `(`                                   
    LeftParen,
    /// `)`                 
    RightParen,
    /// `{`                 
    LeftBrace,
    /// `}`                 
    RightBrace,
    /// `,`                 
    Comma,
    /// `.`                 
    Dot,
    /// `;`                 
    Semicolon,
    /// `-`                 
    Minus,
    /// `+`                 
    Plus,
    /// `/`                 
    Slash,
    /// `*`                 
    Star,
    /// `**`                 
    Pow,
    /// `%`                 
    Percent,

    // One or two character tokens
    /// `!`
    Bang,
    /// `=`
    Equal,
    /// `>`
    Greater,
    /// `<`
    Less,
    /// `!=`
    BangEqual,
    /// `==`
    EqualEqual,
    /// `>=`
    GreaterEqual,
    /// `<=`
    LessEqual,
    /// `++`
    Increment,
    /// `--`
    Decrement,
    /// `..`
    DoubleDot,
    /// `...`
    Ellipsis,
    /// `~`
    Tilde,
    /// `+=`
    PlusEqual,
    /// `-=`
    MinusEqual,
    /// `*=`
    StarEqual,
    /// `/=`
    SlashEqual,
    /// `**=`
    PowEqual,
    /// `%=`
    PercentEqual,

    // Ternary operator
    /// `:`
    Colon,
    /// `?`
    Query,

    /// `=>`
    FatArrow,
    /// `[`
    RightBracket,
    /// `]`
    LeftBracket,

    // Literals
    /// Variables/functions/class names
    Identifier,
    /// `"s"`
    StringLiteral,
    /// `123`
    IntegerLiteral,
    /// `0xABC123`
    HexadecimalLiteral,
    /// `0b010101`
    BinaryLiteral,
    /// `2.79`, `1e5`, `1.31E-5`, `3E+7`
    FloatLiteral,
    /// An interpolated string, i.e. `"Hello, ${name}"`
    InterpolatedString,
    /// The start of an interpolation fragment.
    /// All tokens after this token belong to the interpolated expression.
    InterpFragmentStart,
    /// The end of an interpolation fragment.
    /// All tokens before this token belong to the interpolated expression.
    InterpFragmentEnd,
    /// The end of an interpolated string
    InterpolatedStringEnd,
    // Keywords
    /// `false`
    False,
    /// `true`
    True,
    /// `else`
    Else,
    /// `and`
    And,
    /// `fun`
    Fun,
    /// `for`
    For,
    /// `while`
    While,
    /// `loop`
    Loop,
    /// `if`
    If,
    /// `or`
    Or,
    /// `print`
    Print,
    /// `return`
    Return,
    /// `super`
    Super,
    /// `this`
    This,
    /// `class`
    Class,
    /// `var`
    Var,
    /// `break`
    Break,
    /// `continue`
    Continue,
    /// `match`
    Match,
    /// `nil`
    Nil,
    /// `const`
    Const,
    /// `as`
    As,
    /// `in`
    In,
    /// `import`
    Import,

    /// Used to report errors.
    Unknown,
    /// Special token kind assigned to the bound variable in match statements.
    MatchBoundVar,
    /// End of File.
    EOF,
}

/// Contains the information about the location of a token, AST node or other IR entity.
/// A span may be multi-line.
///
/// ```
/// # use rlox::token::Span;
/// let span = Span::new(1, 1, 2);
/// assert_eq!(span.line, 1);
/// assert_eq!(span.column, 1);
/// assert_eq!(span.length, 2);
/// assert_eq!(span.end(), 3);
/// assert_eq!(span.end_span, None);
/// ```
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Span {
    /// The line where the span starts.
    pub line: usize,
    /// The column on the line where the span starts.
    pub column: usize,
    /// The length of the span. This field is not used if the end span is `Some`.
    pub length: usize,
    /// Optional ending line and location if the span is multi-line.
    /// We can't use the Span here because that would make it a recursive data structure
    /// and we want to avoid extra allocations.
    pub end_span: Option<(usize, usize, usize)>,
}

impl Span {
    /// Creates a new single-line span.
    pub fn new(line: usize, column: usize, length: usize) -> Self {
        Span {
            line,
            column,
            length,
            end_span: None,
        }
    }

    /// Creates a span with the column set to `column + 1 - length`.
    pub(crate) fn with_fixed_column(line: usize, column: usize, length: usize) -> Self {
        Span {
            line,
            column: column - length + 1,
            length,
            end_span: None,
        }
    }

    /// Creates a span corresponding to a built-in or IR-only entity.
    /// All fields of such a span are [`std::usize::MAX`].
    ///
    /// ```
    /// # use rlox::token::Span;
    /// let span = Span::builtin();
    /// assert_eq!(span.line, std::usize::MAX);
    /// assert_eq!(span.column, std::usize::MAX);
    /// assert_eq!(span.length, std::usize::MAX);
    /// assert_eq!(span.end_span, None);
    /// ```
    pub fn builtin() -> Self {
        Span::new(std::usize::MAX, std::usize::MAX, std::usize::MAX)
    }

    /// Combines the given spans. The spans must not be the same.
    /// If the spans are on different lines, a multi-line span will be created.
    /// Single-line spans will contain the start of the left-most span and the end of the right-most span.
    ///
    /// ```
    /// # use rlox::token::Span;
    /// // Combine two same-line spans.
    /// let comb = Span::combined(Span::new(1, 1, 5), Span::new(1, 10, 3));
    /// assert_eq!(comb.line, 1);
    /// assert_eq!(comb.column, 1);
    /// assert_eq!(comb.length, 12);
    /// assert_eq!(comb.end(), 13);
    /// assert_eq!(comb.end_span, None);
    ///
    /// // Combine two different-line spans.
    /// let multi = Span::combined(Span::new(1, 1, 3), Span::new(3, 8, 4));
    /// assert_eq!(comb.line, 1);
    /// assert_eq!(comb.column, 1);
    /// let end = multi.end_span().unwrap();
    /// assert_eq!(end.line, 3);
    /// assert_eq!(end.column, 8);
    /// assert_eq!(end.length, 4);
    /// assert_eq!(end.end(), 12);
    /// ```
    pub fn combined(first: Span, second: Span) -> Self {
        // Cannot combine spans with different lines.
        if first.line != second.line {
            let (span, end_span) = if first.line < second.line
                || (first.line == second.line && first.column < second.column)
            {
                (first, second)
            } else {
                (second, first)
            };
            return Span {
                end_span: Some(end_span.into_tuple()),
                ..span
            };
        }

        // Figure out the order of the tokens
        let (first, second) = if first.column < second.column {
            (first, second)
        } else {
            (second, first)
        };

        Span {
            line: first.line,
            column: first.column,
            length: second.column + second.length - first.column,
            end_span: None,
        }
    }

    /// Transforms the span into a tuple of three elements: the line, the column, and the length.
    ///
    /// ```
    /// # use rlox::token::Span;
    /// let span = Span::new(5, 10, 15);
    /// assert_eq!(span.into_tuple(), (5, 10, 15));
    /// ```
    pub fn into_tuple(self) -> (usize, usize, usize) {
        (self.line, self.column, self.length)
    }

    /// Transforms the end span tuple into a [`Span`] object or returns None.
    ///
    /// ```
    /// # use rlox::token::Span;
    /// let span = Span { line: 1, column: 1, length: 5, end_span: Some((2, 2, 2)) };
    /// assert_eq!(span.end_span(), Some(Span::new(2, 2, 2)))
    /// ```
    pub fn end_span(&self) -> Option<Span> {
        self.end_span
            .map(|(line, col, len)| Span::new(line, col, len))
    }

    /// Returns the end boundary of the span.
    ///
    /// ```
    /// # use rlox::token::Span;
    /// let span = Span::new(1, 1, 5);
    /// assert_eq!(span.end(), 6);
    /// ```
    pub fn end(&self) -> usize {
        self.column + self.length
    }
}

impl TokenKind {
    /// Attempts to convert the TokenKind into a BinOpKind used by the AST.
    pub fn into_binop(self) -> Option<BinOpKind> {
        self.into()
    }
}

impl Into<Option<BinOpKind>> for TokenKind {
    /// Attempts to convert the TokenKind into a BinOpKind used by the AST.
    fn into(self) -> Option<BinOpKind> {
        Some(match self {
            TokenKind::Plus | TokenKind::PlusEqual => BinOpKind::Add,
            TokenKind::Minus | TokenKind::MinusEqual => BinOpKind::Sub,
            TokenKind::Star | TokenKind::StarEqual => BinOpKind::Mul,
            TokenKind::Slash | TokenKind::SlashEqual => BinOpKind::Div,
            TokenKind::Pow | TokenKind::PowEqual => BinOpKind::Pow,
            TokenKind::Percent | TokenKind::PercentEqual => BinOpKind::Rem,
            TokenKind::BangEqual => BinOpKind::Ne,
            TokenKind::EqualEqual => BinOpKind::Eq,
            TokenKind::Greater => BinOpKind::Gt,
            TokenKind::Less => BinOpKind::Lt,
            TokenKind::GreaterEqual => BinOpKind::Ge,
            TokenKind::LessEqual => BinOpKind::Le,
            _ => return None,
        })
    }
}

impl Into<Option<UnOpKind>> for TokenKind {
    /// Attempts to convert the TokenKind into an UnOpKind used by the AST.
    fn into(self) -> Option<UnOpKind> {
        Some(match self {
            TokenKind::Plus => UnOpKind::Plus,
            TokenKind::Minus => UnOpKind::Neg,
            TokenKind::Bang => UnOpKind::Not,
            _ => return None,
        })
    }
}

/// A simple wrapper for adding spans to other types, shamelessly stolen from rustc.
#[derive(Clone)]
pub struct Spanned<T> {
    /// The node the span is for.
    pub node: T,
    /// The span of the associated AST node.
    pub span: Span,
}
impl<T> std::fmt::Debug for Spanned<T>
where
    T: std::fmt::Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        self.node.fmt(f)
    }
}

/// Represents a token produced by the scanner.
/// Tokens are only used to construct the AST
/// and are replaced with spans during compilation.
#[derive(Debug, Clone, PartialEq)]
pub struct Token<'a> {
    /// The kind of the token.
    pub kind: TokenKind,
    /// The lexeme that corresponds to the token.
    /// String literals keep the quotes.
    pub lexeme: &'a str,
    /// The module this token originates from.
    pub module: ModuleId,
    /// The span of the token.
    pub span: Span,
}

impl<'a> Token<'a> {
    /// Creates a new token.
    ///
    /// ```
    /// # use rlox::token::*;
    /// let token = Token::new(TokenKind::Identifier, "var", 1, 1);
    /// assert_eq!(token.kind, TokenKind::Identifier);
    /// assert_eq!(token.lexeme, "var");
    /// assert_eq!(token.span, Span::new(1, 1, 3));
    /// ```
    pub fn new(kind: TokenKind, lexeme: &'a str, line: usize, column: usize) -> Self {
        let length = crate::scanner::number_of_graphemes(lexeme); // XXX: why not .len()?
        Token {
            kind,
            lexeme,
            module: ModuleId::anon(),
            span: Span::new(line, column, length),
        }
    }

    /// Creates a new token with the column value set to `column - length + 1`.
    ///
    /// ```
    /// # use rlox::token::*;
    /// let token = Token::with_fixed_column(TokenKind::Identifier, "var", 1, 3);
    /// assert_eq!(token.kind, TokenKind::Identifier);
    /// assert_eq!(token.lexeme, "var");
    /// assert_eq!(token.span, Span::new(1, 1, 3));
    /// ```
    pub fn with_fixed_column(kind: TokenKind, lexeme: &'a str, line: usize, column: usize) -> Self {
        let length = crate::scanner::number_of_graphemes(lexeme); // XXX: why not .len()?
        Token {
            kind,
            lexeme,
            module: ModuleId::anon(),
            span: Span::with_fixed_column(line, column, length),
        }
    }

    /// Creates a new token.
    ///
    /// ```
    /// # use rlox::token::*;
    /// let token = Token::with_span(TokenKind::Identifier, "var", Span::new(1, 1, 3));
    /// assert_eq!(token.kind, TokenKind::Identifier);
    /// assert_eq!(token.lexeme, "var");
    /// assert_eq!(token.span, Span::new(1, 1, 3));
    /// ```
    pub fn with_span(kind: TokenKind, lexeme: &'a str, span: Span) -> Self {
        Token {
            kind,
            lexeme,
            module: ModuleId::anon(),
            span,
        }
    }

    /// Sets the module's token.
    ///
    /// ```
    /// # use rlox::{token::*, module::ModuleId};
    /// let token = Token::new(TokenKind::Identifier, "var", 1, 1);
    /// assert_eq!(token.module, ModuleId::anon());
    /// assert_eq!(token.set_module(ModuleId::internal()).module, ModuleId::internal());
    /// ```
    pub fn set_module(self, module: ModuleId) -> Self {
        Token { module, ..self }
    }

    /// Adjusts the token's span by the given `line`, `column`, and `length` offsets.
    ///
    ///```
    /// # use rlox::{token::*};
    /// let token = Token::new(TokenKind::Identifier, "var", 1, 5);
    /// assert_eq!(token.span.line, 1);
    /// assert_eq!(token.span.column, 5);
    ///
    /// let token = token.adjust_span_by(10, -3, 0);
    /// assert_eq!(token.span.line, 11);
    /// assert_eq!(token.span.column, 2);
    /// ```
    pub fn adjust_span_by(self, line: isize, column: isize, length: isize) -> Self {
        Self {
            span: Span::new(
                (self.span.line as isize + line) as usize,
                (self.span.column as isize + column) as usize,
                (self.span.length as isize + length) as usize,
            ),
            ..self
        }
    }

    /// A shortcut for `adjust_span_by(0, column, 0). Adjusts only the column.`
    #[inline]
    pub fn adjust_column_by(self, column: isize) -> Self {
        Self::adjust_span_by(self, 0, column, 0)
    }

    /// Creates a special EOF token at the given span.
    /// The literal of a EOF token is `\0`.
    ///
    /// ```
    /// # use rlox::token::*;
    /// let eof = Token::eof(Span::builtin().line, Span::builtin().column);
    /// assert_eq!(eof.kind, TokenKind::EOF);
    /// assert_eq!(eof.lexeme, "\0");
    /// ```
    #[inline]
    pub fn eof(line: usize, column: usize) -> Self {
        Self::internal(TokenKind::EOF, line, column)
    }

    /// Creates a special EOF token at the given span.
    /// The literal of a EOF token is `\0`.
    ///
    /// ```
    /// # use rlox::token::*;
    /// let eof = Token::eof_from_span(Span::builtin());
    /// assert_eq!(eof.kind, TokenKind::EOF);
    /// assert_eq!(eof.lexeme, "\0");
    /// ```
    #[inline]
    pub fn eof_from_span(s: Span) -> Self {
        Self::internal(TokenKind::EOF, s.line, s.column)
    }

    /// Creates a special internal token at the given span.
    /// The literal of a token constructed this way is `\0`.
    ///
    /// ```
    /// # use rlox::token::*;
    /// let token = Token::internal(TokenKind::InterpFragmentStart, Span::builtin().line, Span::builtin().column);
    /// assert_eq!(token.kind, TokenKind::InterpFragmentStart);
    /// assert_eq!(token.lexeme, "\0");
    /// ```
    pub fn internal(kind: TokenKind, line: usize, column: usize) -> Self {
        Token {
            kind,
            lexeme: "\0",
            module: ModuleId::internal(),
            span: Span::new(line, column, 0),
        }
    }

    /// Returns `true` if the token has the [`EOF`] kind.
    ///
    /// ```
    /// # use rlox::token::*;
    /// assert!(Token::eof(Span::builtin().line, Span::builtin().column).is_eof());
    /// assert!(!Token::new(TokenKind::Plus, "+", Span::builtin().line, Span::builtin().column).is_eof());
    /// ```
    ///
    /// [`EOF`]: enum.TokenKind.html#variant.EOF
    pub fn is_eof(&self) -> bool {
        matches!(self.kind, TokenKind::EOF)
    }
}
