//! This module contains the implementation of the Lox Scanner and its pipeline pass.
use unic_ucd::{is_white_space, GeneralCategory as GC};
use unicode_segmentation::UnicodeSegmentation;
use unicode_xid::UnicodeXID;

use std::collections::hash_map::DefaultHasher;
use std::hash::Hasher;

use crate::errors::LoxError;
use crate::{err_context::ErrCtx, module::ModuleId, pipeline::IRPass, token::*, SharedCtx};

/// A scanner with UTF-8 support.
pub struct Scanner<'a> {
    /// A reference to the compilation and execution context.
    ctx: SharedCtx,
    /// The source code to lex as a vec of UTF-8 graphemes.
    source: Vec<&'a str>,
    /// The current position of the scanner
    current: usize,
    /// The line the scanner is currently on.
    line: usize,
    /// The column the scanner is currently on.
    column: usize,
    /// The [`Hasher`: std::hash::Hasher] used to compute the token hash of the source code.
    hasher: DefaultHasher,
    /// The ID of the module the source code belongs to.
    module: ModuleId,
}

impl<'a> IRPass<ErrCtx> for Scanner<'a> {
    type Input = ModuleId;
    type Output = (u64, Vec<Token<'a>>);

    fn apply(self, module: Self::Input) -> Result<Self::Output, ErrCtx> {
        let source = self.ctx.read_shared().get_module_source(module).unwrap();
        self.scan(source, module).map_err(ErrCtx::with_error)
    }

    fn description(&self) -> String {
        String::from("<Scanner: Scans the received source and produces a sequence of tokens>")
    }
}

/// This type is returned by the scan_token() method since in some cases
/// a token sequence may be produced while parsing a single token.
#[derive(Debug)]
enum ScannedToken<'a> {
    Single(Token<'a>),
    Sequence(Vec<Token<'a>>),
}

impl<'a> ScannedToken<'a> {
    /// Returns the inner vec if the variant is `Sequence` or wraps the token
    /// in a vec if the variant is `Single`.
    pub fn into_vec(self) -> Vec<Token<'a>> {
        match self {
            Self::Single(t) => vec![t],
            Self::Sequence(tokens) => tokens,
        }
    }

    /// Returns `true` if the variant is `Single` and the inner token's kind is [`TokenKind::EOF`].
    pub fn is_eof(&self) -> bool {
        match self {
            ScannedToken::Single(t) => t.is_eof(),
            _ => false,
        }
    }

    /// Returns `Ok(token)` if the variant is single, `Err(vec)` otherwise.
    pub fn into_single(self) -> Result<Token<'a>, ScannedToken<'a>> {
        match self {
            ScannedToken::Single(t) => Ok(t),
            seq => Err(seq),
        }
    }
}

impl<'a> Scanner<'a> {
    /// Creates a new scanner.
    ///
    /// ```
    /// # use rlox::scanner::*;
    /// # use rlox::context::*;
    /// let ctx = Context::from_source("code").into_shared();
    /// let (source, id) = {
    ///     let guard = ctx.read_shared();
    ///     let source = guard.get_root_module_source().unwrap();
    ///     let id = guard.get_root_module_id().unwrap();
    ///     (source, id)
    /// };
    /// let scanner = Scanner::new(ctx.clone());
    /// assert_eq!(scanner.scan(&source, id).unwrap().1[0].lexeme, "code");
    /// ```
    pub fn new(ctx: SharedCtx) -> Scanner<'a> {
        Scanner {
            ctx,
            source: vec![],
            current: 0,
            line: 1,
            column: 0,
            hasher: DefaultHasher::new(),
            module: ModuleId::anon(),
        }
    }

    /// Scans the entire source code nd returns the resulting tokens or an error.
    ///
    /// ```
    /// # use rlox::scanner::*;
    /// # use rlox::token::{TokenKind, Span};
    /// # use rlox::context::*;
    /// let ctx = Context::from_source("print 5;").into_shared();
    /// let (source, id) = {
    ///     let guard = ctx.read_shared();
    ///     let source = guard.get_root_module_source().unwrap();
    ///     let id = guard.get_root_module_id().unwrap();
    ///     (source, id)
    /// };
    /// let scanner = Scanner::new(ctx.clone());
    /// let (hash, tokens) = scanner.scan(&source, id).unwrap();
    /// assert_eq!(hash, 0xdefffda662387c0);
    /// assert_eq!(tokens.len(), 4);
    /// assert_eq!(tokens[0].kind, TokenKind::Print);
    /// assert_eq!(tokens[0].span, Span::new(1, 1, 5));
    /// ```
    pub fn scan(
        mut self,
        source: &'a str,
        module: ModuleId,
    ) -> Result<(u64, Vec<Token<'a>>), LoxError> {
        self.module = module;
        self.source = UnicodeSegmentation::graphemes(source, true).collect::<Vec<&str>>();
        let mut tokens: Vec<Token<'a>> = vec![];
        let mut token = self.scan_token().map_err(|e| e.with_module(self.module))?;

        while !token.is_eof() {
            match token {
                ScannedToken::Single(token) => {
                    self.hasher.write(token.lexeme.as_bytes());
                    tokens.push(token.set_module(self.module));
                }
                ScannedToken::Sequence(seq) => {
                    for token in seq {
                        self.hasher.write(token.lexeme.as_bytes());
                        tokens.push(token.set_module(self.module));
                    }
                }
            }
            token = self.scan_token().map_err(|e| e.with_module(self.module))?;
        }

        // Push the EOF token
        tokens.push(token.into_single().unwrap().set_module(self.module));

        let hash = self.hasher.finish();
        Ok((hash, tokens))
    }

    /// Attempts to scan the next token.
    /// If no tokens have left, returns an [`EOF`] token.
    ///
    /// [`EOF`]: enum.TokenKind.html#variant.EOF
    fn scan_token(&mut self) -> Result<ScannedToken<'a>, LoxError> {
        self.skip_whitespace();

        let mut lexeme = match self.advance() {
            Some(lexeme) => lexeme,
            None => {
                return Ok(ScannedToken::Single(Token::eof(
                    self.line,
                    // Column number is one-based
                    self.column + 1,
                )));
            }
        };

        let token = match lexeme {
            "(" => token!(self, lexeme, TokenKind::LeftParen),
            ")" => token!(self, lexeme, TokenKind::RightParen),
            "{" => token!(self, lexeme, TokenKind::LeftBrace),
            "}" => token!(self, lexeme, TokenKind::RightBrace),
            "[" => token!(self, lexeme, TokenKind::LeftBracket),
            "]" => token!(self, lexeme, TokenKind::RightBracket),
            ";" => token!(self, lexeme, TokenKind::Semicolon),
            ":" => token!(self, lexeme, TokenKind::Colon),
            "," => token!(self, lexeme, TokenKind::Comma),
            "~" => token!(self, lexeme, TokenKind::Tilde),
            "?" => token!(self, lexeme, TokenKind::Query),
            "." => {
                let start = self.current - 1;
                let mut kind = TokenKind::Dot;
                if self.r#match(".") {
                    kind = if self.r#match(".") {
                        TokenKind::Ellipsis
                    } else {
                        TokenKind::DoubleDot
                    };
                    lexeme =
                        unsafe { crate::scanner::concat_slices(&self.source[start..self.current]) };
                }
                token!(self, lexeme, kind)
            }
            "-" => {
                consume_if!(self, lexeme, TokenKind::Minus, "-" => TokenKind::Decrement, "=" => TokenKind::MinusEqual)
            }
            "+" => {
                consume_if!(self, lexeme, TokenKind::Plus, "+" => TokenKind::Increment, "=" => TokenKind::PlusEqual)
            }
            "*" => {
                consume_if!(self, lexeme, TokenKind::Star, "*" => TokenKind::Pow, "=" => TokenKind::StarEqual)
            }
            "%" => consume_if!(self, lexeme, TokenKind::Percent, "=" => TokenKind::PercentEqual),
            "!" => consume_if!(self, lexeme, TokenKind::Bang, "=" => TokenKind::BangEqual),
            "<" => consume_if!(self, lexeme, TokenKind::Less, "=" => TokenKind::LessEqual),
            ">" => consume_if!(
                self,
                lexeme,
                TokenKind::Greater,
                "=" => TokenKind::GreaterEqual
            ),
            "=" => {
                consume_if!(self, lexeme, TokenKind::Equal, "=" => TokenKind::EqualEqual, ">" => TokenKind::FatArrow)
            }
            "\"" => return self.string(),
            "/" => {
                if self.r#match("/") {
                    self.comment();
                    return self.scan_token();
                } else if self.r#match("*") {
                    self.multi_line_comment()?;
                    return self.scan_token();
                } else {
                    consume_if!(self, lexeme, TokenKind::Slash, "=" => TokenKind::SlashEqual)
                }
            }
            _ => {
                if self.is_valid_identifier_part(lexeme, true) {
                    self.identifier()
                } else if self.is_digit(lexeme) {
                    self.number()?
                } else {
                    return Err(LoxError::scan(
                        format!("Unexpected character: `{}`", lexeme),
                        Token::with_fixed_column(
                            TokenKind::Unknown,
                            lexeme,
                            self.line,
                            self.column,
                        )
                        .span,
                    ));
                }
            }
        };

        Ok(ScannedToken::Single(token))
    }

    /// Skips a sequence of whitespace characters.
    fn skip_whitespace(&mut self) {
        let mut lit = self.peek();
        while lit.is_some() && lit.unwrap().chars().all(is_white_space) {
            let ch = self.advance().unwrap();
            if ch == "\n" {
                self.line += 1;
                self.column = 0;
            }
            lit = self.peek();
        }
    }

    /// Checks if the given grapheme cluster is a decimal digit
    #[inline]
    fn is_digit(&self, cluster: &'a str) -> bool {
        cluster.chars().all(|c| c.is_ascii_digit())
    }

    /// Checks if given grapheme cluster is a digit of the given radix or an underscore
    #[inline]
    fn is_numeric_str_of_radix_or_underscore(&self, cluster: &'a str, radix: u32) -> bool {
        cluster.chars().all(|c| c.is_digit(radix) || c == '_')
    }

    #[inline(always)]
    fn is_numeric_str_or_underscore(&self, cluster: &'a str) -> bool {
        self.is_numeric_str_of_radix_or_underscore(cluster, 10)
    }

    /// Checks if the given grapheme cluster is a hexadecimal digit or an underscore.
    #[inline(always)]
    fn is_hexadecimal_str_or_underscore(&self, cluster: &'a str) -> bool {
        self.is_numeric_str_of_radix_or_underscore(cluster, 16)
    }

    /// Checks if the given grapheme cluster is a binary digit or an underscore.
    #[inline(always)]
    fn is_binary_str_or_underscore(&self, cluster: &'a str) -> bool {
        self.is_numeric_str_of_radix_or_underscore(cluster, 2)
    }

    /// Checks if given grapheme cluster is an alphabetic character or a symbol.
    fn is_alphabetic_str(&self, cluster: &'a str) -> bool {
        cluster.chars().all(|c| {
            UnicodeXID::is_xid_start(c)
                || c == '_'
                || GC::of(c).is_symbol() && !c.is_ascii_punctuation()
        })
    }

    /// Scans an identifier.
    fn identifier(&mut self) -> Token<'a> {
        let start = self.current - 1;
        // First character is already in the buffer
        let mut lit = self.peek();
        while lit.is_some() && self.is_valid_identifier_part(lit.unwrap(), false) {
            self.advance().unwrap();
            lit = self.peek();
        }

        let lexeme = self.unsafe_slice(start, self.current);
        let kind = match lexeme {
            "import" => TokenKind::Import,
            "false" => TokenKind::False,
            "true" => TokenKind::True,
            "else" => TokenKind::Else,
            "and" => TokenKind::And,
            "fun" => TokenKind::Fun,
            "for" => TokenKind::For,
            "while" => TokenKind::While,
            "loop" => TokenKind::Loop,
            "if" => TokenKind::If,
            "or" => TokenKind::Or,
            "as" => TokenKind::As,
            "in" => TokenKind::In,
            "print" => TokenKind::Print,
            "return" => TokenKind::Return,
            "super" => TokenKind::Super,
            "this" => TokenKind::This,
            "class" => TokenKind::Class,
            "var" => TokenKind::Var,
            "break" => TokenKind::Break,
            "continue" => TokenKind::Continue,
            "match" => TokenKind::Match,
            "nil" => TokenKind::Nil,
            "const" => TokenKind::Const,
            _ => TokenKind::Identifier,
        };
        token!(self, lexeme, kind)
    }

    /// Checks if the provided grapheme is a valid identifier: `[{alphabetic}_][{alphabetic}_0-9]+`.
    /// ```rust,ignore
    /// assert!(self.is_valid_identifier_part("_", 0));
    /// assert!(self.is_valid_identifier_part("8", 1));
    /// assert!(!self.is_valid_identifier_part("8", 0));
    /// ```
    #[rustfmt::skip]
    fn is_valid_identifier_part(&self, chars: &'a str, is_first_char: bool) -> bool {
        self.is_alphabetic_str(chars)
        || (!is_first_char && self.is_numeric_str_or_underscore(chars))
    }

    // TODO: reduce code duplication here

    /// Scans an integer: `[0-9]+`.
    fn scan_integer(&mut self) {
        let mut lit = self.peek();
        while lit.is_some() && self.is_numeric_str_or_underscore(lit.unwrap()) {
            self.advance().unwrap();
            lit = self.peek();
        }
    }

    /// Scans a hexadecimal integer literal: `0x[0-9aA-fF_]+`. The caller must consume
    /// the `0x` prefix.
    fn scan_hex_literal(&mut self) {
        let mut lit = self.peek();
        while lit.is_some() && self.is_hexadecimal_str_or_underscore(lit.unwrap()) {
            self.advance().unwrap();
            lit = self.peek();
        }
    }

    /// Scans a binary integer literal: `0b[01_]+`. The caller must consume
    /// the `0b` prefix.
    fn scan_binary_literal(&mut self) {
        let mut lit = self.peek();
        while lit.is_some() && self.is_binary_str_or_underscore(lit.unwrap()) {
            self.advance().unwrap();
            lit = self.peek();
        }
    }

    /// Scans a numeric literal, either an int or a double.
    fn number(&mut self) -> Result<Token<'a>, LoxError> {
        let start = self.current - 1;
        let mut kind = TokenKind::IntegerLiteral;
        let mut scanned_float = false;

        if self.peek_previous() == Some("0") {
            if self.r#match("x") {
                kind = TokenKind::HexadecimalLiteral
            } else if self.r#match("b") {
                kind = TokenKind::BinaryLiteral;
            }
        }

        // Scan the integer part
        match kind {
            TokenKind::IntegerLiteral => self.scan_integer(),
            TokenKind::HexadecimalLiteral => self.scan_hex_literal(),
            TokenKind::BinaryLiteral => self.scan_binary_literal(),
            _ => unreachable!(),
        }

        // Optionally scan the fractional part
        if let Some(lit) = self.peek() {
            if lit == "." {
                self.advance();
                self.scan_integer();
                scanned_float = true;

                match kind {
                    TokenKind::IntegerLiteral => {
                        kind = TokenKind::FloatLiteral;
                    }
                    TokenKind::HexadecimalLiteral | TokenKind::BinaryLiteral => {
                        // Do not change the kind to detect mixed literals
                    }
                    _ => unreachable!(),
                }
            }
        }

        // A float suffix
        if self.r#match("e") || self.r#match("E") {
            if self.check("+") || self.check("-") {
                self.advance();
            }

            match self.peek() {
                Some(c) if self.is_digit(c) => {
                    self.scan_integer();
                }
                _ => {
                    let lexeme = self.unsafe_slice(start, self.current);
                    let token = token!(self, lexeme, kind);
                    return Err(LoxError::scan(
                        "Expected a digit after the exponent",
                        token.span,
                    ));
                }
            }

            scanned_float = true;
            match kind {
                TokenKind::FloatLiteral | TokenKind::IntegerLiteral => {
                    kind = TokenKind::FloatLiteral
                }
                // Do nothing
                TokenKind::HexadecimalLiteral | TokenKind::BinaryLiteral => {
                    // Do not change the kind to detect mixed literals
                }
                _ => unreachable!(),
            }
        }

        let lexeme = self.unsafe_slice(start, self.current);
        let token = token!(self, lexeme, kind);

        if scanned_float && kind != TokenKind::FloatLiteral {
            Err(LoxError::scan(
                "Hexadecimal and binary float literals are not supported",
                token.span,
            ))
        } else {
            Ok(token)
        }
    }

    /// Scans a string literal.
    fn string(&mut self) -> Result<ScannedToken<'a>, LoxError> {
        let start = self.current - 1;
        // Save the position of the opening "
        let (line, pos) = (self.line, self.column);
        let mut interp_line = line;
        let mut interp_pos = pos;
        let mut interp_start = start;

        // This array will contain the tokens that make up an interpolated string.
        let mut interpolated_tokens =
            vec![Token::internal(TokenKind::InterpolatedString, line, pos)];

        while self.peek() != Some("\"") && !self.is_at_end() {
            let lit = self.peek().unwrap();

            if lit == "\n" {
                self.line += 1;
                self.column = 0;
            }

            let maybe_interp = lit == "$" && self.peek_next() == Some("{");
            if maybe_interp && self.peek_previous() != Some("$") {
                // Handle contiguous interpolated fragments
                if interp_start != self.current {
                    // We reached an interpolated fragment.
                    // First, we need to save the preceding normal string fragment
                    let lexeme = self.unsafe_slice(interp_start, self.current);
                    interpolated_tokens.push(
                        self.make_string_token(interp_line, interp_pos, lexeme)
                            .adjust_column_by(-1), //
                    );
                }

                // Consume the $ and {
                self.advance().unwrap();
                self.advance().unwrap();

                // Second, we scan the interpolated fragment
                interpolated_tokens.extend(self.scan_interp_fragment()?);

                interp_line = self.line;
                interp_pos = self.column;
                interp_start = self.current + 1;
            } else if maybe_interp && self.peek_previous() == Some("$") {
                // This is an escaped interpolation fragment
                let lexeme = self.unsafe_slice(interp_start, self.current - 1);
                interpolated_tokens.push(
                    self.make_string_token(interp_line, interp_pos, lexeme)
                        .adjust_column_by(-1), //
                );
                interp_line = self.line;
                interp_pos = self.column;
                interp_start = self.current;
            }
            // Don't unwrap the advance to let the loop terminate if there's
            // an unexpected EOF
            let _ = self.advance();
        }

        if self.is_at_end() || self.peek() != Some("\"") {
            return Err(LoxError::scan(
                "Unterminated string",
                Span::new(line, pos, 1),
            ));
        }

        // Consume the ending "
        self.advance();

        if interpolated_tokens.len() > 1 {
            // Record the last fragment of the interpolated string
            let lexeme = self.unsafe_slice(interp_start, self.current);
            interpolated_tokens.push(self.make_string_token(interp_line, interp_pos, lexeme));
            // End emit a marker token to indicate the end of the string
            interpolated_tokens.push(Token::internal(
                TokenKind::InterpolatedStringEnd,
                self.line,
                self.column,
            ));
            return Ok(ScannedToken::Sequence(interpolated_tokens));
        }

        let lexeme = unsafe { concat_slices(&self.source[start..self.current]) };
        Ok(ScannedToken::Single(
            self.make_string_token(line, pos, lexeme),
        ))
    }

    /// Scans a single interpolated fragment. The scanner must have consumed the `${` characters
    /// before calling this method. This function will not consume the closing `}`.
    fn scan_interp_fragment(&mut self) -> Result<Vec<Token<'a>>, LoxError> {
        let mut tokens = vec![Token::internal(
            TokenKind::InterpFragmentStart,
            self.line,
            self.column,
        )];

        // Do not consume the closing }
        while !self.check("}") && !self.is_at_end() {
            tokens.extend(self.scan_token()?.into_vec());
        }

        tokens.push(Token::internal(
            TokenKind::InterpFragmentEnd,
            self.line,
            self.column,
        ));

        if tokens.len() == 2 {
            Err(LoxError::parse(
                "An interpolated string fragment must not be empty. Use a double `$` if \
                you want an empty formatter ${}: `$${}`.",
                tokens[0].clone().adjust_span_by(0, 0, 1).span,
            ))
        } else {
            Ok(tokens)
        }
    }

    /// Creates a string token from its lexeme and starting position.
    fn make_string_token(&self, line: usize, pos: usize, lexeme: &'a str) -> Token<'a> {
        if line != self.line {
            let start_span = Span::new(line, pos, 0);
            let end_span = Span::new(self.line, self.column, 0);
            Token::with_span(
                TokenKind::StringLiteral,
                lexeme,
                Span::combined(start_span, end_span),
            )
        } else {
            token!(self, lexeme, TokenKind::StringLiteral)
        }
    }

    /// Scans a comment.
    fn comment(&mut self) {
        while !self.is_at_end() && !self.check("\n") {
            self.advance();
        }
    }

    /// Scans a multi-line comment.
    fn multi_line_comment(&mut self) -> Result<(), LoxError> {
        let (line, pos) = (self.line, self.column);

        while !self.is_at_end() {
            if self.check("*") && self.peek_next() == Some("/") {
                break;
            }

            if self.check("\n") {
                self.advance();
                // We reset the position counter only after the newline token is consumed.
                self.line += 1;
                self.column = 0;
            } else {
                self.advance();
            }
        }

        if self.is_at_end() {
            Err(LoxError::scan(
                "Unterminated multi-line comment",
                Span::new(line, pos, 2),
            ))
        } else {
            self.advance();
            self.advance();
            Ok(())
        }
    }

    /// Attempts to advance to the next grapheme.
    /// If no more graphemes have left, returns `None`.
    #[inline]
    fn advance(&mut self) -> Option<&'a str> {
        if self.is_at_end() {
            None
        } else {
            self.current += 1;
            self.column += 1;
            Some(self.source[self.current - 1])
        }
    }

    /// Attempts to return a reference to the current grapheme.
    /// Returns `None` If there is no one.
    #[inline]
    fn peek(&self) -> Option<&'a str> {
        self.source.get(self.current).copied()
    }

    /// Attempts to return a reference to the next grapheme.
    /// Returns `None` if there is none.
    #[inline]
    fn peek_next(&self) -> Option<&'a str> {
        self.source.get(self.current.wrapping_add(1)).copied()
    }

    /// Attempts to return a reference to the previous grapheme.
    /// Returns `None` if there is none.
    #[inline]
    fn peek_previous(&self) -> Option<&'a str> {
        self.source.get(self.current.wrapping_sub(1)).copied()
    }

    /// Checks if the current grapheme is equal to the provided one.
    #[inline]
    fn check(&self, s: &str) -> bool {
        self.peek().map(|p| p == s).unwrap_or(false)
    }

    /// Compares the provided string `s` to the current token.
    /// Adds matched string to the buffer if they're equal, then return true.
    /// Does nothing and returns false otherwise.
    fn r#match(&mut self, s: &str) -> bool {
        if self.check(s) {
            self.advance().unwrap();
            true
        } else {
            false
        }
    }

    /// Returns true if there is no more graphemes left.
    #[inline]
    fn is_at_end(&self) -> bool {
        self.current >= self.source.len()
    }

    #[inline]
    fn unsafe_slice(&self, start: usize, end: usize) -> &'a str {
        unsafe { concat_slices(&self.source[start..end]) }
    }
}

// Adapted from https://docs.rs/snailquote/0.3.0/x86_64-pc-windows-msvc/src/snailquote/lib.rs.html.
pub(crate) fn unescape_in_place(s: &mut String) -> Option<()> {
    let mut out = String::with_capacity(s.len());
    let mut chars = s.chars();
    while let Some(ch) = chars.next() {
        if ch == '\\' {
            if let Some(next) = chars.next() {
                let escape = match next {
                    'a' => Some('\u{07}'),
                    'b' => Some('\u{08}'),
                    'v' => Some('\u{0B}'),
                    'f' => Some('\u{0C}'),
                    'n' => Some('\n'),
                    'r' => Some('\r'),
                    't' => Some('\t'),
                    'e' | 'E' => Some('\u{1B}'),
                    'u' => Some(parse_unicode(&mut chars)?),
                    _ => None,
                };
                match escape {
                    Some(esc) => {
                        out.push(esc);
                    }
                    None => {
                        out.push(ch);
                        out.push(next);
                    }
                }
            }
        } else {
            out.push(ch);
        }
    }
    *s = out;
    Some(())
}

// Adapted from https://docs.rs/snailquote/0.3.0/x86_64-pc-windows-msvc/src/snailquote/lib.rs.html.
fn parse_unicode<I>(chars: &mut I) -> Option<char>
where
    I: Iterator<Item = char>,
{
    match chars.next() {
        Some('{') => {}
        _ => {
            return None;
        }
    }

    let unicode_seq: String = chars.take_while(|&c| c != '}').collect();

    u32::from_str_radix(&unicode_seq, 16)
        .ok()
        .and_then(char::from_u32)
}

/// Counts the number of grapheme clusters in a string.
#[inline]
pub(crate) fn number_of_graphemes(s: &str) -> usize {
    UnicodeSegmentation::graphemes(s, true).count()
}

/// Concats the given slices into a single `&str` slice.
///
/// # SAFETY
/// The slices must come from the same allocation.
unsafe fn concat_slices<'a>(slices: &[&'a str]) -> &'a str {
    if slices.is_empty() {
        return "";
    } else if slices.len() == 1 {
        return slices[0];
    }

    let mut start = slices[0];
    for slice in slices.iter().skip(1) {
        start = str_concat::concat(start, slice).unwrap();
    }
    start
}

#[allow(clippy::redundant_clone)]
#[cfg(test)]
mod tests {
    use super::*;
    use crate::{Context, SourceInfo};

    macro_rules! check_n_tokens {
        ($tokens:ident, $expected:ident) => {
            assert_eq!(
                $tokens.len(),
                $expected.len(),
                "\nInvalid number of scanned tokens.\n=====> Scanned:\n{:#?}\n=====> Expected:\n{:#?}\n",
                &$tokens,
                &$expected
            );
        };
    }

    macro_rules! check_scanned_tokens {
        ($tokens:ident, $expected:ident) => {
            for (i, (t, exp)) in $tokens.iter().zip($expected.iter()).enumerate() {
                // We don't care about the module
                let mut exp = exp.clone();
                exp.module = t.module;
                assert_eq!(
                    *t,
                    exp,
                    "\n[Token #{}] Incorrectly parsed token.\n=====> Scanned:\n{:#?}\n=====> Expected:\n{:#?}\n",
                    i + 1,
                    $tokens,
                    $expected
                );
            }
        }
    }

    macro_rules! make_state {
        ($source:expr) => {{
            let ctx = Context::default().into_shared();
            let source = ctx
                .write_shared()
                .add_module_and_get_source(SourceInfo::test_info($source));
            (ctx, source.as_ref())
        }};

        ($ctx:ident, $source:expr) => {{
            let source = $ctx
                .write_shared()
                .add_module_and_get_source(SourceInfo::test_info($source));
            source.as_ref()
        }};
    }

    #[test]
    fn test_identifier_parsing() {
        let (ctx, source) = make_state!("_ident Ident 🙈 😱😂😂😭👌");
        let expected = vec![
            Token::new(TokenKind::Identifier, "_ident", 1, 1),
            Token::new(TokenKind::Identifier, "Ident", 1, 8),
            Token::new(TokenKind::Identifier, "🙈", 1, 14),
            Token::new(TokenKind::Identifier, "😱😂😂😭👌", 1, 16),
            Token::eof(1, 21),
        ];

        let scanner = Scanner::new(ctx.clone());
        let (_hash, tokens) = scanner.scan(source, ModuleId::anon()).unwrap();
        check_n_tokens!(tokens, expected);
        check_scanned_tokens!(tokens, expected);
    }

    #[test]
    fn test_literal_parsing() {
        let (ctx, source) = make_state!("1 0. 2.5 \"str\" true false nil print ident fun");
        let expected = vec![
            Token::new(TokenKind::IntegerLiteral, "1", 1, 1),
            Token::new(TokenKind::FloatLiteral, "0.", 1, 3),
            Token::new(TokenKind::FloatLiteral, "2.5", 1, 6),
            Token::new(TokenKind::StringLiteral, "\"str\"", 1, 10),
            Token::new(TokenKind::True, "true", 1, 16),
            Token::new(TokenKind::False, "false", 1, 21),
            Token::new(TokenKind::Nil, "nil", 1, 27),
            Token::new(TokenKind::Print, "print", 1, 31),
            Token::new(TokenKind::Identifier, "ident", 1, 37),
            Token::new(TokenKind::Fun, "fun", 1, 43),
            Token::eof(1, 46),
        ];

        let scanner = Scanner::new(ctx.clone());
        let (_hash, tokens) = scanner.scan(source, ModuleId::anon()).unwrap();
        check_n_tokens!(tokens, expected);
        check_scanned_tokens!(tokens, expected);
    }

    #[test]
    fn test_multiline_string_parsing() {
        let (ctx, source) = make_state!(
            r#"s + "t
t
t" + r"#
        );

        let expected = vec![
            Token::new(TokenKind::Identifier, "s", 1, 1),
            Token::new(TokenKind::Plus, "+", 1, 3),
            Token::with_span(
                TokenKind::StringLiteral,
                "\"t\nt\nt\"",
                Span {
                    line: 1,
                    column: 5,
                    length: 0,
                    end_span: Some((3, 3, 0)),
                },
            ),
            Token::new(TokenKind::Plus, "+", 3, 5),
            Token::new(TokenKind::Identifier, "r", 3, 7),
            Token::eof(3, 8),
        ];

        let scanner = Scanner::new(ctx.clone());
        let (_hash, tokens) = scanner.scan(source, ModuleId::anon()).unwrap();
        check_n_tokens!(tokens, expected);
        check_scanned_tokens!(tokens, expected);
    }

    #[test]
    fn test_extra_integer_literals() {
        let (ctx, source) = make_state!(
            "1_000_000 + 0xDEAD_BABE + 0b1111_1111_1111_1111 8____________________________________1"
        );
        let expected = vec![
            Token::new(TokenKind::IntegerLiteral, "1_000_000", 1, 1),
            Token::new(TokenKind::Plus, "+", 1, 11),
            Token::new(TokenKind::HexadecimalLiteral, "0xDEAD_BABE", 1, 13),
            Token::new(TokenKind::Plus, "+", 1, 25),
            Token::new(TokenKind::BinaryLiteral, "0b1111_1111_1111_1111", 1, 27),
            Token::new(
                TokenKind::IntegerLiteral,
                "8____________________________________1",
                1,
                49,
            ),
            Token::eof(1, 87),
        ];

        let scanner = Scanner::new(ctx.clone());
        let (_hash, tokens) = scanner.scan(source, ModuleId::anon()).unwrap();
        check_n_tokens!(tokens, expected);
        check_scanned_tokens!(tokens, expected);
    }

    #[test]
    fn test_empty_extra_literals_are_scanned_correctly() {
        let (ctx, source) = make_state!("const empty = 0x;\nconst empty = 0b;\n");
        let expected = vec![
            // Hex literal
            Token::new(TokenKind::Const, "const", 1, 1),
            Token::new(TokenKind::Identifier, "empty", 1, 7),
            Token::new(TokenKind::Equal, "=", 1, 13),
            Token::new(TokenKind::HexadecimalLiteral, "0x", 1, 15),
            Token::new(TokenKind::Semicolon, ";", 1, 17),
            // Binary literal
            Token::new(TokenKind::Const, "const", 2, 1),
            Token::new(TokenKind::Identifier, "empty", 2, 7),
            Token::new(TokenKind::Equal, "=", 2, 13),
            Token::new(TokenKind::BinaryLiteral, "0b", 2, 15),
            Token::new(TokenKind::Semicolon, ";", 2, 17),
            Token::eof(3, 1),
        ];

        let scanner = Scanner::new(ctx.clone());
        let (_hash, tokens) = scanner.scan(source, ModuleId::anon()).unwrap();
        check_n_tokens!(tokens, expected);
        check_scanned_tokens!(tokens, expected);
    }

    #[test]
    fn test_extra_integer_literals_do_not_allow_floating_point() {
        let (ctx, source) = make_state!("0x0000.233");
        let scanner = Scanner::new(ctx.clone());
        let err = scanner.scan(source, ModuleId::anon()).unwrap_err();
        assert_eq!(
            err.msg,
            "Hexadecimal and binary float literals are not supported"
        );

        let source = make_state!(ctx, "0b1010.8199999182");
        let scanner = Scanner::new(ctx.clone());
        let err = scanner.scan(source, ModuleId::anon()).unwrap_err();
        assert_eq!(
            err.msg,
            "Hexadecimal and binary float literals are not supported"
        );

        let source = make_state!(ctx, "0bE+5");
        let scanner = Scanner::new(ctx.clone());
        let err = scanner.scan(source, ModuleId::anon()).unwrap_err();
        assert_eq!(
            err.msg,
            "Hexadecimal and binary float literals are not supported"
        );
    }

    #[test]
    fn test_float_parsing() {
        let (ctx, source) = make_state!("0.+1-3.");
        let expected = vec![
            Token::new(TokenKind::FloatLiteral, "0.", 1, 1),
            Token::new(TokenKind::Plus, "+", 1, 3),
            Token::new(TokenKind::IntegerLiteral, "1", 1, 4),
            Token::new(TokenKind::Minus, "-", 1, 5),
            Token::new(TokenKind::FloatLiteral, "3.", 1, 6),
            Token::eof(1, 8),
        ];

        let scanner = Scanner::new(ctx.clone());
        let (_hash, tokens) = scanner.scan(source, ModuleId::anon()).unwrap();
        check_n_tokens!(tokens, expected);
        check_scanned_tokens!(tokens, expected);
    }

    #[test]
    fn test_extra_float_literals() {
        let (ctx, source) = make_state!("10E5 5e+5 3E-10 321.1239E+3");
        let expected = vec![
            Token::new(TokenKind::FloatLiteral, "10E5", 1, 1),
            Token::new(TokenKind::FloatLiteral, "5e+5", 1, 6),
            Token::new(TokenKind::FloatLiteral, "3E-10", 1, 11),
            Token::new(TokenKind::FloatLiteral, "321.1239E+3", 1, 17),
            Token::eof(1, 28),
        ];

        let scanner = Scanner::new(ctx.clone());
        let (_hash, tokens) = scanner.scan(source, ModuleId::anon()).unwrap();
        check_n_tokens!(tokens, expected);
        check_scanned_tokens!(tokens, expected);
    }

    #[test]
    fn test_extra_float_literals_require_at_least_on_digit_after_exponent() {
        let (ctx, source) = make_state!("10.2312e");
        let scanner = Scanner::new(ctx.clone());
        let err = scanner.scan(source, ModuleId::anon()).unwrap_err();
        assert_eq!(err.msg, "Expected a digit after the exponent");

        let source = make_state!(ctx, "-3E+");
        let scanner = Scanner::new(ctx.clone());
        let err = scanner.scan(source, ModuleId::anon()).unwrap_err();
        assert_eq!(err.msg, "Expected a digit after the exponent");
    }

    #[test]
    fn test_lexer() {
        let (ctx, source) = make_state!("1 + 2");
        let expected = vec![
            Token::new(TokenKind::IntegerLiteral, "1", 1, 1),
            Token::new(TokenKind::Plus, "+", 1, 3),
            Token::new(TokenKind::IntegerLiteral, "2", 1, 5),
            Token::eof(1, 6),
        ];

        let scanner = Scanner::new(ctx.clone());
        let (_hash, tokens) = scanner.scan(source, ModuleId::anon()).unwrap();
        check_n_tokens!(tokens, expected);
        check_scanned_tokens!(tokens, expected);
    }

    #[test]
    fn test_comments_are_ignored() {
        let (ctx, source) = make_state!(r"3 //// var b = g(10 + f());");
        let expected = vec![
            Token::new(TokenKind::IntegerLiteral, "3", 1, 1),
            Token::eof(1, 28),
        ];

        let scanner = Scanner::new(ctx.clone());
        let (_hash, tokens) = scanner.scan(source, ModuleId::anon()).unwrap();
        check_n_tokens!(tokens, expected);
        check_scanned_tokens!(tokens, expected);
    }

    #[test]
    fn test_multiline_comments_arg_ignored() {
        let (ctx, source) = make_state!(
            "var a = /* true + false / 0. - f(n\n\
                + g()) */ 10;"
        );
        let expected = vec![
            Token::new(TokenKind::Var, "var", 1, 1),
            Token::new(TokenKind::Identifier, "a", 1, 5),
            Token::new(TokenKind::Equal, "=", 1, 7),
            Token::new(TokenKind::IntegerLiteral, "10", 2, 11),
            Token::new(TokenKind::Semicolon, ";", 2, 13),
            Token::eof(2, 14),
        ];

        let scanner = Scanner::new(ctx.clone());
        let (_hash, tokens) = scanner.scan(source, ModuleId::anon()).unwrap();
        check_n_tokens!(tokens, expected);
        check_scanned_tokens!(tokens, expected)
    }
}
