#[macro_use]
pub(crate) mod macros;
pub mod deep_size_of;
pub mod linked_list_allocator;
pub mod tagged_pointer;

use crate::vm::object::LoxObject;

pub use self::deep_size_of::DeepSizeOf;
pub use self::tagged_pointer::TaggedPointer;

pub type Allocator = self::linked_list_allocator::LinkedListAllocator<LoxObject>;

pub trait LoxAllocator<T>: std::fmt::Debug {
    fn new() -> Self;
    fn alloc(&mut self, obj: T, roots: impl Iterator<Item = TaggedPointer<T>>) -> TaggedPointer<T>;
    fn alloc_without_gc_trigger(&mut self, obj: T) -> TaggedPointer<T>;
    fn collect_garbage(&mut self, roots: impl Iterator<Item = TaggedPointer<T>>);
    /// TODO: get rid of this
    /// This method is required because some collections are allocated using the system allocator.
    fn bump_allocated_bytes(&mut self, amount: isize);
}

/// Yoinked from https://docs.rs/separator/
#[allow(unused)]
pub(crate) fn separated_usize(u: usize) -> String {
    let string = format!("{}", u);
    let mut output = String::new();

    let mut place = string.len();
    let mut later_loop = false;

    for ch in string.chars() {
        if later_loop && place % 3 == 0 {
            output.push(',');
        }

        output.push(ch);
        later_loop = true;
        place -= 1;
    }

    output
}

#[allow(unused)]
pub(crate) fn separated_isize(i: isize) -> String {
    format!(
        "{}{}",
        if i < 0 { "-" } else { "" },
        separated_usize(i.unsigned_abs())
    )
}
