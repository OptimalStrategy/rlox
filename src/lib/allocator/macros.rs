/// From <https://docs.rs/static_assertions>.
#[macro_export]
macro_rules! const_assert {
    ($x:expr $(,)?) => {
        #[allow(unknown_lints, clippy::eq_op)]
        const _: [(); 0 - !{
            const ASSERT: bool = $x;
            ASSERT
        } as usize] = [];
    };
    ($left:expr, $right:expr) => {
        const_assert!($left == $right);
    };
}

#[macro_export]
macro_rules! gc_debug {
    (stats | unsafe $block:block) => {
        #[cfg(any(feature = "gc-debug", feature = "gc-stats-only"))]
        unsafe {
            $block
        }
    };
    (stats | $block:block) => {
        #[cfg(any(feature = "gc-debug", feature = "gc-stats-only"))]
        {
            $block
        }
    };
    (stats | $fmt:tt, $( $frag:expr ),* ) => {
        #[cfg(any(feature = "gc-debug", feature = "gc-stats-only"))]
        {
            println!($fmt, $($frag),*);
        }
    };
    (stats | $msg:tt) => {
        #[cfg(any(feature = "gc-debug", feature = "gc-stats-only"))]
        {
            println!($msg);
        }
    };
    (unsafe $block:block) => {
        #[cfg(feature = "gc-debug")]
        unsafe {
            $block
        }
    };
    ($block:block) => {
        #[cfg(feature = "gc-debug")]
        {
            $block
        }
    };
    ($fmt:tt, $( $frag:expr ),* ) => {
        #[cfg(feature = "gc-debug")]
        {
            println!($fmt, $($frag),*);
        }
    };
}
