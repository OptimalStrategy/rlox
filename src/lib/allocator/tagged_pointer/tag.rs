use super::PtrTag;

/// XXX: decide if this should be a value or a flag
#[derive(Debug, Clone, Copy, PartialEq)]
#[repr(u16)]
pub enum Tag {
    /// 00000000
    Untagged = 0,
    /// 00000010
    Remembered = 1,
    /// 00000100
    CCO = 2,
    /// 00001000
    NativeCCO = 4,
    /// 00010000
    Rest = 8,
}

impl PtrTag for Tag {
    // TODO: make this fast
    #[inline]
    fn from_bits(bits: u16) -> Self {
        if bits == Tag::Untagged as u16 {
            Tag::Untagged
        } else if bits == Tag::Remembered as u16 {
            Tag::Remembered
        } else if bits == Tag::CCO as u16 {
            Tag::CCO
        } else if bits == Tag::NativeCCO as u16 {
            Tag::NativeCCO
        } else {
            Tag::Rest
        }
    }

    #[inline(always)]
    fn into_bits(self) -> u16 {
        self as u16
    }
}

#[cfg(test)]
mod test {}
