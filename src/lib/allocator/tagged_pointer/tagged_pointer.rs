use std::marker::PhantomData;

use super::{PtrTag, POINTER_WIDTH, TAG_MASK};

pub struct TaggedPointer<T, Tag = super::tag::Tag>
where
    Tag: PtrTag,
{
    ptr: *mut T,
    pd: PhantomData<Tag>,
}

impl<T, Tag> TaggedPointer<T, Tag>
where
    Tag: PtrTag,
{
    /// Creates a tagged pointer from a mutable raw pointer.
    ///
    /// # Safety
    /// The wrapper provides the `as_ref()` and `as_mut()` methods which dereference
    /// the wrapped pointer. This operation is unsafe and therefore it is the caller's responsibility
    /// to make sure that the pointer is valid.
    #[inline]
    pub unsafe fn new(ptr: *mut T) -> Self {
        Self {
            ptr,
            pd: PhantomData,
        }
    }

    /// Creates a tagged pointer from a mutable reference.
    ///
    /// # Safety
    /// The wrapper provides the `as_ref()` and `as_mut()` methods which dereference
    /// the wrapped pointer. This operation is unsafe and therefore it is the caller's responsibility
    /// to make sure that the pointer is valid.
    pub unsafe fn from_ref(r#ref: &mut T) -> Self {
        Self::new(r#ref as *mut T)
    }

    #[inline(always)]
    pub fn as_masked_usize(&self) -> usize {
        self.ptr as usize & TAG_MASK
    }

    #[inline(always)]
    pub fn as_ptr(&self) -> *const T {
        self.as_masked_usize() as _
    }

    #[inline(always)]
    pub fn as_mut_ptr(&mut self) -> *mut T {
        self.as_masked_usize() as _
    }

    #[inline(always)]
    pub fn as_usize(&self) -> usize {
        self.ptr as usize
    }

    /// Dereferences the inner (raw) pointer and returns a reference to the underlying type.
    ///
    /// # Safety
    /// Calling this function is equivalent to dereferencing the inner pointer,
    /// which may be invalid.
    #[allow(clippy::should_implement_trait)]
    #[inline(always)]
    pub fn as_ref(&self) -> &T {
        unsafe { &*(self.as_ptr()) }
    }

    /// Dereferences the inner (raw) pointer and returns a mutable reference to the underlying type.
    ///
    /// # Safety
    /// Calling this function is equivalent to dereferencing the inner pointer,
    /// which may be invalid.
    #[allow(clippy::should_implement_trait)]
    #[inline(always)]
    pub fn as_mut(&mut self) -> &mut T {
        unsafe { &mut *(self.as_mut_ptr()) }
    }

    pub fn set_tag(&mut self, tag: Tag) {
        self.ptr = (self.as_masked_usize() | ((tag.into_bits() as usize) << POINTER_WIDTH)) as _;
    }

    pub fn clear_tag(&mut self) -> Tag {
        let tag = self.tag();
        self.ptr = self.as_mut_ptr();
        tag
    }

    #[inline(always)]
    pub fn tag_u16(&self) -> u16 {
        (self.as_usize() >> POINTER_WIDTH) as u16
    }

    #[inline]
    pub fn tag(&self) -> Tag {
        Tag::from_bits(self.tag_u16())
    }
}

impl<T, Tag> PartialEq for TaggedPointer<T, Tag>
where
    Tag: PtrTag,
{
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.ptr.eq(&other.ptr)
    }
}

impl<T, Tag> Eq for TaggedPointer<T, Tag> where Tag: PtrTag {}

impl<T, Tag> std::hash::Hash for TaggedPointer<T, Tag>
where
    Tag: PtrTag,
{
    #[inline]
    fn hash<H>(&self, hasher: &mut H)
    where
        H: std::hash::Hasher,
    {
        self.as_masked_usize().hash(hasher);
    }
}

impl<T, Tag> Copy for TaggedPointer<T, Tag> where Tag: PtrTag + Copy {}

impl<T, Tag> Clone for TaggedPointer<T, Tag>
where
    Tag: PtrTag,
{
    #[inline(always)]
    fn clone(&self) -> Self {
        unsafe { Self::new(self.ptr) }
    }
}

impl<T, Tag> std::fmt::Debug for TaggedPointer<T, Tag>
where
    Tag: PtrTag + std::fmt::Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        if f.alternate() {
            write!(
                f,
                "TaggedPointer {{\n    ptr = {:#p},\n    tag = {:?}\n}}",
                self.as_ptr(),
                self.tag()
            )
        } else {
            write!(f, "TaggedPointer {{ ptr = {:#p} }}", self.as_ptr())
        }
    }
}

impl<T, Tag> std::fmt::Display for TaggedPointer<T, Tag>
where
    Tag: PtrTag + std::fmt::Debug,
    T: std::fmt::Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "TaggedPointer {{ {:#?} @ {:#p} ({:?}) }}",
            self.as_ref(),
            self.as_ptr(),
            self.tag()
        )
    }
}

#[cfg(test)]
mod tests {
    use super::super::tag::Tag;
    use super::*;

    #[test]
    fn test_tagged_pointer() {
        let mut obj = Box::new(32);
        let mut ptr = unsafe { TaggedPointer::new(&mut *obj) };
        let initial_value = ptr.as_masked_usize();
        println!("ptr(usize):  {:#x}", initial_value);
        println!("ptr(*const): {:#p}", ptr.as_ptr());

        // Test an untagged tagged pointer
        assert_eq!(ptr.tag_u16(), Tag::Untagged as u16);
        assert_eq!(ptr.tag(), Tag::Untagged);
        assert_eq!(ptr.as_ref(), &32);
        *ptr.as_mut() = 55;
        assert_eq!(ptr.as_ref(), &55);
        assert_eq!(*obj, 55);

        // Test a tagged tagged pointer
        ptr.set_tag(Tag::Remembered);
        ptr.tag();
        assert_eq!(ptr.as_masked_usize(), initial_value);
        assert_eq!(ptr.tag_u16(), Tag::Remembered as u16);
        assert_eq!(ptr.tag(), Tag::Remembered);
        assert_eq!(ptr.as_ref(), &55);
    }

    #[test]
    fn test_masked_pointer_matches_the_original_one() {
        let mut data = Box::new(42);
        let mut ptr = unsafe { TaggedPointer::<i32, U16Tag>::from_ref(&mut *data) };
        assert_eq!(&mut *data as *mut i32 as usize, ptr.as_masked_usize());
        assert_eq!(&mut *data as *mut i32, ptr.as_mut_ptr());
    }

    #[test]
    fn test_tagged_pointer_arbitrary_u16() {
        let mut data = Box::new(42);
        let mut ptr = unsafe { TaggedPointer::<i32, U16Tag>::from_ref(&mut *data) };

        ptr.set_tag(U16Tag::Untagged);
        assert_eq!(ptr.tag(), U16Tag::Untagged);
        assert_eq!(ptr.tag_u16(), 0);
        assert_eq!(ptr.as_ref(), &42);

        ptr.set_tag(U16Tag::Tag1);
        assert_eq!(ptr.tag(), U16Tag::Tag1);
        assert_eq!(ptr.tag_u16(), 1);
        assert_eq!(ptr.as_ref(), &42);

        ptr.set_tag(U16Tag::Value(1024));
        assert_eq!(ptr.tag(), U16Tag::Value(1024));
        assert_eq!(ptr.tag_u16(), 1024);
        assert_eq!(ptr.as_ref(), &42);

        assert_eq!(ptr.clear_tag(), U16Tag::Value(1024));
        assert_eq!(ptr.tag(), U16Tag::Untagged);

        assert_eq!(ptr.as_ref(), &42);
    }

    #[derive(Debug, Clone, PartialEq)]
    enum U16Tag {
        Untagged,
        Tag1,
        Value(u16),
    }

    impl PtrTag for U16Tag {
        fn from_bits(bits: u16) -> Self {
            match bits {
                0 => Self::Untagged,
                1 => Self::Tag1,
                _ => Self::Value(bits),
            }
        }

        fn into_bits(self) -> u16 {
            match self {
                Self::Untagged => 0,
                Self::Tag1 => 1,
                Self::Value(bits) => bits,
            }
        }
    }
}
