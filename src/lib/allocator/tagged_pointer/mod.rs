pub mod tag;
pub mod tagged_pointer;

pub const POINTER_WIDTH: usize = 48;
pub const TAG_MASK: usize = usize::max_value() >> (64 - POINTER_WIDTH);

pub use self::tagged_pointer::TaggedPointer;

pub trait PtrTag: Clone {
    fn from_bits(bits: u16) -> Self;
    fn into_bits(self) -> u16;
}
