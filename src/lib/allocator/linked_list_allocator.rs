use std::{
    collections::{LinkedList, VecDeque},
    sync::atomic::AtomicUsize,
};

use crate::allocator::DeepSizeOf;

#[allow(unused)]
use super::{separated_isize, separated_usize};
use super::{tagged_pointer::TaggedPointer, LoxAllocator};
use crate::vm::object::{LoxObject, ObjectPointer, Reachability};

/// The threshold before the next garbage collection in bytes.
pub const NEXT_GC_INITIAL_THRESHOLD: usize = 1024 * 1024;
/// The minimum heap size to kick off the gc.
pub const MIN_GC_THRESHOLD: usize = 1024 * 1024;
pub const HEAP_GROWTH_FACTOR: f64 = 2.0;
pub const MIN_MEM_HITS_TO_GROW: usize = 10;

thread_local! {
    pub(crate) static LOX_GC_SYSTEM_ALLOC_BYTES: AtomicUsize = AtomicUsize::new(0);
}

#[derive(Debug)]
pub struct LinkedListAllocator<T> {
    worklist: VecDeque<ObjectPointer>,
    objects: LinkedList<T>,
    bytes_allocated: usize,
    next_gc: usize,
    min_mem_hits: usize,
    multiplier: usize,
}

impl LoxAllocator<LoxObject> for LinkedListAllocator<LoxObject> {
    fn new() -> Self {
        Self {
            worklist: VecDeque::with_capacity(MIN_GC_THRESHOLD / std::mem::size_of::<LoxObject>()),
            objects: LinkedList::new(),
            bytes_allocated: 0,
            min_mem_hits: 0,
            multiplier: 1,
            next_gc: NEXT_GC_INITIAL_THRESHOLD,
        }
    }

    fn alloc(
        &mut self,
        mut obj: LoxObject,
        stack: impl Iterator<Item = ObjectPointer>,
    ) -> ObjectPointer {
        let obj_size = obj.deep_size_of();
        gc_debug!(
            "[Alloc 0] Attempting to alloc a new object: {:<16} size = {}b (Heap = {}b, thresh = {}b)",
            format!("{:?}", obj),
            obj_size,
            separated_usize(self.bytes_allocated),
            separated_usize(self.next_gc)
        );
        self.bytes_allocated += obj_size;
        if self.bytes_allocated > self.next_gc {
            gc_debug!(
                stats | "[Alloc  ] Heap threshold has been exceeded {}b > {}b, executing the GC",
                separated_usize(self.bytes_allocated),
                separated_usize(self.next_gc)
            );
            // Make sure to trace the pointers in the object we are currently allocating.
            self.collect_garbage(stack.chain(obj.contained_pointers().map(|ptr| *ptr)));
        }
        self.alloc_without_gc_trigger(obj)
    }

    fn alloc_without_gc_trigger(&mut self, obj: LoxObject) -> ObjectPointer {
        gc_debug!(
            "[Alloc 1] Allocating a new object:          {:<16} size = {}b (Heap = {}b, thresh = {}b)",
            format!("{:?}", obj),
            obj.deep_size_of(),
            separated_usize(self.bytes_allocated),
            separated_usize(self.next_gc)
        );
        self.objects.push_back(obj);
        unsafe { TaggedPointer::from_ref(self.objects.back_mut().unwrap()) }
    }

    fn collect_garbage(&mut self, roots: impl Iterator<Item = ObjectPointer>) {
        gc_debug!(
            stats | "[GC     ] Starting the garbage collector (heap size: {}b)",
            separated_usize(self.bytes_allocated)
        );
        let _before = self.bytes_allocated;
        self.trace(roots);
        self.next_gc =
            ((self.bytes_allocated as f64 * HEAP_GROWTH_FACTOR) as usize).max(MIN_GC_THRESHOLD);

        if self.next_gc == MIN_GC_THRESHOLD {
            self.min_mem_hits += 1;
        } else {
            self.min_mem_hits = 0;
        }

        if self.min_mem_hits > MIN_MEM_HITS_TO_GROW {
            self.multiplier = (self.multiplier + 1).min(4).max(1);
            self.next_gc =
                (self.next_gc as f64 * HEAP_GROWTH_FACTOR * self.multiplier as f64) as usize;
            self.min_mem_hits = 0;
        }

        gc_debug!(stats |
            "[GC     ] Finished collecting garbage: collected {} bytes (from {}b to {}b; next pass at {}b)",
            _before.checked_sub(self.bytes_allocated)
                .map(separated_usize)
                .unwrap_or_else(|| String::from("<ERROR: UNTRACKED ALLOCATIONS>")),
            separated_usize(_before),
            separated_usize(self.bytes_allocated),
            separated_usize(self.next_gc)
        );
    }

    #[inline]
    fn bump_allocated_bytes(&mut self, amount: isize) {
        crate::gc_debug!(
            "[Alloc B] Bumping bytes_allocated by {}b (from {}b to {}b)",
            amount,
            separated_usize(self.bytes_allocated),
            separated_isize(self.bytes_allocated as isize + amount)
        );
        self.bytes_allocated = (self.bytes_allocated as isize + amount).max(0) as usize;
    }
}

impl LinkedListAllocator<LoxObject> {
    fn trace(&mut self, roots: impl Iterator<Item = ObjectPointer>) {
        for mut root in roots {
            if !root.as_ref().is_reachable() {
                root.as_mut().reachable = Reachability::Reachable;
                gc_debug! {
                    {
                        println!(
                            "[Trace  ] Tracing a root {:?} at {:#p}",
                            root.as_ref(),
                            root.as_ptr()
                       );
                    }
                };
                self.worklist.push_back(root);
                self.trace_obj();
            }
        }

        let mut garbage = vec![];
        let mut node = self.objects.cursor_front_mut();
        while let Some(obj) = node.current() {
            if !obj.is_reachable() {
                obj.reachable = Reachability::Unreachable;
                gc_debug!(
                    "[Collect] Object {:<30} at {:#p} is unreachable, marking as garbage.",
                    format!("{:?}", obj),
                    obj as *const LoxObject
                );
                // TODO: deep_size_of calls are potentially very expensive, we should look into caching them
                // FIXME: use the custom allocator API when it becomes available: https://github.com/rust-lang/wg-allocators/issues/48.
                //        At the moment, object hashmaps (and most other object-internal properties are allocated using the system allocator)
                //        which leads to the recorded allocation size not matching the actual object sizes.
                //        This leads to bad gc performance as setting a field on an object increases the memory usage of the program
                //        while not being tracked by the GC.
                //        This behavior is currently fixed by introducing a bump_allocated_bytes() method that adjusts the tracked bytes
                //        by the given amount.
                self.bytes_allocated = self.bytes_allocated.saturating_sub(obj.deep_size_of());
                garbage.push(node.remove_current().unwrap());
            } else {
                gc_debug!(
                    "[Collect] Object {:<30} at {:#p} is reachable.",
                    format!("{:?}", obj),
                    obj as *const LoxObject
                );
                obj.reachable = Reachability::Undetermined;
                node.move_next()
            }
        }

        gc_debug!("[Collect] Marked {} objects as garbage.", garbage.len());
    }

    pub fn trace_obj(&mut self) {
        while let Some(mut obj) = self.worklist.pop_front() {
            for field in obj.as_mut().contained_pointers() {
                gc_debug!(
                    "{}[Trace  ] Object `{:?}` at {:#p}",
                    " ".repeat(2),
                    field.as_ref(),
                    field.as_ptr()
                );
                if !field.as_ref().is_reachable() {
                    gc_debug!(
                        "{}[Trace  ] Object {:<32} at {:#p} was unreachable, marking as reachable and tracing its fields.",
                        " ".repeat(2),
                        format!("`{:?}`", field.as_ref()),
                        field.as_ptr()
                    );
                    field.as_mut().reachable = Reachability::Reachable;
                    self.worklist.push_back(*field);
                }
            }
            gc_debug!(
                "{}[Trace  ] Finished tracing the object `{:?}` at {:#p}",
                " ".repeat(2),
                field.as_ref(),
                field.as_ptr()
            );
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::allocator::Allocator;
    use crate::vm::{
        object::{object::ClassObject, Object},
        Value,
    };

    macro_rules! set_field {
        ($alloc:ident, $obj:expr, $name:expr, $value:expr) => {{
            let obj = $obj;
            let size_before = obj.fields_alloc_size() as isize;
            obj.set_field($name, $value);
            let size_after = obj.fields_alloc_size() as isize;
            $alloc.bump_allocated_bytes(size_after - size_before);
        }};
    }

    macro_rules! new_obj {
        ($alloc:ident, $stack:ident, $name:tt) => {{
            let class = crate::vm::heap_value::HeapValue::Cls(Box::new(
                ClassObject::empty_with_name($name, $crate::module::ModuleId::anon()),
            ));
            let class = $alloc.alloc(LoxObject::new(class), $stack.iter().cloned());
            let obj = Object::with_class(class);
            let obj_ptr = $alloc.alloc(obj.into_object(), $stack.iter().cloned());
            obj_ptr
        }};
    }

    #[test]
    fn test_gc() {
        let mut a = Allocator::new();
        let mut stack = vec![];

        let ham_ptr = new_obj!(a, stack, "ham");
        stack.push(ham_ptr);

        let bread_ptr = new_obj!(a, stack, "bread");
        stack.push(bread_ptr);

        let cheese_ptr = new_obj!(a, stack, "cheese");
        stack.push(cheese_ptr);

        let mut sandwich_ptr = new_obj!(a, stack, "ham_sandwich_with_cheese");
        set_field!(
            a,
            sandwich_ptr.as_mut().as_object_mut(),
            "extras",
            stack.pop().unwrap().into()
        );
        set_field!(
            a,
            sandwich_ptr.as_mut().as_object_mut(),
            "base",
            stack.pop().unwrap().into()
        );
        set_field!(
            a,
            sandwich_ptr.as_mut().as_object_mut(),
            "protein",
            stack.pop().unwrap().into()
        );

        stack.push(sandwich_ptr);
        a.collect_garbage(stack.iter().cloned());

        let mut jam_mix_ptr = new_obj!(a, stack, "jam_mix");
        stack.push(jam_mix_ptr);
        set_field!(
            a,
            jam_mix_ptr.as_mut().as_object_mut(),
            "jam_1",
            Value::Ptr(new_obj!(a, stack, "strawberry_jam"))
        );
        set_field!(
            a,
            jam_mix_ptr.as_mut().as_object_mut(),
            "jam_2",
            Value::Ptr(new_obj!(a, stack, "raspberry_jam"))
        );

        let sesame_bun_ptr = new_obj!(a, stack, "sesame_bun");
        stack.push(sesame_bun_ptr);

        let loop_ptr = new_obj!(a, stack, "loop");
        stack.push(loop_ptr);

        {
            let mut r#loop = stack.pop().unwrap();
            let loop2 = r#loop;
            set_field!(a, r#loop.as_mut().as_object_mut(), "self", loop2.into());
        }
        {
            let bun = stack.pop().unwrap();
            set_field!(a, stack[0].as_mut().as_object_mut(), "base", bun.into());
            let extras = stack.pop().unwrap();
            set_field!(
                a,
                stack[0].as_mut().as_object_mut(),
                "extras",
                extras.into()
            );
        }
        a.collect_garbage(stack.iter().cloned());
        assert_eq!(true, true, "{:#?}", a.objects);
        assert_eq!(
            a.bytes_allocated,
            if cfg!(feature = "lox-64bit-ints") {
                1010 - 48
            } else {
                1010
            }
        );
        assert_eq!(a.next_gc, MIN_GC_THRESHOLD);
        assert_eq!(a.objects.len(), 12);
        assert_eq!(
            a.objects.iter().nth(2).unwrap().as_class().name,
            "ham_sandwich_with_cheese"
        );
    }
}
