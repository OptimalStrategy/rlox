//! Adapted from <https://github.com/Aeledfyr/deepsize>.

/// This trait was adapted from <https://github.com/Aeledfyr/deepsize> to incorporate
/// RLox VM-specific optimizations, mainly, by eliminating the Context. Lox's objects
/// don't store references or reference-counted pointers in them and thus the only possible allocations
/// are from Strings, Vec's, and HashMaps that own either [`ObjectPointer`]s or [`Values`]. This fact
/// renders the Context redundant.
pub trait DeepSizeOf {
    fn deep_size_of(&self) -> usize {
        std::mem::size_of_val(self) + self.deep_size_of_children()
    }

    fn deep_size_of_children(&self) -> usize;
}

/// Returns an approximate allocation size of a map with the given capacity.
/// The `key` and `Value` must not own memory.
#[inline(always)]
pub(crate) fn deep_size_of_map<Key, Value>(capacity: usize) -> usize {
    capacity * std::mem::size_of::<Option<(u64, Key, Value)>>()
}

/// Returns an approximate allocation size of a map that owns its key's allocations.
/// The `V` must not own memory.
#[inline(always)]
pub(crate) fn deep_size_of_map_with_owned_keys<K, V, S>(
    map: &std::collections::HashMap<K, V, S>,
) -> usize
where
    K: DeepSizeOf + Eq + std::hash::Hash,
    V: DeepSizeOf,
    S: std::hash::BuildHasher,
{
    map.keys()
        .fold(0, |sum, key| sum + key.deep_size_of_children())
        + map.capacity() * std::mem::size_of::<Option<(u64, K, V)>>()
}

/// Returns an approximate allocation size of a vec with the given capacity.
/// The `Value` must not own memory.
#[inline(always)]
pub(crate) fn deep_size_of_vec<Value>(v: &Vec<Value>) -> usize {
    v.capacity() * std::mem::size_of::<Value>()
}

/// Returns an approximate allocation size of a `Cow<'static, str>`.
#[allow(clippy::ptr_arg)]
#[inline(always)]
pub(crate) fn deep_size_of_cow(c: &std::borrow::Cow<'static, str>) -> usize {
    match c {
        std::borrow::Cow::Borrowed(_) => 0,
        std::borrow::Cow::Owned(s) => s.capacity(),
    }
}

impl DeepSizeOf for String {
    #[inline(always)]
    fn deep_size_of_children(&self) -> usize {
        self.capacity()
    }
}
