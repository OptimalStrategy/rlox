//! This module contains the implementation of the Lox REPL.
use crate::{SharedCtx, *};
use compiler::cco::Arity;
use std::io::{stdin, stdout, BufRead, Write};
use vm::Vm;

/// Runs the lox REPL. The loop is endless and will only stop if the user closes STDIN or terminates the program.
///
/// # Overview
/// The REPL works by creating a temporary compiler from the "always valid" permanent compiler
/// for every piece of input. If the input passes all stages of the pipeline, the permanent compiler is
/// replaced with the temporary compiler and the input is appended to the source code. If, however,
/// the input causes an error, the temporary compiler is destroyed and the source is left unchanged.
///
/// The same VM instance is reused for all user inputs. The VM's execution is resumed from past the
/// last instruction executed on the previous iteration.
pub fn repl(ctx: SharedCtx) -> Result<(), i32> {
    let stdin = stdin();
    let mut stdin = stdin.lock();

    let mut compiler = Compiler::new(ctx.clone());
    let dis = ctx.read_shared().config.dis;

    let (base, tmp) = {
        let mut guard = ctx.write_shared();
        let base = guard.add_module(SourceInfo::new(
            String::from("<stream>"),
            String::new(),
            String::from("<base>"),
        ));
        let tmp = guard.add_module(SourceInfo::new(
            String::from("<buffer>"),
            String::new(),
            String::from("<buffer>"),
        ));
        (base, tmp)
    };

    // The number of the statement we want to compile from.
    let mut current_statement = 0;
    let mut last_ip = 0;
    let mut n_globals;
    let mut vm = Vm::new(ctx.clone());
    let mut initialized = false;

    loop {
        let mut input = String::new();
        if initialized {
            print!("|> ");
            stdout().flush().map_err(handle_io)?;
            stdin.read_line(&mut input).map_err(handle_io)?;
            input = input.trim().to_owned();
        } else {
            input = String::from("var _ = nil;");
            initialized = true;
        }

        if input.is_empty() {
            continue;
        }

        if input.starts_with('$') {
            match &input[..] {
                "$state" => {
                    println!("{:#?}", vm.get_state());
                }
                "$source" => println!("{}", ctx.read_shared().get_module_source(base).unwrap()),
                "$tmp_source" => println!("{}", ctx.read_shared().get_module_source(tmp).unwrap()),
                "$compiler" => println!("{:#?}", compiler),
                "$dis" => println!(
                    "{}",
                    ctx.read_shared()
                        .get_module(tmp)
                        .unwrap()
                        .cco()
                        .map(Disassemble::disassemble)
                        .unwrap_or_else(|| String::from("<error: CCO unavailable>"))
                ),
                "$help" => {
                    println!("$state      - dump the vm state");
                    println!("$source     - print the base source");
                    println!("$tmp_source - print the temporary source");
                    println!("$compiler   - dump the compiler state");
                    println!("$dis        - disassemble the CCO");
                }
                rest => {
                    println!("Unknown debug command: `{}`, see $help.", rest);
                }
            }
            stdout().flush().map_err(handle_io)?;
            continue;
        }

        if !input.starts_with('$') && !input.ends_with(';') && !input.ends_with('}') {
            input = format!("print (_ = ({}));", input);
        }

        let previous = ctx.read_shared().get_module_source(base).unwrap();
        let new_source = format!("{}\n{}", previous, input);
        assert!(ctx.write_shared().set_module_source(tmp, &new_source));

        let pipeline = make_pipeline_without_compiler(&ctx);
        let cco = match pipeline.apply(tmp).and_then(|mut ast| {
            // The parse did not result in a new statement
            if ast.body.len() == current_statement {
                return Err(ErrCtx::new());
            }
            let mut temporary_compiler = compiler.clone();
            // Only compile the last statement
            ast.body = vec![ast.body.remove(current_statement)];
            let result = temporary_compiler
                .compile_in_repl_mode(ast, String::from("<stream>"), Arity::Fixed(0))
                .map(|v| {
                    ctx.read_shared().ex.report_all(ctx.clone());
                    ctx.write_shared().ex.clear();
                    v
                });
            compiler = temporary_compiler;
            result
        }) {
            Ok(cco) => cco,
            Err(ex) => {
                ctx.write_shared().ex.extend_from_another_ex(ex);
                ctx.read_shared().ex.report_all(ctx.clone());
                ctx.write_shared().ex.clear();
                continue;
            }
        };

        if dis {
            cco.as_ref().as_cco().dis_print();
        }

        // The new line is syntactically correct
        ctx.write_shared().set_module_source(base, &new_source);
        current_statement += 1;
        n_globals = compiler.n_globals();

        // Clean up the stack if needed
        vm.shrink_stack(n_globals);
        // And set the ip just past the last instruction in the CCO
        vm.set_ip(last_ip);
        // Subtract 4 to account for the removed [MakeModule, Return]
        last_ip = cco.as_ref().as_cco().n_instructions() - 4;

        Context::wait_for_workers(ctx.clone()).unwrap();
        match vm.run(cco) {
            Ok(_) => {
                // Subtract 1 from the instruction pointer to account
                // for the removed [Value, Return].
                // The value is _3_ and not _4_ because the entry-function Return
                // does not increase the ip before terminating the VM.
                last_ip = vm.get_state().ip - 3;
            }
            Err(tb) => {
                {
                    ctx.read_shared().ex.report_all(ctx.clone());
                    ctx.write_shared().ex.clear();
                }
                tb.display(ctx.clone());
            }
        }

        stdout().flush().map_err(handle_io)?;
    }
}

fn handle_io(e: std::io::Error) -> i32 {
    eprintln!("{}", e);
    1
}
