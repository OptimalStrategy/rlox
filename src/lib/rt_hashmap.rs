//! This module defines a type alias for the faster but less safe hashmap used
//! by the VM to speed up hashmap-based code.

/// A faster but less robust hashmap. Currently an `FnvHashmap`.
pub type FastHashMap<K, V> = fnv::FnvHashMap<K, V>;
