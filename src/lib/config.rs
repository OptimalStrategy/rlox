use std::{path::PathBuf, time::Duration};

pub static DEFAULT_LOX_CALL_STACK_SIZE: usize = 1024;
pub static DEFAULT_LOX_INTERNING_THRESHOLD: u8 = 20;
pub static DEFAULT_MAX_COMPILER_THREADS: usize = 8;
pub static DEFAULT_Q_SIZE_THREAD_SPAWN_THRESHOLD: usize = 10;

/// Contains the toggles for all present optimizations.
#[derive(Debug, Clone)]
pub struct OptConfig {
    /// Perform constant folding on the AST.
    pub constant_folding: bool,
    /// Perform peephole optimizations.
    pub peephole_optimizations: bool,
    /// Perform tail call optimization
    pub tail_call_optimization: bool,
    /// Perform dead code elimination on unreachable CFG regions.
    pub dead_code_elimination: bool,
}

impl OptConfig {
    /// Disables all optimizations.
    pub fn all_disabled() -> Self {
        Self {
            constant_folding: false,
            peephole_optimizations: false,
            tail_call_optimization: false,
            dead_code_elimination: false,
        }
    }

    #[inline]
    pub const fn as_u8_array(&self) -> [u8; 4] {
        [
            self.constant_folding as u8,
            self.peephole_optimizations as u8,
            self.tail_call_optimization as u8,
            self.dead_code_elimination as u8,
        ]
    }
}

impl Default for OptConfig {
    fn default() -> Self {
        Self {
            constant_folding: true,
            peephole_optimizations: true,
            tail_call_optimization: true,
            dead_code_elimination: true,
        }
    }
}

/// Contains the compiler configuration.
#[derive(Debug, Clone)]
pub struct CompilerConfig {
    /// Disables all optimizations.
    /// Individual optimizations may be enabled with their respective -C options.
    pub optimize: bool,
    /// Contains the toggles for all present optimizations.
    pub opt_config: OptConfig,
    /// Print the ast after all passes that modify the ast.
    pub print_ast: bool,
    /// Convert the AST to code after all passes that modify the ast.
    pub print_code: bool,
    /// Print full paths when reporting errors.
    pub print_full_path: bool,
    /// Do not execute the compiled code.
    pub no_runtime: bool,
    /// Disassemble the resulting CCO.
    pub dis: bool,
    /// Disassemble all CCOs.
    pub dis_all: bool,
    /// Maximum length for string interning.
    /// If a string's length exceeds the threshold, it will not be interned.
    pub interning_threshold: u8,
    /// The maximum number of call frames on the call stack.
    /// Essentially the recursion limit of the program.
    pub max_call_stack_size: usize,
    /// The directory for cache files and CFG export.
    pub build_dir: PathBuf,
    /// Whether caching (both writing and reading) is enabled.
    pub caching: bool,
    /// An optional timeout value for the Vm to stop after. Providing a non-None value will result into the instrumentation of compiled
    /// code with timeout checks inside loops and function calls (entry + return). Note that the builtins such as `sleep` will not be instrumented.
    pub execution_timeout: Option<Duration>,
    /// Whether to export the control flow graphs of the functions in each module in the ".dot" format
    pub export_cfg: bool,
    /// Whether to export the import graph of the program in the ".dot" format
    pub export_ig: bool,
    /// Whether to check if the import graph contains cycles.
    pub check_ig: bool,
    /// The maximum number of compiler threads.
    pub max_compiler_threads: usize,
    /// The number of modules in the queue required to spawn a new thread.
    pub compiler_queue_size_thread_spawn_threshold: usize,
}

impl CompilerConfig {
    pub fn disable_opts(self) -> Self {
        Self {
            opt_config: OptConfig::all_disabled(),
            ..self
        }
    }

    pub fn disable_opts_in_place(&mut self) {
        self.optimize = false;
        self.opt_config = OptConfig::all_disabled();
    }

    pub fn enable_opts_in_place(&mut self) {
        self.optimize = true;
        self.opt_config = OptConfig::default();
    }

    #[allow(clippy::useless_format)]
    pub fn description(&self) -> String {
        let rows = [
            format!("Call stack size:     {}", self.max_call_stack_size),
            format!("Interning threshold: {}", self.interning_threshold),
            format!("Optimization level:  {}", self.optimize as usize),
            format!("Optimizations:"),
            format!(
                "  Constant Folding:       {}",
                self.opt_config.constant_folding
            ),
            format!(
                "  Peephole Optimizations: {}",
                self.opt_config.peephole_optimizations
            ),
            format!(
                "  Tail Call Optimization: {}",
                self.opt_config.tail_call_optimization
            ),
            format!(
                "  Dead Code Elimination:  {}",
                self.opt_config.dead_code_elimination
            ),
            format!("(misc): cfg_export = {}", self.export_cfg),
            format!("(misc): caching    = {}", self.caching),
        ];
        crate::ast::ast_to_str::generate_message_box(&rows, Some("Config"))
    }
}

impl Default for CompilerConfig {
    fn default() -> Self {
        Self {
            optimize: true,
            opt_config: OptConfig::default(),
            print_ast: false,
            print_code: false,
            print_full_path: false,
            no_runtime: false,
            dis: false,
            dis_all: false,
            caching: true, // TODO: test this and set to true by default
            execution_timeout: None,
            export_cfg: false,
            export_ig: false,
            check_ig: true,
            build_dir: PathBuf::from("loxbuild"),
            interning_threshold: DEFAULT_LOX_INTERNING_THRESHOLD,
            max_call_stack_size: DEFAULT_LOX_CALL_STACK_SIZE,
            max_compiler_threads: DEFAULT_MAX_COMPILER_THREADS,
            compiler_queue_size_thread_spawn_threshold: DEFAULT_Q_SIZE_THREAD_SPAWN_THRESHOLD,
        }
    }
}
