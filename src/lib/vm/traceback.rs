use crate::{
    errors::{construct_error_message, format_error_message, format_with_ctx, LoxErrorKind},
    module::ModuleId,
    token::Span,
    LoxError, SharedCtx,
};

#[derive(Debug)]
pub struct TraceFrame {
    pub name: String,
    pub module: ModuleId,
    pub span: Span,
}

#[derive(Debug)]
pub struct TraceBack {
    pub error: LoxError,
    pub frames: Vec<TraceFrame>,
}

impl TraceBack {
    pub fn display(&self, ctx: SharedCtx) {
        eprintln!("{}", self.generate(ctx));
    }

    pub fn generate(&self, ctx: SharedCtx) -> String {
        let mut tb = String::from("Error traceback (most recent call last):");

        let guard = ctx.read_shared();
        let root = guard.root.as_path();
        for frame in self.frames.iter().skip(1).rev() {
            let source = guard.get_module_source_info(frame.module).unwrap();
            let msg = format_error_message(
                &source.source,
                &format!(
                    "==> File '{}':\n",
                    crate::errors::get_filename(&source.filename, root)
                )[..],
                Some(&frame.name[..]),
                frame.span,
                LoxErrorKind::Traceback,
                "",
                &construct_error_message(&frame.span, &source.source),
            );
            tb.push('\n');
            tb.push_str(&msg[..]);
        }

        tb.push('\n');

        tb.push_str(&format_with_ctx(
            ctx.clone(),
            &self.error,
            Some(&self.frames[0].name[..]),
        ));

        tb
    }
}
