use allocator::tagged_pointer::tagged_pointer::TaggedPointer;
use compiler::bytecode::Bytecode;

use compiler::cco::CompiledCodeObject;
use compiler::chunk::Chunk;
use vm::object::ObjectPointer;

use crate::{allocator::tagged_pointer::tag::Tag, compiler::bytecode::Inst};

static _EMPTY_BYTECODE: Vec<u8> = vec![];

#[derive(Debug, Clone)]
pub struct CallFrame {
    cco: ObjectPointer,
    upvalues: *const ObjectPointer,
    n_upvalues: usize,
    chunk: TaggedPointer<Chunk>,
    bytecode: *const Inst,
    bytecode_len: usize,
    pub(crate) stack_window_index: usize,
    pub(crate) return_address: usize,
}

impl CallFrame {
    pub fn new(mut cco: ObjectPointer, stack_window_index: usize, return_address: usize) -> Self {
        // At this point, the chunks are immutable so aliasing should be okay.
        let mut chunk = unsafe { TaggedPointer::from_ref(&mut cco.as_mut().as_cco_mut().chunk) };
        let (bytecode, bytecode_len) = match chunk.as_mut().bytecode {
            Bytecode::Decoded(ref mut raw) => (raw.as_ptr(), raw.len()),
            Bytecode::Encoded(_) => {
                panic!("The bytecode must be lowered to bytes to be executed.")
            }
            Bytecode::_Empty => (_EMPTY_BYTECODE.as_ptr() as *const _, 0),
        };
        let (upvalues, n_upvalues) = match cco.as_mut().as_heap_value_mut() {
            super::HeapValue::CCO(_) => (std::ptr::null(), 0),
            super::HeapValue::Closure { upvalues, .. } => ((*upvalues).as_ptr(), upvalues.len()),
            _ => unreachable!(),
        };
        Self {
            cco,
            chunk,
            upvalues,
            n_upvalues,
            bytecode,
            bytecode_len,
            stack_window_index,
            return_address,
        }
    }

    #[cfg(any(not(feature = "unchecked-vm"), feature = "trace-lox-calls"))]
    #[inline(always)]
    pub fn new_native(
        cco: ObjectPointer,
        stack_window_index: usize,
        return_address: usize,
    ) -> Self {
        Self::new(cco, stack_window_index, return_address)
    }

    #[cfg(all(feature = "unchecked-vm", not(feature = "trace-lox-calls")))]
    pub fn new_native(
        mut cco: ObjectPointer,
        stack_window_index: usize,
        return_address: usize,
    ) -> Self {
        let chunk = &mut cco.as_mut().as_cco_mut().chunk;
        let (bytecode, bytecode_len) = (_EMPTY_BYTECODE.as_ptr(), 0);
        let chunk = unsafe { TaggedPointer::from_ref(chunk) };
        let upvalues = std::ptr::null_mut();
        Self {
            cco,
            chunk,
            upvalues,
            n_upvalues: 0,
            bytecode,
            bytecode_len,
            stack_window_index,
            return_address,
        }
    }

    #[inline(always)]
    pub fn cco_ptr(&self) -> &ObjectPointer {
        &self.cco
    }

    #[inline]
    pub fn is_native_frame(&self) -> bool {
        self.cco.tag() == Tag::NativeCCO
    }

    #[inline]
    pub fn main(cco: ObjectPointer) -> Self {
        Self::new(cco, 0, 0)
    }

    #[inline(always)]
    pub fn cco(&self) -> &CompiledCodeObject {
        self.cco.as_ref().as_cco()
    }

    #[inline(always)]
    pub fn cco_mut(&mut self) -> &mut CompiledCodeObject {
        self.cco.as_mut().as_cco_mut()
    }

    #[inline(always)]
    pub fn chunk(&self) -> &Chunk {
        self.chunk.as_ref()
    }

    #[inline(always)]
    pub fn chunk_mut(&mut self) -> &mut Chunk {
        self.chunk.as_mut()
    }

    #[inline]
    pub fn bytecode(&self) -> &[Inst] {
        unsafe { std::slice::from_raw_parts(self.bytecode, self.bytecode_len) }
    }

    #[inline]
    pub fn bytecode_len(&self) -> usize {
        self.bytecode_len
    }

    #[cfg(feature = "unchecked-vm")]
    #[inline]
    pub fn byte_at(&self, idx: usize) -> u8 {
        unsafe { *self.bytecode.add(idx) }
    }

    #[cfg(not(feature = "unchecked-vm"))]
    #[inline]
    pub fn byte_at(&self, idx: usize) -> u8 {
        self.bytecode()[idx]
    }

    #[cfg(not(feature = "unchecked-vm"))]
    #[inline]
    pub fn get_upvalue(&self, at: usize) -> Option<&ObjectPointer> {
        if at < self.n_upvalues && !self.upvalues.is_null() {
            // SAFETY:
            // 1) Upvalue arrays cannot be resized or moved.
            // 2) The array is guaranteed to live as long a its parent CCO
            // 3) The CCO is guaranteed to live at least as long as the call frame
            // 4) Therefore, the pointer is guaranteed to valid inside this method.
            unsafe { Some(&*self.upvalues.add(at)) }
        } else {
            None
        }
    }

    #[cfg(feature = "unchecked-vm")]
    #[inline]
    pub fn get_upvalue(&self, at: usize) -> ObjectPointer {
        // SAFETY:
        // 1) Upvalue arrays cannot be resized or moved.
        // 2) The array is guaranteed to live as long a its parent CCO
        // 3) The CCO is guaranteed to live at least as long as the call frame
        // 4) Therefore, the pointer is guaranteed to valid inside this method.
        unsafe { *self.upvalues.add(at) }
    }
}

impl std::fmt::Display for CallFrame {
    #[allow(clippy::useless_format)]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            crate::ast::generate_message_box(
                &[
                    format!(
                        "Function: {} @ {:#p}",
                        self.cco.as_ref().as_cco().name,
                        self.cco.as_ptr()
                    ),
                    format!("is_closure: {}", !self.upvalues.is_null()),
                    format!("n_upvalues: {}", self.n_upvalues),
                    format!("stack window:   {}", self.stack_window_index),
                    format!("return address: {}", self.return_address),
                    format!("(misc):"),
                    format!(
                        "  body size: {} instructions",
                        self.chunk().bytecode.as_raw().map(Vec::len).unwrap_or(0)
                    ),
                    format!(
                        "  info size: {} locations",
                        match &self.chunk().locations {
                            crate::compiler::locations::Locations::Raw(v) => v.len().to_string(),
                            loc => format!("{} ({} RLE)", loc.clone().decode().len(), loc.len(),),
                        },
                    ),
                ],
                Some("call frame")
            )
        )
    }
}
