use std::time::{Duration, Instant};
use std::{cell::RefCell, hash::Hash};
use std::{collections::HashSet, hash::Hasher};

use crate::rt_hashmap::FastHashMap;
use vm::traceback::TraceBack;

use super::{
    builtin::dict::LoxDict,
    immutable_string::ImmutableString,
    module::LoxModule,
    native::CowStr,
    object::object::{BindingStatus, ClassObject},
    preallocated_native_cco_stack::PreallocatedNativeCCOStack,
    traceback::TraceFrame,
    HeapValue, Object,
};
use crate::{
    compiler::cco::{Arity, CompiledCodeObject},
    module::ModuleId,
    token::Span,
    ParamsCountT,
};
use compiler::bytecode::raw::RawOpcode;
use context::SharedCtx;
use errors::LoxError;
use vm::builtin::array::LoxArray;
use vm::call_frame::CallFrame;
use vm::object::native::NativeObject;
use vm::object::{LoxObject, ObjectPointer, Value};

#[cfg(feature = "debug-info")]
mod debug_imports {
    pub use crate::compiler::dis::Disassemble;
}
#[cfg(feature = "debug-info")]
pub use self::debug_imports::*;

// TODO: this should be stored in the config
pub static DEBUG_STACK_PRINT_SIZE: usize = 5;
pub static MAX_PREALLOCATED_NATIVE_CCO_CAP: usize = 50; // 10KB

thread_local! {
    /// This set is used when serializing recursive data structures to avoid blowing up the Rust stack
    /// with recursive calls to Display::fmt.
    pub(crate) static VISITED_POINTERS_SET: RefCell<HashSet<ObjectPointer>> = RefCell::new(HashSet::with_capacity(256));
}

/// Add a pointer to the set of visited pointers.
pub(crate) fn pset_add_pointer(ptr: ObjectPointer) {
    VISITED_POINTERS_SET.with(|set| {
        set.borrow_mut().insert(ptr);
    })
}

/// Check if a pointer is in the set of visited pointers.
pub(crate) fn pset_check_pointer(ptr: &ObjectPointer) -> bool {
    VISITED_POINTERS_SET.with(|set| set.borrow().contains(ptr))
}

/// Remove a pointer from the set of visited pointers.
pub(crate) fn pset_remove_pointer(ptr: &ObjectPointer) -> bool {
    VISITED_POINTERS_SET.with(|set| set.borrow_mut().remove(ptr))
}

#[derive(Debug)]
pub struct VmState {
    pub ip: usize,
    pub stack: Vec<Value>,
    pub call_stack: Vec<CallFrame>,
    pub match_stack: Vec<Value>,
    pub time: Instant,
    pub timeout: Option<Duration>,
    frame: *mut CallFrame,
    native_cco_stack: PreallocatedNativeCCOStack,
}

impl Default for VmState {
    fn default() -> Self {
        Self {
            ip: 0,
            stack: Vec::with_capacity(4096),
            call_stack: Vec::with_capacity(10),
            match_stack: Vec::with_capacity(3),
            frame: std::ptr::null_mut(),
            time: Instant::now(),
            timeout: None,
            native_cco_stack: PreallocatedNativeCCOStack::new(10),
        }
    }
}

#[derive(Debug)]
pub struct Vm {
    ctx: SharedCtx,
    state: VmState,
    builtins: FastHashMap<u8, ObjectPointer>,
    // TODO: get rid of this
    alloc_size_adjustment: isize,
    interning_threshold: usize,
    max_call_stack_size: usize,
    #[cfg(feature = "profile-instructions")]
    inst_map: FastHashMap<(RawOpcode, RawOpcode), usize>,
    #[cfg(feature = "profile-instructions")]
    last_inst: Option<RawOpcode>,
}

impl Vm {
    pub fn new(ctx: SharedCtx) -> Self {
        let (interning_threshold, builtins, max_call_stack_size, timeout) = {
            let guard = ctx.read_shared();
            let thresh = guard.config.interning_threshold as usize;
            let max_call_stack_size = guard.config.max_call_stack_size;
            let timeout = guard.config.execution_timeout;
            let builtins = guard.copy_builtins_map();
            (thresh, builtins, max_call_stack_size, timeout)
        };
        Self {
            ctx,
            builtins,
            state: VmState {
                timeout,
                ..VmState::default()
            },
            alloc_size_adjustment: 0,
            interning_threshold,
            max_call_stack_size,
            #[cfg(feature = "profile-instructions")]
            inst_map: FastHashMap::default(),
            #[cfg(feature = "profile-instructions")]
            last_inst: None,
        }
    }

    #[inline]
    pub fn push<V: Into<Value>>(&mut self, v: V) {
        self.state.stack.push(v.into());
    }

    #[cfg(not(feature = "unchecked-vm"))]
    #[inline]
    pub fn pop(&mut self) -> Value {
        match self.state.stack.pop() {
            Some(v) => v,
            None => panic!(
                "STACK UNDERFLOW AT ip={} in {}",
                self.state.ip,
                self.frame().cco().name
            ),
        }
    }

    #[cfg(feature = "unchecked-vm")]
    #[inline]
    pub fn pop(&mut self) -> Value {
        let len = self.state.stack.len() - 1;
        unsafe {
            self.state.stack.set_len(len);
            std::ptr::read(self.state.stack.as_ptr().add(len))
        }
    }

    #[cfg(not(feature = "unchecked-vm"))]
    #[inline]
    pub fn peek(&self) -> &Value {
        match self.state.stack.last() {
            Some(v) => v,
            None => panic!(
                "STACK UNDERFLOW AT ip={} in {}",
                self.state.ip,
                self.frame().cco().name
            ),
        }
    }

    #[cfg(feature = "unchecked-vm")]
    #[inline]
    pub fn peek(&self) -> &Value {
        let at = self.state.stack.len() - 1;
        unsafe { self.state.stack.get_unchecked(at) }
    }

    #[cfg(not(feature = "unchecked-vm"))]
    #[inline]
    pub fn peek_at(&self, at: usize) -> &Value {
        let at = self.state.stack.len() - at - 1;
        match self.state.stack.get(at) {
            Some(v) => v,
            None => panic!(
                "STACK UNDERFLOW AT ip={} in {}",
                self.state.ip,
                self.frame().cco().name
            ),
        }
    }

    #[cfg(feature = "unchecked-vm")]
    #[inline]
    pub fn peek_at(&self, at: usize) -> &Value {
        let at = self.state.stack.len() - at - 1;
        unsafe { self.state.stack.get_unchecked(at) }
    }

    #[cfg(not(feature = "unchecked-vm"))]
    #[inline]
    pub fn set_stack_at(&mut self, at: usize, obj: Value) {
        let at = self.state.stack.len() - at - 1;
        match self.state.stack.get_mut(at) {
            Some(v) => *v = obj,
            None => panic!(
                "STACK UNDERFLOW AT ip={} in {}",
                self.state.ip,
                self.frame().cco().name
            ),
        }
    }

    #[cfg(feature = "unchecked-vm")]
    #[inline]
    pub fn set_stack_at(&mut self, at: usize, obj: Value) {
        let at = self.state.stack.len() - at - 1;
        unsafe {
            *self.state.stack.get_unchecked_mut(at) = obj;
        }
    }

    pub(crate) fn is_truthy(&self, v: &Value) -> bool {
        match v {
            Value::Int(i) => *i != 0,
            Value::Float(f) => *f != 0.0,
            Value::Bool(b) => *b,
            Value::Nil => false,
            Value::Ptr(ptr) => match ptr.as_ref().as_heap_value().follow_ptr() {
                HeapValue::Str(box s) => !s.as_ref().is_empty(),
                HeapValue::UpValue(up) => self.is_truthy(up),
                HeapValue::Obj(_)
                | HeapValue::CCO(_)
                | HeapValue::Closure { .. }
                | HeapValue::ObjNative(_)
                | HeapValue::Cls(_)
                | HeapValue::BoundMethod { .. } => true,
            },
        }
    }

    #[inline]
    fn pop_n(&mut self, n: usize) -> std::vec::Drain<'_, Value> {
        let len = self.state.stack.len();
        if n > len {
            panic!("STACK UNDERFLOW AT ip={}", self.state.ip)
        } else {
            self.state.stack.drain(len - n..len)
        }
    }

    #[inline]
    fn peek_n(&mut self, n: usize) -> &[Value] {
        let len = self.state.stack.len();
        &self.state.stack[len - n..len]
    }

    #[cfg(not(feature = "unchecked-vm"))]
    #[inline]
    fn const_at(&self, idx: usize) -> &Value {
        match self.frame_unchecked().chunk().constants.get(idx) {
            Some(v) => v,
            None => panic!("MISSING CONSTANT AT {}", idx),
        }
    }

    #[cfg(feature = "unchecked-vm")]
    #[inline]
    fn const_at(&self, idx: usize) -> &Value {
        unsafe { self.frame_unchecked().chunk().constants.get_unchecked(idx) }
    }

    fn const_at_string<'wanted>(&self, idx: usize) -> &'wanted str {
        let s = self.const_at(idx).as_string().unwrap().as_ref();
        // Safety: the str is guaranteed to be alive for the entirety of the program
        // since CCOs are not garbage collected.
        unsafe { std::mem::transmute(s) }
    }

    #[cfg(not(feature = "unchecked-vm"))]
    fn local_at(&self, offset: usize) -> &Value {
        let frame = self.frame_unchecked();
        let frame_start = frame.stack_window_index;
        match self.state.stack.get(frame_start + offset) {
            Some(v) => v,
            None => panic!(
                "INVALID LOCAL ADDRESS `{}` AT {} in `{}` (stack window = {}, stack = {:?})",
                offset,
                self.state.ip,
                frame.cco().name,
                frame_start,
                self.state.stack
            ),
        }
    }

    #[cfg(feature = "unchecked-vm")]
    #[inline]
    fn local_at(&self, offset: usize) -> &Value {
        let frame_start = self.frame_unchecked().stack_window_index;
        unsafe { self.state.stack.get_unchecked(frame_start + offset) }
    }

    fn set_local_at(&mut self, offset: usize, obj: Value) -> &Value {
        let frame = self.frame_unchecked();
        let frame_start = frame.stack_window_index;
        let slot = frame_start + offset;
        if slot >= self.state.stack.len() {
            self.push(obj);
        } else {
            self.state.stack[slot] = obj;
        }
        self.local_at(offset)
    }

    #[inline]
    fn frame_unchecked(&self) -> &CallFrame {
        unsafe { &*self.state.frame }
    }

    #[allow(unused)]
    fn frame_unchecked_mut(&mut self) -> &mut CallFrame {
        unsafe { &mut *self.state.frame }
    }

    #[inline(always)]
    fn frame(&self) -> &CallFrame {
        self.state.call_stack.last().unwrap()
    }

    #[allow(unused)]
    #[inline(always)]
    fn frame_mut(&mut self) -> &mut CallFrame {
        self.state.call_stack.last_mut().unwrap()
    }

    pub(crate) fn alloc(&mut self, obj: LoxObject) -> ObjectPointer {
        let mut guard = self.ctx.write_shared();
        if self.alloc_size_adjustment != 0 {
            guard.bump_allocated_bytes(self.alloc_size_adjustment);
            self.alloc_size_adjustment = 0;
        }
        guard.alloc(
            obj,
            &mut self
                .state
                .stack
                .iter_mut()
                .filter_map(|v| v.as_ptr_mut().copied())
                .chain(self.state.call_stack.iter().map(|f| *f.cco_ptr())),
        )
    }

    #[inline]
    pub(crate) fn alloc_native<T: 'static + NativeObject>(&mut self, obj: T) -> ObjectPointer {
        self.alloc(LoxObject::new(HeapValue::ObjNative(Box::new(obj))))
    }

    pub(crate) fn alloc_or_get_interned(&mut self, s: String) -> ObjectPointer {
        let mut s = ImmutableString::new(s);
        let len = s.len_fast();

        if len <= self.interning_threshold {
            // Intern 1-character strings
            if len == 1 {
                return *self.ctx.write_shared().intern_ptr(s.into_string());
            } else if let Some(ptr) = self.ctx.read_shared().get_interned_by_hash(s.hash()) {
                return *ptr;
            }
        }

        self.alloc(LoxObject::new(HeapValue::Str(Box::new(s))))
    }

    pub(crate) fn alloc_or_get_interned_str(&mut self, s: &str) -> ObjectPointer {
        let len = s.len();
        if len <= self.interning_threshold {
            // Intern 1-character strings
            if len == 1 {
                return *self.ctx.write_shared().intern_ptr(s.to_owned());
            } else if let Some(ptr) = self
                .ctx
                .read_shared()
                .get_interned_by_hash(crate::vm::object::immutable_string::hash(s))
            {
                return *ptr;
            }
        }

        let s = ImmutableString::new(s.to_owned());
        self.alloc(LoxObject::new(HeapValue::Str(Box::new(s))))
    }

    fn alloc_array_from_stack(&mut self, n: usize) -> ObjectPointer {
        let values = self.peek_n(n).to_vec();
        let obj = LoxObject::new(HeapValue::ObjNative(Box::new(LoxArray::new(values))));
        let ptr = self.alloc(obj);
        self.pop_n(n);
        ptr
    }

    fn alloc_dict_from_stack(&mut self, n_pairs: usize) -> ObjectPointer {
        let n_values = n_pairs * 2;
        let mut pairs = Vec::with_capacity(n_pairs);
        for pair in self.peek_n(n_values).chunks_exact(2) {
            pairs.push((pair[0], pair[1]));
        }
        debug_assert_eq!(pairs.len(), n_pairs);
        let obj = LoxObject::new(HeapValue::ObjNative(Box::new(LoxDict::new(pairs))));
        let ptr = self.alloc(obj);
        self.pop_n(n_values);
        ptr
    }

    #[inline]
    fn read_u8(&mut self, b: *const u8) -> u8 {
        self.state.ip += 1;
        unsafe { *b.add(self.state.ip) }
    }

    #[inline]
    fn read_u16(&mut self, b: *const u8) -> u16 {
        let hi = unsafe { *b.add(self.state.ip + 1) };
        let low = unsafe { *b.add(self.state.ip + 2) };
        self.state.ip += 2;
        (hi as u16) << 8 | low as u16
    }

    #[must_use = "This function only creates an error object which must be returned manually."]
    #[inline]
    pub(crate) fn error<S: Into<String>>(&mut self, msg: S) -> LoxError {
        let mut err = LoxError::runtime(msg, self.get_current_span());
        err.module = self.frame().cco().module;
        err
    }

    #[must_use = "This function only creates an error object which must be returned manually."]
    #[inline]
    pub(crate) fn panic<S: Into<String>>(&mut self, msg: S) -> LoxError {
        let mut err = LoxError::panic(msg, self.get_current_span());
        err.module = self.frame().cco().module;
        err
    }

    #[must_use = "This function only creates an error object which must be returned manually."]
    #[inline]
    pub(crate) fn assertion<S: Into<String>>(&mut self, msg: S) -> LoxError {
        let mut err = LoxError::assertion(msg, self.get_current_span());
        err.module = self.frame().cco().module;
        err
    }

    #[inline]
    fn get_current_span(&mut self) -> Span {
        self.try_get_current_span()
            .expect("Could not get the location")
    }

    fn try_get_current_span(&mut self) -> Option<Span> {
        let ip = self.state.ip;
        let mut i = self.state.call_stack.len() - 1;

        // No locations indicates a native call frame.
        while i > 0 && self.state.call_stack[i].chunk().locations.is_empty() {
            i -= 1;
        }

        self.state
            .call_stack
            .get_mut(i)
            .unwrap()
            .chunk_mut()
            .locations
            .location_of_expensive(ip)
            .map(crate::compiler::locations::Location::into_span)
    }

    pub fn type_of(&self, obj: &Value) -> String {
        match obj {
            Value::Int(_) => "int".to_string(),
            Value::Float(_) => "float".to_string(),
            Value::Bool(_) => "bool".to_string(),
            Value::Nil => "nil".to_string(),
            Value::Ptr(ptr) => match ptr.as_ref().as_heap_value().follow_ptr() {
                HeapValue::UpValue(up) => self.type_of(up),
                HeapValue::Str(_) => "str".to_string(),
                HeapValue::Obj(obj) => obj.class_name().to_owned(),
                HeapValue::ObjNative(obj) => obj.lox_name().into_owned(),
                HeapValue::CCO(cco) => format!("<fn: {}>", cco.name),
                // XXX: should this return just fn?
                HeapValue::Closure { cco, .. } => format!("<fn: {}>", cco.as_ref().as_cco().name), // XXX: should this return fn?
                HeapValue::BoundMethod { cco, instance } => format!(
                    "<fn: {}.{}>",
                    instance.as_ref().as_object().class_name(),
                    cco.as_ref().as_cco().name
                ),
                HeapValue::Cls(cls) => format!("<type: {}>", &cls.name),
            },
        }
    }
    pub fn help(&self, obj: &Value) -> String {
        if let Value::Ptr(ptr) = obj {
            match ptr.as_ref().as_heap_value() {
                HeapValue::UpValue(obj) => return self.help(obj),
                HeapValue::ObjNative(obj) => return obj.lox_doc().into_owned(),
                HeapValue::Obj(obj) => {
                    return format!("instance of {}", self.help(&Value::Ptr(obj.class)))
                }
                HeapValue::Cls(cls) => {
                    let mut msg = String::new();
                    msg.extend_one(format!("class {}", &cls.name));
                    if let Some(super_cls) = cls.superclass.as_ref() {
                        msg.extend_one(format!(" < {} {{\n", &super_cls.as_ref().as_class().name));
                    } else {
                        msg.push_str(" {\n")
                    }
                    match cls.methods.len() {
                        0 => msg.push_str("    // Empty\n\n"),
                        _ => cls
                            .methods
                            .get("init")
                            .copied()
                            .iter()
                            .chain(cls.methods.iter().filter_map(|(k, v)| match &k[..] {
                                "init" => None,
                                _ => Some(v),
                            }))
                            .for_each(|cco| {
                                msg.push_str("    ");
                                msg.extend_one(self.cco_doc(cco.as_ref().as_cco()));
                                msg.push_str("\n\n");
                            }),
                    }
                    msg.pop();
                    msg.push('}');
                    return msg;
                }
                HeapValue::BoundMethod { cco, instance } => {
                    let msg = self.cco_doc(cco.as_ref().as_cco());
                    return format!(
                        "/* method */\n{}.{}",
                        instance.as_ref().as_object().class_name(),
                        msg
                    );
                }
                rest if rest.as_cco().is_some() => {
                    return format!("fun {}", self.cco_doc(rest.as_cco().unwrap()));
                }
                _ => {}
            }
        }
        format!("No documentation found for `{}`.", self.type_of(obj))
    }

    fn cco_doc(&self, cco: &CompiledCodeObject) -> String {
        let mut msg = String::new();
        msg.extend_one(format!("{}(", cco.name));

        match cco.arity {
            Arity::Fixed(0) => (),
            Arity::Fixed(n) => {
                for i in 0..(n - 1).min(4) {
                    msg.extend_one(format!("arg{}, ", i));
                }
                msg.extend_one(format!("arg{}", n - 1));
            }
            Arity::Variable => msg.push_str("~args"),
            _ => unreachable!(),
        }
        msg.extend_one(") { ... }");
        msg
    }

    #[inline]
    pub fn active_builtins(&self) -> &FastHashMap<u8, ObjectPointer> {
        &self.builtins
    }

    fn check_arity(&mut self, cco: &CompiledCodeObject, n_args: usize) -> Result<(), LoxError> {
        if !cco.arity.check(n_args as ParamsCountT) {
            Err(self.error(format!(
                "Function `{}` expected {} argument(s) but got {}",
                cco.name, cco.arity, n_args,
            )))
        } else {
            Ok(())
        }
    }

    fn check_no_init_args(&mut self, name: &str, n_args: usize) -> Result<(), LoxError> {
        if n_args as ParamsCountT != 0 {
            Err(self.error(format!(
                "Class `{}` doesn't have an initializer but got {} argument(s)",
                name, n_args,
            )))
        } else {
            Ok(())
        }
    }

    fn handle_varargs(&mut self, cco: &CompiledCodeObject, n_args: &mut usize) {
        if cco.arity.is_variable() {
            let arr = self.alloc_array_from_stack(*n_args);
            self.push(Value::Ptr(arr));
            *n_args = 1;
        }
    }

    fn check_arity_obj(&mut self, obj: &dyn NativeObject, n_args: usize) -> Result<(), LoxError> {
        let arity = obj.arity().unwrap();
        if !arity.check(n_args as ParamsCountT) {
            Err(self.error(format!(
                "Callable `{}` expected {} argument(s) but got {}",
                obj.lox_name(),
                arity,
                n_args,
            )))
        } else {
            Ok(())
        }
    }

    fn call(&mut self, cco: ObjectPointer, n_args: usize) -> Result<(), LoxError> {
        let frame = CallFrame::new(cco, self.state.stack.len() - n_args, self.state.ip);
        let result = self.push_frame(frame);
        // Reset the ip *after* pushing the frame so the possible recursion error
        // has its corresponding span (spans are mapped to the IP).
        self.state.ip = 0;
        result
    }

    #[inline]
    fn push_frame(&mut self, frame: CallFrame) -> Result<(), LoxError> {
        if cfg!(feature = "trace-lox-calls") {
            println!("Push:\n{}", &frame);
        }
        if self.state.call_stack.len() >= self.max_call_stack_size {
            Err(self.error(format!(
                "Maximum recursion depth has been exceeded: {}.",
                self.state.call_stack.len()
            )))
        } else {
            self.state.call_stack.push(frame);
            let index = self.state.call_stack.len() - 1;
            self.state.frame = unsafe { self.state.call_stack.get_unchecked_mut(index) as *mut _ };
            Ok(())
        }
    }

    #[cfg(not(feature = "unchecked-vm"))]
    #[inline]
    fn pop_frame(&mut self) -> CallFrame {
        let frame = if cfg!(feature = "trace-lox-calls") {
            self.state
                .call_stack
                .pop()
                .map(|c| {
                    println!("Pop: {}", c.cco().name);
                    c
                })
                .expect("CALL STACK UNDERFLOW")
        } else {
            self.state.call_stack.pop().expect("CALL STACK UNDERFLOW")
        };
        if std::intrinsics::likely(!self.state.call_stack.is_empty()) {
            let index = self.state.call_stack.len() - 1;
            self.state.frame = unsafe { self.state.call_stack.get_unchecked_mut(index) as *mut _ };
        } else {
            self.state.frame = std::ptr::null_mut();
        }
        if frame.is_native_frame() {
            self.state.native_cco_stack.return_cco();
        }
        frame
    }

    #[cfg(feature = "unchecked-vm")]
    #[inline]
    fn pop_frame(&mut self) -> CallFrame {
        let len = self.state.call_stack.len() - 1;
        let frame = unsafe {
            self.state.call_stack.set_len(len);
            self.state.frame = self
                .state
                .call_stack
                .get_unchecked_mut(len.saturating_sub(1)) as *mut _;
            std::ptr::read(self.state.call_stack.as_ptr().add(len))
        };
        if frame.is_native_frame() {
            self.state.native_cco_stack.return_cco();
        }
        frame
    }

    fn push_native_frame<S: Into<CowStr>>(&mut self, name: S) -> Result<(), LoxError> {
        let module = self.frame_unchecked().cco().module;
        let ptr = match self.state.native_cco_stack.get_cco(name.into(), module) {
            Ok(ok) => ok,
            Err(obj) => self.alloc(*obj),
        };
        let return_address = self.state.ip;
        self.push_frame(CallFrame::new_native(ptr, 0, return_address))
    }

    #[allow(unused)]
    fn debug_vm(&self) {
        let op = RawOpcode::from_u8(self.frame_unchecked().byte_at(self.state.ip));
        println!(
            "[ip={:<03} in {:<08}] {:<016} [{}{}]",
            self.state.ip,
            self.frame().cco().name,
            op.name(),
            if self.state.stack.len() >= DEBUG_STACK_PRINT_SIZE {
                format!("... x {}, ", self.state.stack.len())
            } else {
                String::new()
            },
            self.state
                .stack
                .iter()
                .rev()
                .take(DEBUG_STACK_PRINT_SIZE)
                .rev()
                .map(|v| if cfg!(feature = "debug-stack-repr") {
                    format!("{:?}", v)
                } else {
                    format!("{}", v)
                })
                .collect::<Vec<_>>()
                .join(", ")
        );
    }

    #[cfg(feature = "profile-instructions")]
    fn record_inst_frequency(&mut self) {
        let op = RawOpcode::from_u8(self.frame_unchecked().byte_at(self.state.ip));
        if let Some(last) = self.last_inst {
            *self.inst_map.entry((last, op)).or_insert(0) += 1;
        }
        self.last_inst = Some(op);
    }

    #[cfg(feature = "profile-instructions")]
    fn dump_inst_frequencies(&mut self) {
        let mut freqs = self.inst_map.iter().collect::<Vec<_>>();
        freqs.sort_by_key(|(_, v)| std::cmp::Reverse(*v));
        let freqs = freqs
            .into_iter()
            .map(|((op1, op2), v)| format!("{},{}\t{}", op1.name(), op2.name(), v))
            .collect::<Vec<_>>()
            .join("\n");
        let (build_dir, name, hash) = {
            let guard = self.ctx.read_shared();
            (
                guard.config.build_dir.clone(),
                guard.get_root_module_name().unwrap(),
                guard.get_root_module_source_info().unwrap().source_hash,
            )
        };
        std::fs::create_dir_all(&build_dir).unwrap();

        let time = std::time::SystemTime::now()
            .duration_since(std::time::SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();
        std::fs::write(
            build_dir.join(format!("inst_freqs_{}_{:x}_{}.csv", name, hash, time)),
            freqs,
        )
        .unwrap();
    }

    fn create_upvalues(
        &mut self,
        cco_ptr: ObjectPointer,
        cco: &CompiledCodeObject,
        is_long: bool,
    ) -> Value {
        let mut upvalues = Vec::with_capacity(cco.n_captured);

        let obj = LoxObject::new(HeapValue::Closure {
            upvalues: vec![],
            cco: cco_ptr,
        });
        let mut closure_ptr = self.alloc(obj);
        // Temporarily push the closure pointer onto the stack.
        // capture_upvalues() allocates so we need to make sure that the pointer is reachable
        // in the case the GC kicks in.
        self.push(closure_ptr);

        let insts = self.frame_unchecked().bytecode().as_ptr();
        for _ in 0..cco.n_captured {
            let is_local = self.read_u8(insts) == 1;
            let index = if is_long {
                self.read_u16(insts) as usize
            } else {
                self.read_u8(insts) as usize
            };
            let mut upvalue = if is_local {
                let local = *self.local_at(index);
                let upvalue = self.capture_upvalue(local);
                self.set_local_at(index, Value::Ptr(upvalue));
                upvalue
            } else {
                self.get_upvalue(index)
            };
            // If the upvalue is a pointer to an object
            if let HeapValue::UpValue(Value::Ptr(ref mut ptr)) =
                upvalue.as_mut().as_heap_value_mut()
            {
                // Check that the upvalue at the address is a function pointer
                if let HeapValue::CCO(_) = ptr.as_ref().as_heap_value() {
                    // If the pointers match, copy the upvalue
                    if ptr.as_ptr() == cco_ptr.as_ptr() {
                        *ptr = closure_ptr;
                    }
                }
            }
            upvalues.push(upvalue)
        }

        let _closure_ptr = self.pop();
        match closure_ptr.as_mut().as_heap_value_mut() {
            HeapValue::Closure {
                upvalues: l_upvalues,
                ..
            } => *l_upvalues = upvalues,
            _ => unreachable!(),
        }
        Value::Ptr(closure_ptr)
    }

    #[cfg(not(feature = "unchecked-vm"))]
    #[inline]
    fn get_upvalue(&mut self, index: usize) -> ObjectPointer {
        *self.frame_unchecked().get_upvalue(index).unwrap()
    }

    #[cfg(feature = "unchecked-vm")]
    #[inline]
    fn get_upvalue(&mut self, index: usize) -> ObjectPointer {
        self.frame_unchecked().get_upvalue(index)
    }

    fn capture_upvalue(&mut self, value: Value) -> ObjectPointer {
        let value = match value {
            Value::Ptr(ptr) => {
                // TODO: generalize this for upvalue chains
                if let HeapValue::UpValue(value) = ptr.as_ref().as_heap_value() {
                    if value
                        .as_ptr()
                        .map(|obj| !obj.as_ref().is_upvalue())
                        .unwrap_or(true)
                    {
                        return ptr;
                    }
                }
                match ptr.as_ref().as_heap_value().follow_ptr_tup() {
                    (Some(final_ptr), _) => Value::Ptr(final_ptr),
                    _ => Value::Ptr(ptr),
                }
            }
            _ => value,
        };
        self.alloc(LoxObject::new(HeapValue::UpValue(value)))
    }

    #[inline]
    pub fn get_state(&self) -> &VmState {
        &self.state
    }

    #[inline]
    pub(crate) fn shrink_stack(&mut self, up_to: usize) {
        if up_to < self.state.stack.len() {
            self.state.stack.drain(up_to..);
        }
    }

    #[inline]
    pub fn set_ip(&mut self, ip: usize) {
        self.state.ip = ip;
    }
}

impl Vm {
    #[inline]
    pub fn run_with_new_state(&mut self, cco: ObjectPointer) -> Result<Value, TraceBack> {
        self.state = VmState::default();
        self.run(cco)
    }

    pub fn run(&mut self, cco: ObjectPointer) -> Result<Value, TraceBack> {
        self.state.time = Instant::now();

        if !cco.as_ref().is_cco() {
            panic!("The object pointer must point to a CCO object");
        }
        #[cfg(feature = "debug-info")]
        {
            cco.as_ref().as_cco().dis_print();
        }
        self.push_frame(CallFrame::main(cco)).unwrap();
        let result = match self.run_dispatch_loop(false) {
            Ok(value) => Ok(value),
            Err(e) => {
                let mut frames = Vec::with_capacity(self.state.call_stack.len());
                while !self.state.call_stack.is_empty() {
                    let span = self.get_current_span();
                    let frame = self.frame();
                    frames.push(TraceFrame {
                        name: frame.cco().name.clone().into_owned(),
                        module: frame.cco().module,
                        span,
                    });
                    self.state.ip = self.pop_frame().return_address;
                }
                Err(TraceBack { error: e, frames })
            }
        };
        #[cfg(feature = "profile-instructions")]
        {
            self.dump_inst_frequencies()
        }
        result
    }

    fn run_dispatch_loop(&mut self, return_early: bool) -> Result<Value, LoxError> {
        let initial_cs_size = self.state.call_stack.len();

        let mut instructions = self.frame_unchecked().bytecode().as_ptr();
        let mut len = self.frame_unchecked().bytecode_len();
        while self.state.ip < len {
            #[cfg(feature = "debug-info")]
            {
                self.debug_vm();
            }
            #[cfg(feature = "profile-instructions")]
            {
                self.record_inst_frequency();
            }
            match unsafe { *instructions.add(self.state.ip) } {
                // LoadConst
                0x00 => {
                    let offset = self.read_u8(instructions);
                    self.push(*self.const_at(offset as usize));
                }
                // LoadConstLong
                0x01 => {
                    let offset = self.read_u16(instructions);
                    self.push(*self.const_at(offset as usize));
                }
                // GetLocal
                0x02 => {
                    let offset = self.read_u8(instructions);
                    self.push(*self.local_at(offset as usize));
                }
                // SetLocal
                0x03 => {
                    let offset = self.read_u8(instructions);
                    let obj = self.pop();
                    let obj = *self.set_local_at(offset as usize, obj);
                    self.push(obj);
                }
                // GetLocalLong
                0x04 => {
                    let offset = self.read_u16(instructions);
                    self.push(*self.local_at(offset as usize));
                }
                // SetLocalLong
                0x05 => {
                    let offset = self.read_u16(instructions);
                    let obj = self.pop();
                    let obj = *self.set_local_at(offset as usize, obj);
                    self.push(obj);
                }
                // Pop
                0x06 => {
                    self.pop();
                }
                // PopN
                0x07 => {
                    let n = self.read_u8(instructions);
                    self.pop_n(n as usize);
                }
                // PopNLong
                0x08 => {
                    let n = self.read_u16(instructions);
                    self.pop_n(n as usize);
                }
                // Negate
                0x09 => {
                    let res = match self.pop() {
                        Value::Int(i) => Value::from(-i),
                        Value::Float(f) => Value::from(-f),
                        rest => {
                            return Err(self.error(format!(
                                "Cannot negate an object of type {}",
                                self.type_of(&rest)
                            )))
                        }
                    };
                    self.push(res);
                }
                // Not
                0x0A => {
                    let obj = self.pop();
                    self.push(!self.is_truthy(&obj));
                }
                // Add
                0x0B => {
                    let rhs = *self.peek();
                    let lhs = *self.peek_at(1);
                    let res = binary_op!(self, lhs, rhs, +)?;
                    self.pop_n(2);
                    self.push(res);
                }
                // Sub
                0x0C => {
                    let rhs = self.pop();
                    let lhs = self.pop();
                    let res = binary_op!(self, lhs, rhs, -)?;
                    self.push(res);
                }
                // Div
                0x0D => {
                    let rhs = self.pop();
                    let lhs = self.pop();
                    let res = binary_op!(self, lhs, rhs, div => /)?;
                    self.push(res);
                }
                // Mul
                0x0E => {
                    let rhs = self.pop();
                    let lhs = self.pop();
                    let res = binary_op!(self, lhs, rhs, *)?;
                    self.push(res);
                }
                // Pow
                0x0F => {
                    let rhs = self.pop();
                    let lhs = self.pop();
                    let res = binary_op!(self, lhs, rhs, **)?;
                    self.push(res);
                }
                // Mod
                0x10 => {
                    let rhs = self.pop();
                    let lhs = self.pop();
                    let res = binary_op!(self, lhs, rhs, div => %)?;
                    self.push(res);
                }
                // Cmp
                0x11 => {
                    let mut rhs = self.pop();
                    let mut lhs = self.pop();
                    let res = lhs.eq_cached(&mut rhs);
                    self.push(res);
                }
                // CmpNot
                0x12 => {
                    let mut rhs = self.pop();
                    let mut lhs = self.pop();
                    let res = lhs.eq_cached(&mut rhs);
                    self.push(!res);
                }
                // CmpType
                0x4F => {
                    let class = self.pop();
                    let obj = self.pop();
                    let result = self.type_cmp(&obj, &class);
                    let result = result.ok_or_else(|| match self.stringify(&class) {
                        Ok(s) => self.error(format!("`{}` is not a type", s)),
                        Err(e) => e,
                    })?;
                    self.push(Value::Bool(result));
                }
                // CmpTypeNot
                0x50 => {
                    let class = self.pop();
                    let obj = self.pop();
                    let result = self.type_cmp(&obj, &class);
                    let result = result.ok_or_else(|| match self.stringify(&class) {
                        Ok(s) => self.error(format!("`{}` is not a type", s)),
                        Err(e) => e,
                    })?;
                    self.push(Value::Bool(!result));
                }
                // Jump
                0x13 => {
                    let ip = self.state.ip;
                    let offset = self.read_u16(instructions);
                    self.state.ip = ip + offset as usize;
                    continue;
                }
                // JumpBack
                0x14 => {
                    let ip = self.state.ip;
                    let offset = self.read_u16(instructions);
                    self.state.ip = ip - offset as usize;
                    continue;
                }
                // JumpIfFalse
                0x15 => {
                    let ip = self.state.ip;
                    let offset = self.read_u16(instructions);
                    if !self.is_truthy(self.peek()) {
                        self.state.ip = ip + offset as usize;
                        continue;
                    }
                }
                // FnCall
                0x16 => {
                    let n_args = self.read_u8(instructions) as usize;
                    let r#fn = self.state.stack[self.state.stack.len() - n_args - 1];
                    self.call_value(r#fn, n_args)?;
                    instructions = self.frame_unchecked().bytecode().as_ptr();
                    len = self.frame_unchecked().bytecode_len();
                    continue;
                }
                // TCOCall
                0x47 => {
                    let n_args = self.read_u8(instructions) as usize;
                    // Temporarily pop off the current function and its args
                    let args = self.pop_n(n_args).collect::<Vec<_>>();
                    let r#fn = self.pop();

                    // Pop off the previous function's stack frame, cco, and arguments
                    let prev_return_address = self.tco_clear_stacks();

                    // Push the function and its args back on the stack
                    self.push(r#fn);
                    self.state.stack.extend(args);

                    // Make the new call frame return to the previous function's return address
                    // by setting the ip to it
                    self.state.ip = prev_return_address;
                    self.call_value(r#fn, n_args)?;
                    instructions = self.frame_unchecked().bytecode().as_ptr();
                    len = self.frame_unchecked().bytecode_len();
                    continue;
                }
                // FwdCall
                0x54 => {
                    let n_packed = self.read_u8(instructions);
                    let n_fwd = (n_packed >> 4) as usize;
                    let n_normal = (n_packed & 0b1111) as usize;
                    let n_total = n_fwd + n_normal;

                    let mut forwarded = Vec::with_capacity(
                        // We assume that a rest array has 3 elements on average
                        n_normal + n_fwd * 3,
                    );
                    let mut packed = self.pop_n(n_total as _);
                    let mut packed_fwd = packed.by_ref().take(n_fwd);
                    while let Some(mut arg) = packed_fwd.next() {
                        match arg.as_native_object_mut() {
                            Some(obj) if obj.lox_name() == "array" => {
                                let arr = unsafe {
                                    <dyn NativeObject>::downcast_ref_mut::<LoxArray>(obj)
                                };
                                forwarded.extend(arr.baking_vec().iter().cloned());
                            }
                            _ => {
                                std::mem::drop(packed_fwd);
                                std::mem::drop(packed);
                                let ty = self.type_of(&arg);
                                return Err(self.error(format!(
                                    "Rest arguments must be arrays, but got an object of type `{}`",
                                    ty
                                )));
                            }
                        }
                    }
                    // Append the rest of the arguments
                    forwarded.extend(packed);

                    // Since we've popped off the arguments,
                    // the function should be sitting on the top of the stack; fetch it.
                    let r#fn = *self.peek();

                    // Push the arguments back
                    let n_args = forwarded.len();
                    self.state.stack.append(&mut forwarded);

                    // And call the function
                    self.call_value(r#fn, n_args)?;
                    instructions = self.frame_unchecked().bytecode().as_ptr();
                    len = self.frame_unchecked().bytecode_len();
                    continue;
                }
                // SetLocalFast
                0x17 => {
                    let offset = self.read_u8(instructions);
                    let obj = self.pop();
                    self.set_local_at(offset as usize, obj);
                }
                // Return
                0x18 => {
                    let result = self.pop();

                    let cs_size = self.state.call_stack.len();
                    if cs_size == 1 || return_early && cs_size == initial_cs_size {
                        if return_early {
                            self.cleanup_after_lox_call();
                        } else {
                            self.pop_frame();
                        }
                        return Ok(result);
                    }

                    self.cleanup_after_lox_call();
                    instructions = self.frame_unchecked().bytecode().as_ptr();
                    len = self.frame_unchecked().bytecode_len();
                    self.push(result);
                }
                // MulSub
                0x49 => {
                    // c - a * b
                    let rhs = self.pop();
                    let lhs = self.pop();
                    let rhs = binary_op!(self, lhs, rhs, *)?;
                    let lhs = self.pop();
                    let res = binary_op!(self, lhs, rhs, -)?;
                    self.push(res);
                }
                // MulAdd
                0x4A => {
                    // c + a * b
                    let rhs = *self.peek();
                    let lhs = *self.peek_at(1);
                    let rhs = binary_op!(self, lhs, rhs, *)?;
                    let lhs = *self.peek_at(2);
                    let res = binary_op!(self, lhs, rhs, +)?;
                    self.pop_n(3);
                    self.push(res);
                }
                // GetUpvalue
                0x19 => {
                    let index = self.read_u8(instructions) as usize;
                    let upvalue = self.get_upvalue(index);
                    self.push(match upvalue.as_ref().as_heap_value().follow_ptr_tup() {
                        (_, HeapValue::UpValue(up)) => *up,
                        (Some(ptr), _) => Value::Ptr(ptr),
                        _ => unreachable!(),
                    });
                }
                // GetUpvalueLong
                0x1A => {
                    let index = self.read_u16(instructions) as usize;
                    let upvalue = self.get_upvalue(index);
                    self.push(match upvalue.as_ref().as_heap_value().follow_ptr_tup() {
                        (_, HeapValue::UpValue(up)) => *up,
                        (Some(ptr), _) => Value::Ptr(ptr),
                        _ => unreachable!(),
                    });
                }
                // SetUpvalue
                0x1B => {
                    let index = self.read_u8(instructions) as usize;
                    let value = self.pop();
                    let mut upvalue_ptr = self.get_upvalue(index);
                    upvalue_ptr.as_mut().set_value(HeapValue::UpValue(value));
                    self.push(Value::Ptr(upvalue_ptr));
                }
                // SetUpvalueLong
                0x1C => {
                    let index = self.read_u16(instructions) as usize;
                    let value = self.pop();
                    let mut upvalue_ptr = self.get_upvalue(index);
                    upvalue_ptr.as_mut().set_value(HeapValue::UpValue(value));
                    self.push(Value::Ptr(upvalue_ptr));
                }
                // MakeClosure
                0x1D => {
                    let slot = self.read_u8(instructions) as usize;
                    let cco = *self.const_at(slot);
                    let closure = self.create_upvalues(
                        cco.as_ptr().cloned().unwrap(),
                        match cco.as_heap_value().unwrap() {
                            HeapValue::CCO(cco) => cco,
                            _ => unreachable!(),
                        },
                        false,
                    );
                    self.push(closure);
                }
                // MakeClosureLong
                0x1E => {
                    let slot = self.read_u16(instructions) as usize;
                    let cco = *self.const_at(slot);
                    let closure = self.create_upvalues(
                        cco.as_ptr().cloned().unwrap(),
                        match cco.as_heap_value().unwrap() {
                            HeapValue::CCO(cco) => cco,
                            _ => unreachable!(),
                        },
                        true,
                    );
                    self.push(closure);
                }
                // LoadNativeFn
                0x1F => {
                    let n = self.read_u8(instructions);
                    let ptr = *self.builtins.get(&n).unwrap();
                    self.push(Value::Ptr(ptr));
                }
                // Interpolate
                0x20 => {
                    let n = self.read_u8(instructions);
                    let mut interpolated = String::with_capacity(16); // 16 characters
                    for v in self.pop_n(n as usize).collect::<Vec<_>>() {
                        interpolated.extend(Some(self.stringify(&v)?.into_owned()));
                    }
                    let obj = self.alloc_or_get_interned(interpolated);
                    self.push(obj);
                }
                // InterpolateLong
                0x21 => {
                    let n = self.read_u16(instructions);
                    let mut interpolated = String::with_capacity(16); // 16 characters
                    for v in self.pop_n(n as usize).collect::<Vec<_>>() {
                        interpolated.extend(Some(self.stringify(&v)?.into_owned()));
                    }
                    let obj = self.alloc_or_get_interned(interpolated);
                    self.push(obj);
                }
                // MatchPushBound
                0x23 => {
                    let obj = *self.peek();
                    self.state.match_stack.push(obj);
                }
                // MatchGetBound
                0x24 => {
                    let obj = *self.state.match_stack.last().unwrap();
                    self.push(obj);
                }
                // MatchPopBound
                0x25 => {
                    self.state.match_stack.pop();
                }
                // MakeClass
                0x26 => {
                    let offset = self.read_u8(instructions) as usize;
                    let name = self.const_at_string(offset);
                    let cls =
                        ClassObject::empty_with_name(name, self.frame_unchecked().cco().module);
                    let ptr = self.alloc(LoxObject::new(HeapValue::from(cls)));
                    self.push(Value::Ptr(ptr));
                }
                // MakeClassLong
                0x27 => {
                    let offset = self.read_u16(instructions) as usize;
                    let name = self.const_at_string(offset);
                    let cls =
                        ClassObject::empty_with_name(name, self.frame_unchecked().cco().module);
                    let ptr = self.alloc(LoxObject::new(HeapValue::from(cls)));
                    self.push(Value::Ptr(ptr));
                }
                // Inherit
                0x4C => {
                    let super_cls = *self.peek();
                    let mut cls_ptr = *self.peek_at(1);

                    if super_cls.as_class().is_none() {
                        return Err(self.error(format!(
                            "The superclass must be a class but was `{}`",
                            self.type_of(&super_cls)
                        )));
                    }

                    let cls = cls_ptr.as_class_mut().unwrap();
                    bump_alloc_size!(self, cls, cls.inherit(*super_cls.as_ptr().unwrap()));
                }
                // SetField
                0x28 => {
                    let offset = self.read_u8(instructions) as usize;
                    let obj = *self.peek();
                    let value = *self.peek_at(1);
                    let name = self.const_at_string(offset);
                    let result = self.set_field(obj, name, value)?;
                    let _obj = self.pop();
                    let _value = self.pop();
                    self.push(result);
                }
                // SetFieldLong
                0x29 => {
                    let offset = self.read_u16(instructions) as usize;
                    let obj = *self.peek();
                    let value = *self.peek_at(1);
                    let name = self.const_at_string(offset);
                    let result = self.set_field(obj, name, value)?;
                    let _obj = self.pop();
                    let _value = self.pop();
                    self.push(result);
                }
                // GetField
                0x2A => {
                    let offset = self.read_u8(instructions) as usize;
                    let obj = *self.peek();
                    let name = self.const_at_string(offset);
                    let result = self.get_field(obj, name)?;
                    let _obj = self.pop();
                    self.push(result);
                }
                // GetFieldLong
                0x2B => {
                    let offset = self.read_u16(instructions) as usize;
                    let obj = *self.peek();
                    let name = self.const_at_string(offset);
                    let result = self.get_field(obj, name)?;
                    let _obj = self.pop();
                    self.push(result);
                }
                // SetItem
                0x2C => {
                    let obj = *self.peek_at(2);
                    let iterator = self.get_field(obj, "setitem")?;
                    self.set_stack_at(2, iterator);
                    self.call_value(iterator, 2)?;
                    instructions = self.frame_unchecked().bytecode().as_ptr();
                    len = self.frame_unchecked().bytecode_len();
                    continue;
                }
                // GetItem
                0x2D => {
                    let obj = *self.peek_at(1);
                    let iterator = self.get_field(obj, "getitem")?;
                    self.set_stack_at(1, iterator);
                    self.call_value(iterator, 1)?;
                    instructions = self.frame_unchecked().bytecode().as_ptr();
                    len = self.frame_unchecked().bytecode_len();
                    continue;
                }
                // GetSuper
                0x4D => {
                    let idx = self.read_u8(instructions) as usize;
                    let name = self.const_at_string(idx);
                    let superclass = *self.peek();
                    let instance = *self.peek_at(1);

                    let result = self.get_superclass_method(instance, superclass, name)?;

                    let _cls = self.pop();
                    let _obj = self.pop();

                    self.push(result);
                }
                // MakeMethod
                0x2E => {
                    let method = self.pop();
                    self.peek()
                        .clone()
                        .as_heap_value_mut()
                        .and_then(HeapValue::as_class_mut)
                        .unwrap()
                        .add_method(*method.as_ptr().unwrap());
                }
                // InvokeMethod
                0x2F => {
                    let offset = self.read_u8(instructions) as usize;
                    let name = self.const_at_string(offset);
                    let n_args = self.read_u8(instructions) as usize;
                    let instance = *self.peek_at(n_args);
                    let r#fn = self.get_field(instance, name)?;
                    self.call_value(r#fn, n_args)?;
                    instructions = self.frame_unchecked().bytecode().as_ptr();
                    len = self.frame_unchecked().bytecode_len();
                    continue;
                }
                // InvokeMethodTCO
                0x4B => {
                    let offset = self.read_u8(instructions) as usize;
                    let name = self.const_at_string(offset);
                    let n_args = self.read_u8(instructions) as usize;
                    let instance = *self.peek_at(n_args);
                    let r#fn = self.get_field(instance, name)?;

                    // Temporarily pop off the method's instance and its args
                    let args = self.pop_n(n_args).collect::<Vec<_>>();
                    let r#fn_instance = self.pop();

                    // Pop off the previous function's stack frame, cco, and arguments
                    let prev_return_address = self.tco_clear_stacks();

                    // Push the method's instance and its args back on the stack
                    self.push(r#fn_instance);
                    self.state.stack.extend(args);

                    // Make the new call frame return to the previous function's return address
                    // by setting the ip to it
                    self.state.ip = prev_return_address;
                    self.call_value(r#fn, n_args)?;
                    instructions = self.frame_unchecked().bytecode().as_ptr();
                    len = self.frame_unchecked().bytecode_len();
                    continue;
                }
                // Nil
                0x30 => {
                    self.push(Value::Nil);
                }
                // True
                0x31 => {
                    self.push(Value::Bool(true));
                }
                // False
                0x32 => {
                    self.push(Value::Bool(false));
                }
                // One
                0x33 => {
                    self.push(Value::Int(1));
                }
                // Zero
                0x34 => {
                    self.push(Value::Int(0));
                }
                // Inc
                0x35 => {
                    let slot = self.read_u8(instructions) as usize;
                    let local = *self.local_at(slot);
                    let result = binary_op!(self, local, Value::Int(1), +)?;
                    let result = *self.set_local_at(slot, result);
                    self.push(result);
                }
                // Dec
                0x36 => {
                    let slot = self.read_u8(instructions) as usize;
                    let local = *self.local_at(slot);
                    let result = binary_op!(self, local, Value::Int(1), -)?;
                    let result = *self.set_local_at(slot, result);
                    self.push(result);
                }
                // IncLong
                0x37 => {
                    let slot = self.read_u16(instructions) as usize;
                    let local = *self.local_at(slot);
                    let result = binary_op!(self, local, Value::Int(1), +)?;
                    let result = *self.set_local_at(slot, result);
                    self.push(result);
                }
                // DecLong
                0x38 => {
                    let slot = self.read_u16(instructions) as usize;
                    let local = *self.local_at(slot);
                    let result = binary_op!(self, local, Value::Int(1), -)?;
                    let result = *self.set_local_at(slot, result);
                    self.push(result);
                }
                // GreaterEqual
                0x39 => {
                    let rhs = self.pop();
                    let lhs = self.pop();
                    let res = binary_op!(self, lhs, rhs, ord => >=)?;
                    self.push(res);
                }
                // GreaterThan
                0x3A => {
                    let rhs = self.pop();
                    let lhs = self.pop();
                    let res = binary_op!(self, lhs, rhs, ord => >)?;
                    self.push(res);
                }
                // LessEqual
                0x3B => {
                    let rhs = self.pop();
                    let lhs = self.pop();
                    let res = binary_op!(self, lhs, rhs, ord => <=)?;
                    self.push(res);
                }
                // LessThan
                0x3C => {
                    let rhs = self.pop();
                    let lhs = self.pop();
                    let res = binary_op!(self, lhs, rhs, ord => <)?;
                    self.push(res);
                }
                // Copy
                0x3D => {
                    let obj = *self.peek();
                    self.push(obj);
                }
                // CopyN
                0x53 => {
                    let n = self.read_u8(instructions) as usize;
                    let value = *self.peek();
                    self.state.stack.extend(std::iter::repeat(value).take(n));
                }
                // IsNil
                0x3E => {
                    let obj = self.pop();
                    self.push(Value::Bool(obj.is_nil()))
                }
                // IsNotNil
                0x3F => {
                    let obj = self.pop();
                    self.push(Value::Bool(!obj.is_nil()))
                }
                // IntoIter
                0x40 => {
                    let obj = *self.peek();
                    let iterator = self.get_field(obj, "iterator")?;
                    self.set_stack_at(0, iterator);
                    self.call_value(iterator, 0)?;
                    instructions = self.frame_unchecked().bytecode().as_ptr();
                    len = self.frame_unchecked().bytecode_len();
                    continue;
                }
                // IterNext
                0x41 => {
                    let obj = *self.peek();
                    let iternext = self.get_field(obj, "iternext")?;
                    self.set_stack_at(0, iternext);
                    self.call_value(iternext, 0)?;
                    instructions = self.frame_unchecked().bytecode().as_ptr();
                    len = self.frame_unchecked().bytecode_len();
                    continue;
                }
                // Unwrap
                0x51 => {
                    let mut obj = *self.peek();
                    let obj = obj
                        .as_native_object_mut_with_ptr()
                        .map(|(ptr, obj)| {
                            if obj.lox_name() == "Ok" {
                                obj.get_field(self, ptr, "value").unwrap()
                            } else {
                                Value::Ptr(ptr)
                            }
                        })
                        .unwrap_or(obj);
                    self.set_stack_at(0, obj);
                }
                // GetFieldFast
                0x42 => {
                    let offset = self.read_u8(instructions) as usize;
                    let field_offset = self.read_u8(instructions) as usize;
                    let obj = *self.local_at(offset);
                    let field = self.const_at_string(field_offset);
                    let result = self.get_field(obj, field)?;
                    self.push(result);
                }
                // SetFieldFast
                0x43 => {
                    let offset = self.read_u8(instructions) as usize;
                    let field_offset = self.read_u8(instructions) as usize;
                    let obj = *self.local_at(offset);
                    let field = self.const_at_string(field_offset);
                    let value = *self.peek();
                    let result = self.set_field(obj, field, value)?;
                    self.pop();
                    self.push(result);
                }
                // MakeArr
                0x44 => {
                    let n = self.read_u8(instructions) as usize;
                    let ptr = self.alloc_array_from_stack(n);
                    self.push(Value::Ptr(ptr));
                }
                // MakeDict
                0x48 => {
                    let n = self.read_u8(instructions) as usize;
                    let ptr = self.alloc_dict_from_stack(n);
                    self.push(Value::Ptr(ptr));
                }
                // Print
                0x45 => {
                    let v = self.pop();
                    println!("{}", self.stringify(&v)?);
                }
                // PrintN
                0x46 => {
                    let n = self.read_u8(instructions);
                    let items = self.pop_n(n as usize).collect::<Vec<_>>();
                    let mut output = Vec::with_capacity(items.len());
                    for item in items {
                        output.push(self.stringify(&item)?);
                    }
                    println!("{}", output.join(", "));
                }
                // MakeModule
                0x22 => {
                    let offset = self.read_u16(instructions) as usize;
                    let mut ptr = *self.const_at(offset).as_ptr().unwrap();
                    let module: &mut LoxModule =
                        unsafe { <dyn NativeObject>::downcast_mut::<LoxModule>(&mut ptr) };
                    for export in module.exports_mut().values_mut() {
                        *export = *self.local_at(export.as_int().unwrap() as usize);
                    }
                    self.push(Value::Ptr(ptr))
                }
                // Import
                0x52 => {
                    let offset = self.read_u16(instructions) as usize;
                    let module_id = ModuleId {
                        0: self.const_at(offset).as_int().unwrap() as u64,
                    };
                    let mut guard = self.ctx.write_shared();
                    let module = guard.get_module_mut(module_id).unwrap();
                    if !module.evaluated() {
                        let cco_ptr = module.cco_ptr().unwrap();
                        module.set_evaluated();
                        std::mem::drop(guard);
                        self.push(Value::Ptr(cco_ptr));
                        self.call_value(Value::Ptr(cco_ptr), 0)?;
                        instructions = self.frame_unchecked().bytecode().as_ptr();
                        len = self.frame_unchecked().bytecode_len();
                        continue;
                    } else {
                        let module_ptr = module.lox_module().unwrap();
                        std::mem::drop(guard);
                        self.push(Value::Ptr(module_ptr));
                    }
                }
                // TimeoutCheck
                0x55 => {
                    if self
                        .state
                        .timeout
                        .map(|t| self.state.time.elapsed() >= t)
                        .unwrap_or(false)
                    {
                        return Err(self.error(format!(
                            "TimeoutError: The VM has exceeded the specified timeout `{}ms`",
                            self.state.timeout.unwrap().as_millis()
                        )));
                    }
                }
                // Unknown, MissingJumpLabel, Error
                0xFD | 0xFE | 0xFF => unreachable!(),
                byte => panic!("Unknown opcode: {:#?}", byte),
            }

            self.state.ip += 1;
        }

        Ok(self.state.stack.pop().unwrap_or(Value::Nil))
    }

    /// Attempts to call the given value as a function.
    /// This method automatically prepares the stack for the call.
    #[allow(clippy::branches_sharing_code)]
    pub fn call_function(&mut self, r#fn: Value, args: Vec<Value>) -> Result<Value, LoxError> {
        let ip_before = self.state.ip;
        let n_args = args.len();
        self.push(r#fn);
        self.state.stack.extend(args);
        let result = if r#fn
            .as_heap_value()
            .map(HeapValue::is_obj_native)
            .unwrap_or(false)
        {
            self.call_value(r#fn, n_args)?;
            self.pop()
        } else {
            self.call_value(r#fn, n_args)?;
            self.run_dispatch_loop(true)?
        };
        // Restore the ip to its previous value.
        // We need to do this because the return address captured by the call frame
        // may not match the original value in some cases.
        self.state.ip = ip_before;
        Ok(result)
    }

    /// Pops off the current function's stack frame, cco, and arguments.
    /// Returns the return address of the function.
    ///
    /// Note: The current function must be the _previous_ function, i.e. the function
    /// whose call frame we want to remove.
    #[inline(always)]
    fn tco_clear_stacks(&mut self) -> usize {
        let window = self.frame_unchecked().stack_window_index;
        let _prev_args = self
            .state
            .stack
            .drain(window..self.state.stack.len())
            .for_each(|_| ());
        let _prev_fn = self.pop();
        let _prev_frame = self.pop_frame();
        _prev_frame.return_address
    }

    fn call_value(&mut self, value: Value, mut n_args: usize) -> Result<(), LoxError> {
        if let Value::Ptr(mut ptr) = value {
            let ptr_copy = ptr;
            match ptr.as_mut().as_heap_value_mut().follow_ptr_tup_mut() {
                // The cco is directly pointed at
                (None, HeapValue::CCO(cco)) => {
                    self.check_arity(cco, n_args)?;
                    self.handle_varargs(cco, &mut n_args);
                    return self.call(ptr, n_args);
                }
                // The closure is directly pointed at
                (None, HeapValue::Closure { cco, .. }) => {
                    self.check_arity(cco.as_ref().as_cco(), n_args)?;
                    self.handle_varargs(cco.as_ref().as_cco(), &mut n_args);
                    return self.call(ptr, n_args);
                }
                (None, HeapValue::BoundMethod { cco, instance }) => {
                    self.check_arity(cco.as_ref().as_cco(), n_args)?;
                    self.handle_varargs(cco.as_ref().as_cco(), &mut n_args);
                    return self
                        .call(*cco, n_args)
                        .map(|_| self.push(Value::Ptr(*instance)));
                }
                (None, HeapValue::Cls(cls)) => {
                    let obj = Object::with_class(ptr_copy);
                    let ptr = self.alloc(LoxObject::new(HeapValue::from(obj)));
                    self.set_stack_at(n_args, Value::Ptr(ptr));
                    if let Ok(init) = self.get_field(Value::Ptr(ptr), "init") {
                        return self.call_value(init, n_args);
                    } else {
                        self.check_no_init_args(&cls.name, n_args)?;
                        self.state.ip += 1;
                        return Ok(());
                    }
                }
                (None, HeapValue::ObjNative(obj)) if obj.is_callable() => {
                    self.check_arity_obj(&**obj, n_args)?;
                    let args = self.peek_n(n_args).to_vec();
                    let result = self.call_obj(&mut **obj, ptr_copy, args)?;
                    self.pop_n(n_args);
                    let _fn = self.pop();
                    self.push(result);
                    // Update the ip
                    self.state.ip += 1;
                    return Ok(());
                }
                // The cco is behind a forwarding pointer
                (Some(ref ptr), HeapValue::CCO(cco)) => {
                    self.check_arity(cco, n_args)?;
                    self.handle_varargs(cco, &mut n_args);
                    return self.call(*ptr, n_args);
                }
                // The closure is behind a forwarding pointer
                (Some(ref ptr), HeapValue::Closure { cco, .. }) => {
                    self.check_arity(cco.as_ref().as_cco(), n_args)?;
                    self.handle_varargs(cco.as_ref().as_cco(), &mut n_args);
                    return self.call(*ptr, n_args);
                }
                (Some(_), HeapValue::BoundMethod { cco, instance }) => {
                    self.check_arity(cco.as_ref().as_cco(), n_args)?;
                    self.handle_varargs(cco.as_ref().as_cco(), &mut n_args);
                    return self
                        .call(*cco, n_args)
                        .map(|_| self.push(Value::Ptr(*instance)));
                }
                (Some(ptr), HeapValue::Cls(cls)) => {
                    let obj = Object::with_class(ptr);
                    let ptr = self.alloc(LoxObject::new(HeapValue::from(obj)));
                    self.set_stack_at(n_args, Value::Ptr(ptr));
                    if let Ok(init) = self.get_field(Value::Ptr(ptr), "init") {
                        return self.call_value(init, n_args);
                    } else {
                        self.check_no_init_args(&cls.name, n_args)?;
                        self.state.ip += 1;
                        return Ok(());
                    }
                }
                (Some(ref ptr), HeapValue::ObjNative(obj)) if obj.is_callable() => {
                    self.check_arity_obj(&**obj, n_args)?;
                    let args = self.peek_n(n_args).to_vec();
                    let result = self.call_obj(&mut **obj, *ptr, args)?;
                    self.pop_n(n_args);
                    let _fn = self.pop();
                    self.push(result);
                    // Update the ip
                    self.state.ip += 1;
                    return Ok(());
                }
                _ => (),
            }
        }
        Err(self.error("Can only call functions and classes"))
    }

    fn call_obj(
        &mut self,
        obj: &mut dyn NativeObject,
        this: ObjectPointer,
        args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        self.push_native_frame(obj.lox_name())?;
        let result = bump_alloc_size!(self, obj, obj.call(self, this, args))?;
        self.pop_frame();
        Ok(result)
    }

    #[inline]
    fn cleanup_after_lox_call(&mut self) {
        let window = self.frame_unchecked().stack_window_index;
        self.state.stack.drain(window..self.state.stack.len());

        let _fn = self.pop();
        let prev_frame = self.pop_frame();
        self.state.ip = prev_frame.return_address;
    }

    fn set_field(&mut self, mut obj: Value, field: &str, value: Value) -> Result<Value, LoxError> {
        if let Value::Ptr(ref mut ptr) = obj {
            match ptr.as_mut().as_heap_value_mut() {
                HeapValue::UpValue(obj) => return self.set_field(*obj, field, value),
                HeapValue::Obj(obj) => {
                    let size_before = obj.fields_alloc_size() as isize;
                    obj.set_field(field, value);
                    let size_after = obj.fields_alloc_size() as isize;
                    let size_delta = size_after - size_before;
                    self.bump_alloc_size(size_delta);
                    return Ok(value);
                }
                HeapValue::ObjNative(obj) => {
                    return bump_alloc_size!(self, obj, obj.set_field(self, field, value));
                }
                _ => (),
            }
        }

        Err(self.error(format!(
            "Only instances have fields, got `{}`.",
            self.type_of(&obj)
        )))
    }

    #[inline]
    pub fn stringify(&mut self, obj: &Value) -> Result<CowStr, LoxError> {
        obj.stringify(self)
    }

    pub(crate) fn stringify_ptr(&mut self, ptr: ObjectPointer) -> Result<CowStr, LoxError> {
        // There's a circular reference between the printed objects
        if pset_check_pointer(&ptr) {
            return Ok("...".into());
        }

        // Mark the pointer as visited
        pset_add_pointer(ptr);

        let result = match ptr.as_ref().as_heap_value() {
            HeapValue::Str(box s) => Ok(s.as_ref().to_string().into()),
            rest => {
                if let Ok(r#fn) = self.get_field(Value::Ptr(ptr), "to_str") {
                    let v = self.call_function(r#fn, vec![])?;
                    v.as_heap_value()
                        .and_then(HeapValue::as_string)
                        .map(|v| v.as_ref().to_string().into())
                        .ok_or_else(|| self.error("`to_str()` returned a non-string type"))
                } else {
                    rest.stringify(self)
                }
            }
        };

        // Unmark the pointer
        pset_remove_pointer(&ptr);

        result
    }

    #[inline]
    fn type_cmp(&self, obj: &Value, class: &Value) -> Option<bool> {
        class.as_heap_value().and_then(|v| match v.follow_ptr() {
            HeapValue::Cls(cls) => Some(
                obj.as_object()
                    .map(|obj| obj.is_instance_of(cls))
                    .unwrap_or(false),
            ),
            HeapValue::ObjNative(cls) => Some(match obj.as_native_object() {
                Some(obj) => obj.is_instance_of(&**cls),
                None => cls.is_type_of(obj),
            }),
            HeapValue::UpValue(_) => unreachable!(),
            _ => None,
        })
    }

    pub fn try_cast_with_fallback(
        &mut self,
        obj: Value,
        ty: Value,
        magic_name: &str,
    ) -> Result<Value, LoxError> {
        match self.get_field(obj, magic_name) {
            Ok(value) => self.call_function(value, vec![]),
            Err(_) => {
                let cast = self.get_field(obj, "cast")?;
                self.call_function(cast, vec![ty])
            }
        }
    }

    pub(crate) fn get_field(&mut self, mut obj: Value, field: &str) -> Result<Value, LoxError> {
        if let Value::Ptr(ref mut ptr) = obj {
            let ptr_copy = *ptr;
            match ptr.as_mut().as_heap_value_mut() {
                HeapValue::UpValue(obj) => return self.get_field(*obj, field),
                HeapValue::Obj(obj) => {
                    return match obj.get_field(field) {
                        Some(value) => Ok(*value),
                        None => match obj.get_method(field) {
                            BindingStatus::Bound(ptr) => Ok(Value::Ptr(ptr)),
                            BindingStatus::Unbound(method_ptr) => {
                                let bound = self.alloc(LoxObject::new(HeapValue::BoundMethod {
                                    cco: method_ptr,
                                    instance: ptr_copy,
                                }));
                                bump_obj_alloc_size!(self, obj, obj.bind_method(field, bound));
                                Ok(Value::Ptr(bound))
                            }
                            BindingStatus::NotFound => {
                                Err(self.error(format!("Undefined field `{}`", field)))
                            }
                        },
                    }
                }
                HeapValue::ObjNative(obj) => {
                    let at = self.state.ip;
                    // The key used for caching methods is a hash of the runtime id of the object
                    // (pointer as usize by default) and the field we're fetching.
                    let id = obj.runtime_id();
                    let key = {
                        let mut h = fnv::FnvHasher::default();
                        id.hash(&mut h);
                        field.hash(&mut h);
                        h.finish()
                    };
                    let cache = &mut self.frame_unchecked_mut().chunk_mut().inline_cache;

                    // Try to fetch the method value from the inline cache
                    if let Some(method) = cache.lookup(at, key).cloned() {
                        return Ok(method);
                    }

                    // This is a cache miss, gotta fetch the method via a virtual call
                    let result = bump_alloc_size!(self, obj, obj.get_field(self, ptr_copy, field));
                    let cache = &mut self.frame_unchecked_mut().chunk_mut().inline_cache;
                    return result.map(|v| {
                        cache.update(at, key, v);
                        v
                    });
                }
                HeapValue::Str(s) => {
                    return bump_alloc_size!(self, s, s.get_field(self, ptr_copy, field));
                }
                _ => (),
            }
        }

        Err(self.error(format!(
            "Only instances have fields, got `{}`.",
            self.type_of(&obj)
        )))
    }

    pub(crate) fn get_superclass_method(
        &mut self,
        mut obj: Value,
        superclass: Value,
        method: &str,
    ) -> Result<Value, LoxError> {
        if let Value::Ptr(ref mut ptr) = obj {
            let ptr_copy = *ptr;
            match ptr.as_mut().as_heap_value_mut() {
                HeapValue::UpValue(obj) => {
                    return self.get_superclass_method(*obj, superclass, method)
                }
                HeapValue::Obj(obj) => {
                    return match obj.get_field(method) {
                        Some(value) => Ok(*value),
                        None => match obj
                            .get_superclass_method(method, *superclass.as_ptr().unwrap())
                        {
                            BindingStatus::Bound(ptr) => Ok(Value::Ptr(ptr)),
                            BindingStatus::Unbound(method_ptr) => {
                                let bound = self.alloc(LoxObject::new(HeapValue::BoundMethod {
                                    cco: method_ptr,
                                    instance: ptr_copy,
                                }));
                                let method =
                                    format!("{}.{}", superclass.as_class().unwrap().name, method);
                                bump_obj_alloc_size!(self, obj, obj.bind_method(method, bound));
                                Ok(Value::Ptr(bound))
                            }
                            BindingStatus::NotFound => {
                                Err(self.error(format!("Undefined field `{}`", method)))
                            }
                        },
                    }
                }
                _ => (),
            }
        }

        Err(self.error(format!(
            "Only instances have fields, got `{}`.",
            self.type_of(&obj)
        )))
    }

    #[inline(always)]
    fn bump_alloc_size(&mut self, amount: isize) {
        self.alloc_size_adjustment += amount;
    }
}
