use std::time::Duration;

use crate::{compiler::cco::Arity, types::LoxInt, vm::builtin::result::LoxResult};

use errors::LoxError;
use vm::builtin::fn_wrapper::*;
use vm::object::ObjectPointer;
use vm::{
    native::CowStr,
    object::{native::NativeObject, value::Value},
    vm::Vm,
};

use crossterm::{
    event::{poll, read, Event, KeyCode, KeyEvent},
    terminal,
};

pub(super) struct LoxKeysIter {
    interval: u64,
    iter_next: Option<ObjectPointer>,
}
known_deep_size_fast!(0, LoxKeysIter);

impl LoxKeysIter {
    pub fn new(interval: u64) -> Self {
        Self {
            interval,
            iter_next: None,
        }
    }

    fn read_key(&self, poll_time: u64) -> crossterm::Result<Option<KeyEvent>> {
        terminal::enable_raw_mode().unwrap();
        let result = match poll(Duration::from_millis(poll_time)) {
            Ok(true) => read().map(|event| match event {
                Event::Key(key) => Some(key),
                _ => None,
            }),
            Ok(false) => Ok(None),
            Err(e) => Err(e),
        };
        terminal::disable_raw_mode().unwrap();
        result
    }
}

impl NativeObject for LoxKeysIter {
    fn lox_name(&self) -> CowStr {
        CowStr::Borrowed("KeysIter")
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::Borrowed("<KeysIter instance>"))
    }

    fn get_field(
        &mut self,
        vm: &mut Vm,
        this: ObjectPointer,
        name: &str,
    ) -> Result<Value, LoxError> {
        match name {
            "iternext" => Ok(Value::Ptr(*self.iter_next.get_or_insert_with(|| {
                alloc_trait_fn_vm(
                    vm,
                    "keysIter.iternext",
                    Arity::Fixed(0),
                    "Returns the next key in the input if it's available during the poll interval, nil otherwise.",
                    move |vm, _, _| -> Result<_, _> {
                        let mut ptr = this;
                        let this = unsafe { <dyn NativeObject>::downcast_mut::<LoxKeysIter>(&mut ptr) };
                        match this.read_key(this.interval).map_err(|e| {
                            vm.error(format!("Failed to read a key from STDIN: {}", e))
                        })? {
                            Some(key) => {
                                let value = match key.code {
                                    KeyCode::Char(ch) => {
                                        let dst = &mut [0; 4];
                                        let s = ch.encode_utf8(dst);
                                        Value::Ptr(vm.alloc_or_get_interned_str(s))
                                    }
                                    KeyCode::F(f) => Value::Int(f as LoxInt),
                                    KeyCode::Backspace => {
                                        Value::Ptr(vm.alloc_or_get_interned_str("backspace"))
                                    }
                                    KeyCode::Enter => {
                                        Value::Ptr(vm.alloc_or_get_interned_str("Enter"))
                                    }
                                    KeyCode::Tab => Value::Ptr(vm.alloc_or_get_interned_str("Tab")),
                                    KeyCode::Left => {
                                        Value::Ptr(vm.alloc_or_get_interned_str("left"))
                                    }
                                    KeyCode::Right => {
                                        Value::Ptr(vm.alloc_or_get_interned_str("right"))
                                    }
                                    KeyCode::Up => Value::Ptr(vm.alloc_or_get_interned_str("up")),
                                    KeyCode::Down => {
                                        Value::Ptr(vm.alloc_or_get_interned_str("down"))
                                    }
                                    KeyCode::Home => {
                                        Value::Ptr(vm.alloc_or_get_interned_str("home"))
                                    }
                                    KeyCode::End => Value::Ptr(vm.alloc_or_get_interned_str("end")),
                                    KeyCode::PageUp => {
                                        Value::Ptr(vm.alloc_or_get_interned_str("page_up"))
                                    }
                                    KeyCode::PageDown => {
                                        Value::Ptr(vm.alloc_or_get_interned_str("page_down"))
                                    }
                                    KeyCode::BackTab => {
                                        Value::Ptr(vm.alloc_or_get_interned_str("back_tab"))
                                    }
                                    KeyCode::Delete => {
                                        Value::Ptr(vm.alloc_or_get_interned_str("delete"))
                                    }
                                    KeyCode::Insert => {
                                        Value::Ptr(vm.alloc_or_get_interned_str("insert"))
                                    }
                                    KeyCode::Null => {
                                        Value::Ptr(vm.alloc_or_get_interned_str("null"))
                                    }
                                    KeyCode::Esc => Value::Ptr(vm.alloc_or_get_interned_str("esc")),
                                };
                                Ok(Value::Ptr(LoxResult::alloc_new_ok(vm, value)))
                            }
                            None => Ok(Value::Ptr(LoxResult::alloc_new_err(vm, Value::Nil))),
                        }
                    },
                    &[this],
                )
            }))),
            "iterator" => Ok(Value::Ptr(this)),
            "interval" => Ok(Value::Int(self.interval as LoxInt)),
            _ => Err(vm.error(format!("Undefined property `{}`", name))),
        }
    }

    fn arity(&self) -> Option<Arity> {
        Some(Arity::Optional)
    }

    fn call(
        &mut self,
        vm: &mut Vm,
        this: ObjectPointer,
        args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        if let Some(value) = args.first() {
            match value.as_int() {
                Some(interval) => {
                    self.interval = interval as u64;
                }
                None => {
                    return Err(vm.error(format!(
                        "The interval must be a positive integer, but got an object of  type`{}`",
                        vm.type_of(value)
                    )))
                }
            }
        }
        Ok(Value::Ptr(this))
    }

    fn lox_doc(&self) -> CowStr {
        CowStr::Borrowed(
            r#"
    Lox's builtin class that allows to iterate over input keys.

    KeysIter::iternext()

        Polls for the next key in the input. Returns nil if not key was pressed during the poll interval.

    KeysIter::interval

        The poll interval in milliseconds.

    KeysIter(interval)
        
        Sets the poll interval globally.
"#,
        )
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(self.iter_next.as_mut().into_iter())
    }
}

impl std::fmt::Debug for LoxKeysIter {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "LoxKeysIter {{ keys: Keys {{ .. }}, iter_next: {:?} }}",
            self.iter_next
        )
    }
}
