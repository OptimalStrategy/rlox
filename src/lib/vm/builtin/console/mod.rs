pub mod keys;

use std::io::{stdout, Write};

use crate::rt_hashmap::FastHashMap;
use crossterm::{cursor, ExecutableCommand};

use crate::{compiler::cco::Arity, context::SharedCtx};

use errors::LoxError;
use vm::builtin::fn_wrapper::*;
use vm::object::ObjectPointer;
use vm::{
    native::CowStr,
    object::{native::NativeObject, value::Value},
    vm::Vm,
    HeapValue, LoxObject,
};

use self::keys::LoxKeysIter;

pub struct LoxConsole {
    methods: FastHashMap<&'static str, ObjectPointer>,
}
impl_vtable_deepsize!(LoxConsole, methods, ObjectPointer);

impl LoxConsole {
    pub fn new(ctx: SharedCtx) -> Self {
        let mut methods = FastHashMap::default();
        methods.insert(
            "write",
            alloc_trait_fn_ctx(
                ctx.clone(),
                "Console.write",
                Arity::InRange(1, 2),
                "Writes the string representation of the given object to STDOUT.",
                move |vm, _, args| {
                    let message = vm.stringify(&args[0])?;
                    print!("{}", message);
                    if vm.is_truthy(args.get(1).unwrap_or(&Value::Bool(false))) {
                        stdout()
                            .flush()
                            .map_err(|e| vm.error(format!("Failed to flush STDOUT: {}", e)))?;
                    }
                    Ok(Value::Nil)
                },
            ),
        );
        methods.insert(
            "clear",
            alloc_trait_fn_ctx(
                ctx.clone(),
                "Console.clear",
                Arity::Fixed(0),
                "Clears the console using the following ANSI escape sequence: `\\u{1b}[2J\\u{1b}[H`.",
                move |vm, _, _| {
                    print!("\u{1b}[2J\u{1b}[H");
                    stdout()
                        .flush()
                        .map_err(|e| vm.error(format!("Failed to flush STDOUT: {}", e)))?;
                    Ok(Value::Nil)
                },
            ),
        );
        methods.insert(
            "clear_line",
            alloc_trait_fn_ctx(
                ctx.clone(),
                "Console.clear_line",
                Arity::Fixed(0),
                "Clears the line the cursor is currently on using the following ANSI escape sequence: `\\u{1b}[2K`.",
                move |vm, _, _| {
                    print!("\u{1b}[2K");
                    stdout()
                        .flush()
                        .map_err(|e| vm.error(format!("Failed to flush STDOUT: {}", e)))?;
                    Ok(Value::Nil)
                },
            ),
        );
        methods.insert(
            "clear_line_right",
            alloc_trait_fn_ctx(
                ctx.clone(),
                "Console.clear_line_right",
                Arity::Fixed(0),
                "Clears the line to the right of the cursor using the following ANSI escape sequence: `\\u{1b}[K`.",
                move |vm, _, _| {
                    print!("\u{1b}[K");
                    stdout()
                        .flush()
                        .map_err(|e| vm.error(format!("Failed to flush STDOUT: {}", e)))?;
                    Ok(Value::Nil)
                },
            ),
        );
        methods.insert(
            "reset",
            alloc_trait_fn_ctx(
                ctx.clone(),
                "Console.reset",
                Arity::Fixed(0),
                "Moves the cursor to the upper left corner using the following ANSI escape sequence: `\\u{1b}[H`.",
                move |vm, _, _| {
                    print!("\u{1b}[H");
                    stdout()
                        .flush()
                        .map_err(|e| vm.error(format!("Failed to flush STDOUT: {}", e)))?;
                    Ok(Value::Nil)
                },
            ),
        );
        methods.insert(
            "hide_cursor",
            alloc_trait_fn_ctx(
                ctx.clone(),
                "Console.hide_cursor",
                Arity::Fixed(0),
                "Hides the terminal cursor.",
                move |vm, _, _| {
                    stdout()
                        .execute(cursor::Hide)
                        .map_err(|e| vm.error(format!("Failed to hide the cursor: {}", e)))
                        .and_then(|s| {
                            s.flush()
                                .map_err(|e| vm.error(format!("Failed to flush STDOUT: {}", e)))
                        })?;
                    Ok(Value::Nil)
                },
            ),
        );
        methods.insert(
            "show_cursor",
            alloc_trait_fn_ctx(
                ctx.clone(),
                "Console.show_cursor",
                Arity::Fixed(0),
                "Shows the terminal cursor.",
                move |vm, _, _| {
                    stdout()
                        .execute(cursor::Show)
                        .map_err(|e| vm.error(format!("Failed to show the cursor: {}", e)))
                        .and_then(|s| {
                            s.flush()
                                .map_err(|e| vm.error(format!("Failed to flush STDOUT: {}", e)))
                        })?;
                    Ok(Value::Nil)
                },
            ),
        );

        methods.insert(
            "keys",
            ctx.write_shared()
                .alloc_without_gc_trigger(LoxObject::new(HeapValue::ObjNative(
                    Box::new(LoxKeysIter::new(100)),
                ))),
        );

        Self { methods }
    }
}

impl NativeObject for LoxConsole {
    fn lox_name(&self) -> CowStr {
        CowStr::Borrowed("Console")
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::Borrowed("<Console instance>"))
    }

    fn lox_doc(&self) -> CowStr {
        CowStr::Borrowed(
            r#"
Lox's builtin class that provides methods for interacting with the console.

Console::write(msg, no_flush = false)

    Writes the string representation of the given object to STDOUT. 
    Doesn't flush the STDOUt buffer if the second argument is `true`.

Console::clear()

    Clears the console using the following ANSI escape sequence: `\u{1b}[2J\u{1b}[H`.

Console::clear_line()

    Clears the line the cursor is currently on 
    using the following ANSI escape sequence: `\u{1b}[2K`.

Console::clear_line_right()

    Clears the line to the right of the cursor 
    using the following ANSI escape sequence: `\u{1b}[K`.

Console::reset()
    
    Moves the cursor to the upper left corner using the following ANSI escape sequence: `\u{1b}[H`.

Console::show_cursor()

    Shows the terminal cursor.

Console::hide_cursor()

    Hides the terminal cursor.

Console::keys()

    Returns an iterator over the input keys.
"#,
        )
    }

    fn get_field(&mut self, vm: &mut Vm, _: ObjectPointer, name: &str) -> Result<Value, LoxError> {
        self.methods
            .get(name)
            .map(|ptr| Value::Ptr(*ptr))
            .ok_or_else(|| vm.error(format!("Undefined property `{}`", name)))
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(self.methods.values_mut())
    }
}

impl std::fmt::Debug for LoxConsole {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "LoxConsole {}",
            if f.alternate() {
                format!("{{\n    methods: {:#?}\n}}", self.methods)
            } else {
                format!("{{ methods: {:?} }}", self.methods)
            }
        )
    }
}
