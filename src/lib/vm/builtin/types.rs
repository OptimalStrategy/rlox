use crate::types::{LoxFloat, LoxInt};
use vm::object::heap_value::HeapValue;

use crate::{compiler::cco::Arity, vm::native::CowStr};

use errors::LoxError;
use vm::object::ObjectPointer;
use vm::{
    object::{native::NativeObject, value::Value},
    vm::Vm,
};

#[derive(Debug)]
pub struct LoxIntType;
#[derive(Debug)]
pub struct LoxFloatType;
#[derive(Debug)]
pub struct LoxBoolType;
#[derive(Debug)]
pub struct LoxStringType;

known_deep_size_fast!(0, LoxIntType, LoxFloatType, LoxBoolType, LoxStringType);

impl NativeObject for LoxIntType {
    fn lox_name(&self) -> CowStr {
        CowStr::from("int")
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::from("int"))
    }

    fn lox_doc(&self) -> CowStr {
        CowStr::from("TODO: int")
    }

    fn arity(&self) -> Option<Arity> {
        Some(Arity::Optional)
    }

    fn call(
        &mut self,
        vm: &mut Vm,
        this: ObjectPointer,
        args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        let value = match args.into_iter().next() {
            None => return Ok(Value::Int(0)),
            Some(value) => value,
        };
        let value = match value.as_int() {
            Some(i) => Value::Int(i),
            None => match value {
                Value::Nil => {
                    return Err(vm.error("`nil` is not a valid integer"));
                }
                Value::Ptr(ptr) => match ptr.as_ref().as_heap_value() {
                    HeapValue::Str(box s) => {
                        let i = s.as_ref().parse::<LoxInt>().map_err(|_| {
                            vm.error(format!(
                                "Invalid literal for a decimal integer: {}",
                                s.as_ref()
                            ))
                        })?;
                        Value::Int(i)
                    }
                    HeapValue::Obj(_) | HeapValue::ObjNative(_) => {
                        vm.try_cast_with_fallback(Value::Ptr(ptr), Value::Ptr(this), "to_int")?
                    }
                    HeapValue::Cls(_)
                    | HeapValue::CCO(_)
                    | HeapValue::Closure { .. }
                    | HeapValue::BoundMethod { .. } => {
                        return Err(vm.error(format!(
                            "Couldn't convert an object of type `{}` to int",
                            vm.type_of(&value)
                        )));
                    }
                    HeapValue::UpValue(_) => unreachable!(),
                },
                Value::Float(f) => Value::Int(f.trunc() as _),
                Value::Int(_) | Value::Bool(_) => unreachable!(),
            },
        };
        Ok(value)
    }

    fn is_type_of(&self, instance: &Value) -> bool {
        matches!(
            instance
                .as_heap_value()
                .and_then(|i| i.follow_ptr().as_upvalue())
                .unwrap_or(instance),
            Value::Int(_)
        )
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(std::iter::empty())
    }
}

impl NativeObject for LoxFloatType {
    fn lox_name(&self) -> CowStr {
        CowStr::from("float")
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::from("float"))
    }

    fn lox_doc(&self) -> CowStr {
        CowStr::from("TODO: float")
    }

    fn arity(&self) -> Option<Arity> {
        Some(Arity::Optional)
    }

    fn call(
        &mut self,
        vm: &mut Vm,
        this: ObjectPointer,
        args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        let value = match args.into_iter().next() {
            None => return Ok(Value::Float(0.0)),
            Some(value) => value,
        };
        let value = match value.as_float() {
            Some(f) => Value::Float(f),
            None => match value {
                Value::Nil => {
                    return Err(vm.error("`nil` is not a valid float"));
                }
                Value::Bool(b) => Value::Float(b as LoxInt as _),
                Value::Ptr(ptr) => match ptr.as_ref().as_heap_value() {
                    HeapValue::Str(box s) => {
                        let f = s.as_ref().parse::<LoxFloat>().map_err(|_| {
                            vm.error(format!(
                                "Invalid literal for a floating point number: {}",
                                s.as_ref()
                            ))
                        })?;
                        Value::Float(f)
                    }
                    HeapValue::Obj(_) | HeapValue::ObjNative(_) => {
                        vm.try_cast_with_fallback(Value::Ptr(ptr), Value::Ptr(this), "to_float")?
                    }
                    HeapValue::Cls(_)
                    | HeapValue::CCO(_)
                    | HeapValue::Closure { .. }
                    | HeapValue::BoundMethod { .. } => {
                        return Err(vm.error(format!(
                            "Couldn't convert an object of type `{}` to int",
                            vm.type_of(&value)
                        )));
                    }
                    HeapValue::UpValue(_) => unreachable!(),
                },
                Value::Float(_) | Value::Int(_) => unreachable!(),
            },
        };
        Ok(value)
    }

    fn is_type_of(&self, instance: &Value) -> bool {
        matches!(
            instance
                .as_heap_value()
                .and_then(|i| i.follow_ptr().as_upvalue())
                .unwrap_or(instance),
            Value::Float(_)
        )
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(std::iter::empty())
    }
}

impl NativeObject for LoxBoolType {
    fn lox_name(&self) -> CowStr {
        CowStr::from("bool")
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::from("bool"))
    }

    fn lox_doc(&self) -> CowStr {
        CowStr::from("TODO: bool")
    }

    fn arity(&self) -> Option<Arity> {
        Some(Arity::Optional)
    }

    fn call(
        &mut self,
        vm: &mut Vm,
        this: ObjectPointer,
        args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        let value = match args.into_iter().next() {
            None => return Ok(Value::Float(0.0)),
            Some(value) => value,
        };
        let value = match value {
            Value::Ptr(ptr) => match ptr.as_ref().as_heap_value() {
                HeapValue::Str(box s) => {
                    let b = s.as_ref().parse::<bool>().map_err(|_| {
                        vm.error(format!("Invalid literal for a boolean: {}", s.as_ref()))
                    })?;
                    Value::Bool(b)
                }
                HeapValue::Obj(_) | HeapValue::ObjNative(_) => {
                    vm.try_cast_with_fallback(Value::Ptr(ptr), Value::Ptr(this), "to_bool")?
                }
                HeapValue::Cls(_)
                | HeapValue::CCO(_)
                | HeapValue::Closure { .. }
                | HeapValue::BoundMethod { .. } => Value::Bool(true),
                HeapValue::UpValue(value) => Value::Bool(vm.is_truthy(value)),
            },
            rest => Value::Bool(vm.is_truthy(&rest)),
        };
        Ok(value)
    }

    fn is_type_of(&self, instance: &Value) -> bool {
        matches!(
            instance
                .as_heap_value()
                .and_then(|i| i.follow_ptr().as_upvalue())
                .unwrap_or(instance),
            Value::Bool(_)
        )
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(std::iter::empty())
    }
}

impl NativeObject for LoxStringType {
    fn lox_name(&self) -> CowStr {
        CowStr::from("str")
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::from("str"))
    }

    fn lox_doc(&self) -> CowStr {
        CowStr::from("TODO: str")
    }

    fn arity(&self) -> Option<Arity> {
        Some(Arity::Optional)
    }

    fn call(
        &mut self,
        vm: &mut Vm,
        _this: ObjectPointer,
        args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        let value = match args.into_iter().next() {
            None => return Ok(Value::Float(0.0)),
            Some(value) => value,
        };
        let value = match value
            .as_ptr()
            .and_then(|v| v.as_ref().as_heap_value().as_string())
        {
            Some(_) => value, // Avoid unnecessary string allocations
            _ => {
                let s = vm.stringify(&value)?.into_owned();
                Value::Ptr(vm.alloc_or_get_interned(s))
            }
        };
        Ok(value)
    }

    fn is_type_of(&self, instance: &Value) -> bool {
        instance
            .as_heap_value()
            .and_then(|i| i.as_string())
            .is_some()
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(std::iter::empty())
    }
}
