use crate::rt_hashmap::FastHashMap;
use vm::object::heap_value::HeapValue;
use vm::object::object::LoxObject;

use crate::{compiler::cco::Arity, vm::native::CowStr};

use errors::LoxError;
use vm::builtin::fn_wrapper::*;
use vm::object::ObjectPointer;
use vm::{
    object::{native::NativeObject, value::Value},
    vm::Vm,
};

#[derive(Debug)]
pub struct LoxResult {
    value: Value,
    is_ok: bool,
    methods: FastHashMap<&'static str, ObjectPointer>,
}
impl_vtable_deepsize!(LoxResult, methods, ObjectPointer);

impl LoxResult {
    pub fn new_ok(value: Value) -> Self {
        Self {
            value,
            is_ok: true,
            methods: FastHashMap::default(),
        }
    }

    pub fn new_err(value: Value) -> Self {
        Self {
            value,
            is_ok: false,
            methods: FastHashMap::default(),
        }
    }

    pub fn alloc_new_ok(vm: &mut Vm, value: Value) -> ObjectPointer {
        let ok = Self::new_ok(value);
        vm.alloc(LoxObject::new(HeapValue::ObjNative(Box::new(ok))))
    }

    pub fn alloc_new_err(vm: &mut Vm, value: Value) -> ObjectPointer {
        let err = Self::new_err(value);
        vm.alloc(LoxObject::new(HeapValue::ObjNative(Box::new(err))))
    }

    pub fn alloc_new_based_on_value(vm: &mut Vm, value: Value) -> ObjectPointer {
        let result = match value {
            Value::Nil => Self::new_err(value),
            rest => Self::new_ok(rest),
        };
        vm.alloc(LoxObject::new(HeapValue::ObjNative(Box::new(result))))
    }

    #[inline]
    pub fn ok(&self) -> bool {
        self.is_ok
    }

    #[inline]
    pub fn err(&self) -> bool {
        !self.is_ok
    }

    fn result(this: &mut ObjectPointer) -> &mut Self {
        unsafe { <dyn NativeObject>::downcast_mut::<Self>(this) }
    }

    fn unwrap(vm: &mut Vm, mut this: ObjectPointer, _: Vec<Value>) -> Result<Value, LoxError> {
        let res = Self::result(&mut this);
        if res.is_ok {
            Ok(res.value)
        } else {
            let value = vm.stringify(&res.value)?;
            Err(vm.panic(format!("Called unwrap on an err value: {}", value)))
        }
    }

    fn unwrap_or(_: &mut Vm, mut this: ObjectPointer, args: Vec<Value>) -> Result<Value, LoxError> {
        let res = Self::result(&mut this);
        if res.ok() {
            Ok(res.value)
        } else {
            Ok(args[0])
        }
    }

    fn expect(vm: &mut Vm, mut this: ObjectPointer, args: Vec<Value>) -> Result<Value, LoxError> {
        let res = Self::result(&mut this);
        if res.ok() {
            Ok(res.value)
        } else {
            let message = vm.stringify(&args[0])?;
            Err(vm.panic(message))
        }
    }

    fn map(vm: &mut Vm, mut this: ObjectPointer, args: Vec<Value>) -> Result<Value, LoxError> {
        let res = Self::result(&mut this);
        if res.ok() {
            let result = vm.call_function(args[0], vec![res.value])?;
            Ok(Value::Ptr(Self::alloc_new_ok(vm, result)))
        } else {
            Ok(Value::Ptr(this))
        }
    }

    fn map_err(vm: &mut Vm, mut this: ObjectPointer, args: Vec<Value>) -> Result<Value, LoxError> {
        let res = Self::result(&mut this);
        if res.err() {
            let result = vm.call_function(args[0], vec![res.value])?;
            Ok(Value::Ptr(Self::alloc_new_err(vm, result)))
        } else {
            Ok(Value::Ptr(this))
        }
    }
}

impl NativeObject for LoxResult {
    fn lox_name(&self) -> CowStr {
        CowStr::from(if self.ok() { "Ok" } else { "Err" })
    }

    fn to_string(&self, vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::from(format!(
            "{}({})",
            self.lox_name(),
            self.value.stringify(vm)?,
        )))
    }

    fn lox_doc(&self) -> CowStr {
        CowStr::Borrowed(LOX_RESULT_DOC)
    }

    fn is_instance_of(&self, class: &dyn NativeObject) -> bool {
        let name = class.lox_name();
        name == self.lox_name() || name == "Result"
    }

    fn get_field(
        &mut self,
        vm: &mut Vm,
        this: ObjectPointer,
        name: &str,
    ) -> Result<Value, LoxError> {
        match name {
            "unwrap" => Ok(Value::Ptr(*self.methods.entry("unwrap").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "Result.unwrap",
                        Arity::Fixed(0),
                        " Panics if the object is an error, returns the value otherwise.",
                        move |vm, _, args| -> Result<_, _> { LoxResult::unwrap(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "unwrap_or" => Ok(Value::Ptr(*self.methods.entry("unwrap_or").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "Result.unwrap_or",
                        Arity::Fixed(1),
                        "Returns the unwrapped value if the object is an Ok, and the given value otherwise.",
                        move |vm, _, args| -> Result<_, _> { LoxResult::unwrap_or(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "expect" => Ok(Value::Ptr(*self.methods.entry("expect").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "Result.expect",
                        Arity::Fixed(1),
                        "Attempts to unwrap the value and panics with the given message if not possible.",
                        move |vm, _, args| -> Result<_, _> { LoxResult::expect(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "map" => Ok(Value::Ptr(*self.methods.entry("map").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "Result.map",
                        Arity::Fixed(1),
                        "Calls `f` on the inner value if the current variant is OK, otherwise returns the Err.",
                        move |vm, _, args| -> Result<_, _> { LoxResult::map(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "map_err" => Ok(Value::Ptr(*self.methods.entry("map_err").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "Result.map_err",
                        Arity::Fixed(1),
                        "Calls `f` on the inner value if the current variant is Err, otherwise returns the Ok.",
                        move |vm, _, args| -> Result<_, _> { LoxResult::map_err(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "is_ok" => {
                let is_ok = self.is_ok;
                Ok(Value::Ptr(*self.methods.entry("is_ok").or_insert_with(
                    || {
                        alloc_trait_fn_vm(
                            vm,
                            "Result.is_ok",
                            Arity::Fixed(0),
                            "Returns true if the result is an Ok.",
                            move |_, _, _| -> Result<_, _> { Ok(Value::Bool(is_ok)) },
                            &[this],
                        )
                    },
                )))
            }
            "is_err" => {
                let is_err = !self.is_ok;
                Ok(Value::Ptr(*self.methods.entry("is_err").or_insert_with(
                    || {
                        alloc_trait_fn_vm(
                            vm,
                            "Result.is_err",
                            Arity::Fixed(0),
                            "Returns true if the result is an Err.",
                            move |_, _, _| -> Result<_, _> { Ok(Value::Bool(is_err)) },
                            &[this],
                        )
                    },
                )))
            }
            "value" => Ok(if self.ok() { self.value } else { Value::Nil }),
            "err" => Ok(if !self.ok() {  self.value } else { Value::Nil }),
            _ => Err(vm.error(format!("Undefined property `{}`", name))),
        }
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(self
            .value
            .as_ptr_mut()
            .into_iter()
            .chain(self.methods.values_mut()))
    }
}

#[derive(Debug)]
pub struct LoxResultType;
#[derive(Debug)]
pub struct LoxOkType;
#[derive(Debug)]
pub struct LoxErrType;
known_deep_size_fast!(0, LoxResultType, LoxOkType, LoxErrType);

impl NativeObject for LoxResultType {
    fn lox_name(&self) -> CowStr {
        CowStr::from("Result")
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::from("<cls: Result>"))
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(std::iter::empty())
    }

    fn lox_doc(&self) -> CowStr {
        CowStr::from(LOX_RESULT_DOC)
    }

    fn call(
        &mut self,
        vm: &mut Vm,
        _this: ObjectPointer,
        _args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        Err(vm.error(
            "Result is an abstract type and cannot be constructed directly. \
            Use Ok(...) or Err(...) instead.",
        ))
    }
}

impl NativeObject for LoxOkType {
    fn lox_name(&self) -> CowStr {
        CowStr::from("Ok")
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::from("<cls: Ok>"))
    }

    fn lox_doc(&self) -> CowStr {
        CowStr::from(LOX_RESULT_DOC)
    }

    fn arity(&self) -> Option<Arity> {
        Some(Arity::Fixed(1))
    }

    fn call(
        &mut self,
        vm: &mut Vm,
        _this: ObjectPointer,
        mut args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        Ok(Value::Ptr(LoxResult::alloc_new_ok(vm, args.pop().unwrap())))
    }

    fn is_instance_of(&self, class: &dyn NativeObject) -> bool {
        let name = class.lox_name();
        name == "Ok" || name == "Result"
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(std::iter::empty())
    }
}

impl NativeObject for LoxErrType {
    fn lox_name(&self) -> CowStr {
        CowStr::from("Err")
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::from("<cls: Err>"))
    }

    fn lox_doc(&self) -> CowStr {
        CowStr::from(LOX_RESULT_DOC)
    }

    fn arity(&self) -> Option<Arity> {
        Some(Arity::Fixed(1))
    }

    fn call(
        &mut self,
        vm: &mut Vm,
        _this: ObjectPointer,
        mut args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        Ok(Value::Ptr(LoxResult::alloc_new_err(
            vm,
            args.pop().unwrap(),
        )))
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(std::iter::empty())
    }
}

pub(crate) static LOX_RESULT_DOC: &str = r#"Lox's result type used to report recoverable errors.
Consider the code below:

.. code-block:: lox

   // Divides a by b. Fails if b is zero.
   fun divide(a, b) {
        if (b == 0) {
            return Err("Cannot divide by zero");
        }
        return Ok(a / b);
    }

    var ok = divide(15, 3);
    print ok;  // Prints "Ok(5)"
    print ok.value; // Prints "5"

    var err = divide(31, 0);
    print err;  // Prints "Err('Cannot divide by zero')"
    print err.err;  // Prints "Cannot divide by zero"

You can use `match` to check whether a value is a result,
and which result in particular:

.. code-block:: lox

    fun match_result_type(value) {
        print match value {
            class Result => "value: ${value}",
            _ => "value is not a result: ${value}",
        };
    }

    fun match_result(value) {
        print match value {
            // You can also use `unwrap()`.
            // `unwrap` is a method that panics if the value is not an Ok.
            // Useful if you're 100% sure that the value is Ok,
            // or if you simply don't care.
            class Ok => "Ok: ${value.value}",
            class Err => "Err: ${value.err}",
            _ => "value is not a Result: ${value}",
        };
    }

    var ok = Ok(123);
    print match_result_type(ok); // Prints "value: Ok(123)"
    print match_result(ok);  // Prints "Ok: 123"

    var err = Err("This is an error");
    print match_result_type(err);  // Prints "value: Err('This is an error')"
    print match_result(err);  // Prints "Err: This is an error"

    var not_a_result = true;
    print match_result_type(not_a_result);  // Prints "value is not a result: true"
    print match_result(not_a_result);  // Prints "value is not a result: true""#;
