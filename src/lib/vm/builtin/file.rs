use std::{fs::File, io::Read, os::unix::prelude::MetadataExt, path::PathBuf};

use crate::{rt_hashmap::FastHashMap, SharedCtx};

use crate::{compiler::cco::Arity, vm::native::CowStr};

use errors::LoxError;
use vm::builtin::fn_wrapper::*;
use vm::object::ObjectPointer;
use vm::{
    object::{native::NativeObject, value::Value},
    vm::Vm,
};

use super::result::LoxResult;

#[derive(Debug)]
pub struct LoxFile {
    methods: FastHashMap<&'static str, ObjectPointer>,
}
impl_vtable_deepsize!(LoxFile, methods, ObjectPointer);

impl LoxFile {
    pub fn new(ctx: SharedCtx) -> Self {
        let mut methods = FastHashMap::default();
        methods.insert(
            "open",
            alloc_trait_fn_ctx(
                ctx,
                "File.open",
                Arity::InRange(1, 2),
                "Attempts to open the file at the given path",
                move |vm, _, args| -> Result<_, _> {
                    let path = match &args[0].as_string() {
                        Some(s) => PathBuf::from(s.as_ref()),
                        _ => {
                            return Err(vm.error(format!(
                                "`open` path must be a string, got `{}`",
                                vm.type_of(&args[0])
                            )));
                        }
                    };
                    let mode = match &args.get(1) {
                        Some(s) => match s.as_string() {
                            Some(s) => s.as_ref(),
                            None => {
                                return Err(vm.error(format!(
                                    "`open` mode must be a string, got `{}`",
                                    vm.type_of(&args[0])
                                )))
                            }
                        },
                        None => "r",
                    };
                    let mut options = File::options();
                    let options = options
                        .read(mode.contains('r'))
                        .write(mode.contains('w'))
                        .append(mode.contains('a'))
                        .create(mode.contains('x'));

                    Ok(Value::Ptr(
                        match std::fs::canonicalize(&path).and_then(|path| options.open(path)) {
                            Ok(file) => {
                                let path = std::fs::canonicalize(path).unwrap();
                                let ptr = vm.alloc_native(LoxFileObj::new(file, path));
                                LoxResult::alloc_new_ok(vm, Value::Ptr(ptr))
                            }
                            Err(e) => {
                                let ptr = vm.alloc_or_get_interned(format!(
                                    "Couldn't open the file: {}",
                                    e
                                ));
                                LoxResult::alloc_new_err(vm, Value::Ptr(ptr))
                            }
                        },
                    ))
                },
            ),
        );
        Self { methods }
    }
}

impl NativeObject for LoxFile {
    fn lox_name(&self) -> CowStr {
        CowStr::from("File")
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::from("<type: File>"))
    }

    fn lox_doc(&self) -> CowStr {
        CowStr::Borrowed(
            r#"
    Lox's builtin class for working with files.

    File::open(path, mode='r')

        Attempts to open the file at the given path with the requested mode.
"#,
        )
    }

    fn get_field(&mut self, vm: &mut Vm, _: ObjectPointer, name: &str) -> Result<Value, LoxError> {
        self.methods
            .get(name)
            .map(|ptr| Value::Ptr(*ptr))
            .ok_or_else(|| vm.error(format!("Undefined property `{}`", name)))
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(self.methods.values_mut())
    }
}

#[derive(Debug)]
pub struct LoxFileObj {
    file: File,
    path: PathBuf,
    methods: FastHashMap<&'static str, ObjectPointer>,
}
impl_vtable_deepsize!(LoxFileObj, methods, ObjectPointer);

impl LoxFileObj {
    pub fn new(file: File, path: PathBuf) -> Self {
        Self {
            file,
            path,
            methods: FastHashMap::default(),
        }
    }

    #[allow(unused)]
    fn file(this: &mut ObjectPointer) -> &mut Self {
        unsafe { <dyn NativeObject>::downcast_mut::<Self>(this) }
    }

    fn read(vm: &mut Vm, mut this: ObjectPointer, _: Vec<Value>) -> Result<Value, LoxError> {
        let this = Self::file(&mut this);
        let mut buf = String::new();
        let contents = this.file.read_to_string(&mut buf).map(|_| buf);
        let is_err = contents.is_err();
        let s = contents.unwrap_or_else(|e| format!("Couldn't read form the file: {}", e));
        let ptr = Value::Ptr(vm.alloc_or_get_interned(s));
        Ok(Value::Ptr(if is_err {
            LoxResult::alloc_new_err(vm, ptr)
        } else {
            LoxResult::alloc_new_ok(vm, ptr)
        }))
    }
}

impl NativeObject for LoxFileObj {
    fn lox_name(&self) -> CowStr {
        CowStr::from("File")
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::from(format!(
            "<File: {}{}>",
            self.path.display(),
            self.file
                .metadata()
                .map(|m| format!(", ino={}", m.ino()))
                .unwrap_or_default()
        )))
    }

    fn lox_doc(&self) -> CowStr {
        CowStr::Borrowed("")
    }

    fn is_instance_of(&self, class: &dyn NativeObject) -> bool {
        let name = class.lox_name();
        name == self.lox_name() || name == "File"
    }

    fn get_field(
        &mut self,
        vm: &mut Vm,
        this: ObjectPointer,
        name: &str,
    ) -> Result<Value, LoxError> {
        match name {
            "read" => Ok(Value::Ptr(*self.methods.entry("read").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "File.read",
                        Arity::Optional,
                        "Reads the file and returns its contents as a string.",
                        move |vm, _, args| -> Result<_, _> { LoxFileObj::read(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            _ => Err(vm.error(format!("Undefined property `{}`", name))),
        }
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(self.methods.values_mut())
    }
}
