pub mod array;
pub mod console;
pub mod dict;
pub mod file;
pub mod fn_wrapper;
pub mod intrinsics;
pub mod math;
pub mod result;
pub mod types;

use std::collections::HashMap;
use std::io::{stdin, stdout, Write};
use std::time::SystemTime;
use std::time::{Duration, UNIX_EPOCH};

use self::{
    array::LoxArray, console::LoxConsole, dict::LoxDict, file::LoxFile, intrinsics::LoxIntrinsics,
    math::LoxMath, types::*,
};

use super::{native::CowStr, native::NativeObject, Object};
use crate::{
    types::LoxFloat,
    vm::builtin::result::{LoxErrType, LoxOkType, LoxResultType},
};

use crate::rt_hashmap::FastHashMap;
use compiler::cco::Arity;
use context::SharedCtx;
use errors::LoxError;
use types::LoxInt;
use vm::{
    builtin::fn_wrapper::{RustFn, RustFnWrapper},
    immutable_string::hash,
    object::{heap_value::HeapValue, value::Value, LoxObject, ObjectPointer},
    vm::Vm,
};

#[derive(Debug)]
pub struct Builtins {
    object_class: ObjectPointer,
    name_to_hash: HashMap<String, u8>,
    hash_to_builtin: FastHashMap<u8, ObjectPointer>,
}

impl Builtins {
    pub fn new(object_class: ObjectPointer) -> Self {
        Self {
            object_class,
            name_to_hash: HashMap::new(),
            hash_to_builtin: FastHashMap::default(),
        }
    }

    #[inline]
    pub fn object_class(&self) -> ObjectPointer {
        self.object_class
    }

    #[inline(always)]
    pub fn name_to_hash(&self) -> &HashMap<String, u8> {
        &self.name_to_hash
    }

    #[inline(always)]
    pub fn get_map(&self) -> &FastHashMap<u8, ObjectPointer> {
        &self.hash_to_builtin
    }

    pub fn contained_pointers(&self) -> impl Iterator<Item = ObjectPointer> + '_ {
        self.get_map()
            .values()
            .cloned()
            .chain(std::iter::once(self.object_class))
    }

    pub fn default(ctx: SharedCtx) -> Self {
        let object_class = ctx.read_shared().builtins().object_class;
        let mut this = Self::new(object_class);

        this.add_trait_fn(
            ctx.clone(),
            "clock",
            Arity::Fixed(0),
            "Returns the number of seconds since the UNIX epoch.",
            |_, _, _| {
                Ok(Value::Float(
                    SystemTime::now()
                        .duration_since(UNIX_EPOCH)
                        .unwrap()
                        .as_secs_f64() as LoxFloat,
                ))
            },
        ).add_trait_fn(
            ctx.clone(),
            "sleep",
            Arity::Fixed(1),
            "Pauses the thread for the given number of milliseconds.",
            |vm, _, args| {
                let millis = match args[0].as_int() {
                    Some(millis) => Duration::from_millis(millis as u64),
                    None => return Err(vm.panic(format!("Expected an integer, but got `{}`", vm.type_of(&args[0]))))
                };
                std::thread::sleep(millis);
                Ok(Value::Nil)
            },
        )
        .add_trait_fn(
            ctx.clone(),
            "type",
            Arity::Fixed(1),
            "Returns the name of the type name of the given value.",
            |vm, _, args| {
                let ty = vm.type_of(&args[0]);
                let ptr = vm.alloc_or_get_interned(ty);
                Ok(Value::Ptr(ptr))
            },
        )
        .add_trait_fn(ctx.clone(), "help", Arity::Optional, "", |vm, _, args| {
            let result = if let Some(obj) = args.first() {
                vm.help(obj)
            } else {
                let mut result = Vec::with_capacity(vm.active_builtins().len());
                let mut values = vm.active_builtins().values().collect::<Vec<_>>();
                values.sort_by_key(|k| k.as_ref().as_heap_value().as_obj_native().unwrap().lox_name());
                for ptr in values  {
                    match ptr.as_ref().as_heap_value() {
                        HeapValue::ObjNative(obj) => result.push(
                            match obj.arity() {
                                Some(arity) => format!("{}(): accepts {} argument(s)", obj.lox_name(), arity),
                                None => format!("object {}", obj.lox_name()),
                            }
                        ),
                        _ => unreachable!(),
                    }
                }
                result.join("\n")
            };

            let ptr = vm.alloc_or_get_interned(result);
            Ok(Value::Ptr(ptr))
        })
        .add_trait_fn(
            ctx.clone(),
            "panic",
            Arity::Fixed(1),
            "Terminates the program with the given message.",
            |vm, _, args| {
                let msg = vm.stringify(args.first().unwrap())?;
                Err(vm.panic(msg))
            },
        )
        .add_trait_fn(
            ctx.clone(),
            "assert",
            Arity::InRange(1, 2),
            "Terminates the program with the given message if the first argument is false.",
            |vm, _, args| {
                let msg = match args.get(1) {
                    Some(v) => vm.stringify(v)?,
                    None => "Assertion failed".into(),
                };
                let value = args.first().unwrap();
                if vm.is_truthy(value) {
                    Ok(Value::Nil)
                } else {
                    Err(vm.assertion(msg))
                }
            },
        )
        .add_trait_fn(
            ctx.clone(),
            "object",
            Arity::Fixed(0),
            "Creates a new empty object.",
            move |vm, _, _| {
                let obj = Object::with_class(object_class).into_object();
                let ptr = vm.alloc(obj);
                Ok(Value::Ptr(ptr))
            },
        )
        .add_trait_fn(
            ctx.clone(),
            "to_int",
            Arity::Fixed(1),
            "Attempts to convert the given object to an integer. Panics if the object is not a number.",
            move |vm, _, args| {
                let num = match &args[0] {
                    Value::Int(i) => *i,
                    Value::Float(f) => *f as LoxInt,
                    Value::Bool(b) => *b as LoxInt,
                    _ => return Err(vm.error(format!("Expected a number, but got `{}`", vm.type_of(&args[0])))),
                };
                Ok(Value::Int(num))
            },
        ).
        add_trait_fn(
            ctx.clone(),
            "input",
            Arity::Optional,
            "Reads a line from STDIN with or without a message.",
            move |vm, _, args| {
                if let Some(msg) = args.first() {
                    let msg = vm.stringify(msg)?;
                    print!("{}", msg);
                    stdout()
                        .flush()
                        .map_err(|e| vm.error(format!("Failed to flush STDOUT: {}", e)))?;
                }
                let mut s = String::new();
                stdin()
                    .read_line(&mut s)
                    .map_err(|e| vm.error(format!("Failed to read from STDIN: {}", e)))?;
                if s.ends_with('\n') {
                    s.pop();
                    if s.ends_with('\r') {
                        s.pop();
                    }
                }
                let ptr = vm.alloc_or_get_interned(s);
                Ok(Value::Ptr(ptr))
            },
        );

        // The math singleton has a few methods that need to access its instance.
        let math_ptr = this.add_object(ctx.clone(), "Math", LoxMath::new(ctx.clone()));
        LoxMath::instance(&mut (math_ptr.clone())).add_instance_methods(ctx.clone(), math_ptr);

        this.add_object(ctx.clone(), "Result", LoxResultType);
        this.add_object(ctx.clone(), "Ok", LoxOkType);
        this.add_object(ctx.clone(), "Err", LoxErrType);
        this.add_object(ctx.clone(), "int", LoxIntType);
        this.add_object(ctx.clone(), "float", LoxFloatType);
        this.add_object(ctx.clone(), "bool", LoxBoolType);
        this.add_object(ctx.clone(), "str", LoxStringType);
        this.add_object(ctx.clone(), "Intrinsics", LoxIntrinsics::new(ctx.clone()));
        this.add_object(ctx.clone(), "Console", LoxConsole::new(ctx.clone()));
        this.add_object(ctx.clone(), "File", LoxFile::new(ctx.clone()));
        this.add_object(ctx.clone(), "[]", LoxArray::empty());
        this.add_object(ctx, "{}", LoxDict::empty());

        this
    }

    pub fn add_object<S: Into<String>>(
        &mut self,
        ctx: SharedCtx,
        name: S,
        obj: impl NativeObject + 'static,
    ) -> ObjectPointer {
        let name = name.into();
        let hash = Self::name_hash(&name);
        let ptr = ctx
            .write_shared()
            .alloc_without_gc_trigger(LoxObject::new(HeapValue::ObjNative(Box::new(obj))));
        self.name_to_hash.insert(name, hash);
        self.hash_to_builtin.insert(hash, ptr);
        ptr
    }

    pub fn add_rust_fn<S: Into<CowStr>>(
        &mut self,
        ctx: SharedCtx,
        name: S,
        arity: Arity,
        doc: S,
        r#fn: RustFn,
    ) -> &mut Self {
        let name = name.into();
        let hash = Self::name_hash(&name);
        let wrapped = RustFnWrapper::new(r#fn, arity, name.clone(), doc.into());
        let ptr = ctx
            .write_shared()
            .alloc_without_gc_trigger(LoxObject::new(HeapValue::ObjNative(Box::new(wrapped))));
        self.name_to_hash.insert(name.into_owned(), hash);
        self.hash_to_builtin.insert(hash, ptr);
        self
    }

    pub fn add_trait_fn<S: Into<CowStr>>(
        &mut self,
        ctx: SharedCtx,
        name: S,
        arity: Arity,
        doc: S,
        r#fn: impl Fn(&mut Vm, ObjectPointer, Vec<Value>) -> Result<Value, LoxError> + 'static,
    ) -> &mut Self {
        let name = name.into();
        let hash = Self::name_hash(&name);
        let wrapped = RustFnWrapper::new(r#fn, arity, name.clone(), doc.into());
        let ptr = ctx
            .write_shared()
            .alloc_without_gc_trigger(LoxObject::new(HeapValue::ObjNative(Box::new(wrapped))));
        self.name_to_hash.insert(name.into_owned(), hash);
        self.hash_to_builtin.insert(hash, ptr);
        self
    }

    pub fn remove(&mut self, name: &str) -> bool {
        self.name_to_hash
            .remove(name)
            .and_then(|idx| self.hash_to_builtin.remove(&idx as _))
            .is_some()
    }

    #[inline(always)]
    pub fn name_hash(name: &str) -> u8 {
        (hash(name) % 256) as u8
    }
}
