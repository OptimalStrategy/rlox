use crate::allocator::DeepSizeOf;
use crate::rt_hashmap::FastHashMap;
use vm::object::heap_value::HeapValue;
use vm::object::object::LoxObject;

use crate::{compiler::cco::Arity, types::LoxInt, vm::native::CowStr};

use errors::LoxError;
use vm::builtin::fn_wrapper::*;
use vm::object::ObjectPointer;
use vm::{
    object::{native::NativeObject, value::Value},
    vm::Vm,
};

use super::result::LoxResult;

#[derive(Debug)]
pub struct LoxArrayIterator {
    arr: ObjectPointer,
    iter_next: Option<ObjectPointer>,
    current: usize,
}
known_deep_size_fast!(0, LoxArrayIterator);

impl LoxArrayIterator {
    fn new(arr: ObjectPointer) -> LoxArrayIterator {
        Self {
            arr,
            iter_next: None,
            current: 0,
        }
    }

    #[inline]
    fn arr(&self) -> &LoxArray {
        unsafe { <dyn NativeObject>::downcast::<LoxArray>(&self.arr) }
    }

    fn next(&mut self) -> Value {
        let value = {
            self.current += 1;
            let arr = self.arr();
            if self.current - 1 < arr.values.len() {
                arr.values[self.current - 1]
            } else {
                self.current -= 1;
                Value::Nil
            }
        };
        value
    }
}

impl NativeObject for LoxArrayIterator {
    fn lox_name(&self) -> CowStr {
        CowStr::Borrowed("ArrayIterator")
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(std::iter::once(&mut self.arr).chain(self.iter_next.iter_mut()))
    }

    fn get_field(
        &mut self,
        vm: &mut Vm,
        this: ObjectPointer,
        name: &str,
    ) -> Result<Value, LoxError> {
        match name {
            "iternext" => Ok(Value::Ptr(*self.iter_next.get_or_insert_with(|| {
                alloc_trait_fn_vm(
                    vm,
                    "ArrayIterator.iternext",
                    Arity::Fixed(0),
                    "Returns the next value in the iterator or nil.",
                    move |vm, this, _| -> Result<_, _> {
                        let mut ptr = this;
                        let this = unsafe {
                            <dyn NativeObject>::downcast_mut::<LoxArrayIterator>(&mut ptr)
                        };
                        Ok(Value::Ptr(LoxResult::alloc_new_based_on_value(
                            vm,
                            this.next(),
                        )))
                    },
                    &[this],
                )
            }))),
            _ => Err(vm.error(format!("Undefined property `{}`", name))),
        }
    }
}

#[derive(Debug)]
pub struct LoxArray {
    values: Vec<Value>,
    methods: FastHashMap<&'static str, ObjectPointer>,
}

impl DeepSizeOf for LoxArray {
    fn deep_size_of_children(&self) -> usize {
        let values_mem = crate::allocator::deep_size_of::deep_size_of_vec(&self.values);
        let vtable_mem = crate::allocator::deep_size_of::deep_size_of_map::<
            &'static str,
            ObjectPointer,
        >(self.methods.capacity());

        vtable_mem + values_mem
    }
}

impl LoxArray {
    pub fn new(values: Vec<Value>) -> LoxArray {
        let mut methods = FastHashMap::default();
        methods.reserve(3);
        Self { values, methods }
    }

    #[inline]
    pub fn new_obj(values: Vec<Value>) -> LoxObject {
        LoxObject::new(HeapValue::ObjNative(Box::new(Self::new(values))))
    }

    pub fn empty() -> LoxArray {
        let mut methods = FastHashMap::default();
        methods.reserve(3);
        Self {
            values: Vec::new(),
            methods,
        }
    }

    #[inline]
    pub fn baking_vec(&self) -> &Vec<Value> {
        &self.values
    }

    #[inline]
    fn array(this: &mut ObjectPointer) -> &mut Self {
        unsafe { <dyn NativeObject>::downcast_mut::<Self>(this) }
    }

    fn push(_: &mut Vm, mut this: ObjectPointer, mut args: Vec<Value>) -> Result<Value, LoxError> {
        let arr = Self::array(&mut this);
        arr.values.push(args.pop().unwrap());
        Ok(Value::Nil)
    }

    fn pop(vm: &mut Vm, mut this: ObjectPointer, mut args: Vec<Value>) -> Result<Value, LoxError> {
        let arr = Self::array(&mut this);
        match arr.values.pop() {
            Some(result) => Ok(result),
            None => match args.pop() {
                Some(default) => Ok(default),
                None => Err(vm.error("Cannot pop from an empty array")),
            },
        }
    }

    fn get(vm: &mut Vm, mut this: ObjectPointer, args: Vec<Value>) -> Result<Value, LoxError> {
        let arr = Self::array(&mut this);
        let idx = if let Some(i) = args[0].as_int() {
            i
        } else {
            return Err(vm.error(format!(
                "`get` index must be an integer, got `{}`",
                vm.type_of(&args[0])
            )));
        };
        match arr.values.get(idx as usize) {
            Some(result) => Ok(*result),
            None => match args.get(1) {
                Some(default) => Ok(*default),
                None => Err(vm.error(format!(
                    "Array index is out of bounds: `{}` >= `{}`",
                    idx,
                    arr.values.len()
                ))),
            },
        }
    }

    fn set_item(vm: &mut Vm, mut this: ObjectPointer, args: Vec<Value>) -> Result<Value, LoxError> {
        let arr = Self::array(&mut this);
        let idx = if let Some(i) = args[0].as_int() {
            i
        } else {
            return Err(vm.error(format!(
                "`get` index must be an integer, got `{}`",
                vm.type_of(&args[0])
            )));
        };
        match arr.values.get_mut(idx as usize) {
            Some(result) => {
                *result = args[1];
                Ok(*result)
            }
            None => Err(vm.error(format!(
                "Array index is out of bounds: `{}` >= `{}`",
                idx,
                arr.values.len()
            ))),
        }
    }

    fn map(vm: &mut Vm, mut this: ObjectPointer, mut args: Vec<Value>) -> Result<Value, LoxError> {
        let arr = Self::array(&mut this);
        let f = args.pop().unwrap();
        let mut arr_ptr = Value::Ptr(vm.alloc(Self::new_obj(Vec::with_capacity(arr.values.len()))));

        // Make sure that the freshly-allocated array is reachable
        vm.push(arr_ptr);
        vm.push(f);

        let new_array = &mut Self::array(arr_ptr.as_ptr_mut().unwrap()).values as *mut Vec<_>;
        for obj in &arr.values {
            let f = *vm.get_state().stack.last().unwrap();
            unsafe { &mut *new_array }.push(vm.call_function(f, vec![*obj])?);
        }

        vm.pop(); // f
        Ok(vm.pop())
    }

    fn fill(vm: &mut Vm, mut this: ObjectPointer, args: Vec<Value>) -> Result<Value, LoxError> {
        let arr = Self::array(&mut this);
        let n = match &args[0] {
            Value::Int(n) => *n,
            _ => {
                return Err(vm.error(format!(
                    "The first argument must be an integer, but got `{}`.",
                    vm.type_of(&args[0])
                )))
            }
        };
        arr.values.reserve(n as usize);
        arr.values
            .extend(std::iter::repeat(&args[1]).take(n as usize).cloned());
        Ok(Value::Ptr(this))
    }

    fn len(_: &mut Vm, mut this: ObjectPointer, _: Vec<Value>) -> Result<Value, LoxError> {
        let arr = unsafe { <dyn NativeObject>::downcast_mut::<LoxArray>(&mut this) };
        Ok(Value::Int(arr.values.len() as LoxInt))
    }

    fn copy(vm: &mut Vm, mut this: ObjectPointer) -> Result<Value, LoxError> {
        let arr = Self::array(&mut this);
        let obj = Self::new_obj(arr.values.clone());
        let ptr = vm.alloc(obj);
        Ok(Value::Ptr(ptr))
    }
}

impl NativeObject for LoxArray {
    fn lox_name(&self) -> CowStr {
        CowStr::Borrowed("array")
    }

    fn is_instance_of(&self, obj: &dyn NativeObject) -> bool {
        obj.lox_name() == "array"
    }

    fn to_string(&self, vm: &mut Vm) -> Result<CowStr, LoxError> {
        let mut values = Vec::with_capacity(self.values.len());
        for v in &self.values {
            values.push(v.stringify(vm)?);
        }
        Ok(CowStr::Owned(format!("[{}]", values.join(", "))))
    }

    fn get_field(
        &mut self,
        vm: &mut Vm,
        this: ObjectPointer,
        name: &str,
    ) -> Result<Value, LoxError> {
        match name {
            "push" => Ok(Value::Ptr(*self.methods.entry("push").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "array.push",
                        Arity::Fixed(1),
                        "Pushes a value into the array.",
                        move |vm, this, args| -> Result<_, _> { LoxArray::push(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "length" => Ok(Value::Int(self.values.len() as LoxInt)),
            "get" => Ok(Value::Ptr(*self.methods.entry("get").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "array.get",
                        Arity::InRange(1, 2),
                        "Attempts to get the element at the given index. Panics if the index is not an integer or the \
                              index is out of bounds.",
                        move |vm, this, args| -> Result<_, _> { LoxArray::get(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "getitem" => Ok(Value::Ptr(*self.methods.entry("getitem").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "array.getitem",
                        Arity::Fixed(1),
                        "Attempts to get the element at the given index. Panics if the index is not an integer or the \
                              index is out of bounds.",
                        move |vm, this, args| -> Result<_, _> { LoxArray::get(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "setitem" => Ok(Value::Ptr(*self.methods.entry("setitem").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "array.getitem",
                        Arity::Fixed(2),
                        "Attempts to set the element at the given index. Panics if the index is not an integer or the \
                              index is out of bounds.",
                        move |vm, _, args| -> Result<_, _> { LoxArray::set_item(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "pop" => Ok(Value::Ptr(*self.methods.entry("pop").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "array.pop",
                        Arity::Optional,
                        "Removes and returns the last element in the array or the given default.
                      Panics if no default value was provided.",
                        move |vm, _, args| -> Result<_, _> { LoxArray::pop(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "map" => Ok(Value::Ptr(*self.methods.entry("map").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "array.map",
                        Arity::Fixed(1),
                        "Applies the given function to every element of the array. \
                              Creates and returns a new array for the mapped values.",
                        move |vm, _, args| -> Result<_, _> { LoxArray::map(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "len" => Ok(Value::Ptr(*self.methods.entry("len").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "array.len",
                        Arity::Fixed(0),
                        "Returns the number of elements in the array.",
                        move |vm, _, args| -> Result<_, _> { LoxArray::len(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "iterator" => {
                Ok(Value::Ptr(*self.methods.entry("iterator").or_insert_with(
                    || {
                        alloc_trait_fn_vm(
                            vm,
                            "array.iterator",
                            Arity::Fixed(0),
                            "Creates a new iterator over the array's elements.",
                            move |vm, _, _| -> Result<_, _> {
                                let ptr = vm.alloc(LoxObject::new(HeapValue::ObjNative(Box::new(LoxArrayIterator::new(this)))));
                                Ok(Value::Ptr(ptr))
                             },
                            &[this],
                        )
                    },
                )))
            }
            "fill" => {
                Ok(Value::Ptr(*self.methods.entry("fill").or_insert_with(
                    || {
                        alloc_trait_fn_vm(
                            vm,
                            "array.fill",
                            Arity::Fixed(2),
                            "Fills the array with the given number of shallow copies of the provided value.",
                            move |vm, _, args| -> Result<_, _> {
                                LoxArray::fill(vm, this, args)
                            },
                            &[this],
                        )
                    },
                )))
            }
            "copy" => {
                Ok(Value::Ptr(*self.methods.entry("copy").or_insert_with(
                    || {
                        alloc_trait_fn_vm(
                            vm,
                            "array.copy",
                            Arity::Fixed(0),
                            "Copies the array shallowly.",
                            move |vm, _, _| -> Result<_, _> {
                                LoxArray::copy(vm, this)
                            },
                            &[this],
                        )
                    },
                )))
            }
            _ => Err(vm.error(format!("Undefined property `{}`", name))),
        }
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(self
            .values
            .iter_mut()
            .filter_map(|v| v.as_ptr_mut())
            .chain(self.methods.values_mut()))
    }
}
