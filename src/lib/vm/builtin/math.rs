use rand::{prelude::StdRng, Rng, SeedableRng};
use std::convert::TryFrom;

use crate::rt_hashmap::FastHashMap;
use crate::{compiler::cco::Arity, context::SharedCtx, types::LoxFloat, vm::native::CowStr};

use errors::LoxError;
use vm::builtin::fn_wrapper::*;
use vm::object::ObjectPointer;
use vm::{
    object::{native::NativeObject, value::Value},
    vm::Vm,
};

#[derive(Debug)]
pub struct LoxMath {
    seed: u64,
    rng: StdRng,
    vtable: FastHashMap<&'static str, Value>,
}
impl_vtable_deepsize!(LoxMath, vtable, Value);

impl LoxMath {
    pub fn new(ctx: SharedCtx) -> Self {
        let mut vtable = FastHashMap::default();
        vtable.insert("PI", Value::Float(std::f64::consts::PI));
        vtable.insert("E", Value::Float(std::f64::consts::E));
        vtable.insert(
            "thread_random",
            Value::Ptr(alloc_trait_fn_ctx(
                ctx.clone(),
                "Math.thread_random",
                Arity::Fixed(0),
                "Returns a random number between 0 inclusive and 1 exclusive using the self-reseeding thread RNG.",
                move |_, _, _| {
                    let num = rand::thread_rng().gen_range(0.0, 1.0);
                    Ok(Value::Float(num))
                },
            )),
        );
        vtable.insert(
            "abs",
            Value::Ptr(alloc_trait_fn_ctx(
                ctx.clone(),
                "Math.abs",
                Arity::Fixed(1),
                "Computes the absolute value of the given object. Panics if the object is not a number.",
                move |vm, _, args| {
                    let num = match &args[0] {
                        Value::Int(i) => Value::Int(i.abs()),
                        Value::Float(f) => Value::Float(f.abs()),
                        _ => {
                            return Err(vm.error(format!(
                                "Expected a number, but got `{}`",
                                vm.type_of(&args[0])
                            )))
                        }
                    };
                    Ok(num)
                },
            )),
        );
        vtable.insert(
            "sin",
            Value::Ptr(alloc_trait_fn_ctx(
                ctx.clone(),
                "Math.sin",
                Arity::Fixed(1),
                "Computes the sine of the given object. Panics if the object is not a number.",
                move |vm, _, args| {
                    let num = match &args[0] {
                        Value::Int(i) => *i as LoxFloat,
                        Value::Float(f) => *f,
                        _ => {
                            return Err(vm.error(format!(
                                "Expected a number, but got `{}`",
                                vm.type_of(&args[0])
                            )))
                        }
                    };
                    Ok(Value::Float(num.sin()))
                },
            )),
        );
        vtable.insert(
            "cos",
            Value::Ptr(alloc_trait_fn_ctx(
                ctx.clone(),
                "Math.cos",
                Arity::Fixed(1),
                "Computes the cosine of the given object. Panics if the object is not a number.",
                move |vm, _, args| {
                    let num = args[0].as_float().ok_or_else(|| {
                        vm.error(format!(
                            "Expected a number, but got `{}`",
                            vm.type_of(&args[0])
                        ))
                    })?;
                    Ok(Value::Float(num.cos()))
                },
            )),
        );
        vtable.insert(
            "exp",
            Value::Ptr(alloc_trait_fn_ctx(
                ctx.clone(),
                "Math.exp",
                Arity::Fixed(1),
                "Computes `Math.E ** x` of the given number. Panics if the object is not a number.",
                move |vm, _, args| {
                    let num = args[0].as_float().ok_or_else(|| {
                        vm.error(format!(
                            "Expected a number, but got `{}`",
                            vm.type_of(&args[0])
                        ))
                    })?;
                    Ok(Value::Float(num.exp()))
                },
            )),
        );
        vtable.insert(
            "log",
            Value::Ptr(alloc_trait_fn_ctx(
                ctx.clone(),
                "Math.log",
                Arity::Fixed(1),
                "Computes `ln(x)` of the given number. panics if the object is not a number.",
                move |vm, _, args| {
                    let num = args[0].as_float().ok_or_else(|| {
                        vm.error(format!(
                            "Expected a number, but got `{}`",
                            vm.type_of(&args[0])
                        ))
                    })?;
                    Ok(Value::Float(num.ln()))
                },
            )),
        );
        vtable.insert(
            "clipf",
            Value::Ptr(alloc_trait_fn_ctx(
                ctx,
                "Math.clipf",
                Arity::Fixed(3),
                "Clips the given number to be between min and max. Panics if the any of the arguments isn't a number.",
                move |vm, _, args| {
                    let num = Self::to_float(vm, &args[0])?;
                    let min = Self::to_float(vm, &args[1])?;
                    let max = Self::to_float(vm, &args[2])?;
                    Ok(Value::Float(num.min(max).max(min)))
                }
            )),
        );

        let seed = rand::thread_rng().gen::<u64>();
        Self {
            seed,
            vtable,
            rng: StdRng::seed_from_u64(seed),
        }
    }

    #[allow(clippy::wrong_self_convention)]
    fn to_float(vm: &mut Vm, v: &Value) -> Result<LoxFloat, LoxError> {
        v.as_float()
            .ok_or_else(|| vm.error(format!("Expected a number, but got `{}`", vm.type_of(v))))
    }

    pub(crate) fn instance(this: &mut ObjectPointer) -> &mut LoxMath {
        unsafe { <dyn NativeObject>::downcast_mut::<LoxMath>(this) }
    }

    fn random(mut this: ObjectPointer) -> Value {
        let math = Self::instance(&mut this);
        Value::Float(math.rng.gen_range(0.0, 1.0))
    }

    fn random_in_range(
        vm: &mut Vm,
        mut this: ObjectPointer,
        args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        let low = args[0].as_int().ok_or_else(|| {
            vm.error(format!(
                "Expected the first argument to be an integer, but got `{}`",
                vm.type_of(&args[0])
            ))
        })?;
        let high = args[1].as_int().ok_or_else(|| {
            vm.error(format!(
                "Expected the first argument to be an integer, but got `{}`",
                vm.type_of(&args[1])
            ))
        })?;
        if low >= high {
            return Err(vm.error(format!("Expected low < high, but got {} >= {}", low, high)));
        }
        let math = Self::instance(&mut this);
        let num = math.rng.gen_range(low, high);
        Ok(Value::Int(num))
    }

    fn reseed(vm: &mut Vm, mut this: ObjectPointer, args: Vec<Value>) -> Result<Value, LoxError> {
        let seed = args
            .get(0)
            .map(|v| {
                v.as_int()
                    .ok_or_else(|| {
                        format!(
                            "Expected the first argument to be an integer, but got `{}`",
                            vm.type_of(&args[0])
                        )
                    })
                    .and_then(|v| {
                        u64::try_from(v).map_err(|_| {
                            format!(
                                "The seed must in the range 0 <= seed <= {}, but got {}",
                                u64::MAX,
                                v
                            )
                        })
                    })
                    .map_err(|e| vm.error(e))
            })
            .transpose()?;
        let math = Self::instance(&mut this);
        if let Some(seed) = seed {
            math.seed = seed;
            math.rng = StdRng::seed_from_u64(seed);
            Ok(Value::Nil)
        } else {
            Ok(Value::Int(math.seed as _))
        }
    }

    pub fn add_instance_methods(&mut self, ctx: SharedCtx, this: ObjectPointer) {
        self.vtable.insert(
            "random",
            Value::Ptr(alloc_trait_fn_ctx_with_captures(
                ctx.clone(),
                "Math.random",
                Arity::Fixed(0),
                "Returns a random number between 0 inclusive and 1 exclusive.",
                move |_, _, _| Ok(Self::random(this)),
                &[this],
            )),
        );
        self.vtable.insert(
            "random_in_range",
            Value::Ptr(alloc_trait_fn_ctx_with_captures(
                ctx.clone(),
                "Math.random_in_range",
                Arity::Fixed(2),
                "Returns a random integer in the specified range.",
                move |vm, _, args| Self::random_in_range(vm, this, args),
                &[this],
            )),
        );
        self.vtable.insert(
            "seed",
            Value::Ptr(alloc_trait_fn_ctx_with_captures(
                ctx,
                "Math.seed",
                Arity::Optional,
                "Reseeds the instance RNG (doesn't affect the thread RNG). Returns the current seed if no seed was provided.",
                move |vm, _, args| Self::reseed(vm, this, args),
                &[this],
            )),
        );
    }
}

impl NativeObject for LoxMath {
    fn lox_name(&self) -> CowStr {
        CowStr::from("Math")
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::from("<Math instance>"))
    }

    fn lox_doc(&self) -> CowStr {
        CowStr::Borrowed(
            r#"
    Lox's builtin class that provides mathematical constants and functions.

    Math::PI

        The value of Pi with the following precision: 3.14159265358979323846264338327950288.

    Math::E

        The value of e with the following precision: 2.71828182845904523536028747135266250.

    Math::abs(x)
        
        Computes the absolute value of the given object. Panics if the object is not a number.

    Math::sin(x)

        Computes the sine of the given object. Panics if the object is not a number.

    Math::cos(x)

        Computes the cosine of the given object. Panics if the object is not a number.
    
    Math::exp(x)

        Computes `Math.E ** x` of the given number. Panics if the object is not a number.

    Math::log(x)
        Computes `ln(x)` of the given number. panics if the object is not a number.

    Math::clipf(x, min, max)
        Clips the given number to be between min and max. Panics if the any of the arguments isn't a number.

    Math::thread_random()

        Returns a random number between 0 inclusive and 1 exclusive using the self-reseeding thread RNG.

    Math::random()

        Returns a random number between 0 inclusive and 1 exclusive.

    Math::random_in_range(a, b)
    
        Returns a random integer in the specified range.

    Math::seed(s)

        Reseeds the instance RNG (doesn't affect the thread RNG). 
        Returns the current seed if no seed was provided.
"#,
        )
    }

    fn get_field(&mut self, vm: &mut Vm, _: ObjectPointer, name: &str) -> Result<Value, LoxError> {
        self.vtable
            .get(name)
            .cloned()
            .ok_or_else(|| vm.error(format!("Undefined property `{}`", name)))
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(self.vtable.values_mut().filter_map(|v| v.as_ptr_mut()))
    }
}
