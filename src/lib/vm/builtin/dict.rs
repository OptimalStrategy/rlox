use crate::allocator::DeepSizeOf;

use std::collections::{hash_map::Keys, HashMap};

use crate::rt_hashmap::FastHashMap;
use vm::object::heap_value::HeapValue;
use vm::object::object::LoxObject;

use crate::{
    allocator::deep_size_of::deep_size_of_map, compiler::cco::Arity, types::LoxInt,
    vm::native::CowStr,
};

use errors::LoxError;
use vm::builtin::fn_wrapper::*;
use vm::object::ObjectPointer;
use vm::{
    object::{native::NativeObject, value::Value},
    vm::Vm,
};

use super::result::LoxResult;

#[derive(Debug)]
struct LoxKeys(Keys<'static, Value, LoxDictEntry>);
known_deep_size_fast!(0, LoxKeys);

#[derive(Debug)]
pub struct LoxDictIterator {
    dict: ObjectPointer,
    keys: LoxKeys,
    iter_next: Option<ObjectPointer>,
}
known_deep_size_fast!(0, LoxDictIterator);

impl LoxDictIterator {
    fn new(dict: ObjectPointer) -> Self {
        let keys = LoxKeys({
            let dict = unsafe { <dyn NativeObject>::downcast::<LoxDict>(&dict) };
            let keys: Keys<'_, Value, LoxDictEntry> = dict.map.keys();
            // Safety: this iterator is guaranteed to be valid for the lifetime of the LoxDictIterator object by the GC
            unsafe { std::mem::transmute::<_, Keys<'static, Value, LoxDictEntry>>(keys) }
        });
        Self {
            dict,
            keys,
            iter_next: None,
        }
    }

    fn next(&mut self) -> Value {
        match self.keys.0.next() {
            Some(v) => *v,
            None => Value::Nil,
        }
    }
}

impl NativeObject for LoxDictIterator {
    fn lox_name(&self) -> CowStr {
        CowStr::Borrowed("DictIterator")
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(std::iter::once(&mut self.dict))
    }

    fn get_field(
        &mut self,
        vm: &mut Vm,
        this: ObjectPointer,
        name: &str,
    ) -> Result<Value, LoxError> {
        match name {
            "iternext" => Ok(Value::Ptr(*self.iter_next.get_or_insert_with(|| {
                alloc_trait_fn_vm(
                    vm,
                    "DictIterator.iternext",
                    Arity::Fixed(0),
                    "Returns the next value in the iterator or nil.",
                    move |vm, _, _| -> Result<_, _> {
                        let mut ptr = this;
                        let this = unsafe {
                            <dyn NativeObject>::downcast_mut::<LoxDictIterator>(&mut ptr)
                        };
                        Ok(Value::Ptr(LoxResult::alloc_new_based_on_value(
                            vm,
                            this.next(),
                        )))
                    },
                    &[this],
                )
            }))),
            _ => Err(vm.error(format!("Undefined property `{}`", name))),
        }
    }
}

#[derive(Debug)]
pub struct LoxDictEntry {
    /// This field is Some if this entry's key is a pointer.
    key: Option<ObjectPointer>,
    value: Value,
}
known_deep_size_fast!(0, LoxDictEntry);

#[derive(Debug)]
pub struct LoxDict {
    map: HashMap<Value, LoxDictEntry>,
    methods: FastHashMap<&'static str, ObjectPointer>,
}

impl DeepSizeOf for LoxDict {
    fn deep_size_of_children(&self) -> usize {
        let vtable_mem = deep_size_of_map::<&'static str, ObjectPointer>(self.methods.capacity());
        let values_mem = deep_size_of_map::<Value, LoxDictEntry>(self.map.capacity());

        vtable_mem + values_mem
    }
}
// deepsize::known_deep_size!(0, LoxKeysIter);
// impl_vtable_deepsize!(LoxMath, vtable, Value);

impl LoxDict {
    pub fn new(map: Vec<(Value, Value)>) -> LoxDict {
        let mut methods = FastHashMap::default();
        methods.reserve(3);
        Self {
            map: map
                .into_iter()
                .map(|(k, v)| {
                    (
                        k,
                        LoxDictEntry {
                            key: k.as_ptr().copied(),
                            value: v,
                        },
                    )
                })
                .collect(),
            methods,
        }
    }

    pub fn empty() -> LoxDict {
        let mut methods = FastHashMap::default();
        methods.reserve(3);
        Self {
            map: HashMap::with_capacity(8),
            methods,
        }
    }

    fn insert(
        _: &mut Vm,
        mut this: ObjectPointer,
        mut args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        let dict = unsafe { <dyn NativeObject>::downcast_mut::<LoxDict>(&mut this) };
        let value = args.pop().unwrap();
        let mut key = args.pop().unwrap();
        // Pre-compute string key hashes
        key.as_heap_value_mut()
            .and_then(|v| v.as_string_mut())
            .map(|s| s.hash());
        dict.map.insert(
            key,
            LoxDictEntry {
                key: key.as_ptr().copied(),
                value,
            },
        );
        Ok(Value::Nil)
    }

    fn remove(
        vm: &mut Vm,
        mut this: ObjectPointer,
        mut args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        let key = args.pop().unwrap();
        let dict = unsafe { <dyn NativeObject>::downcast_mut::<LoxDict>(&mut this) };
        match dict.map.remove(&key) {
            Some(result) => Ok(result.value),
            None => match args.pop() {
                Some(default) => Ok(default),
                None => {
                    let key = vm.stringify(&key)?;
                    Err(vm.error(format!("Key not found: `{}`", key)))
                }
            },
        }
    }

    fn get(vm: &mut Vm, mut this: ObjectPointer, args: Vec<Value>) -> Result<Value, LoxError> {
        let dict = unsafe { <dyn NativeObject>::downcast_mut::<LoxDict>(&mut this) };
        match dict.map.get(args.first().unwrap()) {
            Some(result) => Ok(result.value),
            None => match args.get(1) {
                Some(default) => Ok(*default),
                None => {
                    let key = args.first().unwrap();
                    let key = vm.stringify(key)?;
                    Err(vm.error(format!("Key not found: `{}`", key)))
                }
            },
        }
    }

    fn contains(
        _vm: &mut Vm,
        mut this: ObjectPointer,
        args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        let dict = unsafe { <dyn NativeObject>::downcast_mut::<LoxDict>(&mut this) };
        Ok(Value::Bool(dict.map.contains_key(args.first().unwrap())))
    }

    fn set_item(vm: &mut Vm, this: ObjectPointer, args: Vec<Value>) -> Result<Value, LoxError> {
        let value = *args.last().unwrap();
        Self::insert(vm, this, args)?;
        Ok(value)
    }

    fn len(_: &mut Vm, mut this: ObjectPointer, _: Vec<Value>) -> Result<Value, LoxError> {
        let dict = unsafe { <dyn NativeObject>::downcast_mut::<LoxDict>(&mut this) };
        Ok(Value::Int(dict.map.len() as LoxInt))
    }
}

impl NativeObject for LoxDict {
    fn lox_name(&self) -> CowStr {
        CowStr::Borrowed("dict")
    }

    fn is_instance_of(&self, class: &dyn NativeObject) -> bool {
        class.lox_name() == "dict"
    }

    fn to_string(&self, vm: &mut Vm) -> Result<CowStr, LoxError> {
        let mut values = Vec::with_capacity(self.map.len());
        for (k, v) in &self.map {
            values.push(format!("{}: {}", k, v.value.stringify(vm)?));
        }
        Ok(CowStr::Owned(format!("{{{}}}", values.join(", "))))
    }

    fn get_field(
        &mut self,
        vm: &mut Vm,
        this: ObjectPointer,
        name: &str,
    ) -> Result<Value, LoxError> {
        match name {
            "insert" => Ok(Value::Ptr(*self.methods.entry("insert").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "dict.insert",
                        Arity::Fixed(2),
                        "Inserts a value into the dict.",
                        move |vm, _, args| -> Result<_, _> { LoxDict::insert(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "length" => Ok(Value::Int(self.map.len() as LoxInt)),
            "get" => Ok(Value::Ptr(*self.methods.entry("get").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "dict.get",
                        Arity::InRange(1, 2),
                        "Attempts to get the element with the given key. Panics the key is not found in the dict and no default was provided.",
                        move |vm, _, args| -> Result<_, _> { LoxDict::get(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "contains" => Ok(Value::Ptr(*self.methods.entry("contains").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "dict.contains",
                        Arity::Fixed(1),
                        "Returns true if an element with the given key is in the dict.",
                        move |vm, _, args| -> Result<_, _> { LoxDict::contains(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "getitem" => Ok(Value::Ptr(*self.methods.entry("getitem").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "dict.getitem",
                        Arity::Fixed(1),
                        "Attempts to get the element with the given key. Panics the key is not found in the dict.",
                        move |vm, _, args| -> Result<_, _> { LoxDict::get(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "setitem" => Ok(Value::Ptr(*self.methods.entry("setitem").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "dict.setitem",
                        Arity::Fixed(2),
                        "Attempts to insert the element at the given key.",
                        move |vm, _, args| -> Result<_, _> { LoxDict::set_item(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "remove" => Ok(Value::Ptr(*self.methods.entry("remove").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "dict.remove",
                        Arity::InRange(1, 2),
                        "Removes and returns the element with the given key or the given default.
                      Panics if no default value was provided.",
                        move |vm, _, args| -> Result<_, _> { LoxDict::remove(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "len" => Ok(Value::Ptr(*self.methods.entry("len").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "dict.len",
                        Arity::Fixed(0),
                        "Returns the number of elements in the dict.",
                        move |vm, _, args| -> Result<_, _> { LoxDict::len(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "iterator" => {
                Ok(Value::Ptr(*self.methods.entry("iterator").or_insert_with(
                    || {
                        alloc_trait_fn_vm(
                            vm,
                            "dict.iterator",
                            Arity::Fixed(0),
                            "Creates a new iterator over the dict's keys.",
                            move |vm, _, _| -> Result<_, _> {
                                let ptr = vm.alloc(LoxObject::new(HeapValue::ObjNative(Box::new(LoxDictIterator::new(this)))));
                                Ok(Value::Ptr(ptr))
                             },
                        &[this],
                        )
                    },
                )))
            }
            _ => Err(vm.error(format!("Undefined property `{}`", name))),
        }
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(
            self.map
                .values_mut()
                .flat_map(|entry| {
                    entry
                        .key
                        .as_mut()
                        .into_iter()
                        .chain(entry.value.as_ptr_mut().into_iter())
                })
                .chain(self.methods.values_mut()),
        )
    }
}
