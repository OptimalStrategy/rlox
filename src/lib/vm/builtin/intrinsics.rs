use crate::{
    compiler::cco::Arity,
    context::SharedCtx,
    vm::{native::CowStr, HeapValue, LoxObject},
};

use crate::rt_hashmap::FastHashMap;
use errors::LoxError;
use vm::builtin::fn_wrapper::*;
use vm::object::ObjectPointer;
use vm::{
    object::{native::NativeObject, value::Value},
    vm::Vm,
};

use super::array::LoxArray;

#[derive(Debug)]
pub struct LoxIntrinsics {
    methods: FastHashMap<&'static str, ObjectPointer>,
}
impl_vtable_deepsize!(LoxIntrinsics, methods, ObjectPointer);

impl LoxIntrinsics {
    pub fn new(ctx: SharedCtx) -> Self {
        let mut methods = FastHashMap::default();
        methods.insert(
            "debug_obj_info",
            alloc_trait_fn_ctx(
                ctx.clone(),
                "Intrinsics.debug_obj_info",
                Arity::Fixed(1),
                "Returns the internal debug information about an object.",
                move |vm, _, args| -> Result<_, _> {
                    let s = match &args[0] {
                        Value::Ptr(ptr) => format!("{:#?}", ptr.as_ref()),
                        rest => format!("{:#?}", rest),
                    };
                    Ok(Value::Ptr(vm.alloc_or_get_interned(s)))
                },
            ),
        );
        methods.insert(
            "has_field",
            alloc_trait_fn_ctx(
                ctx.clone(),
                "Intrinsics.has_field",
                Arity::Fixed(2),
                "Returns true if the given object has a field or method with the given name. \
                      The second argument will be converted to a string if needed.",
                move |vm, _, args| -> Result<_, _> {
                    let field_name = vm.stringify(&args[1])?;
                    let has_field = vm.get_field(args[0], &field_name).is_ok();
                    Ok(Value::Bool(has_field))
                },
            ),
        );
        methods.insert(
            "set_class",
            alloc_trait_fn_ctx(
                ctx.clone(),
                "Intrinsics.set_class",
                Arity::Fixed(2),
                "Sets the class property on the given object, returning the previous class. The class affects objects' method resolution and other properties.",
                move |vm, _, args| -> Result<_, _> {
                    let mut args = args.into_iter();
                    let mut obj =  args.next().unwrap();
                    let obj = match obj.as_object_mut() {
                        Some(obj) => obj,
                        None => {
                            return Err(vm.error(format!(
                                "Expected the first argument to be an instance of a lox class, but got `{}`",
                                vm.type_of(&obj)
                            )))
                        }
                    };
                    let cls = args.next().unwrap();
                    if cls.as_class().is_none() {
                        return Err(vm.error(format!(
                            "Expected the second argument to be a lox class, but got `{}`",
                            vm.type_of(&cls)
                        )));
                    }
                    let old_class = std::mem::replace(&mut obj.class, *cls.as_ptr().unwrap());
                    obj.bound_methods.clear();
                    Ok(Value::Ptr(old_class))
                },
            ),
        );
        methods.insert(
            "set_name",
            alloc_trait_fn_ctx(
                ctx.clone(),
                "Intrinsics.set_name",
                Arity::Fixed(2),
                "Attempts to change the name of the name of the first argument to the value of the second argument. Does nothing if the change is impossible.",
                move |vm, _, args| -> Result<_, _> {
                    let mut args = args.into_iter();
                    let mut obj =  args.next().unwrap();
                    if let Some(obj) = obj.as_heap_value_mut() {
                        let name = vm.stringify(&args.next().unwrap())?;
                        match obj {
                            HeapValue::Cls(cls) => {
                                cls.name = name.into_owned();
                            }
                            HeapValue::CCO(cco) => {
                                cco.name = name;
                            }
                            HeapValue::Closure { cco, .. }
                            | HeapValue::BoundMethod { cco, .. } => {
                                cco.as_mut().as_cco_mut().name = name;
                            }
                            HeapValue::Str(_)
                            | HeapValue::ObjNative(_)
                            | HeapValue::Obj(_) => {}
                            HeapValue::UpValue(_) => unreachable!(),
                        }
                    };
                    Ok(obj)
                },
            ),
        );
        methods.insert(
            "upvalues",
            alloc_trait_fn_ctx(
                ctx,
                "Intrinsics.upvalues",
                Arity::Fixed(1),
                UPVALUES_DOC,
                move |vm, _, args| -> Result<_, _> {
                    let arg = args.into_iter().next().unwrap();
                    let result = match arg.as_heap_value() {
                        Some(HeapValue::CCO(_)) => {
                            Value::Ptr(vm.alloc_native(LoxArray::new(vec![])))
                        }
                        Some(HeapValue::ObjNative(obj)) if obj.is_callable() => {
                            Value::Ptr(vm.alloc_native(LoxArray::new(vec![])))
                        }
                        Some(HeapValue::Closure { upvalues, .. }) => {
                            let obj = LoxArray::new(
                                upvalues
                                    .iter()
                                    .copied()
                                    .map(|v| {
                                        Value::Ptr(
                                            vm.alloc(IntrinsicUpValue::new_obj(Value::Ptr(v))),
                                        )
                                    })
                                    .collect(),
                            );
                            Value::Ptr(vm.alloc_native(obj))
                        }
                        Some(HeapValue::BoundMethod { cco, .. }) => {
                            let upvalues = cco
                                .as_ref()
                                .try_get_upvalues()
                                .map(|up| {
                                    up.iter()
                                        .copied()
                                        .map(|v| {
                                            Value::Ptr(
                                                vm.alloc(IntrinsicUpValue::new_obj(Value::Ptr(v))),
                                            )
                                        })
                                        .collect()
                                })
                                .unwrap_or_else(Vec::new);
                            Value::Ptr(vm.alloc_native(LoxArray::new(upvalues)))
                        }
                        _ => {
                            return Err(vm.error(format!(
                                "An object of type {} is not a function",
                                vm.type_of(&arg)
                            )))
                        }
                    };
                    Ok(result)
                },
            ),
        );
        Self { methods }
    }
}

impl NativeObject for LoxIntrinsics {
    fn lox_name(&self) -> CowStr {
        CowStr::from("Intrinsics")
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::from("<Intrinsics instance>"))
    }

    fn lox_doc(&self) -> CowStr {
        CowStr::Borrowed(
            r#"
    Lox's builtin class that provides methods for inspection and manipulation of objects.

    Intrinsics::debug_obj_info(obj)

        Returns the internal debug information about an object.

    Intrinsics::upvalues(fn)

        Returns a list of upvalues that the given function captures. Each upvalue in the list is read-only.
        To access a captured object, use the `.val` property. Use `help(Intrinsics.upvalues)` for more info.

    Intrinsics::set_class(obj, cls)
    
        Sets the class property on the given object, invalidating any method bindings the object might have, 
        and returning the previous class. The class affects objects' method resolution and other properties.

    Intrinsics::has_field(obj, field)

        Checks if the given object has a field, method, or static method
        with the provided name."#,
        )
    }

    fn get_field(&mut self, vm: &mut Vm, _: ObjectPointer, name: &str) -> Result<Value, LoxError> {
        self.methods
            .get(name)
            .map(|ptr| Value::Ptr(*ptr))
            .ok_or_else(|| vm.error(format!("Undefined property `{}`", name)))
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(self.methods.values_mut())
    }
}

#[derive(Debug)]
pub struct IntrinsicUpValue {
    val: Value,
}
known_deep_size_fast!(0, IntrinsicUpValue);

impl IntrinsicUpValue {
    pub fn new(val: Value) -> IntrinsicUpValue {
        assert!(
            val.as_heap_value()
                .map(|v| v.as_upvalue().is_some())
                .unwrap_or(false),
            "Given value is not an upvalue: {:?}",
            val
        );
        Self { val }
    }

    pub fn new_obj(val: Value) -> LoxObject {
        LoxObject::new(HeapValue::ObjNative(Box::new(Self::new(val))))
    }
}

impl NativeObject for IntrinsicUpValue {
    fn lox_name(&self) -> CowStr {
        "UpValue".into()
    }

    fn is_instance_of(&self, _class: &dyn NativeObject) -> bool {
        _class.lox_name() == self.lox_name()
    }

    fn to_string(&self, vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(CowStr::Owned(format!(
            "UpValue({})",
            vm.stringify(&self.val)?
        )))
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        self.val.contained_pointers()
    }

    fn get_field(
        &mut self,
        vm: &mut Vm,
        _this: ObjectPointer,
        name: &str,
    ) -> Result<Value, LoxError> {
        match name {
            "val" => Ok(self
                .val
                .as_heap_value()
                .and_then(|v| v.as_upvalue())
                .cloned()
                .unwrap()),
            _ => Err(vm.error(format!("Undefined property `{}`", name))),
        }
    }
}

static UPVALUES_DOC: &str = r#"Returns an array of the upvalues that the given function captures. Panics if the argument is not a function.
Example:

    fun channel(val) {
        var x = val;
        const getter = fun() => x;
        const setter = fun(y) => x = y;
        return fun(i) => match i { 0 => getter, 1 => setter };
    }
    const chan = channel(0);
    const get = chan(0);
    const set = chan(1);
    assert(get() == 0);

    set(5);
    assert(get() == 5);

    const up1 = Intrinsics.upvalues(get)..0;
    const up2 = Intrinsics.upvalues(set)..0;

    assert(get() == up1.val);
    assert(get() == up2.val);

    set(77);

    assert(get() == 77);
    assert(up1.val == 77);
    assert(up2.val == 77);
"#;
