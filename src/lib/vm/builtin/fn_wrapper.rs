use crate::allocator::{deep_size_of::deep_size_of_cow, DeepSizeOf};

use crate::{
    vm::native::CowStr,
    vm::{HeapValue, LoxObject},
    SharedCtx,
};

use compiler::cco::Arity;
use errors::LoxError;
use module::ModuleId;
use vm::object::native::NativeObject;
use vm::object::value::Value;
use vm::object::ObjectPointer;
use vm::vm::Vm;

pub type RustFn = for<'a> fn(&mut Vm, ObjectPointer, Vec<Value>) -> Result<Value, LoxError>;
pub trait RustTraitFn: Fn(&mut Vm, ObjectPointer, Vec<Value>) -> Result<Value, LoxError> {}

pub struct RustFnWrapper<Fun>
where
    Fun: Fn(&mut Vm, ObjectPointer, Vec<Value>) -> Result<Value, LoxError>,
{
    r#fn: Fun,
    arity: Arity,
    name: CowStr,
    doc: CowStr,
    /// XXX: We'll mMake it a single pointer for the time being
    captured_pointer: Option<ObjectPointer>,
}

impl<Fun> RustFnWrapper<Fun>
where
    Fun: Fn(&mut Vm, ObjectPointer, Vec<Value>) -> Result<Value, LoxError>,
{
    pub fn new(r#fn: Fun, arity: Arity, name: CowStr, doc: CowStr) -> Self {
        Self {
            r#fn,
            arity,
            name,
            doc,
            captured_pointer: None,
        }
    }

    pub fn capture_ptrs(mut self, captured: &[ObjectPointer; 1]) -> Self {
        self.captured_pointer = Some(captured[0]);
        self
    }
}

impl<Fun> NativeObject for RustFnWrapper<Fun>
where
    Fun: Fn(&mut Vm, ObjectPointer, Vec<Value>) -> Result<Value, LoxError>,
{
    #[inline]
    fn lox_name(&self) -> CowStr {
        self.name.clone()
    }

    #[inline]
    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(self.captured_pointer.iter_mut())
    }

    #[inline]
    fn lox_doc(&self) -> CowStr {
        self.doc.clone()
    }

    #[inline]
    fn module(&self) -> ModuleId {
        ModuleId::internal()
    }

    #[inline]
    fn arity(&self) -> Option<Arity> {
        Some(self.arity)
    }

    #[inline]
    fn call(
        &mut self,
        vm: &mut Vm,
        this: ObjectPointer,
        args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        (self.r#fn)(vm, self.captured_pointer.unwrap_or(this), args)
    }
}

impl<Fun> DeepSizeOf for RustFnWrapper<Fun>
where
    Fun: Fn(&mut Vm, ObjectPointer, Vec<Value>) -> Result<Value, LoxError>,
{
    #[inline(always)]
    fn deep_size_of_children(&self) -> usize {
        deep_size_of_cow(&self.name) + deep_size_of_cow(&self.doc)
    }
}

impl<Fun> std::fmt::Debug for RustFnWrapper<Fun>
where
    Fun: Fn(&mut Vm, ObjectPointer, Vec<Value>) -> Result<Value, LoxError>,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "RustFnWrapper {{ arity: {:?}, name: {}, captured = {:?}, ... }}",
            self.arity, self.name, self.captured_pointer
        )
    }
}

pub fn alloc_trait_fn_vm<S: Into<CowStr>>(
    vm: &mut Vm,
    name: S,
    arity: Arity,
    doc: S,
    r#fn: impl Fn(&mut Vm, ObjectPointer, Vec<Value>) -> Result<Value, LoxError> + 'static,
    captured_ptrs: &[ObjectPointer; 1],
) -> ObjectPointer {
    let wrapped =
        RustFnWrapper::new(r#fn, arity, name.into(), doc.into()).capture_ptrs(captured_ptrs);
    vm.alloc(LoxObject::new(HeapValue::ObjNative(Box::new(wrapped))))
}

pub fn alloc_trait_fn_ctx<S: Into<CowStr>>(
    ctx: SharedCtx,
    name: S,
    arity: Arity,
    doc: S,
    r#fn: impl Fn(&mut Vm, ObjectPointer, Vec<Value>) -> Result<Value, LoxError> + 'static,
) -> ObjectPointer {
    let wrapped = RustFnWrapper::new(r#fn, arity, name.into(), doc.into());
    let ptr = ctx
        .write_shared()
        .alloc_without_gc_trigger(LoxObject::new(HeapValue::ObjNative(Box::new(wrapped))));
    ptr
}

pub fn alloc_trait_fn_ctx_with_captures<S: Into<CowStr>>(
    ctx: SharedCtx,
    name: S,
    arity: Arity,
    doc: S,
    r#fn: impl Fn(&mut Vm, ObjectPointer, Vec<Value>) -> Result<Value, LoxError> + 'static,
    captured_ptrs: &[ObjectPointer; 1],
) -> ObjectPointer {
    let wrapped =
        RustFnWrapper::new(r#fn, arity, name.into(), doc.into()).capture_ptrs(captured_ptrs);
    let ptr = ctx
        .write_shared()
        .alloc_without_gc_trigger(LoxObject::new(HeapValue::ObjNative(Box::new(wrapped))));
    ptr
}
