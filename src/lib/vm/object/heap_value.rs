use crate::allocator::{deep_size_of::deep_size_of_vec, DeepSizeOf};

use super::{native::CowStr, object::ClassObject, Object, ObjectAddress, ObjectPointer};
use crate::compiler::cco::CompiledCodeObject;
use vm::immutable_string::ImmutableString;
use vm::object::native::NativeObject;
use vm::object::value::Value;

#[derive(Debug)]
pub enum HeapValue {
    UpValue(Value),
    Str(Box<ImmutableString>),
    Obj(Box<Object>),
    Cls(Box<ClassObject>),
    ObjNative(Box<dyn NativeObject>),
    CCO(Box<CompiledCodeObject>),
    Closure {
        cco: ObjectPointer,
        upvalues: Vec<ObjectPointer>,
    },
    BoundMethod {
        cco: ObjectPointer,
        instance: ObjectPointer,
    },
}

impl HeapValue {
    #[inline]
    pub fn is_cco(&self) -> bool {
        matches!(self, HeapValue::CCO(_))
    }

    #[inline]
    pub fn is_obj_native(&self) -> bool {
        matches!(self, HeapValue::ObjNative(_))
    }

    #[inline]
    pub fn as_upvalue(&self) -> Option<&Value> {
        match self {
            Self::UpValue(v) => Some(v),
            _ => None,
        }
    }

    #[inline]
    pub fn as_string(&self) -> Option<&ImmutableString> {
        match self {
            Self::Str(s) => Some(s),
            _ => None,
        }
    }

    #[inline]
    pub fn as_string_mut(&mut self) -> Option<&mut ImmutableString> {
        match self {
            Self::Str(s) => Some(s),
            _ => None,
        }
    }

    #[inline]
    pub fn as_cco(&self) -> Option<&CompiledCodeObject> {
        match self {
            HeapValue::CCO(cco) => Some(cco),
            HeapValue::Closure { cco, .. } => Some(cco.as_ref().as_cco()),
            HeapValue::BoundMethod { cco, .. } => Some(cco.as_ref().as_cco()),
            _ => None,
        }
    }

    #[inline]
    pub fn as_class(&self) -> Option<&ClassObject> {
        match self {
            HeapValue::Cls(cls) => Some(cls),
            HeapValue::UpValue(up) => up.as_class(),
            _ => None,
        }
    }

    #[inline]
    pub fn as_class_mut(&mut self) -> Option<&mut ClassObject> {
        match self {
            HeapValue::Cls(cls) => Some(cls),
            HeapValue::UpValue(up) => up.as_class_mut(),
            _ => None,
        }
    }

    #[inline]
    pub fn as_obj(&self) -> Option<&Object> {
        match self {
            HeapValue::Obj(obj) => Some(obj),
            _ => None,
        }
    }

    #[inline]
    pub fn as_obj_mut(&mut self) -> Option<&mut Object> {
        match self {
            HeapValue::Obj(obj) => Some(obj),
            _ => None,
        }
    }

    #[inline]
    pub fn as_obj_native(&self) -> Option<&dyn NativeObject> {
        match self {
            HeapValue::ObjNative(obj) => Some(&**obj),
            _ => None,
        }
    }

    #[inline]
    pub fn as_obj_native_mut(&mut self) -> Option<&mut dyn NativeObject> {
        match self {
            HeapValue::ObjNative(obj) => Some(&mut **obj),
            _ => None,
        }
    }

    #[inline(always)]
    pub fn follow_ptr(&self) -> &Self {
        self.follow_ptr_tup().1
    }

    #[inline(always)]
    pub fn follow_ptr_mut(&mut self) -> &mut Self {
        self.follow_ptr_tup_mut().1
    }

    /// This will loop indefinitely if there's a cycle in the object tree.
    #[cfg(not(feature = "debug-info"))]
    pub fn follow_ptr_tup(&self) -> (Option<ObjectPointer>, &Self) {
        let mut v = self;
        let mut ptr = None;

        loop {
            match v {
                Self::UpValue(Value::Ptr(p)) => {
                    ptr = Some(*p);
                    v = &p.as_ref().value
                }
                _ => break (ptr, v),
            };
        }
    }

    #[cfg(feature = "debug-info")]
    pub fn follow_ptr_tup(&self) -> (Option<ObjectPointer>, &Self) {
        let mut v = self;
        let mut ptr = None;
        loop {
            match v {
                Self::UpValue(Value::Ptr(p)) => {
                    if ptr.is_some() {
                        println!("[WARNING] Following a forwarding pointer: {:#?}", p)
                    }
                    ptr = Some(*p);
                    v = &p.as_ref().value
                }
                _ => break (ptr, v),
            };
        }
    }

    /// This will loop indefinitely if there's a cycle in the object tree.
    #[cfg(not(feature = "debug-info"))]
    pub fn follow_ptr_tup_mut(&mut self) -> (Option<ObjectPointer>, &mut Self) {
        let mut v = self;
        let mut ptr = None;

        loop {
            match v {
                Self::UpValue(Value::Ptr(p)) => {
                    ptr = Some(*p);
                    v = &mut p.as_mut().value
                }
                _ => break (ptr, v),
            };
        }
    }

    #[cfg(feature = "debug-info")]
    pub fn follow_ptr_tup_mut(&mut self) -> (Option<ObjectPointer>, &mut Self) {
        let mut v = self;
        let mut ptr = None;
        loop {
            match v {
                Self::UpValue(Value::Ptr(p)) => {
                    if ptr.is_some() {
                        println!("[WARNING] Following a forwarding pointer: {:#?}", p)
                    }
                    ptr = Some(p.clone());
                    v = &mut p.as_mut().value
                }
                _ => break (ptr, v),
            };
        }
    }

    pub fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        match self {
            HeapValue::Obj(obj) => Box::new(obj.contained_pointers()),
            HeapValue::ObjNative(obj) => obj.contained_pointers(),
            HeapValue::CCO(cco) => Box::new(
                cco.chunk.code_objects.iter_mut().chain(
                    cco.chunk
                        .constants
                        .iter_mut()
                        .filter_map(|v| v.as_ptr_mut())
                        .chain(cco.chunk.inline_cache.contained_pointers()),
                ),
            ),
            HeapValue::UpValue(v) => Box::new(v.as_ptr_mut().into_iter()),
            HeapValue::Closure { cco, upvalues } => {
                Box::new(cco.as_mut().contained_pointers().chain(upvalues.iter_mut()))
            }
            HeapValue::BoundMethod { cco, instance } => {
                Box::new(std::iter::once(cco).chain(std::iter::once(instance)))
            }
            HeapValue::Cls(cls) => Box::new(cls.contained_pointers()),
            HeapValue::Str(s) => s.contained_pointers(),
        }
    }

    pub fn eq_cached(&mut self, other: &mut Self) -> bool {
        match (self, other) {
            (HeapValue::UpValue(l), HeapValue::UpValue(r)) => l.eq_cached(r),
            (HeapValue::Str(l), HeapValue::Str(r)) => l.hash() == r.hash(),
            (HeapValue::CCO(l), HeapValue::CCO(r)) => l.lox_address() == r.lox_address(),
            (
                HeapValue::Closure {
                    upvalues: l_upvalues,
                    ..
                },
                HeapValue::Closure {
                    upvalues: r_upvalues,
                    ..
                },
            ) => l_upvalues.lox_address() == r_upvalues.lox_address(),
            (
                HeapValue::BoundMethod {
                    cco: l_cco,
                    instance: l_instance,
                },
                HeapValue::BoundMethod {
                    cco: r_cco,
                    instance: r_instance,
                },
            ) => {
                l_cco.lox_address() == r_cco.lox_address()
                    && l_instance.lox_address() == r_instance.lox_address()
            }
            (HeapValue::Obj(l), HeapValue::Obj(r)) => l.lox_address() == r.lox_address(),
            (HeapValue::Cls(l), HeapValue::Cls(r)) => l.lox_address() == r.lox_address(),
            (ref mut l, HeapValue::UpValue(Value::Ptr(r))) => {
                l.eq_cached(r.as_mut().as_heap_value_mut())
            }
            (HeapValue::UpValue(Value::Ptr(l)), ref mut r) => {
                l.as_mut().as_heap_value_mut().eq_cached(r)
            }
            _ => false,
        }
    }

    pub fn stringify(&self, vm: &mut crate::vm::Vm) -> Result<CowStr, crate::LoxError> {
        Ok(match self {
            HeapValue::UpValue(upvalue) => vm.stringify(upvalue)?,
            HeapValue::ObjNative(obj) => obj.to_string(vm)?,
            _ => format!("{}", self).into(),
        })
    }
}

impl PartialEq for HeapValue {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (HeapValue::Str(l), HeapValue::Str(r)) => l.hash_expensive() == r.hash_expensive(),
            (HeapValue::UpValue(l), HeapValue::UpValue(r)) => l.eq(r),
            (HeapValue::Obj(l), HeapValue::Obj(r)) => l.lox_address() == r.lox_address(),
            (HeapValue::Cls(l), HeapValue::Cls(r)) => l.lox_address() == r.lox_address(),
            (HeapValue::CCO(l), HeapValue::CCO(r)) => l.lox_address() == r.lox_address(),
            (
                HeapValue::Closure {
                    upvalues: l_upvalues,
                    ..
                },
                HeapValue::Closure {
                    upvalues: r_upvalues,
                    ..
                },
            ) => l_upvalues.lox_address() == r_upvalues.lox_address(),
            (
                HeapValue::BoundMethod {
                    cco: l_cco,
                    instance: l_instance,
                },
                HeapValue::BoundMethod {
                    cco: r_cco,
                    instance: r_instance,
                },
            ) => {
                l_cco.lox_address() == r_cco.lox_address()
                    && l_instance.lox_address() == r_instance.lox_address()
            }
            (ref l, HeapValue::UpValue(Value::Ptr(r))) => l.eq(&r.as_ref().as_heap_value()),
            (HeapValue::UpValue(Value::Ptr(l)), r) => l.as_ref().as_heap_value().eq(r),
            _ => false,
        }
    }
}

impl DeepSizeOf for HeapValue {
    fn deep_size_of_children(&self) -> usize {
        match self {
            HeapValue::Str(s) => s.deep_size_of_children(),
            HeapValue::Obj(obj) => obj.deep_size_of_children(),
            HeapValue::Cls(cls) => cls.deep_size_of_children(),
            HeapValue::CCO(cco) => cco.deep_size_of_children(),
            HeapValue::ObjNative(obj) => obj.deep_size_of_children(),
            HeapValue::Closure { upvalues, .. } => deep_size_of_vec(upvalues),
            HeapValue::BoundMethod { .. } => 0,
            HeapValue::UpValue(_) => 0,
        }
    }
}

impl From<String> for crate::vm::object::HeapValue {
    fn from(v: String) -> Self {
        Self::Str(Box::new(ImmutableString::new(v)))
    }
}

impl From<&str> for crate::vm::object::HeapValue {
    fn from(v: &str) -> Self {
        Self::Str(Box::new(ImmutableString::new(v)))
    }
}

impl From<CompiledCodeObject> for HeapValue {
    fn from(cco: CompiledCodeObject) -> Self {
        Self::CCO(Box::new(cco))
    }
}

impl From<ClassObject> for HeapValue {
    fn from(cls: ClassObject) -> Self {
        Self::Cls(Box::new(cls))
    }
}

impl From<Object> for HeapValue {
    fn from(obj: Object) -> Self {
        Self::Obj(Box::new(obj))
    }
}

impl From<Box<dyn NativeObject>> for HeapValue {
    fn from(obj: Box<dyn NativeObject>) -> Self {
        Self::ObjNative(obj)
    }
}

impl std::fmt::Display for HeapValue {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                HeapValue::Str(box s) => s.as_ref().to_string(),
                HeapValue::Obj(obj) => {
                    format!("<{} instance>", obj.class_name())
                }
                HeapValue::Cls(cls) => cls.name.clone(),
                HeapValue::CCO(cco) => format!("<fn: {}>", cco.name),
                HeapValue::Closure { cco, .. } => {
                    if cfg!(feature = "debug-info") {
                        format!(
                            "<@{:#x} fn: {}>",
                            self.lox_address(),
                            cco.as_ref().as_cco().name
                        )
                    } else {
                        format!("<fn: {}>", cco.as_ref().as_cco().name)
                    }
                }
                HeapValue::BoundMethod { cco, instance } => {
                    if cfg!(feature = "debug-info") {
                        format!(
                            "<@{:#x} fn: {}.{}>",
                            self.lox_address(),
                            instance.as_ref().as_object().class_name(),
                            cco.as_ref().as_cco().name
                        )
                    } else {
                        format!(
                            "<fn: {}.{}>",
                            instance.as_ref().as_object().class_name(),
                            cco.as_ref().as_cco().name
                        )
                    }
                }
                HeapValue::UpValue(value) => value.to_string(),
                HeapValue::ObjNative(native) => format!(
                    "<native lox object `{}` at {:x}>",
                    native.lox_name(),
                    native.lox_address()
                ),
            }
        )
    }
}
