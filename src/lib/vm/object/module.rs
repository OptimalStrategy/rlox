use std::{borrow::Cow, collections::HashMap};
use vm::native::CowStr;
use vm::native::NativeObject;

use crate::allocator::{deep_size_of::deep_size_of_map_with_owned_keys, DeepSizeOf};

use crate::vm::Vm;

use errors::LoxError;
use module::ModuleId;
use vm::object::value::Value;
use vm::object::ObjectPointer;

#[derive(Debug)]
pub struct LoxModule {
    name: String,
    module_id: ModuleId,
    exports: HashMap<String, Value>,
    // Indices are always integers
    indices: HashMap<String, Value>,
}

impl LoxModule {
    pub fn new(name: String, module_id: ModuleId) -> Self {
        Self {
            name,
            module_id,
            exports: HashMap::new(),
            indices: HashMap::new(),
        }
    }

    pub fn set_export_indices(&mut self, indices: HashMap<String, Value>) {
        self.indices = indices.clone();
        self.exports = indices;
    }

    pub fn set_name(&mut self, name: String) {
        self.name = name;
    }

    pub fn exports_mut(&mut self) -> &mut HashMap<String, Value> {
        self.exports = self.indices.clone();
        &mut self.exports
    }

    #[inline]
    pub fn exports(&self) -> &HashMap<String, Value> {
        &self.exports
    }

    #[inline]
    pub fn indices(&self) -> &HashMap<String, Value> {
        &self.indices
    }
}

impl DeepSizeOf for LoxModule {
    fn deep_size_of_children(&self) -> usize {
        let exports_mem = deep_size_of_map_with_owned_keys(&self.exports);
        exports_mem + self.name.capacity()
    }
}

impl NativeObject for LoxModule {
    fn lox_name(&self) -> CowStr {
        CowStr::Owned(self.name.clone())
    }

    fn lox_doc(&self) -> CowStr {
        if self.exports.is_empty() {
            CowStr::Owned(format!("module {}.lox exports {{}}", self.name))
        } else {
            let mut s = format!("module {}.lox exports {{\n", self.name);
            let mut keys = self.exports.keys().collect::<Vec<_>>();
            keys.sort();
            for export in keys {
                s.push_str(&format!("    {},\n", export));
            }
            s.push('}');
            CowStr::Owned(s)
        }
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(self
            .exports
            .values_mut()
            .filter_map(|value| value.as_ptr_mut()))
    }

    fn module(&self) -> ModuleId {
        self.module_id
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(Cow::Owned(format!("<module: {}>", &self.name)))
    }

    fn get_field(
        &mut self,
        vm: &mut Vm,
        _this: ObjectPointer,
        name: &str,
    ) -> Result<Value, LoxError> {
        self.exports
            .get(name)
            .cloned()
            .ok_or_else(|| vm.error(format!("Undefined property `{}`", name)))
    }

    fn get_item(&mut self, vm: &mut Vm, key: Value) -> Result<Value, LoxError> {
        key.as_string()
            .ok_or_else(|| {
                vm.error(format!(
                    "Export name must be a string, got {:?}",
                    vm.type_of(&key)
                ))
            })
            .and_then(|s| {
                self.exports
                    .get(s.as_ref())
                    .cloned()
                    .ok_or_else(|| vm.error(format!("Undefined export `{}`", key)))
            })
    }
}
