use crate::rt_hashmap::FastHashMap;

use super::{HeapValue, ObjectAddress, Value};
use crate::allocator::{deep_size_of::deep_size_of_map_with_owned_keys, DeepSizeOf};
use crate::{allocator::TaggedPointer, compiler::cco::CompiledCodeObject, module::ModuleId};

use vm::immutable_string::ImmutableString;
use vm::object::ObjectPointer;

#[derive(Debug, Clone)]
pub struct ClassObject {
    // TODO: allocate/intern the name using the lox allocator?
    /// The name of the class.
    pub(crate) name: String,
    /// A pointer to the superclass.
    pub(crate) superclass: Option<ObjectPointer>,
    /// A pointer to the metaclass.
    pub(crate) metaclass: Option<ObjectPointer>,
    /// The methods defined on the class.
    pub(crate) methods: FastHashMap<String, ObjectPointer>,
    /// The method in which this class was defined
    #[allow(unused)]
    pub(crate) module: ModuleId,
}

impl DeepSizeOf for ClassObject {
    #[inline]
    fn deep_size_of_children(&self) -> usize {
        self.name.deep_size_of_children() + deep_size_of_map_with_owned_keys(&self.methods)
    }
}

impl ClassObject {
    pub fn empty_with_name<S: Into<String>>(name: S, module: ModuleId) -> Self {
        Self {
            name: name.into(),
            superclass: None,
            metaclass: None,
            methods: FastHashMap::default(),
            module,
        }
    }

    pub fn add_method(&mut self, method: ObjectPointer) {
        self.methods
            .insert(method.as_ref().as_cco().name.clone().into_owned(), method);
    }

    #[inline]
    pub fn get_method(&self, name: &str) -> Option<ObjectPointer> {
        self.methods.get(name).copied()
    }

    #[inline]
    pub fn inherit(&mut self, superclass: ObjectPointer) {
        self.superclass = Some(superclass);
        self.methods
            .extend(superclass.as_ref().as_class().methods.clone());
    }

    pub fn contained_pointers(&mut self) -> impl Iterator<Item = &'_ mut ObjectPointer> + '_ {
        self.superclass
            .as_mut()
            .into_iter()
            .chain(self.metaclass.as_mut().into_iter())
            .chain(self.methods.values_mut())
    }
}

#[derive(Debug, Clone)]
pub struct Object {
    /// A pointer to the class of this object.
    pub(crate) class: ObjectPointer,
    /// Maybe use `u64` hashes directly to save space?
    pub(crate) fields: FastHashMap<String, Value>,
    /// The vtable with the bound methods.
    pub(crate) bound_methods: FastHashMap<String, ObjectPointer>,
}
impl DeepSizeOf for Object {
    fn deep_size_of_children(&self) -> usize {
        deep_size_of_map_with_owned_keys(&self.fields)
            + deep_size_of_map_with_owned_keys(&self.bound_methods)
    }
}

#[derive(Debug, Clone)]
pub enum BindingStatus {
    Bound(ObjectPointer),
    Unbound(ObjectPointer),
    NotFound,
}

impl Object {
    pub fn with_class(class: ObjectPointer) -> Self {
        Self {
            class,
            fields: FastHashMap::default(),
            bound_methods: FastHashMap::default(),
        }
    }

    pub fn into_object(self) -> LoxObject {
        LoxObject::new(HeapValue::Obj(Box::new(self)))
    }

    #[inline]
    pub fn class(&self) -> &ClassObject {
        self.class.as_ref().as_class()
    }

    #[inline]
    pub fn class_mut(&mut self) -> &mut ClassObject {
        self.class.as_mut().as_class_mut()
    }

    #[inline]
    pub fn class_name(&self) -> &str {
        &self.class.as_ref().as_class().name[..]
    }

    #[inline]
    pub fn set_field(&mut self, name: &str, value: Value) {
        *self
            .fields
            .raw_entry_mut()
            .from_key(name)
            .or_insert_with(|| (name.to_owned(), Value::Nil))
            .1 = value;
    }

    #[inline]
    pub fn get_field(&self, name: &str) -> Option<&Value> {
        self.fields.get(name)
    }

    pub fn is_instance_of(&self, class: &ClassObject) -> bool {
        let mut superclass = self.class();
        loop {
            // XXX: This might break if the allocator starts to move objects
            if superclass.lox_address() == class.lox_address() {
                break true;
            } else if let Some(cls) = &superclass.superclass {
                superclass = cls.as_ref().as_class();
            } else {
                break false;
            }
        }
    }

    #[inline]
    pub fn get_method(&self, name: &str) -> BindingStatus {
        self.bound_methods
            .get(name)
            .copied()
            .map(BindingStatus::Bound)
            .or_else(|| {
                self.class
                    .as_ref()
                    .as_class()
                    .get_method(name)
                    .map(BindingStatus::Unbound)
            })
            .unwrap_or(BindingStatus::NotFound)
    }

    #[inline]
    pub fn get_superclass_method(&self, name: &str, superclass: ObjectPointer) -> BindingStatus {
        // TODO: figure out how to get rid of this allocation
        let cls = superclass.as_ref().as_class();
        self.bound_methods
            .get(&format!("{}.{}", cls.name, name))
            .copied()
            .map(BindingStatus::Bound)
            .or_else(|| {
                let ptr = cls.get_method(name).unwrap();
                Some(BindingStatus::Unbound(ptr))
            })
            .unwrap_or(BindingStatus::NotFound)
    }

    #[inline]
    pub fn bind_method<S: Into<String>>(&mut self, name: S, method: ObjectPointer) {
        self.bound_methods.insert(name.into(), method);
    }

    #[inline]
    pub fn fields_alloc_size(&self) -> usize {
        self.deep_size_of()
    }

    pub fn contained_pointers(&mut self) -> impl Iterator<Item = &'_ mut ObjectPointer> + '_ {
        self.fields
            .values_mut()
            .filter_map(|v| v.as_ptr_mut())
            .chain(self.bound_methods.values_mut())
            .chain(std::iter::once(&mut self.class))
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Reachability {
    Undetermined,
    Unreachable,
    Reachable,
}

#[derive(Debug)]
pub struct LoxObject {
    pub(crate) value: HeapValue,
    pub(crate) reachable: Reachability,
}

impl LoxObject {
    pub fn new<V: Into<HeapValue>>(value: V) -> Self {
        Self {
            value: value.into(),
            reachable: Reachability::Undetermined,
        }
    }

    #[inline]
    pub fn is_reachable(&self) -> bool {
        match self.reachable {
            Reachability::Undetermined => false,
            Reachability::Unreachable => false,
            Reachability::Reachable => true,
        }
    }

    #[inline(always)]
    pub fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        self.value.contained_pointers()
    }

    #[inline]
    pub fn set_value(&mut self, value: HeapValue) {
        self.value = value;
    }

    #[inline(always)]
    pub fn as_heap_value(&self) -> &HeapValue {
        &self.value
    }

    #[inline(always)]
    pub fn as_heap_value_mut(&mut self) -> &mut HeapValue {
        &mut self.value
    }

    #[inline]
    pub fn is_upvalue(&self) -> bool {
        matches!(&self.value, HeapValue::UpValue(_))
    }

    pub fn as_immutable_string(&self) -> &ImmutableString {
        match self.as_heap_value() {
            HeapValue::Str(obj) => obj,
            rest => panic!("Couldn't unwrap {:?} into an ImmutableString", rest),
        }
    }

    pub fn as_immutable_string_mut(&mut self) -> &mut ImmutableString {
        match self.as_heap_value_mut() {
            HeapValue::Str(obj) => obj,
            rest => panic!("Couldn't unwrap {:?} into an ImmutableString", rest),
        }
    }

    pub fn as_object(&self) -> &Object {
        match self.as_heap_value() {
            HeapValue::Obj(obj) => obj,
            rest => panic!("Couldn't unwrap {:?} into an Object", rest),
        }
    }

    pub fn as_object_mut(&mut self) -> &mut Object {
        match self.as_heap_value_mut() {
            HeapValue::Obj(obj) => obj,
            rest => panic!("Couldn't unwrap {:?} into an Object", rest),
        }
    }

    pub fn as_class(&self) -> &ClassObject {
        match self.as_heap_value() {
            HeapValue::Cls(cls) => cls,
            HeapValue::UpValue(_) => self
                .as_heap_value()
                .follow_ptr()
                .as_class()
                .expect("Couldn't unwrap this upvalue into a class object"),
            rest => panic!("Couldn't unwrap {:?} into a ClassObject", rest),
        }
    }

    pub fn as_class_mut(&mut self) -> &mut ClassObject {
        match self.as_heap_value_mut() {
            HeapValue::Cls(cls) => cls,
            rest => panic!("Couldn't unwrap {:?} into a ClassObject", rest),
        }
    }

    pub fn is_cco(&self) -> bool {
        matches!(
            self.as_heap_value(),
            HeapValue::CCO(_) | HeapValue::Closure { .. } | HeapValue::BoundMethod { .. }
        )
    }

    pub fn as_cco(&self) -> &CompiledCodeObject {
        match self.as_heap_value() {
            HeapValue::CCO(cco) => cco,
            HeapValue::Closure { cco, .. } => cco.as_ref().as_cco(),
            HeapValue::BoundMethod { cco, .. } => cco.as_ref().as_cco(),
            rest => panic!("Couldn't unwrap {:?} into a CompiledCodeObject", rest),
        }
    }

    pub fn try_get_upvalues(&self) -> Option<&Vec<ObjectPointer>> {
        match self.as_heap_value() {
            HeapValue::Closure { upvalues, .. } => Some(upvalues),
            HeapValue::BoundMethod { cco, .. } => cco.as_ref().try_get_upvalues(),
            _ => None,
        }
    }

    pub fn as_cco_mut(&mut self) -> &mut CompiledCodeObject {
        match self.as_heap_value_mut() {
            HeapValue::CCO(cco) => cco,
            HeapValue::Closure { cco, .. } => cco.as_mut().as_cco_mut(),
            HeapValue::BoundMethod { cco, .. } => cco.as_mut().as_cco_mut(),
            rest => panic!("Couldn't unwrap {:?} into a CompiledCodeObject", rest),
        }
    }

    pub fn as_heap_value_pointer(&mut self) -> TaggedPointer<HeapValue> {
        unsafe { TaggedPointer::from_ref(&mut self.value) }
    }
}

impl DeepSizeOf for LoxObject {
    #[inline(always)]
    fn deep_size_of_children(&self) -> usize {
        self.value.deep_size_of_children()
    }
}

#[cfg(feature = "gc-debug")]
impl std::ops::Drop for LoxObject {
    fn drop(&mut self) {
        if let Reachability::Undetermined = self.reachable {
            // Do not print anything since unreachable objects are dropped
            // at the end of the program together with the allocator.
        } else {
            println!(
                "[Drop   ] Deallocating `{:?}` (size = {}b)",
                self,
                crate::allocator::separated_usize(self.deep_size_of())
            );
        }
    }
}

impl std::fmt::Display for LoxObject {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.as_heap_value())
    }
}
