pub mod heap_value;
pub mod immutable_string;
pub mod module;
pub mod native;
pub mod object;
pub mod value;

use crate::allocator::TaggedPointer;

pub use self::{
    heap_value::HeapValue,
    immutable_string::ImmutableString,
    object::{LoxObject, Object, Reachability},
    value::Value,
};

pub type ObjectPointer = TaggedPointer<LoxObject>;
known_deep_size_fast!(0, ObjectPointer);

pub trait ObjectAddress {
    fn lox_address(&self) -> usize;
}

impl<T> ObjectAddress for T {
    #[inline(always)]
    fn lox_address(&self) -> usize {
        self as *const _ as usize
    }
}
