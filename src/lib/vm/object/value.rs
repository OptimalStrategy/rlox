use vm::Object;

use super::{
    native::{CowStr, NativeObject},
    object::ClassObject,
    HeapValue, ObjectPointer,
};
use crate::{
    types::{LoxFloat, LoxInt},
    vm::{immutable_string::ImmutableString, vm},
};

#[derive(Debug, Clone, Copy)]
pub enum Value {
    /// An integer number.
    Int(LoxInt),
    /// A floating point number.
    Float(LoxFloat),
    /// A boolean value.
    Bool(bool),
    /// A nil.
    Nil,
    /// A pointer to a `LoxObject`.
    Ptr(ObjectPointer),
}
known_deep_size_fast!(0, Value);

impl Value {
    #[inline]
    pub fn is_nil(&self) -> bool {
        matches!(self, Value::Nil)
    }

    pub fn as_ptr(&self) -> Option<&ObjectPointer> {
        match self {
            Self::Ptr(ptr) => Some(ptr),
            _ => None,
        }
    }

    pub fn as_ptr_mut(&mut self) -> Option<&mut ObjectPointer> {
        match self {
            Self::Ptr(ptr) => Some(ptr),
            _ => None,
        }
    }

    #[inline]
    pub fn as_int(&self) -> Option<LoxInt> {
        match self {
            Self::Int(i) => Some(*i),
            Self::Float(f) if f.fract() == 0.0 => Some(*f as LoxInt),
            Self::Bool(b) => Some(*b as LoxInt),
            _ => None,
        }
    }

    #[allow(clippy::wrong_self_convention)]
    #[inline]
    pub fn into_float(&self) -> Option<LoxFloat> {
        match self {
            Value::Float(f) => Some(*f),
            _ => None,
        }
    }

    #[inline]
    pub fn as_float(&self) -> Option<LoxFloat> {
        match self {
            Self::Float(f) => Some(*f),
            Self::Int(i) => Some(*i as LoxFloat),
            _ => None,
        }
    }

    #[inline]
    pub fn as_string(&self) -> Option<&ImmutableString> {
        self.as_heap_value().and_then(|h| h.as_string())
    }

    #[inline]
    pub fn as_class(&self) -> Option<&ClassObject> {
        self.as_heap_value().and_then(|h| h.as_class())
    }

    #[inline]
    pub fn as_class_mut(&mut self) -> Option<&mut ClassObject> {
        self.as_heap_value_mut().and_then(HeapValue::as_class_mut)
    }

    #[inline]
    pub fn as_object(&self) -> Option<&Object> {
        self.as_heap_value().and_then(HeapValue::as_obj)
    }

    #[inline]
    pub fn as_object_mut(&mut self) -> Option<&mut Object> {
        self.as_heap_value_mut().and_then(HeapValue::as_obj_mut)
    }

    #[inline]
    pub fn as_native_object(&self) -> Option<&dyn NativeObject> {
        self.as_heap_value().and_then(HeapValue::as_obj_native)
    }

    #[inline]
    pub fn as_native_object_mut(&mut self) -> Option<&mut dyn NativeObject> {
        self.as_heap_value_mut()
            .and_then(HeapValue::as_obj_native_mut)
    }

    #[inline]
    pub fn as_native_object_mut_with_ptr(
        &mut self,
    ) -> Option<(ObjectPointer, &mut dyn NativeObject)> {
        self.as_ptr().copied().and_then(move |ptr| {
            self.as_heap_value_mut()
                .and_then(HeapValue::as_obj_native_mut)
                .map(|obj| (ptr, obj))
        })
    }

    pub fn as_heap_value(&self) -> Option<&HeapValue> {
        match self {
            Value::Ptr(ptr) => Some(&ptr.as_ref().value),
            _ => None,
        }
    }

    pub fn as_heap_value_mut(&mut self) -> Option<&mut HeapValue> {
        match self {
            Value::Ptr(ptr) => Some(&mut ptr.as_mut().value),
            _ => None,
        }
    }

    pub fn move_to_stack(self) -> Value {
        match self {
            Value::Ptr(ref ptr) => match ptr.as_ref().as_heap_value().follow_ptr() {
                HeapValue::UpValue(v) => *v,
                _ => self,
            },
            _ => self,
        }
    }

    // TODO: get rid of the allocations
    pub fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        match self {
            Value::Ptr(ptr) => ptr.as_mut().contained_pointers(),
            Value::Int(_) => Box::new(std::iter::empty::<&'_ mut ObjectPointer>()),
            Value::Float(_) => Box::new(std::iter::empty::<&'_ mut ObjectPointer>()),
            Value::Bool(_) => Box::new(std::iter::empty::<&'_ mut ObjectPointer>()),
            Value::Nil => Box::new(std::iter::empty::<&'_ mut ObjectPointer>()),
        }
    }

    pub fn eq_cached(&mut self, other: &mut Self) -> bool {
        match (self, other) {
            (Value::Int(l), Value::Int(r)) => l == r,
            (Value::Float(l), Value::Float(r)) => l == r,
            (Value::Int(l), Value::Float(r)) => (*l as crate::types::LoxFloat) == *r,
            (Value::Float(l), Value::Int(r)) => *l == (*r as crate::types::LoxFloat),
            (Value::Ptr(l), Value::Ptr(r)) => {
                if l.as_ptr() == r.as_ptr() {
                    true
                } else {
                    l.as_mut()
                        .as_heap_value_mut()
                        .eq_cached(r.as_mut().as_heap_value_mut())
                }
            }
            (Value::Ptr(l), ref mut r) => match l.as_mut().as_heap_value_mut() {
                super::HeapValue::UpValue(up) => up.eq_cached(r),
                _ => false,
            },
            (ref mut l, Value::Ptr(r)) => match r.as_mut().as_heap_value_mut() {
                super::HeapValue::UpValue(up) => l.eq_cached(up),
                _ => false,
            },
            (Value::Nil, Value::Nil) => true,
            (Value::Bool(l), Value::Bool(r)) => *l == *r,
            _ => false,
        }
    }

    pub fn stringify(&self, vm: &mut vm::Vm) -> Result<CowStr, crate::LoxError> {
        Ok(match self {
            Value::Ptr(ptr) => vm.stringify_ptr(*ptr)?,
            _ => format!("{}", self).into(),
        })
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Value::Int(l), Value::Int(r)) => l == r,
            (Value::Float(l), Value::Float(r)) => l == r,
            (Value::Int(l), Value::Float(r)) => (*l as crate::types::LoxFloat) == *r,
            (Value::Float(l), Value::Int(r)) => *l == (*r as crate::types::LoxFloat),
            (Value::Ptr(l), Value::Ptr(r)) => {
                if l.as_ptr() == r.as_ptr() {
                    true
                } else {
                    l.as_ref().as_heap_value().eq(r.as_ref().as_heap_value())
                }
            }
            (Value::Ptr(l), r) => match l.as_ref().as_heap_value() {
                super::HeapValue::UpValue(up) => up.eq(r),
                _ => false,
            },
            (ref l, Value::Ptr(r)) => match r.as_ref().as_heap_value() {
                super::HeapValue::UpValue(up) => l.eq(&up),
                _ => false,
            },
            (Value::Nil, Value::Nil) => true,
            (Value::Bool(l), Value::Bool(r)) => *l == *r,
            _ => false,
        }
    }
}

impl Eq for Value {}

impl std::fmt::Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Value::Int(i) => format!("{}", i),
                Value::Float(f) => format!("{}", f),
                Value::Bool(b) => format!("{}", b),
                Value::Nil => String::from("nil"),
                Value::Ptr(ptr) =>
                    if cfg!(feature = "debug-info") {
                        if vm::pset_check_pointer(ptr) {
                            "...".into()
                        } else {
                            vm::pset_add_pointer(*ptr);
                            let res = ptr.as_ref().to_string();
                            vm::pset_remove_pointer(ptr);
                            format!("<@{:p}: {}>", ptr.as_ptr(), res)
                        }
                    } else {
                        format!("<lox object at {:p}>", ptr.as_ptr())
                    },
            }
        )
    }
}

impl std::hash::Hash for Value {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        // Hash the discriminant first
        std::mem::discriminant(self).hash(state);
        match self {
            Value::Int(i) => i.hash(state),
            Value::Float(f) => f.to_be_bytes().hash(state),
            Value::Bool(b) => b.hash(state),
            Value::Nil => {}
            Value::Ptr(ptr) => ptr.as_ptr().hash(state),
        }
    }
}

impl_value_from!(LoxInt, Int);
impl_value_from!(LoxFloat, Float);
impl_value_from!(bool, Bool);
impl_value_from!(ObjectPointer, Ptr);
