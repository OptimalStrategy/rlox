use std::borrow::Cow;

use crate::allocator::DeepSizeOf;

use crate::vm::Vm;

use compiler::cco::Arity;
use errors::LoxError;
use module::ModuleId;
use vm::object::value::Value;
use vm::object::ObjectPointer;

pub type CowStr = Cow<'static, str>;

pub trait NativeObject: DeepSizeOf + std::fmt::Debug {
    /// Should return the name/type of the object.
    fn lox_name(&self) -> CowStr;

    /// Must return all pointers reachable from the object. Failing to return the pointers
    /// may result in the GC freeing the objects they are pointing to.
    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_>;

    /// Returns the object's location in memory by default.
    fn runtime_id(&self) -> usize {
        self as *const _ as *const () as usize
    }

    fn lox_doc(&self) -> CowStr {
        Cow::Owned(format!("Missing a doc entry for `{}`", self.lox_name()))
    }

    fn is_callable(&self) -> bool {
        self.arity().is_some()
    }

    fn module(&self) -> ModuleId {
        ModuleId::internal()
    }

    fn to_string(&self, _vm: &mut Vm) -> Result<CowStr, LoxError> {
        Ok(Cow::Owned(format!("<{} instance>", self.lox_name())))
    }

    fn arity(&self) -> Option<Arity> {
        None
    }

    fn call(
        &mut self,
        vm: &mut Vm,
        _this: ObjectPointer,
        _args: Vec<Value>,
    ) -> Result<Value, LoxError> {
        Err(vm.error(format!(
            "Cannot call an object of type `{}`",
            self.lox_name()
        )))
    }

    fn get_field(
        &mut self,
        vm: &mut Vm,
        _this: ObjectPointer,
        name: &str,
    ) -> Result<Value, LoxError> {
        Err(vm.error(format!("Undefined property `{}`", name)))
    }

    fn set_field(&mut self, vm: &mut Vm, _key: &str, _item: Value) -> Result<Value, LoxError> {
        Err(vm.error(format!("`{}` instances are read-only", self.lox_name())))
    }

    fn get_item(&mut self, vm: &mut Vm, _key: Value) -> Result<Value, LoxError> {
        Err(vm.error(format!(
            "`{}` does not support item access",
            self.lox_name(),
        )))
    }

    fn set_item(&mut self, vm: &mut Vm, _key: Value, _item: Value) -> Result<Value, LoxError> {
        Err(vm.error(format!(
            "`{}` does not support item assignment",
            self.lox_name(),
        )))
    }

    #[inline]
    fn is_instance_of(&self, _class: &dyn NativeObject) -> bool {
        false
    }

    #[inline]
    fn is_type_of(&self, _instance: &Value) -> bool {
        false
    }
}

impl dyn NativeObject {
    /// # Safety
    /// The caller must ensure that the [`NativeObject`] at the pointer address is of type T.
    pub unsafe fn downcast_mut<T: NativeObject>(ptr: &mut ObjectPointer) -> &mut T {
        let obj = ptr
            .as_mut()
            .as_heap_value_mut()
            .as_obj_native_mut()
            .unwrap() as *mut dyn NativeObject as *mut T;
        &mut *obj
    }

    /// # Safety
    /// The caller must ensure that the [`NativeObject`] at the pointer address is of type T.
    pub unsafe fn downcast<T: NativeObject>(ptr: &ObjectPointer) -> &T {
        let obj = ptr.as_ref().as_heap_value().as_obj_native().unwrap() as *const dyn NativeObject
            as *const T;
        &*obj
    }

    /// # Safety
    /// The caller must ensure that the [`NativeObject`] reference is of type T.
    pub unsafe fn downcast_ref_mut<T: NativeObject>(obj: &mut dyn NativeObject) -> &mut T {
        let obj = obj as *mut dyn NativeObject as *mut T;
        &mut *obj
    }
}
