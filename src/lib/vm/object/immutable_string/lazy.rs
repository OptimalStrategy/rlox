use std::ops::Range;

use crate::allocator::DeepSizeOf;

#[derive(Clone, PartialEq, Eq)]
pub enum LazyHash {
    Lazy,
    Ready(u64),
}
known_deep_size_fast!(0, LazyHash);

#[derive(Debug, Clone)]
pub enum LazyGraphemeIndices {
    Lazy,
    Ready(Vec<Range<usize>>),
}

impl LazyGraphemeIndices {
    #[inline]
    pub fn get(&self, idx: usize) -> Option<&Range<usize>> {
        match self {
            Self::Lazy => None,
            Self::Ready(v) => v.get(idx),
        }
    }
}

impl DeepSizeOf for LazyGraphemeIndices {
    fn deep_size_of_children(&self) -> usize {
        match self {
            Self::Lazy => 0,
            Self::Ready(ref idx) => idx.capacity() * std::mem::size_of::<Range<usize>>(),
        }
    }
}

impl std::fmt::Debug for LazyHash {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Lazy => "Lazy".to_owned(),
                Self::Ready(hash) => format!("Ready({})", hash),
            }
        )
    }
}
