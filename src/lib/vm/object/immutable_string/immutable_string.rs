use std::ops::Range;
use vm::object::immutable_string::hash;
use vm::object::immutable_string::lazy::LazyHash;

use crate::{
    vm::{immutable_string::vtable::StringVTable, Value, Vm},
    LoxError, ObjectPointer,
};

use super::vtable::LoxStringMethods;
use crate::allocator::DeepSizeOf;

/// Strings longer than this value will be truncated before printing
pub static STRING_TRUNC_THRESHOLD: usize = 50;

#[derive(Clone)]
pub struct ImmutableString {
    s: String,
    hash: LazyHash,
    pub(super) vtable: Box<StringVTable>,
}
impl DeepSizeOf for ImmutableString {
    fn deep_size_of_children(&self) -> usize {
        // Call deep_size_of() to get an accurate vtable size as it's behind a pointer.
        self.s.capacity() + self.vtable.deep_size_of()
    }
}

#[allow(clippy::len_without_is_empty)]
impl ImmutableString {
    pub fn new<S: Into<String>>(s: S) -> Self {
        let s = s.into();
        ImmutableString {
            s,
            hash: LazyHash::Lazy,
            vtable: Box::new(StringVTable::new()),
        }
    }

    pub fn new_with_hash<S: Into<String>>(s: S) -> Self {
        let mut this = Self::new(s);
        this.compute_hash();
        this
    }

    pub fn and_indices(mut self) -> Self {
        self.compute_indices();
        self
    }

    #[inline]
    pub fn into_string(self) -> String {
        self.s
    }

    #[inline]
    pub fn len(&mut self) -> usize {
        self.vtable.len(self.unsafe_ref())
    }

    /// Attempts to return the grapheme length of the string, if it's available.
    /// Otherwise, returns the byte length of the string as an approximation.
    /// Shouldn't not be used in most string operations as the language semantics define
    /// each string character as a grapheme and not a byte.
    #[inline]
    pub fn len_fast(&mut self) -> usize {
        self.vtable.try_len().unwrap_or(self.s.len())
    }

    #[inline]
    pub fn get_field(
        &self,
        vm: &mut Vm,
        this: ObjectPointer,
        name: &str,
    ) -> Result<Value, LoxError> {
        LoxStringMethods::get_field(vm, this, name)
    }

    #[inline]
    pub fn hash(&mut self) -> u64 {
        match self.hash {
            LazyHash::Lazy => self.compute_hash(),
            LazyHash::Ready(hash) => hash,
        }
    }

    #[inline]
    pub fn index(&mut self, at: usize) -> Option<&str> {
        self.vtable.index(self.unsafe_ref(), at)
    }

    /// Computes and returns the hash of the string without cashing the hash.
    #[inline(always)]
    pub fn hash_expensive(&self) -> u64 {
        match self.hash {
            LazyHash::Lazy => hash(&self.s),
            LazyHash::Ready(h) => h,
        }
    }

    #[inline]
    pub fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(self.vtable.contained_pointers())
    }

    fn compute_hash(&mut self) -> u64 {
        let hash = self.hash_expensive();
        self.hash = LazyHash::Ready(hash);
        hash
    }

    #[inline]
    fn compute_indices(&mut self) -> &Vec<Range<usize>> {
        self.vtable.compute_indices(self.unsafe_ref())
    }

    /// Converts the lifetime of the stored string to whatever lifetime is required
    /// to bypass the vtable mutability limivations.
    fn unsafe_ref<'required>(&'_ self) -> &'required str {
        let this: &'_ str = self.as_ref();
        unsafe { std::mem::transmute(this) }
    }
}

impl std::convert::AsRef<str> for ImmutableString {
    #[inline(always)]
    fn as_ref(&self) -> &str {
        &self.s[..]
    }
}

impl std::fmt::Debug for ImmutableString {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let value = if self.as_ref().len() > STRING_TRUNC_THRESHOLD {
            format!(
                "{:?} <...trunc>",
                &self.as_ref()[0..=STRING_TRUNC_THRESHOLD]
            )
        } else {
            format!("{:?}", self.as_ref())
        };
        write!(
            f,
            "{}",
            if f.alternate() {
                format!(
                    "ImmutableString {{\n    s: {},\n    hash = {:#?}\n}}",
                    value, self.hash
                )
            } else {
                format!("ImmutableString {{ s: {}, hash = {:?} }}", value, self.hash)
            }
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use unicode_segmentation::UnicodeSegmentation;

    #[test]
    fn test_string_indexing() {
        let strings = vec!["", "a string", "8912ywhi21sjakmsaksd,2"];
        for test in strings {
            let mut s = ImmutableString::new(test.to_owned()).and_indices();
            assert_eq!(s.as_ref(), test);
            assert_eq!(s.len(), test.len());

            let graphemes = s
                .as_ref()
                .graphemes(true)
                .map(|s| s.to_owned())
                .collect::<Vec<_>>();

            for (i, g) in graphemes.iter().enumerate() {
                assert_eq!(s.index(i), Some(&g[..]));
            }

            assert_eq!(s.index(0xffff), None);
        }
    }
}
