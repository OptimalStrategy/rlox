pub mod immutable_string;
pub mod lazy;
pub mod vtable;

use std::collections::hash_map::DefaultHasher;
use std::hash::Hasher;

pub use self::immutable_string::ImmutableString;

#[inline]
pub fn hash<T>(obj: T) -> u64
where
    T: std::hash::Hash,
{
    let mut hasher = DefaultHasher::new();
    obj.hash(&mut hasher);
    hasher.finish()
}
