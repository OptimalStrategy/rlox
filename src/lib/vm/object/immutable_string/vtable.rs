use crate::rt_hashmap::FastHashMap;

use std::ops::Range;
use vm::object::immutable_string::lazy::LazyGraphemeIndices;

use crate::{
    allocator::{deep_size_of::deep_size_of_map, DeepSizeOf},
    vm::builtin::result::LoxResult,
};
use unicode_segmentation::UnicodeSegmentation;

use vm::immutable_string::ImmutableString;
use vm::object::heap_value::HeapValue;
use vm::object::object::LoxObject;
use vm::object::ObjectPointer;

use crate::{compiler::cco::Arity, types::LoxInt, vm::native::CowStr};

use errors::LoxError;
use vm::builtin::fn_wrapper::*;
use vm::{
    object::{native::NativeObject, value::Value},
    vm::Vm,
};

#[derive(Debug)]
pub struct StringVTable {
    methods: FastHashMap<&'static str, ObjectPointer>,
    index: LazyGraphemeIndices,
}
impl DeepSizeOf for StringVTable {
    fn deep_size_of_children(&self) -> usize {
        self.index.deep_size_of_children()
            + deep_size_of_map::<&'static str, ObjectPointer>(self.methods.capacity())
    }
}

impl Clone for StringVTable {
    fn clone(&self) -> Self {
        // We can't clone the methods because they're bound to the original string
        Self {
            index: self.index.clone(),
            methods: FastHashMap::default(),
        }
    }
}

impl StringVTable {
    pub fn new() -> Self {
        Self {
            methods: FastHashMap::default(),
            index: LazyGraphemeIndices::Lazy,
        }
    }

    #[allow(clippy::len_without_is_empty)]
    pub fn len(&mut self, s: &str) -> usize {
        match &self.index {
            LazyGraphemeIndices::Lazy => self.compute_indices(s).len(),
            LazyGraphemeIndices::Ready(v) => v.len(),
        }
    }

    #[inline]
    pub fn try_len(&mut self) -> Option<usize> {
        match &self.index {
            LazyGraphemeIndices::Lazy => None,
            LazyGraphemeIndices::Ready(v) => Some(v.len()),
        }
    }

    pub fn index<'a>(&mut self, s: &'a str, at: usize) -> Option<&'a str> {
        match &self.index {
            LazyGraphemeIndices::Lazy => self.compute_indices(s).get(at),
            LazyGraphemeIndices::Ready(indices) => indices.get(at),
        }
        .cloned()
        .map(move |idx| std::str::from_utf8(&s.as_bytes()[idx]).unwrap())
    }

    pub fn compute_indices(&mut self, s: &str) -> &Vec<Range<usize>> {
        let mut indices = UnicodeSegmentation::grapheme_indices(s, true)
            .map(|(r, _)| r)
            .collect::<Vec<_>>();
        indices.push(s.as_bytes().len());

        let indices: Vec<Range<usize>> = indices
            .windows(2)
            .map(|slice| (slice[0]..slice[1]))
            .collect();

        self.index = LazyGraphemeIndices::Ready(indices);
        match &self.index {
            LazyGraphemeIndices::Ready(v) => v,
            LazyGraphemeIndices::Lazy =>
            // SAFETY: we have just assigned the Ready variant to `index` thus this is safe.
            unsafe { std::hint::unreachable_unchecked() }
        }
    }

    #[inline]
    pub fn contained_pointers(&mut self) -> impl Iterator<Item = &'_ mut ObjectPointer> {
        self.methods.values_mut()
    }
}

pub(super) struct LoxStringMethods;

impl LoxStringMethods {
    fn get(vm: &mut Vm, mut this: ObjectPointer, args: Vec<Value>) -> Result<Value, LoxError> {
        let s = this.as_mut().as_immutable_string_mut();
        let idx = if let Some(i) = args[0].as_int() {
            i
        } else {
            return Err(vm.error(format!(
                "`str` index must be an integer, got `{}`",
                vm.type_of(&args[0])
            )));
        };
        match s.index(idx as usize) {
            Some(result) => {
                let grapheme = vm.alloc_or_get_interned(result.to_owned());
                Ok(Value::Ptr(grapheme))
            }
            None => Err(vm.error(format!(
                "String index is out of bounds: `{}` >= `{}`",
                idx,
                s.len()
            ))),
        }
    }

    #[inline]
    fn len(_: &mut Vm, mut this: ObjectPointer) -> Value {
        Value::Int(this.as_mut().as_immutable_string_mut().len() as LoxInt)
    }

    pub(super) fn get_field(
        vm: &mut Vm,
        this: ObjectPointer,
        name: &str,
    ) -> Result<Value, LoxError> {
        let mut local_this = this;
        let s = local_this.as_mut().as_immutable_string_mut();
        match name {
            "length" => Ok(Value::Int(s.len() as LoxInt)),
            "getitem" => Ok(Value::Ptr(*s.vtable.methods.entry("get").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "str.getitem",
                        Arity::Fixed(1),
                        "Attempts to get the symbol at the given index. Panics if the index is not an integer or the \
                              index is out of bounds.",
                        move |vm, _, args| -> Result<_, _> { LoxStringMethods::get(vm, this, args) },
                        &[this],
                    )
                },
            ))),
            "len" => Ok(Value::Ptr(*s.vtable.methods.entry("len").or_insert_with(
                || {
                    alloc_trait_fn_vm(
                        vm,
                        "str.len",
                        Arity::Fixed(0),
                        "Returns the number of UTF-8 graphemes in the string.",
                        move |vm, _, _| -> Result<_, _> { Ok(LoxStringMethods::len(vm, this)) },
                        &[this],
                    )
                },
            ))),
            "iterator" => {
                Ok(Value::Ptr(*s.vtable.methods.entry("iterator").or_insert_with(
                    || {
                        alloc_trait_fn_vm(
                            vm,
                            "str.iterator",
                            Arity::Fixed(0),
                            "Creates a new iterator over the strings's graphemes.",
                            move |vm, _, _| -> Result<_, _> {
                                let ptr = vm.alloc(LoxObject::new(HeapValue::ObjNative(Box::new(LoxStringIterator::new(this)))));
                                Ok(Value::Ptr(ptr))
                             },
                        &[this],
                        )
                    },
                )))
            }
            _ => Err(vm.error(format!("Undefined property `{}`", name))),
        }
    }
}

#[derive(Debug)]
pub struct LoxStringIterator {
    s: ObjectPointer,
    iter_next: Option<ObjectPointer>,
    current: usize,
}
known_deep_size_fast!(0, LoxStringIterator);

impl LoxStringIterator {
    fn new(s: ObjectPointer) -> LoxStringIterator {
        Self {
            s,
            iter_next: None,
            current: 0,
        }
    }

    #[inline]
    fn string(&mut self) -> &mut ImmutableString {
        self.s.as_mut().as_immutable_string_mut()
    }

    fn next(&mut self, vm: &mut Vm) -> Value {
        let value = {
            self.current += 1;
            let current = self.current - 1;
            let s = self.string();
            if current < s.len() {
                let grapheme = s.index(current).unwrap();
                let grapheme = vm.alloc_or_get_interned(grapheme.to_owned());
                Value::Ptr(LoxResult::alloc_new_ok(vm, Value::Ptr(grapheme)))
            } else {
                self.current -= 1;
                Value::Ptr(LoxResult::alloc_new_err(vm, Value::Nil))
            }
        };
        value
    }
}

impl NativeObject for LoxStringIterator {
    fn lox_name(&self) -> CowStr {
        CowStr::from("StrIterator")
    }

    fn contained_pointers(&mut self) -> Box<dyn Iterator<Item = &'_ mut ObjectPointer> + '_> {
        Box::new(std::iter::once(&mut self.s).chain(self.iter_next.as_mut().into_iter()))
    }

    fn get_field(
        &mut self,
        vm: &mut Vm,
        this: ObjectPointer,
        name: &str,
    ) -> Result<Value, LoxError> {
        match name {
            "iternext" => Ok(Value::Ptr(*self.iter_next.get_or_insert_with(|| {
                alloc_trait_fn_vm(
                    vm,
                    "StrIterator.iternext",
                    Arity::Fixed(0),
                    "Returns the next value in the iterator or nil.",
                    move |vm, _, _| -> Result<_, _> {
                        let mut ptr = this;
                        let this = unsafe {
                            <dyn NativeObject>::downcast_mut::<LoxStringIterator>(&mut ptr)
                        };
                        Ok(this.next(vm))
                    },
                    &[this],
                )
            }))),
            _ => Err(vm.error(format!("Undefined property `{}`", name))),
        }
    }
}
