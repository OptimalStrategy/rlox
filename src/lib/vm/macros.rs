use super::HeapValue;
use crate::errors::LoxError;
use crate::vm::object::Value;
use crate::vm::Vm;

#[macro_export]
macro_rules! impl_value_from {
    ($Ty:ty, $variant:ident) => {
        impl From<$Ty> for $crate::vm::object::Value {
            fn from(v: $Ty) -> Self {
                Self::$variant(v)
            }
        }
    };
}

#[macro_export]
macro_rules! bump_alloc_size {
    ($vm:ident, $obj:ident, $expr:expr) => {{
        #[allow(unused)]
        use $crate::allocator::DeepSizeOf;

        let size_before = $obj.deep_size_of() as isize;
        let result = $expr;
        let size_after = $obj.deep_size_of() as isize;
        if cfg!(feature = "gc-debug") {
            println!(
                "[Alloc -] Object size changed from {}b to {}b, delta = {}b",
                $crate::allocator::separated_isize(size_before),
                $crate::allocator::separated_isize(size_after),
                size_after
                    .checked_sub(size_before)
                    .map($crate::allocator::separated_isize)
                    .unwrap_or_else(|| String::from("{WRAPPING SUB ERROR}"))
            );
        }
        let size_delta = size_after
            .checked_sub(size_before)
            .expect("Object size delta has overflowed. This should never happen.");
        $vm.bump_alloc_size(size_delta);
        result
    }};
}

#[macro_export]
macro_rules! bump_obj_alloc_size {
    ($vm:ident, $obj:ident, $expr:expr) => {{
        let size_before = $obj.fields_alloc_size() as isize;
        let result = $expr;
        let size_after = $obj.fields_alloc_size() as isize;
        let size_delta = size_after - size_before;
        $vm.bump_alloc_size(size_delta);
        result
    }};
}

pub(crate) fn op_add(this: &mut Vm, left: &Value, right: &Value) -> Result<Value, LoxError> {
    // Fast path for on-stack floats
    if let Some(left) = left.into_float() {
        let res = right.as_float().map(|right| Value::Float(left + right));
        if let Some(res) = res {
            return Ok(res);
        }
    }

    let left = if let Some(HeapValue::UpValue(value)) = left.as_heap_value() {
        value
    } else {
        left
    };
    let right = if let Some(HeapValue::UpValue(value)) = right.as_heap_value() {
        value
    } else {
        right
    };
    match (left, right) {
        (Value::Int(l), Value::Int(r)) => return Ok(Value::from(l.wrapping_add(*r))),
        (Value::Float(l), Value::Float(r)) => return Ok(Value::from(l + r)),
        (Value::Int(l), Value::Float(r)) => {
            return Ok(Value::from((*l as crate::types::LoxFloat) + *r))
        }
        (Value::Float(l), Value::Int(r)) => {
            return Ok(Value::from(*l + (*r as crate::types::LoxFloat)))
        }
        (Value::Ptr(l), Value::Ptr(r)) => {
            match (l.as_ref().as_heap_value(), r.as_ref().as_heap_value()) {
                (HeapValue::UpValue(l), HeapValue::UpValue(r)) => return op_add(this, l, r),
                (HeapValue::Str(box l), HeapValue::Str(box r)) => {
                    let obj = l.as_ref().to_owned() + r.as_ref();
                    let ptr = this.alloc_or_get_interned(obj);
                    return Ok(Value::Ptr(ptr));
                }
                _ => (),
            }
        }
        _ => (),
    }

    Err(this.error(format!(
        "Cannot apply the binary operator `+` to operands of types `{}` and `{}`",
        this.type_of(left),
        this.type_of(right)
    )))
}

#[macro_export]
macro_rules! binary_op {
    ($self:ident, $left:expr, $right:expr, +) => {{
        $crate::vm::macros::op_add($self, &$left, &$right)
    }};

    ($self:ident, $left:expr, $right:expr, **) => {{
        use $crate::vm::object::Value;
        let left = $left;
        let right = $right;
        let left = match &left {
            Value::Ptr(ptr) => match ptr.as_ref().as_heap_value().follow_ptr() {
                HeapValue::UpValue(up) => &up,
                _ => &left,
            }
            _ => &left,
        };
        let right = match &right {
            Value::Ptr(ptr) => match ptr.as_ref().as_heap_value().follow_ptr() {
                HeapValue::UpValue(up) => &up,
                _ => &right,
            }
            _ => &right,
        };
        match (left, right) {
            (Value::Int(l), Value::Int(r)) => Ok(Value::from(l.saturating_pow(*r as u32))),
            (Value::Float(l), Value::Float(r)) => Ok(Value::from(l.powf(*r))),
            (Value::Int(l), Value::Float(r)) => Ok(Value::from((*l as $crate::types::LoxFloat).powf(*r))),
            (Value::Float(l), Value::Int(r)) => Ok(Value::from(l.powf(*r as $crate::types::LoxFloat))),
            (lhs, rhs) => Err($self.error(
                format!(
                    "Cannot apply the binary operator ** to operands of types `{}` and `{}`",
                    $self.type_of(lhs),
                    $self.type_of(rhs)
                )
            )),
        }
    }};

    ($self:ident, $left:expr, $right:expr, div => $op:tt) => {{
        use $crate::vm::object::Value;
        let left = $left;
        let right = $right;

        if let Some(res) = left.into_float().and_then(|left| {
            match right.as_float() {
                Some(right) if right != 0. => Some(Value::Float(left $op right)),
                _ => None,
            }
        }) {
            Ok(res)
        } else
        {

        let left = match &left {
            Value::Ptr(ptr) => match  ptr.as_ref() .as_heap_value().follow_ptr() {
                HeapValue::UpValue(up) => &up,
                _ => &left,
            }
            _ => &left,
        };
        let right = match &right {
            Value::Ptr(ptr) => match ptr.as_ref() .as_heap_value().follow_ptr() {
                HeapValue::UpValue(up) => &up,
                _ => &right,
            }
            _ => &right,
        };
        match (left, right) {
            (Value::Int(_), Value::Int(0))
            | (Value::Float(_), Value::Int(0)) => {
                Err($self.error(String::from("Division by zero")))
            }
            (Value::Float(_), Value::Float(x))
            | (Value::Int(_), Value::Float(x)) if *x == 0. => {
                Err($self.error(String::from("Division by zero")))
            }
            (Value::Int(l), Value::Int(r)) => Ok(Value::from(l $op r)),
            (Value::Float(l), Value::Float(r)) => Ok(Value::from(l $op r)),
            (Value::Int(l), Value::Float(r)) => Ok(Value::from((*l as $crate::types::LoxFloat) $op *r)),
            (Value::Float(l), Value::Int(r)) => Ok(Value::from(*l $op (*r as $crate::types::LoxFloat))),
            (lhs, rhs) => Err($self.error(
                format!(
                    "Cannot apply the binary operator `{}` to operands of types `{}` and `{}`",
                    stringify!($op),
                    $self.type_of(lhs),
                    $self.type_of(rhs)
                )
            )),
        }
        }
    }};

    ($self:ident, $left:expr, $right:expr, ord => $op:tt) => {{
        use $crate::vm::object::Value;
        let left = $left;
        let right = $right;
        let left = match &left {
            Value::Ptr(ptr) => match  ptr.as_ref().as_heap_value().follow_ptr() {
                HeapValue::UpValue(up) => &up,
                _ => &left,
            }
            _ => &left,
        };
        let right = match &right {
            Value::Ptr(ptr) => match ptr.as_ref().as_heap_value().follow_ptr() {
                HeapValue::UpValue(up) => &up,
                _ => &right,
            }
            _ => &right,
        };
        match (left, right) {
            (Value::Int(l), Value::Int(r)) => Ok(Value::from(l $op r)),
            (Value::Float(l), Value::Float(r)) => Ok(Value::from(l $op r)),
            (Value::Int(l), Value::Float(r)) => Ok(Value::from((*l as $crate::types::LoxFloat) $op *r)),
            (Value::Float(l), Value::Int(r)) => Ok(Value::from(*l $op (*r as $crate::types::LoxFloat))),
            (lhs, rhs) => Err($self.error(
                format!(
                    "Cannot apply the binary operator `{}` to operands of types `{}` and `{}`",
                    stringify!($op),
                    $self.type_of(lhs),
                    $self.type_of(rhs)
                )
            )),
        }
    }};

    ($self:ident, $left:expr, $right:expr, $op:tt) => {{
        use $crate::vm::object::Value;
        let left = $left;
        let right = $right;

        // Fast path for floats
        if let Some(res) = left.into_float().and_then(|left| {
            match right.as_float() {
                Some(right) if right != 0. => Some(Value::Float(left $op right)),
                _ => None,
            }
        }) {
            Ok(res)
        } else
        {

        let left = match &left {
            Value::Ptr(ptr) => match  ptr.as_ref().as_heap_value().follow_ptr() {
                HeapValue::UpValue(up) => &up,
                _ => &left,
            }
            _ => &left,
        };
        let right = match &right {
            Value::Ptr(ptr) => match ptr.as_ref().as_heap_value().follow_ptr() {
                HeapValue::UpValue(up) => &up,
                _ => &right,
            }
            _ => &right,
        };
        match (left, right) {
            (Value::Int(l), Value::Int(r)) => Ok(Value::from((std::num::Wrapping(*l) $op std::num::Wrapping(*r)).0)),
            (Value::Float(l), Value::Float(r)) => Ok(Value::from(l $op r)),
            (Value::Int(l), Value::Float(r)) => Ok(Value::from((*l as $crate::types::LoxFloat) $op *r)),
            (Value::Float(l), Value::Int(r)) => Ok(Value::from(*l $op (*r as $crate::types::LoxFloat))),
            (lhs, rhs) => Err($self.error(
                format!(
                    "Cannot apply the binary operator `{}` to operands of types `{}` and `{}`",
                    stringify!($op),
                    $self.type_of(lhs),
                    $self.type_of(rhs)
                )
            )),
        }
        }
    }};
}
