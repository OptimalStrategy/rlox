use super::{native::CowStr, vm::MAX_PREALLOCATED_NATIVE_CCO_CAP, HeapValue};
use crate::{
    allocator::tagged_pointer::tag::Tag, compiler::cco::CompiledCodeObject, module::ModuleId,
};

use vm::object::{LoxObject, ObjectPointer};

#[allow(clippy::vec_box)]
#[derive(Debug)]
pub(crate) struct PreallocatedNativeCCOStack {
    vec: Vec<Box<LoxObject>>,
    len: usize,
}

impl PreallocatedNativeCCOStack {
    pub fn new(initial_capacity: usize) -> Self {
        let vec = Self::batch_allocate(initial_capacity);
        Self { vec, len: 0 }
    }

    pub fn get_cco(
        &mut self,
        name: CowStr,
        module: ModuleId,
    ) -> Result<ObjectPointer, Box<LoxObject>> {
        if self.len >= self.vec.len() {
            if self.len * 2 > MAX_PREALLOCATED_NATIVE_CCO_CAP {
                let mut cco = Self::new_cco();
                Self::fix_cco(&mut cco, name, module);
                return Err(Box::new(cco));
            } else {
                self.vec.extend(Self::batch_allocate(self.len));
            }
        }

        // SAFETY: we do the bounds check on len above
        self.len += 1;
        let cco: &mut Box<_> = unsafe { self.vec.get_unchecked_mut(self.len - 1) };
        Self::fix_cco(&mut *cco, name, module);
        let mut cco = unsafe { ObjectPointer::new(&mut **cco) };
        cco.set_tag(Tag::NativeCCO);
        Ok(cco)
    }

    #[inline]
    pub fn return_cco(&mut self) {
        self.len = self.len.saturating_sub(1);
    }

    #[inline]
    fn fix_cco(cco: &mut LoxObject, name: CowStr, module: ModuleId) {
        let cco = cco.as_cco_mut();
        cco.name = name;
        cco.module = module;
    }

    #[allow(clippy::vec_box)]
    #[inline]
    fn batch_allocate(cap: usize) -> Vec<Box<LoxObject>> {
        (0..cap).map(|_| Box::new(Self::new_cco())).collect()
    }

    fn new_cco() -> LoxObject {
        // Allocate the chunk on the stack completely zeroed out.
        // This is fine because only two of the chunk's properties are accessed during native calls.
        let mut chunk: crate::compiler::chunk::Chunk = unsafe {
            std::mem::transmute([0u8; std::mem::size_of::<crate::compiler::chunk::Chunk>()])
        };
        // Error reporting code checks the size of the cco's locations
        chunk.locations = crate::compiler::locations::Locations::_Empty;

        // However, this is not exactly the case for debug builds, because
        // CallFrame::new() checks that the bytecode is lowered to bytes.
        #[cfg(any(not(feature = "unchecked-vm"), feature = "trace-lox-calls"))]
        {
            chunk.bytecode = crate::compiler::bytecode::Bytecode::Decoded(Vec::new());
        }

        LoxObject::new(HeapValue::CCO(Box::new(CompiledCodeObject {
            name: "__default_cco".into(),
            chunk,
            arity: crate::compiler::cco::ARITY_NONE,
            n_captured: 0,
            module: ModuleId::anon(),
            version: crate::compiler::version::BytecodeVersion::default(),
        })))
    }
}
