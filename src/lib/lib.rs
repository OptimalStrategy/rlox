//! A rust implementation of the (modified) Lox programming language.
//!
//! This implementation of Lox is fairly different from the official C version.
#![feature(box_patterns)]
#![feature(
    hash_set_entry,
    hash_raw_entry,
    linked_list_cursors,
    thread_id_value,
    extend_one,
    const_likely,
    core_intrinsics
)]
#![feature(specialization)]
#![allow(incomplete_features)] // disables the specialization warning
#![allow(
    clippy::module_inception,
    clippy::new_without_default,
    clippy::unnecessary_wraps,
    clippy::manual_non_exhaustive,
    clippy::from_over_into,
    clippy::upper_case_acronyms,
    clippy::inconsistent_struct_constructor,
    clippy::branches_sharing_code,
    clippy::result_large_err
)]
extern crate crossbeam_channel;
extern crate crossterm;
extern crate fnv;
extern crate rand;
extern crate rlox_graph;
extern crate stacker;
extern crate str_concat;
extern crate unic_ucd;
extern crate unicode_segmentation;
extern crate unicode_xid;

#[macro_use]
mod macros;

pub mod allocator;
pub mod bench_utils;
pub mod compiler;
pub mod config;
pub mod context;
pub mod err_context;
pub mod errors;
pub mod frontend;
pub mod pipeline;
pub mod pipeline_worker;
pub mod repl;
pub mod rt_hashmap;
pub mod serializer;
pub mod test_runner;
pub mod types;
pub mod vm;

pub use compiler::{compiler::Compiler, dis::Disassemble};
pub use config::{CompilerConfig, OptConfig};
pub use constant_folding::ConstantFolder;
pub use context::SharedCtx;
pub use errors::LoxError;
pub use frontend::*;
pub use parser::Parser;
use pipeline::cache::BinaryCacheLoaderPass;
pub use pipeline::{IRPass, Pipeline};
pub use resolver::Resolver;
pub use scanner::Scanner;
pub use serializer::ByteCodeSerializer;
pub use source_info::SourceInfo;
pub use vm::object::ObjectPointer;

pub const MAX_PARAMETERS_COUNT: usize = 8;
pub type ParamsCountT = u8;
pub type Context = crate::context::Context<crate::allocator::Allocator>;

#[allow(unused)]
use ast::ast_to_str;
use err_context::ErrCtx;
use module::ModuleId;

pub fn make_pipeline(
    ctx: SharedCtx,
) -> impl IRPass<ErrCtx, Input = ModuleId, Output = ObjectPointer> + std::fmt::Debug {
    let (print_ast, print_code, folding, interning_threshold) = {
        let guard = ctx.read_shared();
        (
            guard.config.print_ast,
            guard.config.print_code,
            guard.config.opt_config.constant_folding,
            guard.config.interning_threshold,
        )
    };
    Pipeline::with_stage(
        Scanner::new(ctx.clone()),
        BinaryCacheLoaderPass::new(
            ctx.clone(),
            Pipeline::with_stage(Parser::new(ctx.clone()), Resolver::new(ctx.clone()))
                .add_pass(ConstantFolder::new(folding, interning_threshold))
                .maybe_print_ast(ctx.clone(), print_ast)
                .maybe_print_code(ctx.clone(), print_code)
                .add_pass(Compiler::new(ctx.clone()))
                .stage(),
        ),
    )
    .map_desc(
        move |cco| {
            pipeline_worker::cache_cco(ctx.clone(), cco);
            cco
        },
        "BinaryCacheWriter",
        "Caches the CCO",
    )
}

pub fn make_pipeline_without_compiler(
    ctx: &SharedCtx,
) -> impl IRPass<ErrCtx, Input = ModuleId, Output = crate::ast::AST<'_>> + std::fmt::Debug {
    let (print_ast, folding, interning_threshold) = {
        let guard = ctx.read_shared();
        (
            guard.config.print_ast,
            guard.config.opt_config.constant_folding,
            guard.config.interning_threshold,
        )
    };
    Pipeline::with_pass(Scanner::new(ctx.clone()))
        .add_pass(Parser::new(ctx.clone()))
        .add_pass(Resolver::new(ctx.clone()))
        .add_pass(ConstantFolder::new(folding, interning_threshold))
        .maybe_print_ast(ctx.clone(), print_ast)
}

const_assert!(std::mem::size_of::<self::ast::ExprKind>(), 96);
const_assert!(std::mem::size_of::<self::ast::Expr>(), 152);
const_assert!(std::mem::size_of::<self::vm::object::Value>() <= 24);
const_assert!(std::mem::size_of::<self::vm::heap_value::HeapValue>(), 40); // Was 176; TODO: reduce this size
