#![allow(clippy::type_complexity)]
use super::{
    combinators::{DebugPass, IdentityPass, MapPass},
    AstPrinter, CodePrinter, IRPass, R,
};
use crate::{
    ast::{ast_to_str::generate_message_box, AST},
    SharedCtx,
};
use std::{
    fmt::{self, Formatter},
    marker::PhantomData,
};

// XXX: Maybe we shouldn't have the error specified as a generic?
/// A type that holds the steps of a pipeline.
pub struct Pipeline<S, E> {
    stage: S,
    _err: PhantomData<E>,
}

/// A stage of a pipeline.
pub struct PipelineStage<P, N, E>
where
    P: IRPass<E>,
    N: IRPass<E, Input = P::Output>,
{
    pass: P,
    next: N,
    _err: PhantomData<E>,
}

impl<P, N, E> IRPass<E> for PipelineStage<P, N, E>
where
    P: IRPass<E>,
    N: IRPass<E, Input = P::Output>,
{
    type Input = P::Input;
    type Output = N::Output;

    fn apply(self, ir: Self::Input) -> R<Self::Output, E> {
        self.next.apply(self.pass.apply(ir)?)
    }

    fn description(&self) -> String {
        format!(
            "{}\n [pass] {}",
            self.pass.description(),
            self.next.description(),
        )
    }
}

impl<P, N, E> fmt::Debug for PipelineStage<P, N, E>
where
    P: IRPass<E>,
    N: IRPass<E, Input = P::Output>,
{
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl<T, E> Pipeline<T, E> {
    #[allow(clippy::new_ret_no_self)]
    pub fn new() -> Pipeline<IdentityPass<T>, E> {
        Pipeline {
            stage: IdentityPass::new(),
            _err: PhantomData,
        }
    }

    pub fn with_pass<P>(pass: P) -> Pipeline<PipelineStage<IdentityPass<T>, P, E>, E>
    where
        P: IRPass<E, Input = T>,
    {
        Pipeline::new().add_pass(pass)
    }

    pub fn with_stage<P1, P2>(pass1: P1, pass2: P2) -> Pipeline<PipelineStage<P1, P2, E>, E>
    where
        P1: IRPass<E, Input = T>,
        P2: IRPass<E, Input = P1::Output>,
    {
        Pipeline {
            stage: pass1,
            _err: PhantomData,
        }
        .add_pass(pass2)
    }

    pub fn stage(self) -> T {
        self.stage
    }
}

impl<T, E> Pipeline<T, E>
where
    T: IRPass<E>,
{
    pub fn add_pass<P>(self, next: P) -> Pipeline<PipelineStage<T, P, E>, E>
    where
        P: IRPass<E, Input = T::Output>,
    {
        Pipeline {
            stage: PipelineStage {
                pass: self.stage,
                next,
                _err: PhantomData,
            },
            _err: PhantomData,
        }
    }

    pub fn map<F, U>(self, f: F) -> Pipeline<PipelineStage<T, MapPass<F, T::Output, U>, E>, E>
    where
        F: Fn(T::Output) -> U,
    {
        self.add_pass(MapPass::new(f))
    }

    pub fn map_desc<F, U, S>(
        self,
        f: F,
        name: S,
        description: S,
    ) -> Pipeline<PipelineStage<T, MapPass<F, T::Output, U>, E>, E>
    where
        F: Fn(T::Output) -> U,
        S: Into<String>,
    {
        self.add_pass(MapPass::with_description(f, description).and_name(name))
    }

    pub fn debug(self) -> Pipeline<PipelineStage<T, DebugPass<T::Output>, E>, E>
    where
        T::Output: std::fmt::Debug,
    {
        self.add_pass(DebugPass::new(false))
    }

    pub fn debug_verbose(self) -> Pipeline<PipelineStage<T, DebugPass<T::Output>, E>, E>
    where
        T::Output: std::fmt::Debug,
    {
        self.add_pass(DebugPass::new(true))
    }

    #[inline(always)]
    pub fn print_ast<'a>(self, ctx: SharedCtx) -> Pipeline<PipelineStage<T, AstPrinter<'a>, E>, E>
    where
        T: IRPass<E, Output = AST<'a>>,
    {
        self.maybe_print_ast(ctx, true)
    }

    pub fn maybe_print_ast<'a>(
        self,
        ctx: SharedCtx,
        print: bool,
    ) -> Pipeline<PipelineStage<T, AstPrinter<'a>, E>, E>
    where
        T: IRPass<E, Output = AST<'a>>,
    {
        self.add_pass(AstPrinter::new(ctx, print))
    }

    #[inline(always)]
    pub fn print_code<'a>(self, ctx: SharedCtx) -> Pipeline<PipelineStage<T, CodePrinter<'a>, E>, E>
    where
        T: IRPass<E, Output = AST<'a>>,
    {
        self.maybe_print_code(ctx, true)
    }

    pub fn maybe_print_code<'a>(
        self,
        ctx: SharedCtx,
        print: bool,
    ) -> Pipeline<PipelineStage<T, CodePrinter<'a>, E>, E>
    where
        T: IRPass<E, Output = AST<'a>>,
    {
        self.add_pass(CodePrinter::new(ctx, print))
    }

    pub fn execute(self, ir: T::Input) -> R<T::Output, E> {
        self.stage.apply(ir)
    }
}

impl<T, E> IRPass<E> for Pipeline<T, E>
where
    T: IRPass<E>,
{
    type Input = T::Input;
    type Output = T::Output;

    fn apply(self, ir: T::Input) -> R<T::Output, E> {
        self.execute(ir)
    }

    fn description(&self) -> String {
        format!("{:?}", self)
    }
}

impl<S, E> fmt::Debug for Pipeline<S, E> {
    default fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "Pipeline<passes = ?>")
    }
}

impl<S, E> fmt::Debug for Pipeline<S, E>
where
    S: fmt::Debug,
{
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let mut rows = vec!["Pipeline:".to_owned()];
        rows.extend(
            format!(" [pass] {:?}", self.stage)
                .lines()
                .enumerate()
                .map(|(i, s)| format!("{}.{}", i + 1, s)),
        );
        write!(f, "{}", generate_message_box(&rows, None))
    }
}
