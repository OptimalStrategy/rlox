#[macro_use]
mod macros;
pub mod cache;
pub mod combinators;
pub mod pipeline;
pub use crate::pipeline::combinators::*;
pub use crate::pipeline::pipeline::Pipeline;

pub type R<T, E> = Result<T, E>;

pub trait IRPass<E> {
    type Input;
    type Output;

    fn apply(self, ir: Self::Input) -> R<Self::Output, E>;
    fn description(&self) -> String;
}

#[cfg(test)]
mod tests {
    use super::{
        combinators::{DebugPass, MapPass},
        pipeline::Pipeline,
        *,
    };
    use crate::ast::AST;
    use crate::err_context::ErrCtx;
    use crate::{module::ModuleId, token::Token};
    use std::marker::PhantomData;

    type L<'a> = PhantomData<&'a ()>;

    #[test]
    fn test_pipeline_builder() {
        let pipeline = Pipeline::new()
            .add_pass(Scanner::new())
            .add_pass(Parser::new())
            .add_pass(Compiler::new())
            .add_pass(DeadCodeElimination)
            .add_pass(Emitter);
        let _res = pipeline.execute("1 + 2").unwrap();
    }

    #[test]
    fn test_map_pass() {
        let pipeline = Pipeline::<_, ()>::new()
            .add_pass(MapPass::new(|x| x * x))
            .add_pass(DebugPass::new(true));
        assert_eq!(pipeline.execute(25).unwrap(), 625);
    }

    #[derive(Debug)]
    struct CCO;
    struct SourceMeta(u64);
    struct BasicBlocks {
        _blocks: Vec<()>,
        _meta: SourceMeta,
    }
    #[derive(Debug)]
    struct Parser<'a>(L<'a>);
    #[derive(Debug)]
    struct Scanner<'a>(L<'a>);
    #[derive(Debug)]
    struct Compiler<'a>(L<'a>);
    #[derive(Debug)]
    struct DeadCodeElimination;
    #[derive(Debug)]
    struct Emitter;

    impl<'a> IRPass<ErrCtx> for Scanner<'a> {
        type Input = &'a str;
        type Output = (SourceMeta, Vec<Token<'a>>);

        fn apply(self, _ir: Self::Input) -> R<Self::Output, ErrCtx> {
            eprintln!("Executing the scanner...");
            Ok((SourceMeta(0), vec![]))
        }

        fn description(&self) -> String {
            format!("{:?}", self)
        }
    }

    impl<'a> IRPass<ErrCtx> for Parser<'a> {
        type Input = (SourceMeta, Vec<Token<'a>>);
        type Output = (SourceMeta, AST<'a>);
        fn apply(self, ir: Self::Input) -> R<Self::Output, ErrCtx> {
            eprintln!("Executing the parser...");
            Ok((ir.0, AST::new(vec![], ModuleId::anon())))
        }

        fn description(&self) -> String {
            format!("{:?}", self)
        }
    }

    impl<'a, E> IRPass<E> for Compiler<'a> {
        type Input = (SourceMeta, AST<'a>);
        type Output = BasicBlocks;

        fn apply(self, _ir: Self::Input) -> R<Self::Output, E> {
            eprintln!("Compiling the ast...");
            Ok(BasicBlocks {
                _meta: _ir.0,
                _blocks: vec![],
            })
        }

        fn description(&self) -> String {
            format!("{:?}", self)
        }
    }

    impl<E> IRPass<E> for DeadCodeElimination {
        type Input = BasicBlocks;
        type Output = BasicBlocks;

        fn apply(self, ir: Self::Input) -> R<Self::Output, E> {
            println!("Performing DCE...");
            Ok(ir)
        }

        fn description(&self) -> String {
            format!("{:?}", self)
        }
    }

    impl IRPass<ErrCtx> for Emitter {
        type Input = BasicBlocks;
        type Output = CCO;

        fn apply(self, _ir: Self::Input) -> R<Self::Output, ErrCtx> {
            println!("Executing the bytecode emitter...");
            Ok(CCO)
        }

        fn description(&self) -> String {
            format!("{:?}", self)
        }
    }

    impl<'a> Parser<'a> {
        pub fn new() -> Self {
            Self(PhantomData)
        }
    }

    impl<'a> Scanner<'a> {
        pub fn new() -> Self {
            Self(PhantomData)
        }
    }

    impl<'a> Compiler<'a> {
        pub fn new() -> Self {
            Self(PhantomData)
        }
    }
}
