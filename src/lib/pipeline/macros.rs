#[macro_export]
macro_rules! gen_phantom_data_new {
    ($Type:tt) => {
        impl<T> $Type<T> {
            pub fn new() -> Self {
                Self {
                    _marker: PhantomData,
                }
            }
        }
    };
    ($Type:tt) => {
        impl<T> $Type<T> {
            pub fn new() -> Self {
                Self {
                    _marker: PhantomData,
                }
            }
        }
    };
}
