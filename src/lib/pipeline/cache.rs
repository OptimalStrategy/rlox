use crate::{err_context::ErrCtx, ByteCodeSerializer, Compiler, IRPass, Scanner, SharedCtx};

pub struct BinaryCacheLoaderPass<P> {
    ctx: SharedCtx,
    fallback: P,
}

impl<P> BinaryCacheLoaderPass<P> {
    pub fn new(ctx: SharedCtx, fallback: P) -> Self {
        Self { ctx, fallback }
    }
}

impl<'a, P> IRPass<ErrCtx> for BinaryCacheLoaderPass<P>
where
    P: IRPass<
        ErrCtx,
        Input = <Scanner<'a> as IRPass<ErrCtx>>::Output,
        Output = <Compiler<'a> as IRPass<ErrCtx>>::Output,
    >,
{
    type Input = P::Input;
    type Output = P::Output;

    fn apply(self, ir: Self::Input) -> super::R<Self::Output, ErrCtx> {
        let (hash, tokens) = ir;

        if let Some(tok) = tokens.first() {
            let module_id = tok.module;

            let blob = {
                let mut guard = self.ctx.write_shared();
                guard.set_module_hash(module_id, hash);
                // Prevent the fuzzer from creating millions of directories
                #[cfg(not(fuzzing))]
                {
                    guard.init_module_cache(module_id);
                }
                if guard.config.caching {
                    let module = guard.get_module(module_id).unwrap();
                    let path = module.bin_cache();
                    thread_debug!(
                        "Attempting to load `{}` from cache: {}",
                        module.name(),
                        path.display()
                    );
                    std::fs::read(path).ok()
                } else {
                    None
                }
            };

            if let Some(raw) = blob {
                match self.ctx.deserialize(module_id, &raw) {
                    Ok(ok) => {
                        thread_debug!(
                            "Successfully loaded `{}` from cache",
                            self.ctx.read_shared().get_module(module_id).unwrap().name()
                        );
                        return Ok(ok);
                    }
                    Err(_e) => {
                        thread_debug!(
                            "Failed to load `{}` from cache: {:?}",
                            self.ctx.read_shared().get_module(module_id).unwrap().name(),
                            _e
                        );
                    }
                }
            } else {
                thread_debug!(
                    "Failed to load `{}` from cache: not cached",
                    self.ctx.read_shared().get_module(module_id).unwrap().name(),
                );
            }
        }

        self.fallback.apply((hash, tokens))
    }

    fn description(&self) -> String {
        format!(
            "<BinaryCacheLoader: Loads the script from cache or continues the pipeline>\n [pass] {}",
            self.fallback.description()
        )
    }
}
