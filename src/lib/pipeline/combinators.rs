use super::{IRPass, R};
use crate::{
    ast::{ast_to_code::ast_to_code, ast_to_str, AST},
    SharedCtx,
};
use std::marker::PhantomData;

pub struct IdentityPass<T> {
    _marker: PhantomData<T>,
}
impl<T> IdentityPass<T> {
    pub fn new() -> Self {
        Self {
            _marker: PhantomData,
        }
    }
}

impl<T, E> IRPass<E> for IdentityPass<T> {
    type Input = T;
    type Output = T;

    fn apply(self, ir: Self::Input) -> R<Self::Output, E> {
        Ok(ir)
    }

    fn description(&self) -> String {
        String::from("<IdentityPass: Returns the received input>")
    }
}

pub struct DebugPass<T> {
    verbose: bool,
    _marker: PhantomData<T>,
}

impl<T> DebugPass<T> {
    pub fn new(verbose: bool) -> Self {
        Self {
            verbose,
            _marker: PhantomData,
        }
    }
}

impl<T, E> IRPass<E> for DebugPass<T>
where
    T: std::fmt::Debug,
{
    type Input = T;
    type Output = T;

    fn apply(self, ir: Self::Input) -> R<Self::Output, E> {
        if self.verbose {
            println!("{:#?}", &ir);
        } else {
            println!("{:?}", &ir);
        }
        Ok(ir)
    }

    fn description(&self) -> String {
        String::from("<DebugPass: Prints and returns the received input>")
    }
}

pub struct AstPrinter<'a> {
    print: bool,
    ctx: SharedCtx,
    _marker: PhantomData<&'a ()>,
}

impl<'a> AstPrinter<'a> {
    pub fn new(ctx: SharedCtx, print: bool) -> Self {
        Self {
            ctx,
            print,
            _marker: PhantomData,
        }
    }
}

impl<'a, E> IRPass<E> for AstPrinter<'a> {
    type Input = AST<'a>;
    type Output = AST<'a>;

    fn apply(self, ir: Self::Input) -> R<Self::Output, E> {
        if self.print {
            println!(
                "{}",
                ast_to_str(
                    &ir,
                    self.ctx
                        .read_shared()
                        .get_module(ir.module)
                        .map(|m| &m.source_info)
                )
            );
        }
        Ok(ir)
    }

    fn description(&self) -> String {
        format!("<AstPrinter: Prints the AST [enabled = {}]>", self.print)
    }
}
pub struct CodePrinter<'a> {
    print: bool,
    ctx: SharedCtx,
    _marker: PhantomData<&'a ()>,
}

impl<'a> CodePrinter<'a> {
    pub fn new(ctx: SharedCtx, print: bool) -> Self {
        Self {
            ctx,
            print,
            _marker: PhantomData,
        }
    }
}

impl<'a, E> IRPass<E> for CodePrinter<'a> {
    type Input = AST<'a>;
    type Output = AST<'a>;

    fn apply(self, ir: Self::Input) -> R<Self::Output, E> {
        if self.print {
            println!(
                "{}",
                ast_to_code(
                    &ir,
                    self.ctx
                        .read_shared()
                        .get_module(ir.module)
                        .map(|m| &m.source_info)
                )
            );
        }
        Ok(ir)
    }

    fn description(&self) -> String {
        format!(
            "<CodePrinter: Converts the AST to code [enabled = {}]>",
            self.print
        )
    }
}

pub struct MapPass<F, T, U>
where
    F: Fn(T) -> U,
{
    f: F,
    name: Option<String>,
    description: Option<String>,
    _t: PhantomData<T>,
    _u: PhantomData<U>,
}

impl<F, T, U> MapPass<F, T, U>
where
    F: Fn(T) -> U,
{
    pub fn new(f: F) -> Self {
        Self {
            f,
            name: None,
            description: None,
            _t: PhantomData,
            _u: PhantomData,
        }
    }

    pub fn with_description<S: Into<String>>(f: F, desc: S) -> Self {
        Self {
            f,
            name: None,
            description: Some(desc.into()),
            _t: PhantomData,
            _u: PhantomData,
        }
    }
    pub fn and_name<S: Into<String>>(self, name: S) -> Self {
        Self {
            name: Some(name.into()),
            ..self
        }
    }
}

impl<F, T, U, E> IRPass<E> for MapPass<F, T, U>
where
    F: Fn(T) -> U,
{
    type Input = T;
    type Output = U;

    fn apply(self, ir: Self::Input) -> R<Self::Output, E> {
        let f = self.f;
        Ok(f(ir))
    }

    fn description(&self) -> String {
        format!(
            "<{}: {}>",
            self.name.as_ref().map(|s| &s[..]).unwrap_or("MapPass"),
            self.description
                .as_ref()
                .map(|s| &s[..])
                .unwrap_or("Applies the given closure to the given input")
        )
    }
}
