//! This module defines the types for recording and reporting Lox errors.
use std::path::Path;

use crate::{module::ModuleId, token::*, SharedCtx};

/// The maximum length of a lexeme in an error. Any lexeme
/// longer than this value will be truncated.
const MAX_ERROR_LEXEME_LENGTH: usize = 30;

/// The kind of a lox error.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum LoxErrorKind {
    /// Represents a scan error.
    Scan,
    /// Represents a parse error.
    Parse,
    /// Represents a parse error produced when a ternary operator is nested.
    /// Suggests to wrap the nested operator in a pair of parentheses.
    ParseNestedTernary,
    /// Represents a compile error.
    Compile,
    /// Represents a resolution error.
    Resolution,
    /// Represents a resolution error that
    /// suggests to use a wildcard as a variable name.
    ResolutionSuggestWildcard,
    /// Represents an error occurred at runtime.
    Runtime,
    /// A runtime panic.
    Panic,
    /// A runtime assertion error.
    Assertion,
    /// Represents a warning that does not
    /// prevent the program from running.
    Warning,
    /// Represents a part of a traceback.
    Traceback,
}

/// A lox error. Contains the message, span, and module of the error.
#[derive(Debug, Clone)]
pub struct LoxError {
    /// The span where the error originates from.
    pub span: Span,
    /// THe error message.
    pub msg: String,
    /// The module the error originates from.
    pub module: ModuleId,
    /// The kind of the error.
    pub kind: LoxErrorKind,
    /// The opening token of the error is a delimiter error.
    pub opener: Option<Span>,
}

impl LoxError {
    /// Creates a new error with the given data and its module set to [`ModuleId::internal()`].
    pub fn new<S: Into<String>>(
        msg: S,
        span: Span,
        kind: LoxErrorKind,
        opener: Option<Span>,
    ) -> Self {
        LoxError {
            msg: msg.into(),
            span,
            module: ModuleId::internal(),
            kind,
            opener,
        }
    }

    /// Creates a new [`LoxErrorKind::Scan`] error.
    pub fn scan<S: Into<String>>(msg: S, span: Span) -> Self {
        LoxError::new(msg, span, LoxErrorKind::Scan, None)
    }

    /// Creates a new [`LoxErrorKind::Parse`] error.
    pub fn parse<S: Into<String>>(msg: S, span: Span) -> Self {
        LoxError::new(msg, span, LoxErrorKind::Parse, None)
    }

    /// Creates a new [`LoxErrorKind::ParseNestedTernary`] error.
    pub fn parse_nested_ternary<S: Into<String>>(msg: S, query: Span, span: Span) -> Self {
        LoxError::new(msg, query, LoxErrorKind::ParseNestedTernary, Some(span))
    }

    /// Creates a new [`LoxErrorKind::Resolution`] error.
    pub fn resolution<S: Into<String>>(msg: S, span: Span) -> Self {
        LoxError::new(msg, span, LoxErrorKind::Resolution, None)
    }

    /// Creates a new [`LoxErrorKind::ResolutionSuggestWildcard`] error.
    pub fn resolution_wildcard<S: Into<String>>(msg: S, span: Span) -> Self {
        LoxError::new(msg, span, LoxErrorKind::ResolutionSuggestWildcard, None)
    }

    /// Creates a new [`LoxErrorKind::Compile`] error.
    pub fn compile<S: Into<String>>(msg: S, span: Span) -> Self {
        LoxError::new(msg, span, LoxErrorKind::Compile, None)
    }

    /// Creates a new [`LoxErrorKind::Runtime`] error.
    pub fn runtime<S: Into<String>>(msg: S, span: Span) -> Self {
        LoxError::new(msg, span, LoxErrorKind::Runtime, None)
    }

    /// Creates a new [`LoxErrorKind::Panic`] error.
    pub fn panic<S: Into<String>>(msg: S, span: Span) -> Self {
        LoxError::new(msg, span, LoxErrorKind::Panic, None)
    }

    /// Creates a new [`LoxErrorKind::Assertion`] error.
    pub fn assertion<S: Into<String>>(msg: S, span: Span) -> Self {
        LoxError::new(msg, span, LoxErrorKind::Assertion, None)
    }

    /// Creates a new warning.
    pub fn warning<S: Into<String>>(msg: S, span: Span) -> Self {
        LoxError::new(msg, span, LoxErrorKind::Warning, None)
    }

    /// Returns `true` if the error is a warning.
    #[inline]
    pub fn is_warning(&self) -> bool {
        matches!(self.kind, LoxErrorKind::Warning)
    }

    /// Creates a new [`LoxError`] object with the `kind` set to [`Parse`] and the provided opener token.
    ///
    /// [`LoxError`]: struct.LoxError.html
    /// [`Compile`]: enum.LoxErrorKind.html#variant.Parse
    pub fn unclosed_delimiter<S: Into<String>>(msg: S, span: Span, opener: Span) -> Self {
        LoxError::new(msg, span, LoxErrorKind::Parse, Some(opener))
    }

    /// Returns `true` if the error is a delimiter error.
    pub fn is_delimiter_error(&self) -> bool {
        self.opener.is_some()
    }

    /// Returns `true` if the error is wildcard suggestion error.
    pub fn is_wildcard_suggestion(&self) -> bool {
        self.kind == LoxErrorKind::ResolutionSuggestWildcard
    }

    /// Updates the module of the error.
    pub fn with_module(self, module: ModuleId) -> Self {
        Self { module, ..self }
    }
}

/// Checks if the given vector of errors contains only warnings.
pub fn warnings_only(errors: &[LoxError]) -> bool {
    errors.iter().all(|e| e.kind == LoxErrorKind::Warning)
}

/// Reports a single error.
/// Consider the following code sample:
/// ```javascript
/// var x ;
/// ```
/// This is what the reported error looks like:
/// ```bash
/// ==> File '<source>':
/// --> [line 1, at 7] Error at ';': Expected a '=' after the variable name
/// |
/// |    var x ;
/// |          ^
/// ```
pub fn report_error(err: &LoxError, source: &str, filename: &str, root: Option<&Path>) {
    let file = format!(
        "==> File '{}':\n",
        root.map(|root| get_filename(filename, root))
            .unwrap_or(filename)
    );

    let msg = match err.kind {
        LoxErrorKind::ParseNestedTernary => {
            construct_error_message_for_nested_ternary_operator(&err.opener.unwrap(), source)
        }
        LoxErrorKind::ResolutionSuggestWildcard => {
            construct_error_message_and_suggest_wildcard(&err.span, source)
        }
        _ if err.is_delimiter_error() => {
            construct_error_message_for_unclosed_delimiter(&err.span, &err.opener.unwrap(), source)
        }
        _ => construct_error_message(&err.span, source),
    };

    eprintln!(
        "{}",
        format_error_message(source, &file, None, err.span, err.kind, &err.msg, &msg)
    )
}

/// Formats the given error and returns the result as a string.
pub fn format_with_ctx(ctx: SharedCtx, err: &LoxError, function: Option<&str>) -> String {
    let guard = ctx.read_shared();
    let root = guard.root.as_path();
    let print_full_path = guard.config.print_full_path;
    let source_info = guard.get_module_source_info(err.module).unwrap();
    let filename = &source_info.filename;
    let source = &source_info.source;
    let file = format!(
        "==> File '{}':\n",
        if print_full_path {
            filename
        } else {
            get_filename(filename, root)
        }
    );

    let msg = match err.kind {
        LoxErrorKind::ParseNestedTernary => {
            construct_error_message_for_nested_ternary_operator(&err.opener.unwrap(), source)
        }
        LoxErrorKind::ResolutionSuggestWildcard => {
            construct_error_message_and_suggest_wildcard(&err.span, source)
        }
        _ if err.is_delimiter_error() => {
            construct_error_message_for_unclosed_delimiter(&err.span, &err.opener.unwrap(), source)
        }
        _ => construct_error_message(&err.span, source),
    };

    format_error_message(source, &file, function, err.span, err.kind, &err.msg, &msg)
}

/// Formats the given error message in a pretty way.
pub fn format_error_message(
    source: &str,
    file: &str,
    function: Option<&str>,
    span: Span,
    kind: LoxErrorKind,
    error_message: &str,
    error_body: &str,
) -> String {
    format!(
        "{}--> [Line {}, at {}{}] {}{}: {}\n{}",
        file,
        span.line,
        span.column,
        function.map(|f| format!(", in {}", f)).unwrap_or_default(),
        kind,
        // the error is single-line
        if span.end_span().is_none() {
            match get_error_lexeme(source, &span) {
                Some("") => " at end".to_string(),
                Some(lexeme) if kind != LoxErrorKind::Runtime => {
                    format!(" at `{}`", maybe_truncate(lexeme))
                }
                _ => "".to_string(),
            }
        } else {
            // do not display the "at `{}`"" for multi-line errors
            "".to_string()
        },
        error_message,
        error_body,
    )
}

/// Constructs the highlighted part of an error.
pub fn construct_error_message(span: &Span, source: &str) -> String {
    if span.end_span().is_some() {
        return construct_multiline_error_message(span, source);
    }
    let (line, diff) = get_error_line(source, span.line);
    let hats = "^".repeat(span.end() - span.column);
    format!(
        "|\n|    {}\n|{}{}",
        line,
        " ".repeat((span.column + 3) - diff),
        hats
    )
}

/// Constructs a multi-line error message.
fn construct_multiline_error_message(span: &Span, source: &str) -> String {
    let end = span.end_span().unwrap();
    let lines: Vec<&str> = source.split('\n').collect();
    let mut err_lines = vec![];

    let mut line_diffs = vec![];
    let mut min_diff = std::usize::MAX;

    for line in lines.iter().take(end.line).skip(span.line - 1) {
        let mut diff = line.len();
        let line = line.trim_start();
        diff -= line.len();

        if diff < min_diff {
            min_diff = diff;
        }

        line_diffs.push(diff)
    }

    for (i, diff) in (span.line - 1..end.line).zip(line_diffs.iter()) {
        let line: String = lines[i][min_diff..].trim_end().to_string();

        if line.chars().all(|ref c| c.is_ascii_whitespace()) {
            continue;
        }

        let n_hats = if i == span.line - 1 {
            line.len() - span.column + diff + 1
        } else if i == end.line - 1 {
            end.end() - 1
        } else {
            line.len()
        };
        let local_diff = diff - min_diff;

        err_lines.push(format!("|    {}", line));
        err_lines.push(format!(
            "|    {}{}",
            " ".repeat(local_diff),
            "^".repeat(n_hats - local_diff)
        ));
    }

    err_lines.join("\n")
}

/// Constructs an error messages for the nested ternary operator error, suggesting
/// to wrap the nested ternary in a pair of parentheses.
fn construct_error_message_for_nested_ternary_operator(span: &Span, source: &str) -> String {
    let (line, diff) = get_error_line(source, span.line);
    let hats = "^".repeat(span.end() - span.column);
    let spaces = " ".repeat((span.column + 3) - diff);
    let mut hint_line = line.to_string();
    hint_line.replace_range(
        span.column - diff - 1..span.end() - diff - 1,
        &format!(
            "({})",
            &hint_line[span.column - diff - 1..span.end() - diff - 1]
        ),
    );

    let error = format!(
        "|\n\
         |    {}\n\
         |{}{} help: consider wrapping the operator in a pair of parentheses `()`:\n\
         |\n\
         |    {}\n\
         |{}^{}^",
        //
        line,
        spaces,
        hats,
        hint_line,
        spaces,
        " ".repeat(span.length),
    );

    error
}

/// Constructs an error message of the unused variable error, suggesting
/// to prefix the variable with wildcard.
fn construct_error_message_and_suggest_wildcard(span: &Span, source: &str) -> String {
    let (line, diff) = get_error_line(source, span.line);
    let hats = "^".repeat(span.end() - span.column);
    let spaces = " ".repeat((span.column + 3) - diff);
    let mut hint_line = line.to_string();
    hint_line.replace_range(
        span.column - diff - 1..span.end() - diff - 1,
        &format!(
            "_{}",
            &hint_line[span.column - diff - 1..span.end() - diff - 1]
        ),
    );

    let error = format!(
        "|\n\
         |    {}\n\
         |{}{} help: consider replacing (or prefixing) the variable with an underscore:\n\
         |\n\
         |    {}\n\
         |{}^",
        //
        line,
        spaces,
        hats,
        hint_line,
        spaces,
    );

    error
}

/// Find the requested line of the source code, trimming the whitespace at the start,
/// and returning the line and the number of whitespace characters removed.
fn get_error_line(source: &str, tok_line: usize) -> (&str, usize) {
    let lines: Vec<&str> = source.split('\n').collect();
    let line = lines.get(tok_line - 1).unwrap_or(&""); // XXX: might be confusing
    let mut diff = line.len();
    let line = line.trim_start();
    diff -= line.len();

    (line, diff)
}

/// Returns the lexeme corresponding to the given span.
fn get_error_lexeme<'a>(source: &'a str, span: &Span) -> Option<&'a str> {
    let lines: Vec<&str> = source.split('\n').collect();
    let line = &lines[span.line - 1];
    // column number is one-based
    line.get(span.column - 1..span.end() - 1)
}

/// Constructs the highlighted part for an unclosed delimiter error.
pub fn construct_error_message_for_unclosed_delimiter(
    span: &Span,
    opener: &Span,
    source: &str,
) -> String {
    let (opener_line, opener_diff) = get_error_line(source, opener.line);
    let (closer_line, closer_diff) = get_error_line(source, span.line);

    let opener_msg = " ".repeat((opener.column - 1) - opener_diff);
    let opener_msg = format!("{}- unclosed delimiter", opener_msg);
    let closer_msg = " ".repeat((span.column - 1) - closer_diff);
    let closer_msg = format!(
        "{}^-- expected to be closed here [{}:{}]",
        closer_msg, span.line, span.column
    );

    let ellipsis = if span.line - opener.line > 1 {
        "  ... \n"
    } else {
        "|\n"
    };

    format!(
        "|\n\
         |    {}\n\
         |    {}\n\
         {}|    {}\n\
         |    {}\n",
        opener_line, opener_msg, ellipsis, closer_line, closer_msg
    )
}

/// Truncates the given string if its length exceeds [`MAX_ERROR_LEXEME_LENGTH`].
pub fn maybe_truncate(s: &str) -> String {
    if s.len() <= MAX_ERROR_LEXEME_LENGTH {
        s.to_owned()
    } else {
        format!("{}...", &s[0..MAX_ERROR_LEXEME_LENGTH])
    }
}

impl std::fmt::Display for LoxErrorKind {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match &self {
                LoxErrorKind::Scan => "LexError",
                LoxErrorKind::Parse | LoxErrorKind::ParseNestedTernary => "ParseError",
                LoxErrorKind::Resolution | LoxErrorKind::ResolutionSuggestWildcard =>
                    "ResolutionError",
                LoxErrorKind::Compile => "CompileError",
                LoxErrorKind::Runtime => "RuntimeError",
                LoxErrorKind::Panic => "Panic",
                LoxErrorKind::Assertion => "AssertionError",
                LoxErrorKind::Warning => "Warning",
                LoxErrorKind::Traceback => "",
            }
        )
    }
}

impl std::fmt::Display for LoxError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "[{} @ {}:{}] {}",
            self.kind, self.span.line, self.span.column, self.msg
        )
    }
}

impl std::error::Error for LoxError {}

/// Removes the root part from the given path, returning the stem, if possible.
#[inline]
pub fn get_filename<'a>(filename: &'a str, root: &Path) -> &'a str {
    match root.to_str().and_then(|root| filename.split_once(root)) {
        Some((_stem, rest)) => rest.trim_start_matches('/'),
        None => filename,
    }
}
