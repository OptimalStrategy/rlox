//! This module defines the types used by the runtime to represent integers and floats.

/// The Lox integer type, a 128-bit integer.
#[cfg(not(feature = "lox-64bit-ints"))]
pub type LoxInt = i128;

/// The Lox integer type, a 64-bit integer.
#[cfg(feature = "lox-64bit-ints")]
pub type LoxInt = i64;

/// The Lox float type, a double precision floating point number.
pub type LoxFloat = f64;
