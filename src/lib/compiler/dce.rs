use super::{
    basic_block::{BasicBlock, BasicBlocks, BlockOrJump, Jump},
    bytecode::OpCode,
    locations::Location,
};

use rlox_graph::{
    graphviz::{AttrStyle, NodeShape},
    DiGraph, NodeId,
};
use std::{collections::HashMap, convert::TryInto, path::Path};

#[derive(Debug, Clone)]
struct Instruction {
    opcode: OpCode,
    location: Location,
    ir_id: usize,
    instruction_id: usize,
}

impl std::hash::Hash for Instruction {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.ir_id.hash(state);
    }
}

#[allow(unused)]
pub struct DeadCodeElimination<'b> {
    cfg: DiGraph<Instruction>,
    root_id: Option<NodeId>,
    last_return: Option<NodeId>,
    blocks: &'b mut BasicBlocks,
}

impl<'b> DeadCodeElimination<'b> {
    pub fn perform_dce(
        blocks: &'b mut BasicBlocks,
        dir: Option<&Path>,
        name: &str,
    ) -> Result<(), std::io::Error> {
        let (result, new_blocks) = {
            let mut dce = DeadCodeElimination::from_blocks(blocks);

            let result_pre = if let Some(path) = dir {
                dce.export_graph(&path.join(format!("graph_{}.dot", name)))
            } else {
                Ok(())
            };

            dce.eliminate_unreachable_regions();

            let result_post = if let Some(path) = dir {
                dce.export_graph(&path.join(format!("graph_dce_{}.dot", name)))
            } else {
                Ok(())
            };

            let blocks = dce.into_blocks();
            (result_pre.and(result_post), blocks)
        };

        let _ = std::mem::replace(blocks, new_blocks);
        result
    }

    fn into_blocks(self) -> BasicBlocks {
        let mut n_blocks = 0;
        let mut blocks = Vec::new();

        let mut instructions = self.cfg.into_data();
        let n_instructions = instructions.len();

        instructions.sort_by_key(|inst| inst.ir_id);

        let mut current_block = BasicBlock::new(0);
        for inst in instructions {
            match inst.opcode {
                OpCode::Jump(_) | OpCode::JumpBack(_) | OpCode::JumpIfFalse(_) => {
                    blocks.push(BlockOrJump::Block(current_block));
                    n_blocks += 1;
                    current_block = BasicBlock::new(n_blocks);
                    blocks.push(BlockOrJump::Jump(
                        Jump::new(inst.opcode, inst.location.into_span()).unwrap(),
                    ));
                }
                _ => {
                    current_block.emit(inst.opcode, inst.location);
                }
            }
        }

        if !current_block.is_empty() {
            blocks.push(BlockOrJump::Block(current_block));
        }

        BasicBlocks::from_parts(blocks, n_blocks, n_instructions)
    }

    fn from_blocks(blocks: &'b mut BasicBlocks) -> Self {
        if blocks.instruction_count() == 0 {
            return Self {
                cfg: DiGraph::new(),
                root_id: None,
                last_return: None,
                blocks,
            };
        }

        let mut cfg = DiGraph::new();
        let mut instructions = Vec::with_capacity(blocks.instruction_count());
        let mut label_to_id = HashMap::new();
        let old_blocks = std::mem::replace(blocks, BasicBlocks::new()).into_blocks();

        // Id counter for IR instructions
        let mut ir_i = 0;
        // Id counter for executable instructions (corresponding to their IP)
        let mut inst_i = 0;
        // The id of the root node
        let mut root_id = None;
        for block in old_blocks {
            match block {
                BlockOrJump::Block(b) => {
                    for (opcode, location) in b {
                        ir_i += 1;
                        let inst = Instruction {
                            opcode: opcode.clone(),
                            location,
                            ir_id: ir_i - 1,
                            instruction_id: if opcode.is_label() {
                                std::usize::MAX
                            } else {
                                inst_i += 1;
                                inst_i - 1
                            },
                        };
                        let id = cfg.add_node(inst);
                        instructions.push(id);
                        if let OpCode::Label(lbl) = opcode {
                            label_to_id.insert(lbl, id);
                        }
                        if ir_i == 1 {
                            root_id = Some(id);
                        }
                    }
                }
                BlockOrJump::Jump(j) => {
                    ir_i += 1;
                    inst_i += 1;
                    let inst = Instruction {
                        opcode: j.opcode(),
                        location: Location::from_span(j.origin),
                        ir_id: ir_i - 1,
                        instruction_id: inst_i - 1,
                    };
                    let id = cfg.add_node(inst);
                    instructions.push(id);
                    if ir_i == 1 {
                        root_id = Some(id);
                    }
                }
            }
        }

        let mut last_return = None;
        for slice in instructions.windows(2) {
            let [prev, next]: [NodeId; 2] = slice.try_into().unwrap();
            let inst_prev = cfg.get_node(prev).unwrap().data().opcode.clone();
            if let OpCode::Return = &cfg.get_node(next).unwrap().data().opcode {
                last_return = Some(next);
            }
            match inst_prev {
                // Jumps connect to their labels and not the next instruction in the sequence
                OpCode::Jump(lbl) | OpCode::JumpBack(lbl) => {
                    let lbl_node_id = *label_to_id.get(&lbl).unwrap();
                    cfg.add_edge(prev, lbl_node_id);
                }
                // Conditional jumps connect to both a label and the next instruction
                OpCode::JumpIfFalse(lbl) => {
                    let lbl_node_id = *label_to_id.get(&lbl).unwrap();
                    cfg.add_edges(prev, &[lbl_node_id, next]);
                }
                // Return statements are terminal nodes
                OpCode::Return => {}
                // And the rest of the instructions just chain together
                _ => {
                    cfg.add_edge(prev, next);
                }
            }
        }

        Self {
            cfg,
            root_id,
            last_return,
            blocks,
        }
    }

    fn eliminate_unreachable_regions(&mut self) -> usize {
        if let Some(root) = self.root_id {
            self.cfg.remove_unreachable(root).len()
        } else {
            0
        }
    }

    #[allow(clippy::option_map_unit_fn)]
    fn export_graph(&self, filename: &Path) -> Result<(), std::io::Error> {
        let dot = self.cfg.export_dot_with_edge_formatter(
            |node, attrs| {
                self.root_id.map(|id| {
                    if id == node.id() {
                        attrs.hex_color("#3b8a3f");
                    }
                });
                self.last_return.map(|id| {
                    if id == node.id() {
                        attrs.color("red");
                    }
                });
                match &node.data().opcode {
                    OpCode::Label(lbl) => {
                        attrs.shape(NodeShape::Circle);
                        format!("{}", lbl)
                    }
                    OpCode::Jump(lbl) | OpCode::JumpBack(lbl) | OpCode::JumpIfFalse(lbl) => {
                        let data = node.data();
                        format!(
                            "\"{}: {} [ip: {:>03}]\"",
                            data.opcode.name(),
                            lbl,
                            data.instruction_id
                        )
                    }
                    _ => {
                        attrs.shape(NodeShape::Box);
                        let data = node.data();
                        format!(
                            "\"{} [ip: {:>03}]\"",
                            data.opcode.name(),
                            data.instruction_id
                        )
                    }
                }
            },
            |from, _, attrs| {
                if let OpCode::JumpIfFalse(_) = from.data().opcode {
                    attrs.style(AttrStyle::Dashed);
                }
            },
        );
        std::fs::write(filename, dot)?;
        Ok(())
    }
}
