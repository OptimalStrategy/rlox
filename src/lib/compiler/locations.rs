use crate::allocator::DeepSizeOf;

use crate::token::Span;

/// A location of an instruction
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Location {
    pub line: u16,
    pub column: u16,
}
known_deep_size_fast!(0, Location);

impl Location {
    /// TODO: think if we should limit `Span` to u16
    pub fn new(line: usize, column: usize) -> Self {
        Self {
            line: line as u16,
            column: column as u16,
        }
    }

    pub fn from_span(s: Span) -> Self {
        Self::new(s.line, s.column)
    }

    pub fn into_u32(self) -> u32 {
        (self.line as u32) << 16 | self.column as u32
    }

    pub fn from_u32(value: u32) -> Self {
        Location {
            line: (value >> 16) as u16,
            column: (value & 65535) as u16,
        }
    }

    pub fn into_span(self) -> Span {
        Span::new(self.line as usize, self.column as usize, 1)
    }
}

impl std::fmt::Display for Location {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:<03}:{:<02}", self.line, self.column)
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct EncodedLocation {
    pub pattern: u32,
    pub reps: u16,
}
known_deep_size_fast!(0, EncodedLocation);

impl EncodedLocation {
    pub fn new(pattern: u32) -> Self {
        Self { pattern, reps: 1 }
    }

    pub fn from_location(location: &Location) -> Self {
        Self {
            pattern: location.into_u32(),
            reps: 1,
        }
    }
}

#[allow(clippy::manual_non_exhaustive)]
#[derive(Debug, Clone, PartialEq)]
pub enum Locations {
    Raw(Vec<Location>),
    Encoded(Vec<EncodedLocation>),
    #[doc(hidden)]
    _Empty,
}
impl DeepSizeOf for Locations {
    fn deep_size_of_children(&self) -> usize {
        match self {
            Locations::Raw(v) => crate::allocator::deep_size_of::deep_size_of_vec(v),
            Locations::Encoded(v) => crate::allocator::deep_size_of::deep_size_of_vec(v),
            Locations::_Empty => 0,
        }
    }
}

impl From<Vec<Location>> for Locations {
    fn from(locs: Vec<Location>) -> Self {
        Self::Raw(locs)
    }
}

impl From<Vec<EncodedLocation>> for Locations {
    fn from(locs: Vec<EncodedLocation>) -> Self {
        Self::Encoded(locs)
    }
}

impl Locations {
    /// Returns the location at the given index. Returns an error if the locations are encoded.
    #[inline]
    pub fn location_of(&self, i: usize) -> Result<Option<Location>, String> {
        match self {
            Self::Raw(raw) => Ok(raw.get(i).copied()),
            Self::Encoded(_) => Err(String::from("Cannot index into encoded locations.")),
            Self::_Empty => Err(String::from("No locations are available")),
        }
    }

    /// Decodes the locations if they're encoded and returns the location at the given index.
    pub fn location_of_expensive(&mut self, i: usize) -> Option<Location> {
        if let Self::Encoded(_) = self {
            let mut tmp = Locations::Raw(vec![]);
            std::mem::swap(&mut tmp, self);
            *self = tmp.decode();
        }
        self.location_of(i).unwrap()
    }

    pub fn from_span_vec(source: Vec<Span>) -> Self {
        Self::Raw(source.into_iter().map(Location::from_span).collect())
    }

    #[inline]
    pub fn len(&self) -> usize {
        match self {
            Locations::Raw(v) => v.len(),
            Locations::Encoded(v) => v.len(),
            Locations::_Empty => 0,
        }
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    #[inline]
    pub fn as_encoded(&self) -> Option<&Vec<EncodedLocation>> {
        match self {
            Self::Raw(_) | Self::_Empty => None,
            Self::Encoded(v) => Some(v),
        }
    }

    pub fn encode(self) -> Self {
        let raw: Vec<u32> = match self {
            Self::Raw(raw) if !raw.is_empty() => raw.into_iter().map(Location::into_u32).collect(),
            Self::Raw(_) | Self::_Empty => return Self::Encoded(vec![]),
            encoded => return encoded,
        };

        let mut encoded = vec![EncodedLocation::new(*raw.first().unwrap())];
        for pat in raw.into_iter().skip(1) {
            let last = encoded.last_mut().unwrap();
            if pat == last.pattern {
                last.reps += 1;
            } else {
                encoded.push(EncodedLocation::new(pat))
            }
        }

        Locations::Encoded(encoded)
    }

    pub fn encode_self(&mut self) {
        if let Self::Raw(_) = self {
            let mut tmp = Locations::Raw(vec![]);
            std::mem::swap(&mut tmp, self);
            *self = tmp.encode();
        }
    }

    pub fn decode_self(&mut self) {
        if let Self::Encoded(_) = self {
            let mut tmp = Locations::Encoded(vec![]);
            std::mem::swap(&mut tmp, self);
            *self = tmp.decode();
        }
    }

    pub fn decode(self) -> Self {
        let encoded = match self {
            Self::Raw(_) => return self,
            Self::_Empty => return Self::Raw(vec![]),
            Self::Encoded(encoded) => encoded,
        };
        let raw = encoded
            .into_iter()
            .flat_map(|e: EncodedLocation| {
                std::iter::repeat(e.pattern)
                    .take(e.reps as usize)
                    .map(Location::from_u32)
            })
            .collect();
        Self::Raw(raw)
    }
}

trait NoQuotesStringFmt {
    fn fmt(self) -> String;
}

impl NoQuotesStringFmt for String {
    fn fmt(self) -> String {
        self
    }
}

impl std::fmt::Display for Locations {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Locations::Raw(raw) =>
                    if f.alternate() {
                        format!("{:#?}", raw)
                    } else {
                        format!("{:?}", raw)
                    },
                Locations::Encoded(encoded) => {
                    let encoded = encoded
                        .iter()
                        .map(|e| format!("{}x{}", e.pattern, e.reps).fmt())
                        .collect::<Vec<_>>();
                    if f.alternate() {
                        format!("Encoded({:#?}", encoded)
                    } else {
                        format!("Encoded({:?}", encoded)
                    }
                }
                Locations::_Empty => "_Empty".to_string(),
            }
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    extern crate rand;
    use super::tests::rand::{rngs::StdRng, Rng, RngCore, SeedableRng};

    #[test]
    fn test_location_binary_encoding_random() {
        const N_TESTS: u8 = 100;
        let seed = rand::thread_rng().next_u64();
        let mut rng = StdRng::seed_from_u64(seed);
        eprintln!("Seed: {}", seed);

        for i in 0..N_TESTS {
            let line = rng.gen_range(0, 65536) as u16;
            let column = rng.gen_range(0, 65536) as u16;
            test_location_binary_encoding(line, column, i);
        }
    }

    fn test_location_binary_encoding(line: u16, column: u16, test_number: u8) {
        let location = Location::new(line as usize, column as usize);
        let encoded = location.into_u32();
        let decoded = Location::from_u32(encoded);
        assert_eq!(
            location, decoded,
            "[TEST #{}] line = {}, col = {}",
            test_number, line, column
        );
    }

    #[test]
    fn test_run_length_encoding_deterministic() {
        test_location_run_length_encoding(vec![1, 1, 1, 2, 2, 3, 3, 3, 4, 5, 6, 1, 1], 0);
    }

    #[test]
    fn test_run_length_encoding_random() {
        const N_TESTS: u8 = 100;
        const MAX_REPS: u32 = 20;
        const REP_CHANCE: f64 = 0.3;
        const MAX_TEST_GEN_STOP_THRESHOLD: u8 = 50;

        let seed = rand::thread_rng().next_u64();
        let mut rng = StdRng::seed_from_u64(seed);
        eprintln!("Seed: {}", seed);

        for i in 0..N_TESTS {
            let n_samples = rng.gen_range(0, MAX_TEST_GEN_STOP_THRESHOLD);
            let mut samples = vec![];
            while samples.len() < n_samples as usize {
                let number = rng.gen_range(0, 2u64.pow(32) - 1) as u32;
                let n_reps = if rng.gen::<f64>() < REP_CHANCE {
                    rng.gen_range(2, MAX_REPS)
                } else {
                    1
                } as usize;
                samples.extend(std::iter::repeat(number).take(n_reps))
            }

            test_location_run_length_encoding(samples, i);
        }
    }

    #[test]
    fn test_display_impl() {
        assert_eq!("009:05", format!("{}", Location { line: 9, column: 5 }));
        assert_eq!(
            "053:99",
            format!(
                "{}",
                Location {
                    line: 53,
                    column: 99
                }
            )
        );
        assert_eq!(
            "1000:132",
            format!(
                "{}",
                Location {
                    line: 1000,
                    column: 132,
                }
            )
        );
    }

    fn test_location_run_length_encoding(raw: Vec<u32>, test_number: u8) {
        let locations = Locations::Raw(raw.into_iter().map(Location::from_u32).collect::<Vec<_>>());
        let encoded = locations.clone().encode();
        let decoded = encoded.clone().decode();
        assert_eq!(
            locations, decoded,
            "[TEST #{}] Invalid encoding/decoding, encoded = {:#}",
            test_number, encoded
        );

        let mut cc_locations = locations.clone();
        cc_locations.encode_self();
        cc_locations.decode_self();
        let result = locations == cc_locations;
        assert!(
            result,
            "[TEST #{}] Invalid encoding/decoding with *_self() methods, encoded = {:#}",
            test_number,
            cc_locations.encode()
        );
    }
}
