use super::{
    bytecode::{label::Label, Inst, OpCode},
    locations::{Location, Locations},
};
use crate::{token::Span, LoxError};

use std::collections::HashMap;

#[derive(Debug, Clone)]
pub struct BasicBlock {
    pub bytecode: Vec<OpCode>,
    pub locations: Vec<Location>,
    pub block_id: usize,
}

impl BasicBlock {
    pub fn new(block_id: usize) -> Self {
        Self {
            bytecode: vec![],
            locations: vec![],
            block_id,
        }
    }

    pub fn emit(&mut self, op: OpCode, loc: Location) {
        self.bytecode.push(op);
        self.locations.push(loc);
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.bytecode.len()
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.bytecode.is_empty()
    }
}

impl IntoIterator for BasicBlock {
    type Item = (OpCode, Location);
    type IntoIter = std::iter::Zip<std::vec::IntoIter<OpCode>, std::vec::IntoIter<Location>>;

    fn into_iter(self) -> Self::IntoIter {
        self.bytecode.into_iter().zip(self.locations.into_iter())
    }
}

#[derive(Debug, Clone)]
pub struct Jump {
    opcode: OpCode,
    pub origin: Span,
}

impl Jump {
    pub fn new(opcode: OpCode, origin: Span) -> Result<Self, String> {
        if !opcode.is_jump_opcode() {
            return Err(format!(
                "Cannot create a jump from a non-jump opcode: {:#?}",
                opcode
            ));
        }
        Ok(Self { opcode, origin })
    }

    #[inline(always)]
    pub fn opcode(&self) -> OpCode {
        self.opcode.clone()
    }

    pub fn label(&self) -> &Label {
        match &self.opcode {
            OpCode::Jump(label) => label,
            OpCode::JumpBack(label) => label,
            OpCode::JumpIfFalse(label) => label,
            _ => unreachable!(),
        }
    }

    pub fn label_mut(&mut self) -> &mut Label {
        match &mut self.opcode {
            OpCode::Jump(label) => label,
            OpCode::JumpBack(label) => label,
            OpCode::JumpIfFalse(label) => label,
            _ => unreachable!(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct RelativeJump {
    jump_instruction_ips: Vec<(usize, usize)>,
    target_instruction: Option<usize>,
    location: Location,
}

impl RelativeJump {
    fn offset_for(&self, jump: usize) -> Option<usize> {
        self.target_instruction
            .map(|target| (target as isize - jump as isize).unsigned_abs())
    }
}

#[derive(Debug, Clone)]
pub enum BlockOrJump {
    Block(BasicBlock),
    Jump(Jump),
}

impl BlockOrJump {
    pub fn into_block(self) -> Result<BasicBlock, BlockOrJump> {
        match self {
            Self::Block(b) => Ok(b),
            _ => Err(self),
        }
    }

    pub fn as_block(&self) -> Option<&BasicBlock> {
        match self {
            Self::Block(ref b) => Some(b),
            Self::Jump(_) => None,
        }
    }

    pub fn as_block_mut(&mut self) -> Option<&mut BasicBlock> {
        match self {
            Self::Block(ref mut b) => Some(b),
            Self::Jump(_) => None,
        }
    }
}

#[derive(Debug, Clone)]
pub struct BasicBlocks {
    blocks: Vec<BlockOrJump>,
    current_block: usize,
    /// The total number of instructions in this chain of basic blocks.
    instruction_count: usize,
}

impl Default for BasicBlocks {
    fn default() -> Self {
        Self::new()
    }
}

impl BasicBlocks {
    pub fn new() -> Self {
        Self {
            current_block: 0,
            instruction_count: 0,
            blocks: vec![BlockOrJump::Block(BasicBlock::new(0))],
        }
    }

    pub fn from_parts(
        blocks: Vec<BlockOrJump>,
        current_block: usize,
        instruction_count: usize,
    ) -> Self {
        Self {
            instruction_count,
            blocks,
            current_block,
        }
    }

    pub fn into_blocks(self) -> Vec<BlockOrJump> {
        self.blocks
    }

    #[inline(always)]
    pub fn instruction_count(&self) -> usize {
        self.instruction_count
    }

    #[inline(always)]
    pub fn get_mut(&mut self, idx: usize) -> Option<&mut BlockOrJump> {
        self.blocks.get_mut(idx)
    }

    #[inline(always)]
    pub fn block_id(&self) -> usize {
        self.current_block
    }

    pub fn last_block_mut(&mut self) -> Option<&mut BasicBlock> {
        self.blocks.last_mut().and_then(|b| b.as_block_mut())
    }

    pub fn emit(&mut self, op: OpCode, span: Span) {
        self.instruction_count += 1;
        let block = self.last_block_mut().unwrap();
        block.emit(op, Location::from_span(span))
    }

    #[inline(always)]
    pub fn repeat_last_loc(&mut self) -> Result<(), String> {
        self.repeat_last_loc_times(1)
    }

    pub fn last_opcode_is_return(&self) -> bool {
        match self.blocks.last() {
            Some(BlockOrJump::Block(block)) => block.bytecode.last() == Some(&OpCode::Return),
            _ => false,
        }
    }

    pub fn repeat_last_loc_times(&mut self, n: usize) -> Result<(), String> {
        match self.blocks.last_mut().unwrap() {
            BlockOrJump::Block(b) => match b.locations.last().copied() {
                Some(loc) => {
                    b.locations.extend(std::iter::repeat(loc).take(n));
                    Ok(())
                }
                None => Err(format!("The block is empty (block_id = {})", b.block_id)),
            },
            // Jumps are always followed by a block
            _ => unreachable!(),
        }
    }

    pub fn emit_jump(&mut self, jump: Jump) -> usize {
        self.blocks.push(BlockOrJump::Jump(jump));
        self.current_block += 1;
        self.instruction_count += 1;
        self.blocks
            .push(BlockOrJump::Block(BasicBlock::new(self.current_block)));
        self.blocks.len() - 2
    }

    pub fn into_bytecode(self) -> (Vec<OpCode>, Locations) {
        let (opcodes, locations) = self.blocks.into_iter().fold(
            (
                Vec::with_capacity(self.instruction_count),
                Vec::with_capacity(self.instruction_count),
            ),
            |(mut ops, mut locs), b| match b {
                BlockOrJump::Jump(jump) => {
                    ops.push(jump.opcode());
                    locs.extend(vec![Location::from_span(jump.origin); 3]);
                    (ops, locs)
                }
                BlockOrJump::Block(b) => {
                    ops.extend(b.bytecode);
                    locs.extend(b.locations);
                    (ops, locs)
                }
            },
        );
        (opcodes, Locations::Raw(locations))
    }

    pub fn into_raw_bytecode(self) -> Result<(Vec<Inst>, Locations), LoxError> {
        let mut bytecode = Vec::with_capacity(self.instruction_count);
        let mut locations = Vec::with_capacity(self.instruction_count);
        let mut label_to_jump = HashMap::<_, RelativeJump>::new();

        let mut i = 0;
        for block in self.blocks {
            match block {
                BlockOrJump::Jump(jump) => {
                    let location = Location::from_span(jump.origin);
                    bytecode.push(jump.opcode());
                    locations.extend(std::iter::repeat(location).take(3));
                    i += 3;
                    if let Some(rel_jump) = label_to_jump.get_mut(jump.label()) {
                        rel_jump
                            .jump_instruction_ips
                            .push((i - 3, bytecode.len() - 1));
                    } else {
                        label_to_jump.insert(
                            *jump.label(),
                            RelativeJump {
                                jump_instruction_ips: vec![(i - 3, bytecode.len() - 1)],
                                target_instruction: None,
                                location,
                            },
                        );
                    }
                }
                BlockOrJump::Block(block) => {
                    for (loc, op) in block.locations.iter().zip(block.bytecode.iter()) {
                        match op {
                            OpCode::Label(label) => {
                                if let Some(jump) = label_to_jump.get_mut(label) {
                                    jump.target_instruction = Some(i);
                                } else {
                                    label_to_jump.insert(
                                        *label,
                                        RelativeJump {
                                            jump_instruction_ips: vec![],
                                            target_instruction: Some(i),
                                            location: *loc,
                                        },
                                    );
                                }
                            }
                            rest => {
                                let n_bytes = rest.as_bytes().len();
                                i += n_bytes;
                                bytecode.push(rest.clone());
                                locations.extend(std::iter::repeat(loc).take(n_bytes));
                            }
                        }
                    }
                }
            }
        }

        let mut had_unresolved_jump = false;
        for (lbl, jump_target) in label_to_jump.iter() {
            if jump_target.target_instruction.is_none() {
                eprintln!(
                    "These jumps to {} do not have a target instruction: {:#?}",
                    lbl, jump_target
                );
                had_unresolved_jump = true;
                continue;
            }

            for (jump, pos) in &jump_target.jump_instruction_ips {
                let offset = jump_target.offset_for(*jump).unwrap();
                if offset >= 65536 {
                    return Err(LoxError::compile(
                        "Too much code to jump over",
                        Span::new(
                            jump_target.location.line as usize,
                            jump_target.location.column as usize,
                            1,
                        ),
                    ));
                }

                match bytecode.get_mut(*pos).unwrap() {
                    OpCode::Jump(l) | OpCode::JumpBack(l) | OpCode::JumpIfFalse(l) => {
                        *l = Label::Jump(offset as u16);
                    }
                    inst => panic!(
                        "Attempted to patch a non-jump instruction `{:?}` (ip={}) with jump = {:#?}",
                        inst, jump, jump_target,
                    ),
                }
            }
        }

        // This should never happen if jumps are emitted correctly, hence it's ok to panic.
        if had_unresolved_jump {
            panic!(
                "Failed to patch one or more jumps, jump list:\n{:#?}",
                label_to_jump
            );
        }

        let bytecode: Vec<Inst> = bytecode.into_iter().flat_map(|op| op.as_bytes()).collect();

        debug_assert_eq!(
            bytecode.len(),
            locations.len(),
            "The number of locations does not match the number of bytes in the resulting bytecode."
        );

        Ok((bytecode, Locations::Raw(locations).encode()))
    }
}
