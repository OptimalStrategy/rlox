#[macro_export]
macro_rules! loop_scope {
    ($self:ident, $span:expr, $code:block) => {{
        let loop_scope_depth = $self.loop_scope_depth;
        $self.loop_scope_depth = $self.begin_scope();
        $code
        $self.instrument_timeout($span);
        $self.loop_scope_depth = loop_scope_depth;
        $self.end_scope($span)
    }};

    ($self:ident, $span:expr, $code:expr) => {{
        loop_scope!($self, $span, { $code })
    }};
}

#[macro_export]
macro_rules! trie_seq {
    ($root:ident, [$( $opcode:expr ),*], $optimizer:expr) => {{
        use $crate::compiler::peephole_optimizer::{PeepholeTrieNode, none};
         PeepholeTrieNode::insert(&mut $root, &[$($opcode),*], PeepholeTrieNode::without_verifier($optimizer, none()));
    }};

    ($root:ident, [$( $opcode:expr ),*], $optimizer:expr, $verifier:expr) => {{
        use $crate::compiler::peephole_optimizer::{PeepholeTrieNode, none};
         PeepholeTrieNode::insert(&mut $root, &[$($opcode),*], PeepholeTrieNode::with_opt($optimizer, $verifier, none()));
    }};
}

#[macro_export]
macro_rules! compile_var {
    ($self:ident, $ex:expr, $token:expr) => {{
        let token = $token;
        $self.compile_expr(
            &Expr {
                span: token.span,
                kind: ExprKind::Variable(token),
            },
            $ex,
        )
    }};
}
