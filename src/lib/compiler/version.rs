pub const CURRENT_VERSION: BytecodeVersion = BytecodeVersion { major: 1, minor: 6 };

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct BytecodeVersion {
    pub major: u16,
    pub minor: u16,
}

impl BytecodeVersion {
    #[inline]
    pub fn into_u32(self) -> u32 {
        self.into()
    }
}

impl Into<u32> for BytecodeVersion {
    #[inline]
    fn into(self) -> u32 {
        (self.major as u32) << 16 | self.minor as u32
    }
}

impl From<u32> for BytecodeVersion {
    #[inline]
    fn from(v: u32) -> Self {
        Self {
            major: (v >> 16) as u16,
            minor: (v & 0xFF) as u16,
        }
    }
}

known_deep_size_fast!(0, BytecodeVersion);

impl Default for BytecodeVersion {
    #[inline]
    fn default() -> Self {
        CURRENT_VERSION
    }
}

impl std::fmt::Display for BytecodeVersion {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "v{}.{}", self.major, self.minor)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_version_serialization() {
        assert_eq!(
            BytecodeVersion::from(CURRENT_VERSION.into_u32()),
            CURRENT_VERSION
        );
    }
}
