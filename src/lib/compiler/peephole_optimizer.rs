use std::{cell::RefCell, collections::HashMap};

use super::{
    basic_block::BasicBlock,
    bytecode::{OpCode, RawOpcode},
};

pub fn optimize_block(block: &mut BasicBlock) -> bool {
    OPCODE_TRIE.with(|trie| {
        if trie.borrow_mut().is_none() {
            *trie.borrow_mut() = Some(PeepholeOptimizer::with_default_opts());
        }
        trie.borrow().as_ref().unwrap().optimize(block)
    })
}

thread_local! {
    static OPCODE_TRIE: RefCell<Option<PeepholeOptimizer>> = RefCell::new(None);
}

type Optimizer = dyn Fn(Vec<OpCode>) -> Vec<OpCode>;
type Verifier = dyn Fn(&[OpCode]) -> bool;
type OpCodeMap = HashMap<RawOpcode, PeepholeTrieNode>;

#[derive(Debug)]
pub struct PeepholeOptimizer {
    optimizations: PeepholeTrieNode,
}

impl PeepholeOptimizer {
    fn with_default_opts() -> Self {
        let mut root = PeepholeTrieNode::new(none());
        trie_seq!(root, [RawOpcode::Cmp, RawOpcode::Nil], |_| vec![
            OpCode::IsNil
        ]);
        trie_seq!(root, [RawOpcode::CmpNot, RawOpcode::Nil], |_| vec![
            OpCode::IsNotNil
        ]);
        trie_seq!(root, [RawOpcode::Pop, RawOpcode::SetLocal], |op| vec![
            OpCode::SetLocalFast(op[0].operand_u8())
        ]);
        trie_seq!(
            root,
            [RawOpcode::GetLocal, RawOpcode::SetLocalFast],
            |op| vec![OpCode::SetLocal(op.first().unwrap().operand_u8())],
            |ops| match ops[0..2] {
                [OpCode::SetLocalFast(l), OpCode::GetLocal(r)] => l == r,
                _ => unreachable!(),
            }
        );
        // GetLocal -> Copy substitution
        trie_seq!(
            root,
            [RawOpcode::GetLocal, RawOpcode::GetLocal],
            |op| vec![
                OpCode::GetLocal(op.first().unwrap().operand_u8()),
                OpCode::Copy
            ],
            |ops| match ops[0..2] {
                [OpCode::GetLocal(l), OpCode::GetLocal(r)] => l == r,
                _ => unreachable!(),
            }
        );
        // GetLocal(x) Copy GetLocal(x) -> CopyN(2) GetLocal(x)
        trie_seq!(
            root,
            [RawOpcode::GetLocal, RawOpcode::Copy, RawOpcode::GetLocal],
            |op| vec![
                OpCode::GetLocal(op.first().unwrap().operand_u8()),
                OpCode::CopyN(2)
            ],
            |ops| match ops[0..3] {
                [OpCode::GetLocal(l), OpCode::Copy, OpCode::GetLocal(r)] => l == r,
                _ => unreachable!(),
            }
        );
        // GetLocal(x) CopyN(n) GetLocal(x) -> CopyN(n + 1) GetLocal(x)
        trie_seq!(
            root,
            [RawOpcode::GetLocal, RawOpcode::CopyN, RawOpcode::GetLocal],
            |op| vec![
                OpCode::GetLocal(op.first().unwrap().operand_u8()),
                OpCode::CopyN(op[1].operand_u8() + 1)
            ],
            |ops| match ops[0..3] {
                [OpCode::GetLocal(l), OpCode::CopyN(n), OpCode::GetLocal(r)] => l == r && n < 255,
                _ => unreachable!(),
            }
        );
        // GetField GetLocal - >GetFieldFast
        trie_seq!(root, [RawOpcode::GetField, RawOpcode::GetLocal], |ops| {
            vec![OpCode::GetFieldFast(
                ops[0].operand_u8(),
                ops[1].operand_u8(),
            )]
        });
        trie_seq!(root, [RawOpcode::SetField, RawOpcode::GetLocal], |ops| {
            vec![OpCode::SetFieldFast(
                ops[0].operand_u8(),
                ops[1].operand_u8(),
            )]
        });
        trie_seq!(root, [RawOpcode::Sub, RawOpcode::Mul], |_| {
            vec![OpCode::MulSub]
        });
        trie_seq!(root, [RawOpcode::Add, RawOpcode::Mul], |_| {
            vec![OpCode::MulAdd]
        });
        trie_seq!(root, [RawOpcode::Pop, RawOpcode::Pop], |_| {
            vec![OpCode::PopN(2)]
        });
        trie_seq!(root, [RawOpcode::Pop, RawOpcode::PopN], |ops| {
            let n = ops[0].operand_u8() as u16 + 1;
            vec![if n >= 256 {
                OpCode::PopNLong(n)
            } else {
                OpCode::PopN(n as u8)
            }]
        });
        trie_seq!(root, [RawOpcode::PopN, RawOpcode::Pop], |ops| {
            let n = ops[1].operand_u8() as u16 + 1;
            vec![if n >= 256 {
                OpCode::PopNLong(n)
            } else {
                OpCode::PopN(n as u8)
            }]
        });
        trie_seq!(root, [RawOpcode::PopN, RawOpcode::PopN], |ops| {
            let n = ops[0].operand_u8() as u16 + ops[1].operand_u8() as u16;
            vec![if n >= 256 {
                OpCode::PopNLong(n)
            } else {
                OpCode::PopN(n as u8)
            }]
        });
        Self {
            optimizations: root,
        }
    }

    pub fn optimize(&self, block: &mut BasicBlock) -> bool {
        let mut boundary_index = block.len() - 1;
        let mut node = &self.optimizations;
        for (i, opcode) in block.bytecode.iter().enumerate().rev() {
            match node.get(opcode.get_raw_opcode_or_label()) {
                // A branch or a leaf, save it's index
                Some(next) => {
                    boundary_index = i;
                    node = next;
                }
                // There is no children, which means that we have reached a leaf
                None => {
                    break;
                }
            }
        }

        let opcodes = &block.bytecode[boundary_index..];

        // The node is probably not in the trie
        if !node.has_optimizer() {
            return false;
        }

        if node.verify(opcodes) {
            let opcodes = node.optimize(block.bytecode.drain(boundary_index..).collect());
            let location = block.locations.drain(boundary_index..).last().unwrap();
            block
                .locations
                .extend(std::iter::repeat(location).take(opcodes.len()));
            block.bytecode.extend(opcodes);
            true
        } else {
            false
        }
    }
}

pub struct PeepholeTrieNode {
    opt: Option<(Box<Optimizer>, Box<Verifier>)>,
    children: OpCodeMap,
}

impl PeepholeTrieNode {
    pub fn new(children: OpCodeMap) -> Self {
        Self {
            opt: None,
            children,
        }
    }

    pub fn with_opt(
        optimizer: impl Fn(Vec<OpCode>) -> Vec<OpCode> + 'static,
        verifier: impl Fn(&[OpCode]) -> bool + 'static,
        children: HashMap<RawOpcode, PeepholeTrieNode>,
    ) -> Self {
        Self {
            opt: Some((Box::new(optimizer), Box::new(verifier))),
            children,
        }
    }

    pub fn without_verifier(
        optimizer: impl Fn(Vec<OpCode>) -> Vec<OpCode> + 'static,
        children: HashMap<RawOpcode, PeepholeTrieNode>,
    ) -> Self {
        Self::with_opt(optimizer, default_verifier, children)
    }

    #[inline]
    pub fn has_optimizer(&self) -> bool {
        self.opt.is_some()
    }

    pub fn verify(&self, opcodes: &[OpCode]) -> bool {
        (self.opt.as_ref().unwrap().1)(opcodes)
    }

    pub fn optimize(&self, opcodes: Vec<OpCode>) -> Vec<OpCode> {
        (self.opt.as_ref().unwrap().0)(opcodes)
    }

    #[inline]
    pub fn get(&self, opcode: RawOpcode) -> Option<&PeepholeTrieNode> {
        self.children.get(&opcode)
    }

    #[inline]
    pub fn get_mut(&mut self, opcode: RawOpcode) -> Option<&mut PeepholeTrieNode> {
        self.children.get_mut(&opcode)
    }

    pub fn add_child(&mut self, op: RawOpcode, child: PeepholeTrieNode) {
        self.children.insert(op, child);
    }

    pub fn insert(root: &mut PeepholeTrieNode, sequence: &[RawOpcode], new_node: PeepholeTrieNode) {
        let mut node = root;
        for op in sequence {
            if node.get(*op).is_none() {
                node.add_child(*op, PeepholeTrieNode::new(none()));
            }
            node = node.get_mut(*op).unwrap();
        }
        *node = new_node;
    }
}

impl std::fmt::Debug for PeepholeTrieNode {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            if f.alternate() {
                format!(
                    "PeepholeTrieNode {{\n    opt: {},\n    children: {}\n}}",
                    self.opt.is_some(),
                    format!("{:#?}", self.children)
                        .split('\n')
                        .enumerate()
                        .map(|(i, l)| if i == 0 {
                            l.to_owned()
                        } else {
                            format!("    {}", l)
                        })
                        .collect::<Vec<_>>()
                        .join("\n")
                )
            } else {
                format!(
                    "PeepholeTrieNode {{ opt: {}, children: {:?} }}",
                    self.opt.is_some(),
                    self.children
                )
            }
        )
    }
}

fn default_verifier(_: &[OpCode]) -> bool {
    true
}

fn none() -> OpCodeMap {
    OpCodeMap::new()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::compiler::locations::Location;

    #[test]
    fn test_opt_getlocal_getlocal_to_getlocal_copy() {
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::GetLocal(32), Location::from_u32(0));
        block.emit(OpCode::GetLocal(32), Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::GetLocal(32), OpCode::Copy]);
    }

    #[test]
    fn test_opt_getlocal_copy_getlocal_to_getlocal_copyn() {
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::GetLocal(32), Location::from_u32(0));
        block.emit(OpCode::Copy, Location::from_u32(0));
        block.emit(OpCode::GetLocal(32), Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::GetLocal(32), OpCode::CopyN(2)]);
    }

    #[test]
    fn test_opt_getlocal_copyn_getlocal_to_getlocal_copyn() {
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::GetLocal(32), Location::from_u32(0));
        block.emit(OpCode::CopyN(3), Location::from_u32(0));
        block.emit(OpCode::GetLocal(32), Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::GetLocal(32), OpCode::CopyN(4)]);
    }

    #[test]
    fn test_opt_getlocal_setfield_to_setfieldfast() {
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::GetLocal(0), Location::from_u32(1));
        block.emit(OpCode::SetField(3), Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::SetFieldFast(0, 3)]);
    }

    #[test]
    fn test_opt_getlocal_getfield_to_getfieldfast() {
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::GetLocal(0), Location::from_u32(1));
        block.emit(OpCode::GetField(3), Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::GetFieldFast(0, 3)]);
    }

    #[test]
    fn test_opt_setlocal_pop_to_setfast() {
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::Nil, Location::from_u32(1));
        block.emit(OpCode::SetLocal(42), Location::from_u32(0));
        block.emit(OpCode::Pop, Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::Nil, OpCode::SetLocalFast(42)]);
    }

    #[test]
    fn test_opt_setfast_get_with_set() {
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::Nil, Location::from_u32(0));
        block.emit(OpCode::SetLocalFast(2), Location::from_u32(0));
        block.emit(OpCode::GetLocal(2), Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::Nil, OpCode::SetLocal(2)]);

        // Make sure that different locals don't get optimized
        block.emit(OpCode::One, Location::from_u32(0));
        block.emit(OpCode::SetLocalFast(27), Location::from_u32(0));
        block.emit(OpCode::GetLocal(2), Location::from_u32(0));
        assert!(!optimize_block(&mut block));
        assert_eq!(
            block.bytecode,
            vec![
                OpCode::Nil,
                OpCode::SetLocal(2),
                OpCode::One,
                OpCode::SetLocalFast(27),
                OpCode::GetLocal(2)
            ]
        );
    }

    #[test]
    fn test_opt_nil_cmp_to_isnil() {
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::Nil, Location::from_u32(0));
        block.emit(OpCode::Cmp, Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::IsNil]);
    }

    #[test]
    fn test_opt_nil_cmpnot_to_isnotnil() {
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::Nil, Location::from_u32(0));
        block.emit(OpCode::CmpNot, Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::IsNotNil]);
    }

    #[test]
    fn test_opt_mul_sub() {
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::Mul, Location::from_u32(0));
        block.emit(OpCode::Sub, Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::MulSub]);
    }

    #[test]
    fn test_opt_mul_add() {
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::Mul, Location::from_u32(0));
        block.emit(OpCode::Add, Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::MulAdd]);
    }

    #[test]
    fn test_opt_pop_to_popn() {
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::Pop, Location::from_u32(0));
        block.emit(OpCode::Pop, Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::PopN(2)]);
    }

    #[test]
    fn test_opt_pop_popn_to_popn() {
        // Pop with PopN
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::Pop, Location::from_u32(0));
        block.emit(OpCode::PopN(3), Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::PopN(4)]);

        // PopN with Pop
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::PopN(3), Location::from_u32(0));
        block.emit(OpCode::Pop, Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::PopN(4)]);
    }

    #[test]
    fn test_opt_popn_popn_to_popn() {
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::PopN(12), Location::from_u32(0));
        block.emit(OpCode::PopN(3), Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::PopN(15)]);
    }

    #[test]
    fn test_opt_popn_to_popnlong() {
        // PopN(255) + pop
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::PopN(255), Location::from_u32(0));
        block.emit(OpCode::Pop, Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::PopNLong(256)]);

        // Pop + PopN(255)
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::Pop, Location::from_u32(0));
        block.emit(OpCode::PopN(255), Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::PopNLong(256)]);

        // PopN(22) + PoN(255)
        let mut block = BasicBlock::new(0);
        block.emit(OpCode::PopN(22), Location::from_u32(0));
        block.emit(OpCode::PopN(255), Location::from_u32(0));

        assert!(optimize_block(&mut block));
        assert_eq!(block.bytecode, vec![OpCode::PopNLong(277)]);
    }
}
