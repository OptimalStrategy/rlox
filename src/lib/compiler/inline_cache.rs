use crate::{vm::Value, ObjectPointer};

#[derive(Debug, Clone)]
pub struct CacheEntry {
    // TODO: For now this only works with native singleton classes.
    key: u64,
    value: Value,
}

#[derive(Debug, Clone)]
pub struct InlineCache {
    cache: Vec<Option<CacheEntry>>,
}

impl InlineCache {
    pub fn new(size: usize) -> Self {
        Self {
            cache: vec![None; size],
        }
    }

    #[cfg(not(feature = "unchecked-vm"))]
    #[inline]
    pub fn get(&self, at: usize) -> Option<&CacheEntry> {
        self.cache.get(at).and_then(|v| v.as_ref())
    }

    #[cfg(feature = "unchecked-vm")]
    #[inline]
    pub fn get(&self, at: usize) -> Option<&CacheEntry> {
        unsafe { self.cache.get_unchecked(at) }.as_ref()
    }

    #[cfg(not(feature = "unchecked-vm"))]
    #[inline]
    pub fn set(&mut self, at: usize, entry: CacheEntry) {
        *self.cache.get_mut(at).unwrap() = Some(entry);
    }

    #[cfg(feature = "unchecked-vm")]
    #[inline]
    pub fn set(&mut self, at: usize, entry: CacheEntry) {
        unsafe { *self.cache.get_unchecked_mut(at) = Some(entry) };
    }

    #[inline]
    pub fn lookup(&self, at: usize, key: u64) -> Option<&Value> {
        if self.cache.is_empty() {
            return None;
        }
        if let Some(entry) = self.get(at) {
            if entry.key == key {
                return Some(&entry.value);
            }
        }
        None
    }

    #[inline]
    pub fn update(&mut self, at: usize, key: u64, value: Value) {
        if !self.cache.is_empty() {
            self.set(at, CacheEntry { key, value });
        }
    }

    #[inline]
    pub fn contained_pointers(&mut self) -> impl Iterator<Item = &'_ mut ObjectPointer> + '_ {
        self.cache
            .iter_mut()
            .filter_map(|e| e.as_mut().and_then(|e| e.value.as_ptr_mut()))
    }

    #[inline]
    pub fn invalidate(&mut self) {
        self.cache.iter_mut().for_each(|slot| *slot = None)
    }
}
