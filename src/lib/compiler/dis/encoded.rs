use super::Disassemble;
use crate::compiler::{
    bytecode::OpCode,
    chunk::Chunk,
    locations::{Location, Locations},
};

pub struct EncodedDisassembler<'a> {
    name: Option<&'a str>,
    chunk: &'a Chunk,
    locations: Locations,
}

impl<'a> EncodedDisassembler<'a> {
    pub(crate) fn new(name: Option<&'a str>, chunk: &'a Chunk) -> Self {
        Self {
            name,
            chunk,
            locations: chunk.locations.clone().decode(),
        }
    }

    fn const_at(&self, at: usize) -> String {
        format!(
            "'{}'",
            self.chunk
                .const_at(at)
                .map(|v| format!("{}", v))
                .unwrap_or_else(|| format!("ERR: INVALID CONST INDEX `{}`", at))
        )
    }

    fn line(&self, ip: usize) -> Location {
        self.locations
            .location_of(ip)
            .unwrap()
            .unwrap_or_else(|| panic!("Couldn't get the location data for ip={}", ip))
    }

    fn line_fmt(&self, ir_ip: usize, ip: usize) -> String {
        format!("{}|ip={:<03}", self.line(ir_ip), ip)
    }

    fn dis_upvalues(
        &self,
        ir_ip: usize,
        i: &mut usize,
        is_long: bool,
        upvalues: &[crate::compiler::compiler::UpValue],
    ) -> String {
        upvalues
            .iter()
            .map(|up| {
                if is_long {
                    *i += 2;
                } else {
                    *i += 1;
                }
                *i += 1;
                format!(
                    "{}| {:<16}  {:>7} {:>4}",
                    self.line_fmt(ir_ip, *i),
                    "",
                    if up.is_local { "local" } else { "upvalue" },
                    up.index
                )
            })
            .collect::<Vec<_>>()
            .join("\n")
    }
}

impl<'a> Disassemble for EncodedDisassembler<'a> {
    fn disassemble(&self) -> String {
        let mut out = format!("<chunk: {}>\n", self.name.unwrap_or("<stream>"));

        let mut i = 0;
        for (ir_ip, op) in self.chunk.bytecode.as_encoded().unwrap().iter().enumerate() {
            let value = match op {
                // 0-byte operands
                OpCode::Nil
                | OpCode::One
                | OpCode::Zero
                | OpCode::True
                | OpCode::False
                | OpCode::Pop
                | OpCode::Copy
                | OpCode::Print
                | OpCode::Negate
                | OpCode::Not
                | OpCode::Add
                | OpCode::Sub
                | OpCode::Mul
                | OpCode::Div
                | OpCode::Pow
                | OpCode::Mod
                | OpCode::Cmp
                | OpCode::CmpNot
                | OpCode::CmpType
                | OpCode::CmpTypeNot
                | OpCode::LessEqual
                | OpCode::LessThan
                | OpCode::GreaterEqual
                | OpCode::GreaterThan
                | OpCode::Return
                | OpCode::MatchGetBound
                | OpCode::MatchPopBound
                | OpCode::MatchPushBound
                | OpCode::GetItem
                | OpCode::SetItem
                | OpCode::MakeMethod
                | OpCode::IsNil
                | OpCode::IsNotNil
                | OpCode::IntoIter
                | OpCode::IterNext
                | OpCode::Unwrap
                | OpCode::MulAdd
                | OpCode::MulSub
                | OpCode::TimeoutCheck
                | OpCode::Inherit => {
                    i += 1;
                    format!("{}| {:<016}", self.line_fmt(ir_ip, i - 1), op.name())
                }
                OpCode::MissingJumpLabel => {
                    i += 1;
                    format!(
                        "{}| <ERROR: {:<016}> ",
                        self.line_fmt(ir_ip, i - 1),
                        op.name()
                    )
                }
                OpCode::Error => {
                    i += 1;
                    format!("{}| <ERROR> ", self.line_fmt(ir_ip, i - 1))
                }
                // 1-byte operands
                OpCode::LoadConst(operand)
                | OpCode::MakeClass(operand)
                | OpCode::SetField(operand)
                | OpCode::GetField(operand)
                | OpCode::GetSuper(operand) => {
                    i += 2;
                    format!(
                        "{}| {:<16} {:>4} {:>04}",
                        self.line_fmt(ir_ip, i - 2),
                        op.name(),
                        operand,
                        self.const_at(*operand as usize)
                    )
                }
                OpCode::GetLocal(operand)
                | OpCode::SetLocal(operand)
                | OpCode::Inc(operand)
                | OpCode::Dec(operand)
                | OpCode::SetLocalFast(operand) => {
                    i += 2;
                    format!(
                        "{}| {:<16} {:>4}",
                        self.line_fmt(ir_ip, i - 2),
                        op.name(),
                        format!("@{}", operand)
                    )
                }
                OpCode::MakeArr(operand)
                | OpCode::MakeDict(operand)
                | OpCode::LoadNativeFn(operand)
                | OpCode::GetUpvalue(operand)
                | OpCode::SetUpvalue(operand)
                | OpCode::Interpolate(operand)
                | OpCode::PrintN(operand)
                | OpCode::PopN(operand)
                | OpCode::CopyN(operand)
                | OpCode::FnCall(operand)
                | OpCode::TCOCall(operand) => {
                    i += 2;
                    format!(
                        "{}| {:<16} {:>4}",
                        self.line_fmt(ir_ip, i - 2),
                        op.name(),
                        operand
                    )
                }
                OpCode::FwdCall(operand) => {
                    i += 2;
                    let first = *operand >> 4;
                    let second = *operand & 0b1111;
                    format!(
                        "{}| {:<16} {:>2},{:>2}",
                        self.line_fmt(ir_ip, i - 2),
                        op.name(),
                        first,
                        second,
                    )
                }
                OpCode::LoadConstLong(operand)
                | OpCode::MakeClassLong(operand)
                | OpCode::SetFieldLong(operand)
                | OpCode::GetFieldLong(operand)
                | OpCode::GetSuperLong(operand)
                | OpCode::MakeModule(operand)
                | OpCode::Import(operand) => {
                    i += 3;
                    format!(
                        "{}| {:<016} {:>04} {:>04}",
                        self.line_fmt(ir_ip, i - 3),
                        op.name(),
                        operand,
                        self.const_at(*operand as usize)
                    )
                }

                OpCode::GetLocalLong(operand)
                | OpCode::SetLocalLong(operand)
                | OpCode::IncLong(operand)
                | OpCode::DecLong(operand) => {
                    i += 3;
                    format!(
                        "{}| {:<16} {:>4}",
                        self.line_fmt(ir_ip, i - 3),
                        op.name(),
                        format!("@{}", operand)
                    )
                }
                OpCode::PopNLong(operand)
                | OpCode::GetUpvalueLong(operand)
                | OpCode::SetUpvalueLong(operand)
                | OpCode::InterpolateLong(operand) => {
                    i += 3;
                    format!(
                        "{}| {:<16} {:>4}",
                        self.line_fmt(ir_ip, i - 3),
                        op.name(),
                        operand
                    )
                }
                OpCode::Jump(operand) | OpCode::JumpIfFalse(operand) => {
                    let s = format!(
                        "{}| {:<18} {:>4} [{} -> {}]",
                        self.line_fmt(ir_ip, i),
                        op.name(),
                        operand,
                        i,
                        operand.map_jump(|jump| i as u16 + jump)
                    );
                    i += 3;
                    s
                }
                OpCode::JumpBack(operand) => {
                    let s = format!(
                        "{}| {:<18} {:>4} [{} -> {}]",
                        self.line_fmt(ir_ip, i),
                        op.name(),
                        operand,
                        i,
                        operand.map_jump(|jump| i as u16 - jump)
                    );
                    i += 3;
                    s
                }
                OpCode::Label(lbl) => format!(
                    "{}| {:<18} {:<4}",
                    format!("{}|ip={:<03}", self.line(ir_ip), "---"),
                    op.name(),
                    lbl.to_string()
                ),
                OpCode::GetFieldFast(instance, name) | OpCode::SetFieldFast(instance, name) => {
                    i += 3;
                    format!(
                        "{}| {:<16} {:>4}{:>4} {:<4}",
                        self.line_fmt(ir_ip, i - 3),
                        op.name(),
                        format!("@{}", instance),
                        name,
                        self.const_at(*name as usize)
                    )
                }
                OpCode::InvokeMethod(name, args) | OpCode::InvokeMethodTCO(name, args) => {
                    i += 3;
                    format!(
                        "{}| {:<16} {:>4} {:>4} {:<4}",
                        self.line_fmt(ir_ip, i - 3),
                        op.name(),
                        name,
                        self.const_at(*name as usize),
                        args,
                    )
                }
                OpCode::MakeClosure(slot, upvalues) => {
                    i += 2;
                    let mut s = format!(
                        "{}| {:<16} {:>4} {:<4}",
                        self.line_fmt(ir_ip, i - 2),
                        op.name(),
                        slot,
                        self.const_at(*slot as usize),
                    );
                    if !upvalues.is_empty() {
                        s.push_str(&format!(
                            "\n{}",
                            self.dis_upvalues(ir_ip, &mut i, false, upvalues)
                        ));
                    }
                    s
                }
                OpCode::MakeClosureLong(slot, upvalues) => {
                    i += 3;
                    let mut s = format!(
                        "{}| {:<16} {:>4} {:<4}",
                        self.line_fmt(ir_ip, i - 2),
                        op.name(),
                        slot,
                        self.const_at(*slot as usize),
                    );
                    if !upvalues.is_empty() {
                        s.push_str(&format!(
                            "\n{}",
                            self.dis_upvalues(ir_ip, &mut i, true, upvalues)
                        ));
                    }
                    s
                }
            };
            out.push_str(&format!("{}\n", value));
        }

        let nested = self
            .chunk
            .code_objects
            .iter()
            .flat_map(|ptr| {
                let s = ptr.as_ref().as_cco().disassemble();
                s.split('\n')
                    .map(|line| format!("\t{}", line))
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>()
            .join("\n");

        format!(
            "{}<nested cco objects: {}>{}",
            out,
            self.chunk.code_objects.len(),
            if nested.is_empty() {
                "".to_string()
            } else {
                format!("\n{}", nested)
            },
        )
    }
}
