use super::Disassemble;
use crate::compiler::bytecode::RawOpcode;
use crate::compiler::chunk::Chunk;
use compiler::locations::Location;
use compiler::locations::Locations;
use vm::object::Value;

pub struct RawDisassembler<'a> {
    name: Option<&'a str>,
    chunk: &'a Chunk,
    locations: Locations,
}

impl<'a> RawDisassembler<'a> {
    pub fn new(name: Option<&'a str>, chunk: &'a Chunk) -> Self {
        Self {
            name,
            chunk,
            locations: chunk.locations.clone().decode(),
        }
    }

    fn const_at(&self, at: usize) -> String {
        self.chunk
            .const_at(at)
            .map(|v| match v.as_string() {
                Some(s) => {
                    let mut s = format!("{:?}", s.as_ref());
                    format!("'{}'", s.drain(1..s.len() - 1).collect::<String>())
                }
                None => format!("'{}'", v),
            })
            .unwrap_or_else(|| format!("ERR: INVALID CONST INDEX `{}`", at))
    }

    fn const_at_and_then<T>(
        &self,
        at: usize,
        f: impl Fn(&Value) -> Result<T, String>,
    ) -> Result<T, String> {
        self.chunk
            .const_at(at)
            .ok_or_else(|| format!("ERR: INVALID CONST INDEX `{}`", at))
            .and_then(f)
    }

    fn line(&self, ip: usize) -> Location {
        self.locations
            .location_of(ip)
            .unwrap()
            .unwrap_or_else(|| panic!("Couldn't get the location data for ip={}", ip))
    }

    fn line_fmt(&self, ip: usize) -> String {
        format!("{}|ip={:<03}", self.line(ip), ip)
    }

    fn dis_upvalues(&self, i: &mut usize, n_captured: usize, bytecode: &[u8]) -> String {
        let is_long = n_captured > 256;
        let mut s = Vec::new();
        for _ in 0..n_captured {
            let is_local = bytecode[*i];
            let index = if is_long {
                *i += 2;
                BE_from!(bytecode[*i - 2..*i])
            } else {
                *i += 1;
                bytecode[*i] as u16
            };
            *i += 1;
            s.push(format!(
                "{}| {:<16}  {:>7} {:>4}",
                self.line_fmt(*i),
                "",
                if is_local == 1 { "local" } else { "upvalue" },
                index
            ));
        }
        s.join("\n")
    }
}

impl<'a> Disassemble for RawDisassembler<'a> {
    fn disassemble(&self) -> String {
        let mut out = format!("<chunk: {}>\n", self.name.unwrap_or("<stream>"));

        let mut i = 0;
        let bytecode = self.chunk.bytecode.as_raw().unwrap();
        while i < bytecode.len() {
            let op = RawOpcode::from_u8(bytecode[i]);
            let value = match op {
                // 0-byte operands
                RawOpcode::Nil
                | RawOpcode::One
                | RawOpcode::Zero
                | RawOpcode::True
                | RawOpcode::False
                | RawOpcode::Pop
                | RawOpcode::Copy
                | RawOpcode::Print
                | RawOpcode::Negate
                | RawOpcode::Not
                | RawOpcode::Add
                | RawOpcode::Sub
                | RawOpcode::Mul
                | RawOpcode::Div
                | RawOpcode::Pow
                | RawOpcode::Mod
                | RawOpcode::Cmp
                | RawOpcode::CmpNot
                | RawOpcode::CmpType
                | RawOpcode::CmpTypeNot
                | RawOpcode::LessEqual
                | RawOpcode::LessThan
                | RawOpcode::GreaterEqual
                | RawOpcode::GreaterThan
                | RawOpcode::Return
                | RawOpcode::MatchGetBound
                | RawOpcode::MatchPopBound
                | RawOpcode::MatchPushBound
                | RawOpcode::GetItem
                | RawOpcode::SetItem
                | RawOpcode::MakeMethod
                | RawOpcode::IsNil
                | RawOpcode::IsNotNil
                | RawOpcode::IntoIter
                | RawOpcode::IterNext
                | RawOpcode::Unwrap
                | RawOpcode::MulAdd
                | RawOpcode::MulSub
                | RawOpcode::TimeoutCheck
                | RawOpcode::Inherit => {
                    i += 1;
                    format!("{}| {:<016}", self.line_fmt(i - 1), op.name())
                }
                RawOpcode::MissingJumpLabel => {
                    i += 1;
                    format!("{}| <ERROR: {:<016}> ", self.line_fmt(i - 1), op.name())
                }
                RawOpcode::Unknown => {
                    i += 1;
                    format!(
                        "{}| <ERROR: Unknown byte value {}> ",
                        self.line_fmt(i - 1),
                        bytecode[i - 1]
                    )
                }
                RawOpcode::Error => {
                    i += 1;
                    format!("{}| <ERROR> ", self.line_fmt(i - 1))
                }
                // 1-byte operands
                RawOpcode::LoadConst
                | RawOpcode::MakeClass
                | RawOpcode::SetField
                | RawOpcode::GetField
                | RawOpcode::GetSuper => {
                    i += 2;
                    let operand = bytecode[i - 1];
                    format!(
                        "{}| {:<016} {:>4} {:>04}",
                        self.line_fmt(i - 2),
                        op.name(),
                        operand,
                        self.const_at(operand as usize)
                    )
                }
                RawOpcode::GetLocal
                | RawOpcode::SetLocal
                | RawOpcode::Inc
                | RawOpcode::Dec
                | RawOpcode::SetLocalFast => {
                    i += 2;
                    let operand = bytecode[i - 1];
                    format!(
                        "{}| {:<16} {:>4}",
                        self.line_fmt(i - 2),
                        op.name(),
                        format!("@{}", operand),
                    )
                }
                RawOpcode::MakeArr
                | RawOpcode::MakeDict
                | RawOpcode::LoadNativeFn
                | RawOpcode::GetUpvalue
                | RawOpcode::SetUpvalue
                | RawOpcode::Interpolate
                | RawOpcode::PrintN
                | RawOpcode::PopN
                | RawOpcode::CopyN
                | RawOpcode::FnCall
                | RawOpcode::TCOCall => {
                    i += 2;
                    let operand = bytecode[i - 1];
                    format!("{}| {:<16} {:>4}", self.line_fmt(i - 2), op.name(), operand)
                }
                RawOpcode::FwdCall => {
                    i += 2;
                    let operand = bytecode[i - 1];
                    let first = operand >> 4;
                    let second = operand & 0b1111;
                    format!(
                        "{}| {:<16} {:>4} + {}",
                        self.line_fmt(i - 2),
                        op.name(),
                        first,
                        second,
                    )
                }
                RawOpcode::LoadConstLong
                | RawOpcode::MakeClassLong
                | RawOpcode::SetFieldLong
                | RawOpcode::GetFieldLong
                | RawOpcode::GetSuperLong
                | RawOpcode::MakeModule
                | RawOpcode::Import => {
                    i += 3;
                    let operand = BE_from!(bytecode[i - 2..i]);
                    format!(
                        "{}| {:<016} {:>04} {:>04}",
                        self.line_fmt(i - 3),
                        op.name(),
                        operand,
                        self.const_at(operand as usize)
                    )
                }

                RawOpcode::GetLocalLong
                | RawOpcode::SetLocalLong
                | RawOpcode::IncLong
                | RawOpcode::DecLong => {
                    i += 3;
                    let operand = BE_from!(bytecode[i - 2..i]);
                    format!(
                        "{}| {:<16} {:>4}",
                        self.line_fmt(i - 3),
                        op.name(),
                        format!("@{}", operand),
                    )
                }
                RawOpcode::PopNLong
                | RawOpcode::GetUpvalueLong
                | RawOpcode::SetUpvalueLong
                | RawOpcode::InterpolateLong => {
                    i += 3;
                    let operand = BE_from!(bytecode[i - 2..i]);
                    format!("{}| {:<16} {:>4}", self.line_fmt(i - 3), op.name(), operand)
                }
                RawOpcode::Jump | RawOpcode::JumpIfFalse => {
                    let operand = BE_from!(bytecode[i + 1..i + 3]);
                    let s = format!(
                        "{}| {:<16} {:>4} [{} -> {}]",
                        self.line_fmt(i),
                        op.name(),
                        operand,
                        i,
                        i as u16 + operand
                    );
                    i += 3;
                    s
                }
                RawOpcode::JumpBack => {
                    let operand = BE_from!(bytecode[i + 1..i + 3]);
                    let s = format!(
                        "{}| {:<16} {:>4} [{} -> {}]",
                        self.line_fmt(i),
                        op.name(),
                        operand,
                        i,
                        i as u16 - operand
                    );
                    i += 3;
                    s
                }
                RawOpcode::GetFieldFast | RawOpcode::SetFieldFast => {
                    i += 3;
                    let instance = bytecode[i - 2];
                    let name = bytecode[i - 1];
                    format!(
                        "{}| {:<16} {:>4}{:>4} {:<4}",
                        self.line_fmt(i - 3),
                        op.name(),
                        format!("@{}", instance),
                        name,
                        self.const_at(name as usize)
                    )
                }
                RawOpcode::InvokeMethod | RawOpcode::InvokeMethodTCO => {
                    i += 3;
                    let name = bytecode[i - 2];
                    let args = bytecode[i - 1];
                    format!(
                        "{}| {:<16} {:>4} {:>4} {:<4}",
                        self.line_fmt(i - 3),
                        op.name(),
                        name,
                        self.const_at(name as usize),
                        args,
                    )
                }

                RawOpcode::MakeClosure | RawOpcode::MakeClosureLong => {
                    i += 2;
                    let slot = if op == RawOpcode::MakeClosure {
                        bytecode[i - 1] as u16
                    } else {
                        i += 1;
                        BE_from!(bytecode[i - 2..i])
                    };
                    let mut s = format!(
                        "{}| {:<16} {:>4} {:<4}",
                        self.line_fmt(i - 2),
                        op.name(),
                        slot,
                        self.const_at(slot as usize),
                    );
                    match self.const_at_and_then(slot as usize, |v| match v {
                        Value::Ptr(ptr) => Ok(ptr.as_ref().as_cco().n_captured),
                        _ => Err(format!(
                            "The constant at `{}` is not a function pointer: {:?}",
                            slot, v
                        )),
                    }) {
                        Ok(n_captured) => {
                            s.push_str(&format!(
                                "\n{}",
                                self.dis_upvalues(&mut i, n_captured, bytecode)
                            ));
                        }
                        Err(e) => {
                            s.push_str(&format!("\n{}", e));
                        }
                    }
                    s
                }
            };
            out.push_str(&format!("{}\n", value));
        }

        let nested = self
            .chunk
            .code_objects
            .iter()
            .flat_map(|ptr| {
                let s = ptr.as_ref().as_cco().disassemble();
                s.split('\n')
                    .map(|line| format!("\t{}", line))
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>()
            .join("\n");

        format!(
            "{}<nested cco objects: {}>{}",
            out,
            self.chunk.code_objects.len(),
            if nested.is_empty() {
                "".to_string()
            } else {
                format!("\n{}", nested)
            },
        )
    }
}
