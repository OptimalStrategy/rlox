pub mod encoded;
pub mod raw;

pub use self::encoded::EncodedDisassembler;
pub use self::raw::RawDisassembler;

pub trait Disassemble {
    fn disassemble(&self) -> String;
    fn dis_print(&self) {
        println!("{}", self.disassemble());
    }
}
