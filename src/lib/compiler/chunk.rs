use crate::allocator::DeepSizeOf;

use super::{
    bytecode::Bytecode,
    dis::{Disassemble, EncodedDisassembler, RawDisassembler},
    inline_cache::InlineCache,
    locations::Locations,
};
use crate::vm::object::Value;
use vm::object::ObjectPointer;

#[derive(Debug, Clone)]
pub struct Chunk {
    pub bytecode: Bytecode,
    /// These constants are guaranteed to be non-heap allocated, except for Strings.
    /// Constant strings are guaranteed to live until the VM terminates.
    pub constants: Vec<Value>,
    pub code_objects: Vec<ObjectPointer>,
    pub locations: Locations,
    /// A simple inline cache for vtable lookups
    pub inline_cache: InlineCache,
}

impl DeepSizeOf for Chunk {
    // This implementation shouldn't ever be used -- consider putting `unreachable!()` inside?
    fn deep_size_of_children(&self) -> usize {
        self.locations.deep_size_of_children()
            + self.bytecode.deep_size_of_children()
            + crate::allocator::deep_size_of::deep_size_of_vec(&self.constants)
            + crate::allocator::deep_size_of::deep_size_of_vec(&self.code_objects)
    }
}

impl Chunk {
    pub fn new<L>(
        bytecode: Bytecode,
        locations: L,
        constants: Vec<Value>,
        code_objects: Vec<ObjectPointer>,
    ) -> Self
    where
        L: Into<Locations>,
    {
        let cache_size = bytecode.raw_size_slow();
        Self {
            bytecode,
            locations: locations.into(),
            constants,
            code_objects,
            inline_cache: InlineCache::new(cache_size),
        }
    }

    pub fn empty() -> Self {
        Self::new(
            Bytecode::Encoded(Vec::with_capacity(0)),
            Locations::Raw(Vec::with_capacity(0)),
            Vec::with_capacity(0),
            Vec::with_capacity(0),
        )
    }

    #[inline(always)]
    pub fn const_at(&self, at: usize) -> Option<&Value> {
        self.constants.get(at)
    }

    pub fn dis(&self, name: Option<&str>) -> String {
        match self.bytecode {
            Bytecode::Encoded(_) => EncodedDisassembler::new(name, self).disassemble(),
            Bytecode::Decoded(_) => RawDisassembler::new(name, self).disassemble(),
            Bytecode::_Empty => String::from("<empty>"),
        }
    }
}

impl Disassemble for Chunk {
    fn disassemble(&self) -> String {
        self.dis(None)
    }
}
