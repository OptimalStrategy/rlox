use crate::allocator::DeepSizeOf;

use super::{chunk::Chunk, dis::Disassemble, version::BytecodeVersion};
use crate::{module::ModuleId, vm::native::CowStr, ParamsCountT as PC};

pub const ARITY_NONE: Arity = Arity::Fixed(0);

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Arity {
    /// A fixed arity value. May be used by Lox functions.
    Fixed(PC),
    /// A variable arity value. May be used by Lox functions.
    Variable,
    /// An arity value for functions that accept up to N arguments.
    /// May be used only by native functions.
    LessEqual(PC),
    /// An option arity, equivalent to LessEqual(1).
    /// May be used only by native functions.
    Optional,
    /// An arity value for function that expect from N to K arguments.
    /// May be used only by native functions.
    InRange(PC, PC),
}
known_deep_size_fast!(0, Arity);

impl Arity {
    pub fn check(&self, n: PC) -> bool {
        match self {
            Arity::Fixed(a) => *a == n,
            Arity::Variable => true,
            Arity::LessEqual(a) => n <= *a,
            Arity::Optional => n <= 1,
            Arity::InRange(low, high) => n >= *low && n <= *high,
        }
    }

    #[inline]
    pub fn is_variable(&self) -> bool {
        matches!(self, Arity::Variable)
    }
}

impl std::fmt::Display for Arity {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Fixed(n) => n.to_string(),
                Arity::LessEqual(n) => format!("{} or less", n),
                Arity::Optional => "0 or 1".to_string(),
                Arity::InRange(l, h) => format!("from {} to {}", l, h),
                Arity::Variable => "any".to_string(),
            }
        )
    }
}

#[derive(Clone)]
pub struct CompiledCodeObject {
    pub name: CowStr,
    pub chunk: Chunk,
    pub arity: Arity,
    pub n_captured: usize,
    pub module: ModuleId,
    pub version: BytecodeVersion,
}

impl CompiledCodeObject {
    pub fn new<S: Into<CowStr>>(
        name: S,
        chunk: Chunk,
        arity: Arity,
        n_captured: usize,
        module: ModuleId,
    ) -> Self {
        Self {
            name: name.into(),
            chunk,
            arity,
            n_captured,
            module,
            version: BytecodeVersion::default(),
        }
    }

    pub fn and_version(self, version: BytecodeVersion) -> Self {
        Self { version, ..self }
    }

    pub fn empty() -> Self {
        Self::new(
            CowStr::Borrowed("<unknown>"),
            Chunk::empty(),
            Arity::Fixed(0),
            0,
            ModuleId::anon(),
        )
    }

    pub fn n_instructions(&self) -> usize {
        match &self.chunk.bytecode {
            super::bytecode::Bytecode::Encoded(v) => v.len(),
            super::bytecode::Bytecode::Decoded(v) => v.len(),
            super::bytecode::Bytecode::_Empty => 0,
        }
    }
}

impl Disassemble for CompiledCodeObject {
    fn disassemble(&self) -> String {
        let dis = self.chunk.dis(Some(self.name.as_ref()));
        let header = format!(
            "== CodeObj `{}` [arity = {}, captured = {}] ==",
            if self.name.is_empty() {
                "__main__"
            } else {
                &self.name
            },
            self.arity,
            self.n_captured
        );
        format!("{}\n{}", header, dis)
    }
}

impl std::fmt::Debug for CompiledCodeObject {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            if f.alternate() {
                format!(
                    "CompiledCodeObject {{\n\tname: '{}',\n\tarity: {},\n\tn_captured: {},\n\tmodule: {:?}\n\tversion: {},\n\t...\n}}",
                    self.name, self.arity, self.n_captured, self.module, self.version
                )
            } else {
                format!(
                    "CompiledCodeObject {{ name: '{}', arity: {}, n_captured: {}, module: {:?}, version: {}, ... }}",
                    self.name, self.arity, self.n_captured, self.module, self.version
                )
            }
        )
    }
}

impl DeepSizeOf for CompiledCodeObject {
    #[inline(always)]
    fn deep_size_of_children(&self) -> usize {
        // CCOs are immutable and are allocated in the permanent space,
        // which means that the sizes of their heap allocations are irrelevant for the GC.
        // As a simple optimization, we return the sizes that actually matter -- the sizes of the CCO object itself and the possible-heap-allocated name.
        match &self.name {
            std::borrow::Cow::Borrowed(_) => 0,
            std::borrow::Cow::Owned(s) => s.capacity(),
        }
    }
}
