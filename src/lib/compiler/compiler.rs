use std::collections::HashMap;
use std::marker::PhantomData;
use ModuleId;

use compiler::locations::Locations;
use vm::immutable_string::hash;
use vm::object::ObjectPointer;

use super::{
    basic_block::{BasicBlocks, BlockOrJump, Jump},
    bytecode::{label::Label, Bytecode, OpCode},
    cco::{Arity, CompiledCodeObject},
    chunk::Chunk,
    dce::DeadCodeElimination,
    dis::Disassemble,
    peephole_optimizer,
};

use crate::{
    ast::*,
    err_context::ErrCtx,
    pipeline::IRPass,
    token::{Span, Token, TokenKind},
    vm::{
        immutable_string::ImmutableString,
        object::{LoxObject, Value},
        HeapValue,
    },
    CompilerConfig, LoxError, ParamsCountT, SharedCtx,
};

#[derive(Debug, Clone)]
pub struct Compiler<'a> {
    ctx: SharedCtx,
    config: CompilerConfig,
    scope_depth: usize,
    loop_scope_depth: usize,
    labels: Vec<Label>,
    locals: Vec<Local<'a>>,
    upvalues: Vec<UpValue>,
    /// Using a pointer for this is safe because inner compilers are created by a parent compiler
    /// temporarily and therefore can't outlive it.
    enclosing: Option<*mut Compiler<'a>>,
    /// The `break` and `continue` statements in the scope.
    loop_flow: Vec<LoopFlow>,
    /// The id of the current loop.
    loop_id: usize,
    globals: HashMap<String, usize>,
    blocks: BasicBlocks,
    constants: Vec<Value>,
    used_consts: HashMap<(u8, u64), usize>,
    code_objects: Vec<ObjectPointer>,
    resulting_cco: ObjectPointer,
    // The address of the Ok builtin.
    err_address: Option<u8>,
    // peephole_optimizer: PeepholeOptimizer,
    _marker: PhantomData<&'a ()>,
}

impl<'a> Compiler<'a> {
    pub fn new(ctx: SharedCtx) -> Self {
        let cco = CompiledCodeObject::empty();
        let (config, resulting_cco, err_address) = {
            let mut guard = ctx.write_shared();
            (
                guard.config.clone(),
                guard.alloc_without_gc_trigger(LoxObject::new(cco)),
                guard.builtins().name_to_hash().get("Err").copied(),
            )
        };
        Self {
            ctx,
            config,
            scope_depth: 0,
            loop_scope_depth: 0,
            enclosing: None,
            labels: vec![],
            blocks: BasicBlocks::new(),
            constants: vec![],
            used_consts: HashMap::new(),
            locals: vec![],
            upvalues: vec![],
            globals: HashMap::new(),
            loop_flow: vec![],
            loop_id: 0,
            code_objects: vec![],
            resulting_cco,
            err_address,
            _marker: PhantomData,
        }
    }

    pub fn compile(
        mut self,
        ast: AST<'a>,
        name: String,
        arity: Arity,
    ) -> Result<ObjectPointer, ErrCtx> {
        self._compile_internal(ast, name, arity, false)
    }

    pub fn compile_in_repl_mode(
        &mut self,
        ast: AST<'a>,
        name: String,
        arity: Arity,
    ) -> Result<ObjectPointer, ErrCtx> {
        if let Some(block) = self.blocks.last_block_mut() {
            if let Some(&[OpCode::MakeModule(_), OpCode::Return]) =
                block.bytecode.get(block.bytecode.len().wrapping_sub(2)..)
            {
                // Pop the Return opcode
                block.bytecode.pop();
                block.locations.pop();
                // Pop the MakeModule opcode
                block.bytecode.pop();
                block.locations.pop();
            }
        }
        let result = self._compile_internal(ast, name, arity, true);
        for l in &mut self.locals {
            l.make_owned();
        }
        result
    }

    fn _compile_internal(
        &mut self,
        ast: AST<'a>,
        name: String,
        arity: Arity,
        repl_mode: bool,
    ) -> Result<ObjectPointer, ErrCtx> {
        let mut ex = ErrCtx::with_local_module(ast.module);
        for stmt in &ast.body {
            match self.compile_stmt(stmt, &mut ex) {
                Ok(_) => (),
                Err(e) => ex.record(e),
            }
        }

        if let Some(stmt) = ast.body.last() {
            let ptr = self
                .ctx
                .read_shared()
                .get_module(ast.module)
                .unwrap()
                .lox_module()
                .unwrap();
            let idx = self.store_const(Value::Ptr(ptr));
            if idx >= 65536 {
                ex.record(LoxError::compile(
                    format!("Too many globals to export: {} > 65535", idx),
                    stmt.span,
                ));
            }
            self.instrument_timeout(stmt.span);
            self.emit(OpCode::MakeModule(idx as _), stmt.span);
            self.blocks.emit(OpCode::Return, stmt.span);
        }
        if let Err(e) = self.patch_cco(ast.module, name, arity, repl_mode, &mut ex) {
            ex.record(e);
        }

        if ex.had_error {
            return Err(ex);
        }

        {
            let mut guard = self.ctx.write_shared();
            guard.ex.extend_from_another_ex(ex);

            let module = guard.get_module_mut(ast.module).unwrap();
            module.set_cco(self.resulting_cco);

            let globals = self.globals.clone();
            unsafe { module.lox_module_object_mut() }
                .unwrap()
                .set_export_indices(
                    globals
                        .into_iter()
                        .map(|(k, v)| (k, Value::Int(v as _)))
                        .collect(),
                );
        }
        Ok(self.resulting_cco)
    }

    pub fn clone_cco(&mut self) {
        let cco = self.cco();
        let new_cco = cco.clone();
        let new_cco = self
            .ctx
            .write_shared()
            .alloc_without_gc_trigger(LoxObject::new(new_cco));
        self.resulting_cco = new_cco;
    }

    #[inline]
    pub fn n_globals(&self) -> usize {
        self.globals.len()
    }

    #[inline(always)]
    fn cco(&mut self) -> &mut CompiledCodeObject {
        self.resulting_cco.as_mut().as_cco_mut()
    }

    fn patch_cco(
        &mut self,
        module: ModuleId,
        name: String,
        arity: Arity,
        repl_mode: bool,
        ex: &mut ErrCtx,
    ) -> Result<(), LoxError> {
        self.optional_dce(&name, module);

        if self.ctx.read_shared().config.dis_all {
            self.patch(name.clone(), module, arity, false).unwrap();
            let _ = ex.maybe_record(self.patch(name, module, arity, true).map(|_| {
                if self.enclosing.is_none() {
                    self.cco().dis_print()
                }
            }));
        } else {
            let mut cco = self.resulting_cco;
            let n_captured = self.upvalues.len();
            let (constants, code_objects, blocks) = if repl_mode {
                (
                    self.constants.clone(),
                    self.code_objects.clone(),
                    self.blocks.clone(),
                )
            } else {
                (
                    std::mem::take(&mut self.constants),
                    std::mem::take(&mut self.code_objects),
                    std::mem::take(&mut self.blocks),
                )
            };
            if let Some((bytecode, locations)) = ex.maybe_record(blocks.into_raw_bytecode()) {
                Self::patch_fast(
                    cco.as_mut().as_cco_mut(),
                    name,
                    module,
                    arity,
                    n_captured,
                    Bytecode::Decoded(bytecode),
                    locations,
                    constants,
                    code_objects,
                );
            }
        }
        Ok(())
    }

    fn optional_dce(&mut self, name: &str, module: ModuleId) {
        let (export_cfg, do_dce) = {
            let guard = self.ctx.read_shared();
            let export_cfg = guard.config.export_cfg;
            let do_dce = guard.config.opt_config.dead_code_elimination;
            (export_cfg, do_dce)
        };

        let build_dir = if export_cfg {
            self.ctx.write_shared().init_module_cache(module)
        } else {
            None
        };
        if do_dce {
            if let Err(e) =
                DeadCodeElimination::perform_dce(&mut self.blocks, build_dir.as_deref(), name)
            {
                eprintln!("[WARNING] Could not export the CFG for {}: {}", name, e);
            }
        }
    }

    fn patch(
        &mut self,
        name: String,
        module: ModuleId,
        arity: Arity,
        raw: bool,
    ) -> Result<(), LoxError> {
        // TODO: get rid of the clones
        let (bytecode, locations) = if raw {
            let (bytecode, locations) = self.blocks.clone().into_raw_bytecode()?;
            (Bytecode::Decoded(bytecode), locations)
        } else {
            let (bytecode, locations) = self.blocks.clone().into_bytecode();
            (Bytecode::Encoded(bytecode), locations)
        };
        let constants = self.constants.clone();
        let code_objects = self.code_objects.clone();
        let n_captured = self.upvalues.len();
        Self::patch_fast(
            self.cco(),
            name,
            module,
            arity,
            n_captured,
            bytecode,
            locations,
            constants,
            code_objects,
        );
        Ok(())
    }

    #[allow(clippy::too_many_arguments)]
    fn patch_fast(
        cco: &mut CompiledCodeObject,
        name: String,
        module: ModuleId,
        arity: Arity,
        n_captured: usize,
        bytecode: Bytecode,
        locations: Locations,
        constants: Vec<Value>,
        code_objects: Vec<ObjectPointer>,
    ) {
        cco.name = name.into();
        cco.module = module;
        cco.arity = arity;
        cco.n_captured = n_captured;
        cco.chunk = Chunk::new(bytecode, locations, constants, code_objects);
    }

    fn compile_stmt(&mut self, stmt: &Stmt<'a>, ex: &mut ErrCtx) -> Result<(), LoxError> {
        match &stmt.kind {
            StmtKind::ExprStmt(expr) => {
                self.compile_expr(expr, ex)?;
                self.emit_pop(expr.span);
            }
            StmtKind::Print(ref expr) => {
                let mut comma = expr;
                let mut n_on_stack = 1;

                while let ExprKind::Comma(left, right) = &comma.kind {
                    self.compile_expr(left, ex)?;
                    n_on_stack += 1;
                    comma = right;
                }
                self.compile_expr(comma, ex)?;

                if n_on_stack == 1 {
                    self.emit(OpCode::Print, stmt.span);
                } else if n_on_stack < 256 {
                    self.emit(OpCode::PrintN(n_on_stack as u8), stmt.span);
                } else {
                    return Err(LoxError::compile(
                        format!(
                            "Cannot print more than 256 values at once, got {}",
                            n_on_stack
                        ),
                        stmt.span,
                    ));
                }
            }
            StmtKind::Var(ref vars) => {
                for var in vars {
                    self.compile_var_stmt(&var.node, var.span, ex)?;
                }
            }
            StmtKind::Block(statements) => {
                self.compile_block(statements, stmt.span, ex)?;
            }
            StmtKind::For(r#for) => {
                self.compile_for_stmt(r#for, stmt.span, ex)?;
            }
            StmtKind::ForEach(r#for) => {
                self.compile_for_each_stmt(r#for, stmt.span, ex)?;
            }
            StmtKind::While(ref cond, ref body) => {
                self.compile_while_stmt(cond, body, stmt.span, ex)?;
            }
            StmtKind::If(r#if) => {
                self.compile_if_stmt(r#if, stmt.span, ex)?;
            }
            StmtKind::Return(ref r) => {
                if let Some(ref expr) = r {
                    self.compile_expr(expr, ex)?;
                } else {
                    self.emit(OpCode::Nil, stmt.span);
                }
                self.emit(OpCode::Return, stmt.span);
            }
            StmtKind::Break => {
                let n_locals = self.count_locals_in_scope(self.loop_scope_depth);
                let jump_index = self
                    .emit_pop_many(n_locals, stmt.span)?
                    .emit_jump(OpCode::Jump(Label::FlowLabel), stmt.span);
                self.loop_flow.push(LoopFlow {
                    jump_index,
                    loop_id: self.loop_id,
                });
            }
            StmtKind::Continue => {
                let n_locals = self.count_locals_in_scope(self.loop_scope_depth);
                let jump_index = self
                    .emit_pop_many(n_locals, stmt.span)?
                    .emit_jump(OpCode::JumpBack(Label::FlowLabel), stmt.span);
                self.loop_flow.push(LoopFlow {
                    jump_index,
                    loop_id: self.loop_id,
                });
            }
            StmtKind::Function(ref r#fn) => {
                self.compile_function(r#fn, stmt.span, ex)?;
            }
            StmtKind::Class(cls) => {
                let already_exists = self.add_local(&cls.name)?;
                let s = self.store_str(cls.name.lexeme.to_owned());
                let index = self.store_const(s);
                self.emit_choose_inst(
                    index,
                    OpCode::MakeClass(index as u8),
                    OpCode::MakeClassLong(index as u16),
                    stmt.span,
                )?;
                if let Some(super_cls) = &cls.superclass {
                    self.compile_expr(super_cls, ex)?;
                    self.emit(OpCode::Inherit, super_cls.span);
                }
                self.define_variable(&cls.name, Some(already_exists))?;
                if let Some(super_cls) = &cls.superclass {
                    self.begin_scope();
                    self.add_local(&Token::with_span(TokenKind::Super, "super", super_cls.span))?;
                }

                self.begin_scope();

                if !cls.methods.is_empty() {
                    // Load the class object on the stack
                    compile_var!(self, ex, cls.name.clone())?;

                    for method in cls.methods.iter() {
                        // We compile methods as lambdas to avoid reserving local slots
                        match self.compile_lambda(method, method.name.span, None, ex) {
                            Ok(_) => {
                                self.emit(OpCode::MakeMethod, method.name.span);
                            }
                            Err(e) => ex.record(e),
                        }
                    }

                    self.emit_pop(cls.name.span);
                }

                self.end_scope(cls.name.span)?;
                if let Some(super_cls) = &cls.superclass {
                    self.end_scope(super_cls.span)?;
                }
            }
            StmtKind::Import(i) => {
                let package_name = i
                    .alias
                    .as_ref()
                    .unwrap_or_else(|| i.package.last().unwrap());
                self.define_variable(package_name, None)?;
                let idx = self.store_const(Value::Int(i.module.0 as _)) as u16;
                self.emit(OpCode::Import(idx), i.package.last().unwrap().span);

                for binding in &i.bindings {
                    let expr = Expr {
                        kind: ExprKind::GetAttr(Box::new(crate::ast::GetAttr {
                            node: Expr {
                                kind: ExprKind::Variable(package_name.clone()),
                                span: binding.export.span,
                            },
                            field: binding.export.clone(),
                        })),
                        span: binding.export.span,
                    };
                    self.compile_expr(&expr, ex)?;
                    self.define_variable(&binding.bind_to, None)?;
                }
            }
            StmtKind::_Empty => {}
        }
        Ok(())
    }

    fn compile_block(
        &mut self,
        block: &[Stmt<'a>],
        span: Span,
        ex: &mut ErrCtx,
    ) -> Result<(), LoxError> {
        self.begin_scope();
        for stmt in block {
            match self.compile_stmt(stmt, ex) {
                Ok(_) => (),
                Err(e) => ex.record(e),
            }
        }
        self.end_scope(span)?;
        Ok(())
    }

    fn compile_function(
        &mut self,
        r#fn: &FnInfo<'a>,
        span: Span,
        ex: &mut ErrCtx,
    ) -> Result<(), LoxError> {
        // Reserve a local slot for the function
        let slot = self.emit_const(Value::Nil, span)?;
        self.define_variable(&r#fn.name, None)?;

        // The compiled cco is assigned to the reserved slot in compile_lambda()
        self.compile_lambda(r#fn, span, Some(slot), ex)
    }

    fn compile_lambda(
        &mut self,
        r#fn: &FnInfo<'a>,
        span: Span,
        reserved_slot: Option<usize>,
        ex: &mut ErrCtx,
    ) -> Result<(), LoxError> {
        let mut c = Self::new(self.ctx.clone());
        c.enclosing = Some(self as *mut _);

        for param in &r#fn.params {
            c.add_local(param)?;
        }

        if r#fn.is_method() {
            let this = Token::with_span(TokenKind::This, "this", span);
            c.add_local(&this)?;
        }

        c.instrument_timeout(span);
        c.compile_block(&r#fn.body, span, ex)?;

        // Compile `return nil` only if the last opcode is not a Return.
        if !c.blocks.last_opcode_is_return() {
            c.emit(OpCode::Nil, span);
            c.emit(OpCode::Return, span);
        }

        c.patch_cco(
            r#fn.name.module,
            r#fn.name.lexeme.to_owned(),
            match &r#fn.params {
                Params::Params(v) => Arity::Fixed(v.len() as ParamsCountT),
                Params::VarArgs(_) => Arity::Variable,
            },
            false,
            ex,
        )?;

        {
            self.code_objects.push(c.resulting_cco);
            let cco = c.cco();

            if cco.n_captured > 0 {
                let slot = match reserved_slot {
                    Some(slot) => {
                        self.constants[slot] = Value::Ptr(c.resulting_cco);
                        slot
                    }
                    None => self.store_const(Value::Ptr(c.resulting_cco)),
                };
                if slot < 256 {
                    self.emit(OpCode::MakeClosure(slot as u8, c.upvalues), span);
                } else if slot < 65536 {
                    self.emit(OpCode::MakeClosureLong(slot as u16, c.upvalues), span);
                } else {
                    return Err(LoxError::compile(
                        format!("Cannot capture more than 65536 variables: {}", slot),
                        r#fn.name.span,
                    ));
                }
                if reserved_slot.is_some() {
                    self.emit_set_local(r#fn.name.lexeme, r#fn.name.span)?;
                    self.emit_pop(r#fn.name.span);
                }
            } else {
                match reserved_slot {
                    Some(slot) => {
                        self.constants[slot] = Value::Ptr(c.resulting_cco);
                    }
                    None => {
                        self.emit_const(Value::Ptr(c.resulting_cco), r#fn.name.span)?;
                    }
                }
            }
        }

        Ok(())
    }

    fn compile_var_stmt(
        &mut self,
        var: &Var<'a>,
        span: Span,
        ex: &mut ErrCtx,
    ) -> Result<(), LoxError> {
        if let Some(ref init) = var.initializer {
            self.compile_expr(init, ex)?;
        } else {
            self.emit(OpCode::Nil, span);
        }
        self.define_variable(&var.name, None)
    }

    fn define_variable(&mut self, name: &Token<'a>, exists: Option<bool>) -> Result<(), LoxError> {
        let exists = if let Some(exists) = exists {
            exists
        } else {
            self.add_local(name)?
        };

        // Reuse the stack slot if a global variable with
        // this name has already been declared.
        if exists {
            self.emit_set_local(name.lexeme, name.span)?;
            self.emit_pop(name.span);
        } else if self.scope_depth == 0 {
            // If the stack slot for this global has not been used before,
            // we map the global's name to the slot.
            self.globals
                .insert(name.lexeme.to_owned(), self.globals.len());
        }

        Ok(())
    }

    fn compile_while_stmt(
        &mut self,
        condition: &Expr<'a>,
        body: &Stmt<'a>,
        span: Span,
        ex: &mut ErrCtx,
    ) -> Result<(), LoxError> {
        self.begin_loop();

        let loop_start_label = self.emit_label(span);
        self.compile_expr(condition, ex)?;

        let body_end_label = self.reserve_label();
        self.emit_jump(OpCode::JumpIfFalse(body_end_label), span);
        self.emit_pop(span);

        loop_scope!(self, span, self.compile_stmt(body, ex)?)?;

        self.emit_jump(OpCode::JumpBack(loop_start_label), span);
        self.emit_reserved_label(body_end_label, span);
        self.emit_pop(span);

        let loop_end_label = self.emit_label(span);
        self.patch_loop_control_flow(loop_start_label, loop_end_label);

        self.end_loop();
        Ok(())
    }

    fn compile_for_stmt(
        &mut self,
        stmt: &For<'a>,
        span: Span,
        ex: &mut ErrCtx,
    ) -> Result<(), LoxError> {
        self.begin_loop();

        if let Some(ref init) = stmt.initializer {
            // we need an extra scope for the initializer to ensure that the variables
            // are not popped off by continue statements.
            self.begin_scope();
            self.compile_stmt(init, ex)?;
        }

        let mut loop_start_label = self.emit_label(span);
        if let Some(ref cond) = stmt.condition {
            self.compile_expr(cond, ex)?;
        } else {
            self.emit(OpCode::True, span);
        }

        let body_end_label = self.reserve_label();
        self.emit_jump(OpCode::JumpIfFalse(body_end_label), span);
        self.emit_pop(span);

        if let Some(ref inc) = stmt.increment {
            let body_start_label = self.reserve_label();
            self.emit_jump(OpCode::Jump(body_start_label), span);
            let increment_start_label = self.emit_label(span);

            self.compile_expr(inc, ex)?;
            self.emit_pop(inc.span);

            self.emit_jump(OpCode::JumpBack(loop_start_label), span);
            loop_start_label = increment_start_label;
            self.emit_reserved_label(body_start_label, span);
        }

        loop_scope!(self, span, self.compile_stmt(&stmt.body, ex)?)?;

        self.emit_jump(OpCode::JumpBack(loop_start_label), span);
        self.emit_reserved_label(body_end_label, span);
        self.emit_pop(span);

        let loop_end_label = self.emit_label(span);
        self.patch_loop_control_flow(loop_start_label, loop_end_label);

        if stmt.initializer.is_some() {
            self.end_scope(span)?;
        }

        self.end_loop();
        Ok(())
    }

    fn compile_for_each_stmt(
        &mut self,
        stmt: &ForEach<'a>,
        span: Span,
        ex: &mut ErrCtx,
    ) -> Result<(), LoxError> {
        self.begin_loop();
        self.begin_scope();

        let iterator = Token::with_span(
            TokenKind::Identifier,
            {
                // SAFETY: the returned reference is guaranteed to live as long as the context
                let lifetime_fix: &'a str = unsafe {
                    std::mem::transmute(
                        self.ctx
                            .write_shared()
                            .intern(format!("[iterator@{}:{}", span.line, span.column)),
                    )
                };
                lifetime_fix
            },
            span,
        );

        // Compile the iterator object
        self.compile_expr(&stmt.iterator, ex)?;
        self.emit(OpCode::IntoIter, span);
        self.add_local(&iterator)?;

        // Reserve a local slot for the loop variable
        self.emit(OpCode::Nil, stmt.variable.span);
        self.add_local(&stmt.variable)?;

        let loop_start_label = self.emit_label(stmt.variable.span);

        // Call `iternext()` on the iterator object and set the loop variable
        self.compile_expr(
            &Expr {
                kind: ExprKind::Variable(iterator),
                span,
            },
            ex,
        )?;
        self.emit(OpCode::IterNext, stmt.variable.span);
        // Unwrap the Ok value if the Ok/Err iterator semantics are used
        if self.err_address.is_some() {
            self.emit(OpCode::Unwrap, stmt.variable.span);
        }
        self.emit_set_local(stmt.variable.lexeme, stmt.variable.span)?;

        // Assume the Ok/Err iterator semantics if err is available
        if let Some(err) = self.err_address {
            // Check if the iterator returned a non-err value
            self.emit(OpCode::LoadNativeFn(err), stmt.variable.span);
            self.emit(OpCode::CmpTypeNot, stmt.variable.span);
        // Otherwise, assume the non-nil/nil semantics
        } else {
            // Check if the iterator returned a non-nil value
            self.emit(OpCode::IsNotNil, stmt.variable.span);
        }

        let body_end_label = self.reserve_label();
        self.emit_jump(OpCode::JumpIfFalse(body_end_label), span);
        // Pop off the loop variable
        self.emit_pop(stmt.variable.span);

        loop_scope!(self, span, self.compile_stmt(&stmt.body, ex)?)?;
        self.emit_jump(OpCode::JumpBack(loop_start_label), stmt.variable.span);

        self.emit_reserved_label(body_end_label, stmt.variable.span);
        self.emit_pop(stmt.variable.span);

        let loop_end_label = self.emit_label(stmt.variable.span);
        self.patch_loop_control_flow(loop_start_label, loop_end_label);

        self.end_scope(span)?;
        self.end_loop();

        Ok(())
    }

    fn instrument_timeout(&mut self, span: Span) {
        if self.config.execution_timeout.is_some() {
            self.emit(OpCode::TimeoutCheck, span);
        }
    }

    fn compile_if_stmt(
        &mut self,
        stmt: &If<'a>,
        span: Span,
        ex: &mut ErrCtx,
    ) -> Result<(), LoxError> {
        self.compile_expr(&stmt.condition, ex)?;

        let else_start_label = self.reserve_label();
        // Check whether the condition is true and jump to the else branch
        self.emit_jump(OpCode::JumpIfFalse(else_start_label), span);
        // Pop the condition off the stack if the check succeeds
        self.emit_pop(span);

        // Compile the if body
        self.compile_stmt(&stmt.then, ex)?;

        // Jump over the else branch
        let else_end_label = self.reserve_label();
        self.emit_jump(OpCode::Jump(else_end_label), span);

        self.emit_reserved_label(else_start_label, span);
        self.emit_pop(span);

        // Compile the else branch if it exists
        if let Some(branch) = &stmt.otherwise {
            self.compile_stmt(branch, ex)?;
        }

        self.emit_reserved_label(else_end_label, span);
        Ok(())
    }

    fn store_str(&mut self, s: String) -> Value {
        let s = ImmutableString::new_with_hash(s).and_indices();
        if let Some(idx) = self.used_consts.get(&(3, s.hash_expensive())) {
            return *self.constants.get(*idx).unwrap();
        }
        let ptr = if s.as_ref().len() <= self.config.interning_threshold as usize {
            *self.ctx.write_shared().intern_ptr(s.into_string())
        } else {
            self.ctx
                .write_shared()
                .alloc_without_gc_trigger(LoxObject::new(HeapValue::Str(Box::new(s))))
        };
        Value::Ptr(ptr)
    }

    fn compile_expr(&mut self, expr: &Expr<'a>, ex: &mut ErrCtx) -> Result<(), LoxError> {
        match &expr.kind {
            ExprKind::Binary(op, left, right) => {
                self.compile_expr(left, ex)?;
                let opcode = match op.node {
                    BinOpKind::Add => OpCode::Add,
                    BinOpKind::Sub => OpCode::Sub,
                    BinOpKind::Mul => OpCode::Mul,
                    BinOpKind::Pow => OpCode::Pow,
                    BinOpKind::Div => OpCode::Div,
                    BinOpKind::Rem => OpCode::Mod,
                    BinOpKind::Eq => OpCode::Cmp,
                    BinOpKind::Ne => OpCode::CmpNot,
                    BinOpKind::Lt => OpCode::LessThan,
                    BinOpKind::Le => OpCode::LessEqual,
                    BinOpKind::Ge => OpCode::GreaterEqual,
                    BinOpKind::Gt => OpCode::GreaterThan,
                    BinOpKind::And => {
                        let end_label = self.reserve_label();
                        self.emit_jump(OpCode::JumpIfFalse(end_label), op.span);
                        self.emit_pop(op.span);
                        self.compile_expr(right, ex)?;
                        self.emit_reserved_label(end_label, op.span);
                        return Ok(());
                    }
                    BinOpKind::Or => {
                        let else_label = self.reserve_label();
                        let end_label = self.reserve_label();
                        self.emit_jump(OpCode::JumpIfFalse(else_label), op.span);
                        self.emit_jump(OpCode::Jump(end_label), op.span);
                        self.emit_reserved_label(else_label, op.span);
                        self.emit_pop(op.span);
                        self.compile_expr(right, ex)?;
                        self.emit_reserved_label(end_label, op.span);
                        return Ok(());
                    }
                };
                self.compile_expr(right, ex)?;
                self.emit(opcode, op.span);
            }
            ExprKind::Unary(op, operand) => {
                self.compile_expr(operand, ex)?;
                let opcode = match op.node {
                    UnOpKind::Not => OpCode::Not,
                    UnOpKind::Neg => OpCode::Negate,
                    UnOpKind::Plus => return Ok(()),
                };
                self.emit(opcode, op.span);
            }
            ExprKind::Lit(ref lit) => match lit.value.clone() {
                LitValue::Bool(b) => {
                    self.emit(if b { OpCode::True } else { OpCode::False }, lit.token.span);
                }
                LitValue::Nil => {
                    self.emit(OpCode::Nil, lit.token.span);
                }
                LitValue::Str(v) => {
                    let value = self.store_str(v);
                    self.emit_const(value, lit.token.span)?;
                }
                LitValue::Integer(v) => match v {
                    0 => {
                        self.emit(OpCode::Zero, lit.token.span);
                    }
                    1 => {
                        self.emit(OpCode::One, lit.token.span);
                    }
                    _ => {
                        self.emit_const(Value::from(v), lit.token.span)?;
                    }
                },
                LitValue::Float(v) => {
                    self.emit_const(Value::from(v), lit.token.span)?;
                }
            },
            ExprKind::Comma(left, right) => {
                let mut n_on_stack = 1;
                self.compile_expr(left, ex)?;

                let mut comma = right;
                while let ExprKind::Comma(left, right) = &comma.kind {
                    self.compile_expr(left, ex)?;
                    n_on_stack += 1;
                    comma = right;
                }

                self.emit_pop_many(n_on_stack, expr.span)?;
                self.compile_expr(comma, ex)?;
            }
            ExprKind::ArgForwarding(expr) => self.compile_expr(expr, ex)?,
            ExprKind::Call {
                callee,
                args,
                tco: tco_eligible,
                fwd: has_fwd_args,
            } => {
                // Emit InvokeMethod if the callee is a field access expression.
                let instruction = match &callee.kind {
                    ExprKind::GetAttr(box GetAttr { node, field }) => {
                        self.compile_expr(node, ex)?;
                        let s = self.store_str(field.lexeme.to_owned());
                        let idx = self.store_const(s);
                        if idx < 256 {
                            if *tco_eligible && self.config.opt_config.tail_call_optimization {
                                OpCode::InvokeMethodTCO(idx as u8, args.node.len() as u8)
                            } else {
                                OpCode::InvokeMethod(idx as u8, args.node.len() as u8)
                            }
                        } else {
                            self.emit(OpCode::GetFieldLong(idx as u16), expr.span);
                            OpCode::FnCall(args.node.len() as u8)
                        }
                    }
                    _ => {
                        self.compile_expr(callee, ex)?;
                        if *tco_eligible && self.config.opt_config.tail_call_optimization {
                            OpCode::TCOCall(args.node.len() as u8)
                        } else {
                            OpCode::FnCall(args.node.len() as u8)
                        }
                    }
                };
                if *has_fwd_args {
                    let mut fwd_count = 0;
                    for arg in &args.node {
                        if let ExprKind::ArgForwarding(_) = arg.kind {
                            fwd_count += 1
                        }
                        self.compile_expr(arg, ex)?;
                    }

                    // We leave 4 bits per argument for the arg counts
                    let normal_count = args.node.len() - fwd_count;
                    if fwd_count > 16 || normal_count > 16 {
                        return Err(LoxError::compile(
                            format!(
                                "Too many rest or forwarded arguments : {} > 16 and/or {} > 16",
                                fwd_count, normal_count
                            ),
                            expr.span,
                        ));
                    }

                    let count = (fwd_count as u8) << 4 | (normal_count as u8);
                    self.emit(OpCode::FwdCall(count), args.span);
                } else {
                    for arg in &args.node {
                        self.compile_expr(arg, ex)?;
                    }
                    self.emit(instruction, args.span);
                }
            }
            ExprKind::GetAttr(ref get) => {
                self.compile_expr(&get.node, ex)?;
                let s = self.store_str(get.field.lexeme.to_owned());
                let idx = self.store_const(s);
                self.emit_choose_inst(
                    idx,
                    OpCode::GetField(idx as u8),
                    OpCode::GetFieldLong(idx as u16),
                    expr.span,
                )?;
            }
            ExprKind::SetAttr(ref set) => {
                self.compile_expr(&set.value, ex)?;
                self.compile_expr(&set.node, ex)?;
                let s = self.store_str(set.field.lexeme.to_owned());
                let idx = self.store_const(s);
                self.emit_choose_inst(
                    idx,
                    OpCode::SetField(idx as u8),
                    OpCode::SetFieldLong(idx as u16),
                    expr.span,
                )?;
            }
            ExprKind::GetItem(ref obj, ref item) => {
                self.compile_expr(obj, ex)?;
                self.compile_expr(item, ex)?;
                self.emit(OpCode::GetItem, expr.span);
            }
            ExprKind::SetItem(ref set) => {
                self.compile_expr(&set.node, ex)?;
                self.compile_expr(&set.key, ex)?;
                self.compile_expr(&set.value, ex)?;
                self.emit(OpCode::SetItem, expr.span);
            }
            ExprKind::InterStr(s) => {
                for frag in &s.fragments {
                    match frag {
                        InterFrag::Str(l) => {
                            let v = self.store_str(l.value.clone().into_string());
                            self.emit_const(v, l.token.span)?;
                        }
                        InterFrag::Expr(expr) => {
                            self.compile_expr(expr, ex)?;
                        }
                    }
                }
                let n = s.fragments.len();
                if n < 256 {
                    self.emit(OpCode::Interpolate(n as u8), expr.span);
                } else if n < 65536 {
                    self.emit(OpCode::InterpolateLong(n as u16), expr.span);
                } else {
                    return Err(LoxError::compile(
                        format!("Too many string interpolation fragments: {} (max 65536)", n),
                        expr.span,
                    ));
                }
            }
            ExprKind::Array(elems) => {
                for e in elems {
                    self.compile_expr(e, ex)?;
                }
                if elems.len() > 256 {
                    return Err(LoxError::compile(
                        format!(
                            "Too many elements in the array initializer: {} while max is 256",
                            elems.len()
                        ),
                        expr.span,
                    ));
                }
                self.emit(OpCode::MakeArr(elems.len() as u8), expr.span);
            }
            ExprKind::Dict(pairs) => {
                for (k, v) in pairs {
                    self.compile_expr(k, ex)?;
                    self.compile_expr(v, ex)?;
                }
                self.emit(OpCode::MakeDict(pairs.len() as u8), expr.span);
            }
            ExprKind::This => {
                self.emit_get_local("this", expr.span)?;
            }
            ExprKind::Super(s) => {
                self.emit_get_local("this", expr.span)?;
                self.emit_get_local("super", expr.span)?;
                let val = self.store_str(s.method.lexeme.to_owned());
                let idx = self.store_const(val);
                self.emit_choose_inst(
                    idx,
                    OpCode::GetSuper(idx as u8),
                    OpCode::GetSuperLong(idx as u16),
                    s.method.span,
                )?;
            }
            ExprKind::Ternary(ref ternary) => {
                self.compile_ternary_expr(ternary, expr.span, ex)?;
            }
            ExprKind::Match(ref m) => {
                self.compile_match_expr(m, expr.span, ex)?;
            }
            ExprKind::Variable(name) => {
                if name.kind == TokenKind::MatchBoundVar {
                    self.emit(OpCode::MatchGetBound, name.span);
                    return Ok(());
                }
                self.emit_get_local(name.lexeme, name.span)?;
            }
            ExprKind::PrefixIncDec(inc) => {
                let arg = self.resolve_local(inc.name.lexeme);
                match arg {
                    // Inc/Dec opcodes can only operate on local variables.
                    // In the case of an upvalue, we compile the increment/decrement into an add/sub instruction.
                    LocalResolutionResult::NotFound => {
                        // Either resolves to an upvalue load or fails.
                        self.compile_expr(
                            &Expr {
                                kind: ExprKind::Variable(inc.name.clone()),
                                span: inc.name.span,
                            },
                            ex,
                        )?;
                        self.emit(OpCode::One, inc.name.span);
                        self.emit(
                            match inc.kind {
                                IncDecKind::Increment => OpCode::Add,
                                IncDecKind::Decrement => OpCode::Sub,
                            },
                            inc.name.span,
                        );
                        self.emit_set_local(inc.name.lexeme, inc.name.span)?;
                    }
                    LocalResolutionResult::FoundLocal(idx) => {
                        if let IncDecKind::Increment = inc.kind {
                            self.emit_choose_inst(
                                idx,
                                OpCode::Inc(idx as u8),
                                OpCode::IncLong(idx as u16),
                                expr.span,
                            )?;
                        } else {
                            self.emit_choose_inst(
                                idx,
                                OpCode::Dec(idx as u8),
                                OpCode::DecLong(idx as u16),
                                expr.span,
                            )?;
                        }
                    }
                    LocalResolutionResult::FoundUpValue(_) => unreachable!(),
                }
            }
            ExprKind::PostfixIncDec(inc) => {
                let arg = self.resolve_local(inc.name.lexeme);
                let expr = Expr {
                    kind: ExprKind::Variable(inc.name.clone()),
                    span: inc.name.span,
                };
                match arg {
                    // Inc/Dec opcodes can only operate on local variables.
                    // In the case of an upvalue, we compile the increment/decrement into an add/sub instruction.
                    LocalResolutionResult::NotFound => {
                        // Load the variable twice so we can have the unmodified value
                        // on the stack at the end of the operation.
                        // The loads either resolve to an upvalue load or fail to compile.
                        self.compile_expr(&expr, ex)?;
                        self.compile_expr(&expr, ex)?;
                        self.emit(OpCode::One, inc.name.span);
                        self.emit(
                            match inc.kind {
                                IncDecKind::Increment => OpCode::Add,
                                IncDecKind::Decrement => OpCode::Sub,
                            },
                            inc.name.span,
                        );
                        self.emit_set_local(inc.name.lexeme, inc.name.span)?;
                        self.emit_pop(inc.name.span); // pop the SetLocal result
                    }
                    LocalResolutionResult::FoundLocal(idx) => {
                        self.compile_expr(&expr, ex)?;
                        if let IncDecKind::Increment = inc.kind {
                            self.emit_choose_inst(
                                idx,
                                OpCode::Inc(idx as u8),
                                OpCode::IncLong(idx as u16),
                                expr.span,
                            )?;
                        } else {
                            self.emit_choose_inst(
                                idx,
                                OpCode::Dec(idx as u8),
                                OpCode::DecLong(idx as u16),
                                expr.span,
                            )?;
                        }
                        self.emit_pop(inc.name.span); // pop the Inc/Dec result
                    }
                    LocalResolutionResult::FoundUpValue(_) => unreachable!(),
                }
            }
            ExprKind::Assignment(asn) => {
                self.compile_expr(&asn.value, ex)?;
                self.emit_set_local(asn.name.lexeme, asn.name.span)?;
            }
            ExprKind::Lambda(ref r#fn) => {
                self.compile_lambda(r#fn, expr.span, None, ex)?;
            }
            ExprKind::Grouping(expr) => {
                self.compile_expr(expr, ex)?;
            }
        }
        Ok(())
    }

    fn compile_ternary_expr(
        &mut self,
        expr: &Ternary<'a>,
        span: Span,
        ex: &mut ErrCtx,
    ) -> Result<(), LoxError> {
        self.compile_expr(&expr.predicate, ex)?;

        let else_start_label = self.reserve_label();
        // Check whether the condition is true and jump to the else branch
        self.emit_jump(OpCode::JumpIfFalse(else_start_label), span);
        // Pop the condition off the stack if the check succeeds
        self.emit_pop(span);

        // Compile the then branch
        self.compile_expr(&expr.then, ex)?;

        // Jump over the else branch
        let else_end_label = self.reserve_label();
        self.emit_jump(OpCode::Jump(else_end_label), span);

        self.emit_reserved_label(else_start_label, span);
        self.emit_pop(span);

        // Compile the else branch
        self.compile_expr(&expr.otherwise, ex)?;

        self.emit_reserved_label(else_end_label, span);
        Ok(())
    }

    fn compile_match_expr(
        &mut self,
        expr: &Match<'a>,
        span: Span,
        ex: &mut ErrCtx,
    ) -> Result<(), LoxError> {
        // Compile the value to match on.
        self.compile_expr(&expr.value, ex)?;

        if let Some(var) = &expr.bound_var {
            self.emit(OpCode::MatchPushBound, var.span);
        }

        let end_jump_label = self.reserve_label();

        for arm in &expr.arms {
            if arm.is_placeholder {
                // Compile the fall back branch. The wildcard branch doesn't have a pattern
                // so we pop off only the match value.
                self.emit_pop(span);
                self.compile_statement_into_expression(&arm.body, ex)?;
                // This branch is guaranteed to be the last in the list of arms so it's fine to break here.
                break;
            }
            // Copy the match value to the top of the stack
            self.emit(OpCode::Copy, span);
            // Compile the pattern value.
            self.compile_expr(&arm.pat, ex)?;
            if let MatchPatKind::TypeEquality = arm.pat_kind {
                self.emit(OpCode::CmpType, arm.pat.span);
            } else {
                self.emit(OpCode::Cmp, arm.pat.span);
            }
            // Jump to the next branch if the match result is False.
            let next_branch_label = self.reserve_label();
            self.emit_jump(OpCode::JumpIfFalse(next_branch_label), span);
            // Pop off the pattern and match values if the comparison succeeds.
            self.emit_pop(span);
            self.emit_pop(span);
            // Compile the match arm.
            self.compile_statement_into_expression(&arm.body, ex)?;

            // Jump to the end of the match expression.
            self.emit_jump(OpCode::Jump(end_jump_label), span);

            //  Patch the next branch jump and pop off the pattern value.
            self.emit_reserved_label(next_branch_label, span);
            self.emit_pop(span);
        }

        self.emit_reserved_label(end_jump_label, span);

        if let Some(var) = &expr.bound_var {
            self.emit(OpCode::MatchPopBound, var.span);
        }

        Ok(())
    }

    fn compile_statement_into_expression(
        &mut self,
        stmt: &Stmt<'a>,
        ex: &mut ErrCtx,
    ) -> Result<(), LoxError> {
        match stmt.kind {
            StmtKind::ExprStmt(ref expr) => {
                self.compile_expr(expr, ex)?;
            }
            _ => {
                self.compile_stmt(stmt, ex)?;
                //  Emit a nil if the match arm is a statement.
                self.emit(OpCode::Nil, stmt.span);
            }
        }
        Ok(())
    }

    fn emit_label(&mut self, origin: Span) -> Label {
        let label = self.reserve_label();
        self.emit_reserved_label(label, origin)
    }

    fn reserve_label(&mut self) -> Label {
        let label = Label::Label {
            block: self.blocks.block_id() as u16,
            label: self.labels.len() as u16,
        };
        self.labels.push(label);
        label
    }

    fn emit_reserved_label(&mut self, mut label: Label, origin: Span) -> Label {
        if let Label::Label { ref mut block, .. } = &mut label {
            *block = self.blocks.block_id() as u16;
        }
        self.emit(OpCode::Label(label), origin);
        label
    }

    #[inline]
    fn emit(&mut self, op: OpCode, span: Span) -> &mut Self {
        let is_label = op.is_label();
        if op == OpCode::Return {
            self.instrument_timeout(span);
        }
        self.blocks.emit(op, span);
        if !is_label && self.config.optimize && self.config.opt_config.peephole_optimizations {
            peephole_optimizer::optimize_block(self.blocks.last_block_mut().unwrap());
        }
        self
    }

    fn emit_choose_inst(
        &mut self,
        idx: usize,
        op256: OpCode,
        op65536: OpCode,
        span: Span,
    ) -> Result<(), LoxError> {
        if idx < 256 {
            self.emit(op256, span);
        } else if idx < 65536 {
            self.emit(op65536, span);
        } else {
            return Err(LoxError::compile(format!("The operand is too large for the 8-bit and 16-bit versions of the instruction [{:?}]: {}", op256, idx), span));
        }

        Ok(())
    }

    #[inline(always)]
    fn emit_pop(&mut self, span: Span) -> &mut Self {
        self.emit(OpCode::Pop, span);
        self
    }

    fn emit_pop_many(&mut self, n: usize, span: Span) -> Result<&mut Self, LoxError> {
        if n == 0 {
            return Ok(self);
        }
        if n == 1 {
            self.emit(OpCode::Pop, span);
        } else if n < 256 {
            self.emit(OpCode::PopN(n as u8), span);
        } else if n < 65536 {
            self.emit(OpCode::PopNLong(n as u16), span);
        } else {
            return Err(LoxError::compile("Too many operands to pop", span));
        }
        Ok(self)
    }

    #[inline(always)]
    fn emit_jump(&mut self, opcode: OpCode, origin: Span) -> usize {
        self.blocks.emit_jump(Jump::new(opcode, origin).unwrap())
    }

    fn store_const<V: Into<Value>>(&mut self, value: V) -> usize {
        let mut value = value.into();
        let hash = match &mut value {
            Value::Int(i) => (0, hash(i)),
            Value::Float(f) => (1, hash(f.to_be_bytes())),
            Value::Bool(b) => (2, hash(b)),
            Value::Ptr(p) => match p.as_mut().as_heap_value_mut() {
                HeapValue::Str(s) => (3, s.hash()),
                _ => (4, hash(p.as_usize())),
            },
            _ => {
                self.constants.push(value);
                return self.constants.len() - 1;
            }
        };
        if let Some(idx) = self.used_consts.get(&hash) {
            *idx
        } else {
            let len = self.constants.len();
            self.constants.push(value);
            self.used_consts.insert(hash, len);
            len
        }
    }

    fn emit_const(&mut self, value: Value, span: Span) -> Result<usize, LoxError> {
        let idx = self.store_const(value);
        if idx < 256 {
            self.emit(OpCode::LoadConst(idx as u8), span);
        } else if idx < 65536 {
            self.emit(OpCode::LoadConstLong(idx as u16), span);
        } else {
            return Err(LoxError::compile(
                "Cannot have more than 65536 constants.",
                span,
            ));
        }
        Ok(idx)
    }

    fn begin_scope(&mut self) -> usize {
        self.scope_depth += 1;
        self.scope_depth
    }

    fn end_scope(&mut self, origin: Span) -> Result<(), LoxError> {
        self.scope_depth -= 1;
        let mut n_locals = 0;

        for local in self.locals.iter().rev() {
            if local.depth <= self.scope_depth {
                break;
            }
            n_locals += 1;
        }
        self.locals
            .drain(self.locals.len() - n_locals..self.locals.len());

        if n_locals > 0 {
            self.emit_pop_many(n_locals, origin).map(|_| ())
        } else {
            Ok(())
        }
    }

    fn add_local(&mut self, name: &Token<'a>) -> Result<bool, LoxError> {
        if self.locals.len() >= 65536 {
            return Err(LoxError::compile(
                "Cannot have more than 65536 local variables.",
                name.span,
            ));
        }

        for l in self.locals.iter().rev() {
            // The variable is already in the scope so we do not need to reserve a slot
            if l.depth == self.scope_depth && l.same_lexeme(name.lexeme) {
                return Ok(true);
            }
        }

        self.locals.push(Local {
            name: MaybeOwned::Borrowed(name.lexeme),
            depth: self.scope_depth,
            is_captured: false,
        });
        Ok(false)
    }

    fn emit_set_local(&mut self, name: &str, span: Span) -> Result<(), LoxError> {
        let mut arg = self.resolve_local(name);
        if let LocalResolutionResult::NotFound = arg {
            arg = self.resolve_upvalue(name)
        }
        match arg {
            LocalResolutionResult::NotFound => {
                return Err(LoxError::compile(
                    format!("Undefined variable `{}`", name),
                    span,
                ));
            }
            LocalResolutionResult::FoundLocal(slot) => {
                if slot < 256 {
                    self.emit(OpCode::SetLocal(slot as u8), span);
                } else {
                    // Guaranteed to fit since the compiler checks that there's 65536 variables max
                    self.emit(OpCode::SetLocalLong(slot as u16), span);
                }
            }
            LocalResolutionResult::FoundUpValue(slot) => {
                if slot < 256 {
                    self.emit(OpCode::SetUpvalue(slot as u8), span);
                } else {
                    // Guaranteed to fit since the compiler checks that there's 65536 variables max
                    self.emit(OpCode::SetUpvalueLong(slot as u16), span);
                }
            }
        }

        Ok(())
    }

    fn emit_get_local(&mut self, name: &str, span: Span) -> Result<(), LoxError> {
        let mut arg = self.resolve_local(name);
        if let LocalResolutionResult::NotFound = arg {
            arg = self.resolve_upvalue(name)
        }
        match arg {
            LocalResolutionResult::NotFound => {
                let id = self
                    .ctx
                    .read_shared()
                    .builtins()
                    .name_to_hash()
                    .get(name)
                    .copied();
                if let Some(id) = id {
                    self.emit(OpCode::LoadNativeFn(id), span);
                } else {
                    return Err(LoxError::compile(
                        format!("Undefined variable `{}`", name),
                        span,
                    ));
                }
            }
            LocalResolutionResult::FoundLocal(slot) => {
                if slot < 256 {
                    self.emit(OpCode::GetLocal(slot as u8), span);
                } else {
                    self.emit(OpCode::GetLocalLong(slot as u16), span);
                }
            }
            LocalResolutionResult::FoundUpValue(slot) => {
                if slot < 256 {
                    self.emit(OpCode::GetUpvalue(slot as u8), span);
                } else {
                    self.emit(OpCode::GetUpvalueLong(slot as u16), span);
                }
            }
        }

        Ok(())
    }

    fn resolve_local(&mut self, name: &str) -> LocalResolutionResult {
        for (i, l) in self.locals.iter().enumerate().rev() {
            if l.same_lexeme(name) {
                return LocalResolutionResult::FoundLocal(i);
            }
        }
        LocalResolutionResult::NotFound
    }

    fn resolve_upvalue(&mut self, name: &str) -> LocalResolutionResult {
        if let Some(compiler) = self.enclosing() {
            if let LocalResolutionResult::FoundLocal(local) = compiler.resolve_local(name) {
                compiler.locals[local].is_captured = true;
                return self.add_upvalue(local, true);
            } else if let LocalResolutionResult::FoundUpValue(upvalue) =
                compiler.resolve_upvalue(name)
            {
                return self.add_upvalue(upvalue, false);
            }
        }

        LocalResolutionResult::NotFound
    }

    #[inline]
    fn enclosing(&mut self) -> Option<&mut Compiler<'a>> {
        self.enclosing.map(|c| unsafe { &mut *c })
    }

    fn add_upvalue(&mut self, index: usize, is_local: bool) -> LocalResolutionResult {
        for (i, up) in self.upvalues.iter().enumerate() {
            if up.index == index && up.is_local == is_local {
                return LocalResolutionResult::FoundUpValue(i);
            }
        }
        self.upvalues.push(UpValue { index, is_local });
        LocalResolutionResult::FoundUpValue(self.upvalues.len() - 1)
    }

    fn count_locals_in_scope(&self, scope_id: usize) -> usize {
        self.locals
            .iter()
            .rev()
            .take_while(|l| l.depth >= scope_id)
            .count()
    }

    fn patch_loop_control_flow(&mut self, loop_start_label: Label, loop_end_label: Label) {
        for i in (0..self.loop_flow.len()).rev() {
            if self.loop_flow[i].loop_id != self.loop_id {
                break;
            }

            let flow = self.loop_flow.pop().unwrap();
            match self.blocks.get_mut(flow.jump_index).unwrap() {
                BlockOrJump::Jump(jump) => match jump.opcode() {
                    OpCode::JumpBack(_) => {
                        *jump.label_mut() = loop_start_label;
                    }
                    OpCode::Jump(_) | OpCode::JumpIfFalse(_) => {
                        *jump.label_mut() = loop_end_label;
                    }
                    _ => unreachable!(),
                },
                BlockOrJump::Block(_) => unreachable!(),
            }
        }
    }

    #[inline(always)]
    fn begin_loop(&mut self) {
        self.loop_id += 1;
    }

    #[inline(always)]
    fn end_loop(&mut self) {
        self.loop_id -= 1;
    }
}

#[derive(Debug, Clone)]
enum MaybeOwned<'a> {
    Owned(String),
    Borrowed(&'a str),
}

#[derive(Debug, Clone)]
struct Local<'a> {
    name: MaybeOwned<'a>,
    depth: usize,
    is_captured: bool,
}

impl<'a> Local<'a> {
    #[inline]
    fn same_lexeme(&self, lex: &str) -> bool {
        match &self.name {
            MaybeOwned::Owned(o) => o == lex,
            MaybeOwned::Borrowed(b) => *b == lex,
        }
    }

    #[inline]
    fn make_owned(&mut self) {
        if let MaybeOwned::Borrowed(b) = self.name {
            self.name = MaybeOwned::Owned(b.to_owned());
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct UpValue {
    pub index: usize,
    pub is_local: bool,
}
known_deep_size_fast!(0, UpValue);

#[derive(Debug, Clone)]
enum LocalResolutionResult {
    NotFound,
    FoundLocal(usize),
    FoundUpValue(usize),
}

#[derive(Debug, Clone)]
struct LoopFlow {
    loop_id: usize,
    jump_index: usize,
}

impl<'a> IRPass<ErrCtx> for Compiler<'a> {
    type Input = AST<'a>;
    type Output = ObjectPointer;

    fn apply(self, ir: Self::Input) -> Result<Self::Output, ErrCtx> {
        let name = self
            .ctx
            .read_shared()
            .get_module(ir.module)
            .unwrap()
            .name()
            .to_owned();
        self.compile(ir, format!("{}.lox", name), Arity::Fixed(0))
    }

    fn description(&self) -> String {
        String::from("<Compiler: Compiles the AST to bytecode>")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::make_pipeline;
    use compiler::bytecode::builder::BytecodeBuilder;
    use config::CompilerConfig;
    use context::Context;
    use source_info::SourceInfo;

    #[allow(clippy::redundant_clone)]
    fn compile<S: Into<String>>(ctx: SharedCtx, source: S) -> Result<ObjectPointer, ()> {
        let ctx = ctx.clone();
        let id = ctx.write_shared().add_module(SourceInfo::test_info(source));
        make_pipeline(ctx.clone()).apply(id).map_err(|ex| {
            ctx.write_shared().ex.extend_from_another_ex(ex);
            ctx.read_shared().ex.report_all(ctx.clone());
        })
    }

    macro_rules! compile_and_test {
        ($ctx:ident, $source:expr, $expected:expr) => {
            compile_and_test!($ctx, $source, $expected, 0)
        };
        ($ctx:ident, $source:expr, $expected:expr, $i:expr) => {{
            let ptr = compile($ctx.clone(), $source).unwrap();
            let cco = ptr.as_ref().as_cco();
            assert_eq!(
                cco.chunk
                    .bytecode
                    .as_raw()
                    .unwrap()
                    .into_iter()
                    .copied()
                    .collect::<Vec<u8>>(),
                $expected,
                "[TEST #{}] FAILED; DIS:\n{}\n\n",
                $i,
                cco.disassemble()
            );
        }};
    }

    fn ctx() -> SharedCtx {
        Context::default()
            .set_config(CompilerConfig::default().disable_opts())
            .into_shared()
    }

    #[test]
    fn test_primitive_expression_compilation() {
        let ctx = ctx();
        let tests = vec![
            "1 + 2;",
            "print \"str\";",
            "print !(((3 + 2) * 2 - 3 / (-5) ** 2) % 77 == 0) != true;
             print (3, 4, 5, 6, 7);
             (3, 4, 5, 6, 7);
             print \"interop ${1 + 2} = 3\";",
        ];
        let expected = vec![
            BytecodeBuilder::new()
                .op_one()
                .op_load_const(2)
                .op_add()
                .op_pop()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_load_const("str")
                .op_print()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_load_const(3)
                .op_load_const(2)
                .op_add()
                .op_load_const(2)
                .op_mul()
                .op_load_const(3)
                .op_load_const(5)
                .op_negate()
                .op_load_const(2)
                .op_pow()
                .op_div()
                .op_sub()
                .op_load_const(77)
                .op_mod()
                .op_zero()
                .op_cmp()
                .op_not()
                .op_true()
                .op_cmp_not()
                .op_print()
                .op_load_const(3)
                .op_load_const(4)
                .op_load_const(5)
                .op_load_const(6)
                .op_pop_n(4)
                .op_load_const(7)
                .op_print()
                .op_load_const(3)
                .op_load_const(4)
                .op_load_const(5)
                .op_load_const(6)
                .op_pop_n(4)
                .op_load_const(7)
                .op_pop()
                .op_load_const("interop ")
                .op_one()
                .op_load_const(2)
                .op_add()
                .op_load_const(" = 3")
                .op_interpolate(3)
                .op_print()
                .build_bytes(),
        ];

        for (i, (t, expected)) in tests.into_iter().zip(expected.into_iter()).enumerate() {
            compile_and_test!(ctx, t, expected, i);
        }
    }

    #[test]
    fn test_variable_declarations() {
        let ctx = ctx();
        let tests = vec![
            "var a; print a;",
            "var a = 3, b = 4; print a + b;",
            "{ var a; print a; }",
            "var a; { var b, c; print b, c; }",
        ];
        let expected = vec![
            BytecodeBuilder::new()
                .add_local("a", Value::Nil)
                .op_get_local(0)
                .op_print()
                .build_bytes(),
            BytecodeBuilder::new()
                .add_local("a", 3)
                .add_local("b", 4)
                .op_get_local(0)
                .op_get_local(1)
                .op_add()
                .op_print()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_nil()
                .op_get_local(0)
                .op_print()
                .op_pop()
                .build_bytes(),
            BytecodeBuilder::new()
                .add_local("a", Value::Nil)
                .op_nil()
                .op_nil()
                .op_get_local(1)
                .op_get_local(2)
                .op_print_n(2)
                .op_pop_n(2)
                .build_bytes(),
        ];

        for (i, (t, expected)) in tests.into_iter().zip(expected.into_iter()).enumerate() {
            compile_and_test!(ctx, t, expected, i);
        }
    }

    #[test]
    fn test_logical_operators() {
        let ctx = ctx();
        let tests = vec![
            "print false or \"str\";",
            "print 4 == 2 ** 2 and true;",
            "print 1 or 2 and 3;",
        ];
        let expected = vec![
            BytecodeBuilder::new()
                .op_false()
                .op_jump_if_false(6)
                .op_jump(6)
                .op_pop()
                .op_load_const("str")
                .op_print()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_load_const(4)
                .op_load_const(2)
                .op_load_const(2)
                .op_pow()
                .op_cmp()
                .op_jump_if_false(5)
                .op_pop()
                .op_true()
                .op_print()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_one()
                .op_jump_if_false(6)
                .op_jump(12)
                .op_pop()
                .op_load_const(2)
                .op_jump_if_false(6)
                .op_pop()
                .op_load_const(3)
                .op_print()
                .build_bytes(),
        ];

        for (i, (t, expected)) in tests.into_iter().zip(expected.into_iter()).enumerate() {
            compile_and_test!(ctx, t, expected, i);
        }
    }

    #[test]
    fn test_if_statement() {
        let ctx = ctx();
        let tests = vec![
            "var a = 3; if (a == 3) { a; print a * 3; }",
            "var a = 3; if (a == 3) { a; print a * 3; } else { var b; print b; }",
            "var a = 2; if (a == 3) { a; print a * 3; } else { var b; print b; }",
        ];
        let expected = vec![
            BytecodeBuilder::new()
                .op_load_const(3)
                .op_get_local(0)
                .op_load_const(3)
                .op_cmp()
                .op_jump_if_false(16)
                .op_pop()
                .op_get_local(0)
                .op_pop()
                .op_get_local(0)
                .op_load_const(3)
                .op_mul()
                .op_print()
                .op_jump(4)
                .op_pop()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_load_const(3)
                .op_get_local(0)
                .op_load_const(3)
                .op_cmp()
                .op_jump_if_false(16)
                .op_pop()
                .op_get_local(0)
                .op_pop()
                .op_get_local(0)
                .op_load_const(3)
                .op_mul()
                .op_print()
                .op_jump(9)
                .op_pop()
                .op_nil()
                .op_get_local(1)
                .op_print()
                .op_pop()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_load_const(2)
                .op_get_local(0)
                .op_load_const(3)
                .op_cmp()
                .op_jump_if_false(16)
                .op_pop()
                .op_get_local(0)
                .op_pop()
                .op_get_local(0)
                .op_load_const(3)
                .op_mul()
                .op_print()
                .op_jump(9)
                .op_pop()
                .op_nil()
                .op_get_local(1)
                .op_print()
                .op_pop()
                .build_bytes(),
        ];

        for (i, (t, expected)) in tests.into_iter().zip(expected.into_iter()).enumerate() {
            compile_and_test!(ctx, t, expected, i);
        }
    }

    #[test]
    fn test_while_statement() {
        let ctx = ctx();
        let tests = vec![
            r#"
            var a = 0;
            var b = 1;
            var n = 10;
        
            while (--n >= 0) { 
                var tmp = b;
                b = a + b;
                a = tmp;
            }
        
            print a;"#,
        ];
        let expected = vec![BytecodeBuilder::new()
            .op_zero()
            .op_one()
            .op_load_const(10)
            .op_dec(2)
            .op_zero()
            .op_greater_equal()
            .op_jump_if_false(23)
            .op_pop()
            .op_get_local(1)
            .op_get_local(0)
            .op_get_local(1)
            .op_add()
            .op_set_local(1)
            .op_pop()
            .op_get_local(3)
            .op_set_local(0)
            .op_pop()
            .op_pop()
            .op_jump_back(24)
            .op_pop()
            .op_get_local(0)
            .op_print()
            .build_bytes()];

        for (i, (t, expected)) in tests.into_iter().zip(expected.into_iter()).enumerate() {
            compile_and_test!(ctx, t, expected, i);
        }
    }

    #[test]
    fn test_for_statement() {
        let ctx = ctx();
        let tests = vec![
            "for (var i = 0; i < 3; ++i) { print i; }",
            "var i = 0; for (; i < 3; ++i) { print i; }",
            "for (var i = 0; ; ++i) { if (i > 3) break; }",
            "for (var i = 0;;) { if (++i > 3) break; }",
            "for (;;) { break; }",
        ];
        let expected = vec![
            BytecodeBuilder::new()
                .op_zero()
                .op_get_local(0)
                .op_load_const(3)
                .op_less_than()
                .op_jump_if_false(19)
                .op_pop()
                .op_jump(9)
                .op_inc(0)
                .op_pop()
                .op_jump_back(15)
                .op_get_local(0)
                .op_print()
                .op_jump_back(9)
                .op_pop()
                .op_pop()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_zero()
                .op_get_local(0)
                .op_load_const(3)
                .op_less_than()
                .op_jump_if_false(19)
                .op_pop()
                .op_jump(9)
                .op_inc(0)
                .op_pop()
                .op_jump_back(15)
                .op_get_local(0)
                .op_print()
                .op_jump_back(9)
                .op_pop()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_zero()
                .op_true()
                .op_jump_if_false(32)
                .op_pop()
                .op_jump(9)
                .op_inc(0)
                .op_pop()
                .op_jump_back(11)
                .op_get_local(0)
                .op_load_const(3)
                .op_greater_than()
                .op_jump_if_false(10)
                .op_pop()
                .op_break(0, 11)
                .op_jump(4)
                .op_pop()
                .op_jump_back(22)
                .op_pop()
                .op_pop()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_zero()
                .op_true()
                .op_jump_if_false(23)
                .op_pop()
                .op_inc(0)
                .op_load_const(3)
                .op_greater_than()
                .op_jump_if_false(10)
                .op_pop()
                .op_break(0, 11)
                .op_jump(4)
                .op_pop()
                .op_jump_back(21)
                .op_pop()
                .op_pop()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_true()
                .op_jump_if_false(10)
                .op_pop()
                .op_break(0, 7)
                .op_jump_back(8)
                .op_pop()
                .build_bytes(),
        ];

        for (i, (t, expected)) in tests.into_iter().zip(expected.into_iter()).enumerate() {
            compile_and_test!(ctx, t, expected, i);
        }
    }

    #[test]
    fn test_break_and_continue() {
        let ctx = ctx();
        let tests = vec![
            r#"
            var a = 5;
            while (a > 0) { 
                --a;
                if (a == 2) continue;
                --a;
                if (a == 0) break;
            }
            print a;"#,
        ];
        let expected = vec![BytecodeBuilder::new()
            .op_load_const(5)
            .op_get_local(0)
            .op_zero()
            .op_greater_than()
            .op_jump_if_false(44)
            .op_pop()
            .op_dec(0)
            .op_pop()
            .op_get_local(0)
            .op_load_const(2)
            .op_cmp()
            .op_jump_if_false(10)
            .op_pop()
            .op_continue(0, 20)
            .op_jump(4)
            .op_pop()
            .op_dec(0)
            .op_pop()
            .op_get_local(0)
            .op_zero()
            .op_cmp()
            .op_jump_if_false(10)
            .op_pop()
            .op_break(0, 11)
            .op_jump(4)
            .op_pop()
            .op_jump_back(45)
            .op_pop()
            .op_get_local(0)
            .op_print()
            .build_bytes()];

        for (i, (t, expected)) in tests.into_iter().zip(expected.into_iter()).enumerate() {
            compile_and_test!(ctx, t, expected, i);
        }
    }

    #[test]
    fn test_match_expression() {
        let ctx = ctx();
        let tests = vec![
            "var a = match 0 { 3 => 1 }; print a;",
            "var a = match 0 as v { 3 => v + 1 }; print a;",
            r#"var a = match 1 + 5 as v {
                3 => v + 10,
                _ => match v {
                    7 => "seven",
                    _ => print "end";
                },
            };
            print a;"#,
        ];
        let expected = vec![
            BytecodeBuilder::new()
                .op_zero()
                .op_copy()
                .op_load_const(0)
                .op_cmp()
                .op_jump_if_false(9)
                .op_pop()
                .op_pop()
                .op_one()
                .op_jump(6)
                .op_pop()
                .op_pop()
                .op_nil()
                .op_get_local(0)
                .op_print()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_zero()
                .op_match_push_bound()
                .op_copy()
                .op_load_const(0)
                .op_cmp()
                .op_jump_if_false(11)
                .op_pop()
                .op_pop()
                .op_match_get_bound()
                .op_one()
                .op_add()
                .op_jump(6)
                .op_pop()
                .op_pop()
                .op_nil()
                .op_match_pop_bound()
                .op_get_local(0)
                .op_print()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_one()
                .op_load_const(0)
                .op_add()
                .op_match_push_bound()
                .op_copy()
                .op_load_const(1)
                .op_cmp()
                .op_jump_if_false(12)
                .op_pop()
                .op_pop()
                .op_match_get_bound()
                .op_load_const(2)
                .op_add()
                .op_jump(26)
                .op_pop()
                .op_pop()
                .op_match_get_bound()
                .op_copy()
                .op_load_const(3)
                .op_cmp()
                .op_jump_if_false(10)
                .op_pop()
                .op_pop()
                .op_load_const(4)
                .op_jump(9)
                .op_pop()
                .op_pop()
                .op_load_const(5)
                .op_print()
                .op_nil()
                .op_match_pop_bound()
                .op_get_local(0)
                .op_print()
                .build_bytes(),
        ];

        for (i, (t, expected)) in tests.into_iter().zip(expected.into_iter()).enumerate() {
            compile_and_test!(ctx, t, expected, i);
        }
    }

    #[test]
    fn test_arithmetic_peephole_opts() {
        let ctx = ctx();
        {
            ctx.write_shared().config.opt_config.peephole_optimizations = true;
        }
        let tests = vec!["1 + 2 * 3;", "1 - 2 * 3;", "2 * 3 + 1;", "2 * 3 - 1;"];
        let expected = vec![
            BytecodeBuilder::new()
                .op_one()
                .op_load_const(2)
                .op_load_const(3)
                .op_mul_add()
                .op_pop()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_one()
                .op_load_const(2)
                .op_load_const(3)
                .op_mul_sub()
                .op_pop()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_load_const(2)
                .op_load_const(3)
                .op_mul()
                .op_one()
                .op_add()
                .op_pop()
                .build_bytes(),
            BytecodeBuilder::new()
                .op_load_const(2)
                .op_load_const(3)
                .op_mul()
                .op_one()
                .op_sub()
                .op_pop()
                .build_bytes(),
        ];

        for (i, (t, expected)) in tests.into_iter().zip(expected.into_iter()).enumerate() {
            compile_and_test!(ctx, t, expected, i);
        }
    }

    #[test]
    fn test_control_flow_folding_eliminates_code() {
        let ctx = ctx();
        {
            ctx.write_shared().config.opt_config.constant_folding = true;
        }
        let source = r#"if (true) {
            print "always true";
        }
        
        if (false) {
            print "eliminated";
        }
        
        if (true) {
            print "always true";
        } else {
            print "always false";
        }
        
        if (false) {
            print "always false";
        } else {
            print "always true";
        }
        
        while (false) {
            print "always false";
        }"#;

        let expected = BytecodeBuilder::new()
            .op_load_const("always true")
            .op_print()
            .op_load_const("always true")
            .op_print()
            .op_load_const("always true")
            .op_print()
            .build_bytes();

        compile_and_test!(ctx, source, expected, 1);

        {
            ctx.write_shared().config.opt_config.constant_folding = false;
        }
        let expected_unoptimized = BytecodeBuilder::new()
            .op_true()
            .op_jump_if_false(10)
            .op_pop()
            .op_load_const("always true")
            .op_print()
            .op_jump(4)
            .op_pop()
            .op_false()
            .op_jump_if_false(10)
            .op_pop()
            .op_load_const("eliminated")
            .op_print()
            .op_jump(4)
            .op_pop()
            .op_true()
            .op_jump_if_false(10)
            .op_pop()
            .op_load_const("always true")
            .op_print()
            .op_jump(7)
            .op_pop()
            .op_load_const("always false")
            .op_print()
            .op_false()
            .op_jump_if_false(10)
            .op_pop()
            .op_load_const("always false")
            .op_print()
            .op_jump(7)
            .op_pop()
            .op_load_const("always true")
            .op_print()
            .op_false()
            .op_jump_if_false(10)
            .op_pop()
            .op_load_const("always false")
            .op_print()
            .op_jump_back(8)
            .op_pop()
            .build_bytes();

        compile_and_test!(ctx, source, expected_unoptimized, 2);
    }

    #[test]
    fn test_timeout_instrumentation() {
        let ctx = ctx();
        {
            ctx.write_shared().config.execution_timeout =
                Some(std::time::Duration::from_millis(1000));
        }
        let tests = vec!["1 + 2 * 3;", "for (;;) {}", "while (true) {}", "loop {}"];
        let expected = vec![
            BytecodeBuilder::new()
                .op_one()
                .op_load_const(2)
                .op_load_const(3)
                .op_mul()
                .op_add()
                .op_pop()
                .build_bytes_with_timeout(),
            BytecodeBuilder::new()
                .op_true()
                .op_jump_if_false(8)
                .op_pop()
                .op_timeout()
                .op_jump_back(6)
                .op_pop()
                .build_bytes_with_timeout(),
            BytecodeBuilder::new()
                .op_true()
                .op_jump_if_false(8)
                .op_pop()
                .op_timeout()
                .op_jump_back(6)
                .op_pop()
                .build_bytes_with_timeout(),
            BytecodeBuilder::new()
                .op_true()
                .op_jump_if_false(8)
                .op_pop()
                .op_timeout()
                .op_jump_back(6)
                .op_pop()
                .build_bytes_with_timeout(),
        ];

        for (i, (t, expected)) in tests.into_iter().zip(expected.into_iter()).enumerate() {
            compile_and_test!(ctx, t, expected, i);
        }
    }
}
