#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[repr(u8)]
pub enum RawOpcode {
    LoadConst = 0x00,
    LoadConstLong = 0x01,
    GetLocal = 0x02,
    SetLocal = 0x03,
    GetLocalLong = 0x04,
    SetLocalLong = 0x05,
    Pop = 0x06,
    PopN = 0x07,
    PopNLong = 0x08,
    Negate = 0x09,
    Not = 0x0A,
    Add = 0x0B,
    Sub = 0x0C,
    Div = 0x0D,
    Mul = 0x0E,
    Pow = 0x0F,
    Mod = 0x10,
    Cmp = 0x11,
    CmpNot = 0x12,
    Jump = 0x13,
    JumpBack = 0x14,
    JumpIfFalse = 0x15,
    FnCall = 0x16,
    SetLocalFast = 0x17,
    Return = 0x18,
    GetUpvalue = 0x19,
    GetUpvalueLong = 0x1A,
    SetUpvalue = 0x1B,
    SetUpvalueLong = 0x1C,
    MakeClosure = 0x1D,
    MakeClosureLong = 0x1E,
    LoadNativeFn = 0x1F,
    Interpolate = 0x20,
    InterpolateLong = 0x21,
    MakeModule = 0x22,
    MatchPushBound = 0x23,
    MatchGetBound = 0x24,
    MatchPopBound = 0x25,
    MakeClass = 0x26,
    MakeClassLong = 0x27,
    SetField = 0x28,
    SetFieldLong = 0x29,
    GetField = 0x2A,
    GetFieldLong = 0x2B,
    SetItem = 0x2C,
    GetItem = 0x2D,
    MakeMethod = 0x2E,
    InvokeMethod = 0x2F,
    Nil = 0x30,
    True = 0x31,
    False = 0x32,
    One = 0x33,
    Zero = 0x34,
    Inc = 0x35,
    Dec = 0x36,
    IncLong = 0x37,
    DecLong = 0x38,
    GreaterEqual = 0x39,
    GreaterThan = 0x3A,
    LessEqual = 0x3B,
    LessThan = 0x3C,
    Copy = 0x3D,
    IsNil = 0x3E,
    IsNotNil = 0x3F,
    IntoIter = 0x40,
    IterNext = 0x41,
    GetFieldFast = 0x42,
    SetFieldFast = 0x43,
    MakeArr = 0x44,
    Print = 0x45,
    PrintN = 0x46,
    TCOCall = 0x47,
    MakeDict = 0x48,
    MulSub = 0x49,
    MulAdd = 0x4A,
    InvokeMethodTCO = 0x4B,
    Inherit = 0x4C,
    GetSuper = 0x4D,
    GetSuperLong = 0x4E,
    CmpType = 0x4F,
    CmpTypeNot = 0x50,
    Unwrap = 0x51,
    Import = 0x52,
    CopyN = 0x53,
    FwdCall = 0x54,
    TimeoutCheck = 0x55,
    Unknown = 0xFD,
    MissingJumpLabel = 0xFE,
    Error = 0xFF,
}

impl RawOpcode {
    pub fn from_u8(byte: u8) -> RawOpcode {
        if byte <= RawOpcode::TimeoutCheck as u8 {
            unsafe { std::mem::transmute(byte) }
        } else {
            #[cfg(debug_assertions)]
            {
                eprintln!(
                    "[DEBUG] Attempted to construct a RawOpCode from an invalid byte value: {}",
                    byte
                );
            }
            RawOpcode::Unknown
        }
    }

    pub fn name(&self) -> String {
        format!("{:?}", self)
    }
}

impl Into<u8> for RawOpcode {
    #[inline(always)]
    fn into(self) -> u8 {
        self as u8
    }
}
