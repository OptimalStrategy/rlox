#[derive(Debug, Clone, Copy)]
pub enum Label {
    /// An unpatched label.
    FlowLabel,
    Label {
        label: u16,
        block: u16,
    },
    Jump(u16),
}
known_deep_size_fast!(0, Label);

impl Label {
    pub fn as_jump(&self) -> Option<u16> {
        match self {
            Self::Jump(jump) => Some(*jump),
            Self::Label { .. } => None,
            Self::FlowLabel => None,
        }
    }

    pub fn as_label(&self) -> Option<u16> {
        match self {
            Self::Label { label, .. } => Some(*label),
            Self::Jump(_) => None,
            Self::FlowLabel => None,
        }
    }

    pub fn map_jump<F>(self, f: F) -> Self
    where
        F: Fn(u16) -> u16,
    {
        match self {
            Self::Jump(jump) => Self::Jump(f(jump)),
            lbl => lbl,
        }
    }
}

impl std::hash::Hash for Label {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        match self {
            Self::Label { label, .. } => label.hash(state),
            Self::Jump(offset) => offset.hash(state),
            Self::FlowLabel => panic!("Cannot compute the hash of an unpatched label"),
        }
    }
}

impl PartialEq for Label {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Label { label: left, .. }, Self::Label { label: right, .. }) => left == right,
            (Self::Jump(left), Self::Jump(right)) => left == right,
            _ => false,
        }
    }
}

impl Eq for Label {}

impl std::fmt::Display for Label {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Jump(jump) => jump.to_string(),
                Self::Label { label, .. } => format!("L{}", label),
                Self::FlowLabel => "FlowLabel".to_string(),
            }
        )
    }
}
