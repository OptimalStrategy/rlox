use crate::allocator::DeepSizeOf;

use super::RawOpcode;
use super::{label::Label, Inst};

use compiler::compiler::UpValue;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum OpCode {
    /// Load a number at an 8-bit address [1 + 1 byte]
    LoadConst(u8),
    /// Load a number at a 16-bit address [1 + 2 bytes]
    LoadConstLong(u16),
    /// Get a local variable at an 8-bit stack address [1 + 1 byte]
    GetLocal(u8),
    /// Set a local variable at an 8-bit stack address [1 + 1 byte]
    SetLocal(u8),
    /// Get a local variable at a 16-bit stack address [1 + 2 bytes]
    GetLocalLong(u16),
    /// Set a local variable at a 16-bit stack address [1 + 2 bytes]
    SetLocalLong(u16),
    /// Pop a value off the stack
    Pop,
    /// Pop up to 256 values off the stack [1 + 1 byte]
    PopN(u8),
    /// Pop up to 65536 values off the stack [1 + 2 bytes]
    PopNLong(u16),
    /// Unary Operators (Require 1 value on the stack)
    Negate,
    Not,
    /// Binary Operators (Require 2 values on the stack)
    Add,
    Sub,
    Div,
    Mul,
    Pow,
    Mod,
    Cmp,
    CmpNot,
    CmpType,
    CmpTypeNot,
    /// Jump forth by up to 65536 instructions [1 + 2 bytes].
    /// This instruction may be an abstract (unpatched) jump.
    Jump(Label),
    /// Jump back by up to 65536 instructions [1 + 2 bytes]
    /// This instruction may be an abstract (unpatched) jump.
    JumpBack(Label),
    /// Jump forth by up to 65536 instructions if the stack top
    /// is zero [1 + 2 bytes]
    /// This instruction may be an abstract (unpatched) jump.
    JumpIfFalse(Label),
    /// An abstract jump label.
    Label(Label),
    /// Call a function with up to 256 arguments [1 + 1 bytes]
    FnCall(u8),
    /// Perform TCO on a function with up to 256 arguments [1 + 1 bytes]
    TCOCall(u8),
    /// Forward the arguments as an array to the callee [1 + 1 bytes].
    /// The first 4 bits are the number of rest arguments, the other 4 bits are the number of normal arguments.
    /// Each rest argument must be an array.
    FwdCall(u8),
    /// Sets a local variable without pushing the result onto the stack [1 + 1 bytes].
    SetLocalFast(u8),
    /// Return from a function call
    Return,
    /// Get an upvalue at an 8-bit address
    GetUpvalue(u8),
    /// Get an upvalue at a 16-bit address
    GetUpvalueLong(u16),
    /// Set an upvalue at an 8-bit address
    SetUpvalue(u8),
    /// Set an upvalue at a 16-bit address
    SetUpvalueLong(u16),
    /// Create a new closure from a function at an 8-bit address.
    /// The opcode is followed by N upvalues: [is_local, index = 1 + 1 bytes]
    MakeClosure(u8, Vec<UpValue>),
    /// Create a new closure from a function at a 16-bit address.
    /// The opcode is followed by N upvalues: [is_local, index = 1 + 2 bytes]
    MakeClosureLong(u16, Vec<UpValue>),
    /// Load a native function at an 8-bit address [1 byte]
    LoadNativeFn(u8),
    /// Pop up to 256 interpolation fragments off the stack and concatenate
    /// them into a string [1 + 1 bytes]
    Interpolate(u8),
    /// Pop up to 65536 interpolation fragments off the stack and concatenate
    /// them into a string [1 + 2 bytes]
    InterpolateLong(u16),
    /// Creates a module object using the global variables that are currently on the stack.
    /// Requires one operand - the pointer to the module object stored at 16-bit constant address [1 + 2 bytes]
    MakeModule(u16),
    /// Imports the module with the id stored as a constant at the operand address [1 + 2 bytes]
    Import(u16),
    /// Copies the value on the stack to the match bound variable stack. [1 byte]
    MatchPushBound,
    /// Pushes the value of the match bound variable onto the stack. [1 byte]
    MatchGetBound,
    /// Pops the match bound variable off the match stack [1 byte]
    MatchPopBound,
    /// Create a new class using a name at an 8-bit stack address [1 + 1 bytes]
    MakeClass(u8),
    /// Create a new class using a name at a 16-bit stack address [1 + 2 bytes]
    MakeClassLong(u16),
    /// Inherit from a class. Requires two operands on the stack: the superclass and the to-be-subclass class.
    Inherit,
    /// Loads a method from the parent class, binds it to the current class, and pushes it onto the stack.
    /// Requires two operands on the stack and a method name 8-bit constant index [1 + 1 bytes]
    GetSuper(u8),
    /// Loads a method from the parent class, binds it to the current class, and pushes it onto the stack.
    /// Requires two operands on the stack and a method name 16-bit constant index [1 + 2 bytes]
    GetSuperLong(u16),
    /// Sets a field on an instance. Requires 2 operands on the stack:
    /// a class instance and a value. Requires a name at an 8-bit stack address [1 byte + 1 bytes]
    SetField(u8),
    /// Sets a field on an instance. Requires 2 operands on the stack:
    /// a class instance and a value. Requires a name at a 16-bit stack address [1 byte + 2 bytes]
    SetFieldLong(u16),
    /// Gets a field on an instance. Requires 1 operand on the stack: a class instance.
    /// Requires a name at an 8-bit stack address [1 byte + 1 bytes]
    GetField(u8),
    /// Gets a field on an instance. Requires 1 operand on the stack: a class instance.
    /// Requires a name at a 16-bit stack address [1 byte + 1 bytes]
    GetFieldLong(u16),
    /// Calls the magic method setitem(key, value) on an instance. Requires 3 operands
    /// on the stack: a class instance, a key, and a value. [1 byte]
    SetItem,
    /// Calls the magic method getitem(key) on an instance. Requires 2 operands
    /// on the stack: a class instance and a key. [1 byte]
    GetItem,
    /// Adds a method to the method table. Requires two operands on the stack:
    /// a class object and a method object.
    MakeMethod,
    /// Combines the GetField and FnCall instructions into one. Requires one operand on the stack:
    /// a class instance. Requires a name at an 8-bit address and an 8-bit argument count. [1 + 2 bytes]
    InvokeMethod(u8, u8),
    /// Combines the GetField and TCOCall instructions into one. Requires one operand on the stack:
    /// a class instance. Requires a name at an 8-bit address and an 8-bit argument count. [1 + 2 bytes]
    InvokeMethodTCO(u8, u8),
    /// Push a nil onto the stack [1 byte]
    Nil,
    /// Push a True onto the stack [1 byte]
    True,
    /// Push a False onto the stack [1 byte]
    False,
    /// Push a 1 onto the stack [1 byte]
    One,
    /// Push a 0 onto the stack [1 byte]
    Zero,
    /// Increment a variable at an 8-bit address [1 + 1 byte]
    Inc(u8),
    /// Decrement a variable at an 8-bit address [1 + 1 byte]
    Dec(u8),
    /// Increment a variable at a 16-bit address [1 + 2 bytes]
    IncLong(u16),
    /// Decrement a variable at a 16-bit address [1 + 2 bytes]
    DecLong(u16),
    /// Apply >= to the two operands on the stack [1 byte]
    GreaterEqual,
    /// Apply > to the two operands on the stack [1 byte]
    GreaterThan,
    /// Apply <= to the two operands on the stack [1 byte]
    LessEqual,
    /// Apply < to the two operands on the stack [1 byte]
    LessThan,
    /// Copy the value on the stack [1 byte]
    Copy,
    /// Copy the value on the stack N times [1 + 1 byte]
    CopyN(u8),
    /// Push true on the stack if the stack top is Nil
    IsNil,
    /// Push true on the stack if the stack top is not Nil
    IsNotNil,
    /// Call iterator() on the object on the top of the stack
    IntoIter,
    /// Call iternext() on the object on the top of the stack
    IterNext,
    /// Unwrap an Ok value
    Unwrap,
    /// Combines the GetLocal and GetField instructions. Requires two operands:
    /// an 8-bit instance address and an 8-bit name address [1 + 2 bytes]
    GetFieldFast(u8, u8),
    /// Combines the GetLocal and SetField instructions. Requires two operands:
    /// an 8-bit instance local address and an 8-bit name address [1 + 2 bytes]
    SetFieldFast(u8, u8),
    /// Create an array of up to 256 values on the stack. [1 + 1 bytes]
    MakeArr(u8),
    /// Create a dict of up to 256 value pairs on the stack. [1 + 1 bytes]
    MakeDict(u8),
    /// Pop off and print the value on the stack [1 byte]
    Print,
    /// Pop off and print up to 256 values on the stack [1 + 1 byte]
    PrintN(u8),
    /// Pop off three values off the stack, multiply the first two, then subtract the third. [1 byte]
    MulSub,
    /// Pop off three values off the stack, multiply the first two, then add the third. [1 byte]
    MulAdd,
    /// Check if the Vm has execution the execution timeout.
    TimeoutCheck,
    /// An internal debug-only instruction.
    MissingJumpLabel,
    /// An internal instruction representing an error or the absence of an instruction.
    Error,
}
impl DeepSizeOf for OpCode {
    #[inline]
    fn deep_size_of_children(&self) -> usize {
        match self {
            Self::MakeClosure(_, v) => crate::allocator::deep_size_of::deep_size_of_vec(v),
            Self::MakeClosureLong(_, v) => crate::allocator::deep_size_of::deep_size_of_vec(v),
            _ => 0,
        }
    }
}

impl OpCode {
    #[inline]
    pub fn is_label(&self) -> bool {
        matches!(self, OpCode::Label(_))
    }

    #[inline]
    pub fn is_return(&self) -> bool {
        matches!(self, OpCode::Return)
    }

    pub fn operands(&self) -> Vec<u8> {
        self.as_bytes().iter().skip(1).copied().collect()
    }

    pub fn operand_u8(&self) -> u8 {
        self.try_operand_u8().unwrap()
    }

    pub fn try_operand_u8(&self) -> Option<u8> {
        self.as_bytes().iter().skip(1).take(1).next().copied()
    }

    #[inline(always)]
    pub fn get_byte_marker(&self) -> u8 {
        self.get_raw_opcode() as u8
    }

    pub fn get_raw_opcode(&self) -> RawOpcode {
        if self.is_label() {
            panic!("Abstract labels must be eliminated before conversion to raw opcodes.")
        }
        self.get_raw_opcode_or_label()
    }

    pub fn get_raw_opcode_or_label(&self) -> RawOpcode {
        match self {
            OpCode::LoadConst(_) => RawOpcode::LoadConst,
            OpCode::LoadConstLong(_) => RawOpcode::LoadConstLong,
            OpCode::GetLocal(_) => RawOpcode::GetLocal,
            OpCode::SetLocal(_) => RawOpcode::SetLocal,
            OpCode::GetLocalLong(_) => RawOpcode::GetLocalLong,
            OpCode::SetLocalLong(_) => RawOpcode::SetLocalLong,
            OpCode::Pop => RawOpcode::Pop,
            OpCode::PopN(_) => RawOpcode::PopN,
            OpCode::PopNLong(_) => RawOpcode::PopNLong,
            OpCode::Negate => RawOpcode::Negate,
            OpCode::Not => RawOpcode::Not,
            OpCode::Add => RawOpcode::Add,
            OpCode::Sub => RawOpcode::Sub,
            OpCode::Div => RawOpcode::Div,
            OpCode::Mul => RawOpcode::Mul,
            OpCode::Pow => RawOpcode::Pow,
            OpCode::Mod => RawOpcode::Mod,
            OpCode::Cmp => RawOpcode::Cmp,
            OpCode::CmpNot => RawOpcode::CmpNot,
            OpCode::CmpType => RawOpcode::CmpType,
            OpCode::CmpTypeNot => RawOpcode::CmpTypeNot,
            OpCode::Jump(_) => RawOpcode::Jump,
            OpCode::JumpBack(_) => RawOpcode::JumpBack,
            OpCode::JumpIfFalse(_) => RawOpcode::JumpIfFalse,
            OpCode::FnCall(_) => RawOpcode::FnCall,
            OpCode::TCOCall(_) => RawOpcode::TCOCall,
            OpCode::SetLocalFast(_) => RawOpcode::SetLocalFast,
            OpCode::Return => RawOpcode::Return,
            OpCode::GetUpvalue(_) => RawOpcode::GetUpvalue,
            OpCode::GetUpvalueLong(_) => RawOpcode::GetUpvalueLong,
            OpCode::SetUpvalue(_) => RawOpcode::SetUpvalue,
            OpCode::SetUpvalueLong(_) => RawOpcode::SetUpvalueLong,
            OpCode::MakeClosure(_, _) => RawOpcode::MakeClosure,
            OpCode::MakeClosureLong(_, _) => RawOpcode::MakeClosureLong,
            OpCode::LoadNativeFn(_) => RawOpcode::LoadNativeFn,
            OpCode::Interpolate(_) => RawOpcode::Interpolate,
            OpCode::InterpolateLong(_) => RawOpcode::InterpolateLong,
            OpCode::MakeModule(_) => RawOpcode::MakeModule,
            OpCode::Import(_) => RawOpcode::Import,
            OpCode::MatchPushBound => RawOpcode::MatchPushBound,
            OpCode::MatchGetBound => RawOpcode::MatchGetBound,
            OpCode::MatchPopBound => RawOpcode::MatchPopBound,
            OpCode::MakeClass(_) => RawOpcode::MakeClass,
            OpCode::MakeClassLong(_) => RawOpcode::MakeClassLong,
            OpCode::Inherit => RawOpcode::Inherit,
            OpCode::GetSuper(_) => RawOpcode::GetSuper,
            OpCode::GetSuperLong(_) => RawOpcode::GetSuperLong,
            OpCode::SetField(_) => RawOpcode::SetField,
            OpCode::SetFieldLong(_) => RawOpcode::SetFieldLong,
            OpCode::GetField(_) => RawOpcode::GetField,
            OpCode::GetFieldLong(_) => RawOpcode::GetFieldLong,
            OpCode::SetItem => RawOpcode::SetItem,
            OpCode::GetItem => RawOpcode::GetItem,
            OpCode::MakeMethod => RawOpcode::MakeMethod,
            OpCode::InvokeMethod(_, _) => RawOpcode::InvokeMethod,
            OpCode::InvokeMethodTCO(_, _) => RawOpcode::InvokeMethodTCO,
            OpCode::Nil => RawOpcode::Nil,
            OpCode::True => RawOpcode::True,
            OpCode::False => RawOpcode::False,
            OpCode::One => RawOpcode::One,
            OpCode::Zero => RawOpcode::Zero,
            OpCode::Inc(_) => RawOpcode::Inc,
            OpCode::Dec(_) => RawOpcode::Dec,
            OpCode::IncLong(_) => RawOpcode::IncLong,
            OpCode::DecLong(_) => RawOpcode::DecLong,
            OpCode::GreaterEqual => RawOpcode::GreaterEqual,
            OpCode::GreaterThan => RawOpcode::GreaterThan,
            OpCode::LessEqual => RawOpcode::LessEqual,
            OpCode::LessThan => RawOpcode::LessThan,
            OpCode::Copy => RawOpcode::Copy,
            OpCode::CopyN(_) => RawOpcode::CopyN,
            OpCode::FwdCall(_) => RawOpcode::FwdCall,
            OpCode::IsNil => RawOpcode::IsNil,
            OpCode::IsNotNil => RawOpcode::IsNotNil,
            OpCode::IntoIter => RawOpcode::IntoIter,
            OpCode::IterNext => RawOpcode::IterNext,
            OpCode::Unwrap => RawOpcode::Unwrap,
            OpCode::GetFieldFast(_, _) => RawOpcode::GetFieldFast,
            OpCode::SetFieldFast(_, _) => RawOpcode::SetFieldFast,
            OpCode::MakeArr(_) => RawOpcode::MakeArr,
            OpCode::MakeDict(_) => RawOpcode::MakeDict,
            OpCode::Print => RawOpcode::Print,
            OpCode::PrintN(_) => RawOpcode::PrintN,
            OpCode::MulSub => RawOpcode::MulSub,
            OpCode::MulAdd => RawOpcode::MulAdd,
            OpCode::TimeoutCheck => RawOpcode::TimeoutCheck,
            OpCode::MissingJumpLabel => RawOpcode::MissingJumpLabel,
            OpCode::Error | OpCode::Label(_) => RawOpcode::Error,
        }
    }

    pub fn as_bytes(&self) -> Vec<Inst> {
        let mut marker = vec![self.get_byte_marker()];
        let operands = match self {
            OpCode::LoadConst(b) => BE!(b),
            OpCode::LoadConstLong(b) => BE!(b),
            OpCode::GetLocal(b) => BE!(b),
            OpCode::SetLocal(b) => BE!(b),
            OpCode::GetLocalLong(b) => BE!(b),
            OpCode::SetLocalLong(b) => BE!(b),
            OpCode::Pop => vec![],
            OpCode::PopN(b) => BE!(b),
            OpCode::PopNLong(b) => BE!(b),
            OpCode::Negate => vec![],
            OpCode::Not => vec![],
            OpCode::Add => vec![],
            OpCode::Sub => vec![],
            OpCode::Div => vec![],
            OpCode::Mul => vec![],
            OpCode::Pow => vec![],
            OpCode::Mod => vec![],
            OpCode::Cmp => vec![],
            OpCode::CmpNot => vec![],
            OpCode::CmpType => vec![],
            OpCode::CmpTypeNot => vec![],
            OpCode::Jump(b) => BE!(b.as_jump().expect("Cannot serialize abstract jumps")),
            OpCode::JumpBack(b) => BE!(b.as_jump().expect("Cannot serialize abstract jumps")),
            OpCode::JumpIfFalse(b) => BE!(b.as_jump().expect("Cannot serialize abstract jumps")),
            OpCode::FnCall(b) => BE!(b),
            OpCode::TCOCall(b) => BE!(b),
            OpCode::FwdCall(b) => BE!(b),
            OpCode::SetLocalFast(b) => BE!(b),
            OpCode::Return => vec![],
            OpCode::GetUpvalue(b) => BE!(b),
            OpCode::GetUpvalueLong(b) => BE!(b),
            OpCode::SetUpvalue(b) => BE!(b),
            OpCode::SetUpvalueLong(b) => BE!(b),
            OpCode::MakeClosure(slot, up) => BE!(slot)
                .into_iter()
                .chain(up.iter().flat_map(|up| {
                    BE!(up.is_local as u8)
                        .into_iter()
                        .chain(BE!(up.index as u8))
                }))
                .collect(),
            OpCode::MakeClosureLong(slot, up) => BE!(slot)
                .into_iter()
                .chain(up.iter().flat_map(|up| {
                    BE!(up.is_local as u8)
                        .into_iter()
                        .chain(BE!(up.index as u16))
                }))
                .collect(),
            OpCode::LoadNativeFn(b) => BE!(b),
            OpCode::Interpolate(b) => BE!(b),
            OpCode::InterpolateLong(b) => BE!(b),
            OpCode::MakeModule(b) => BE!(b),
            OpCode::Import(b) => BE!(b),
            OpCode::MatchPushBound => vec![],
            OpCode::MatchGetBound => vec![],
            OpCode::MatchPopBound => vec![],
            OpCode::MakeClass(b) => BE!(b),
            OpCode::MakeClassLong(b) => BE!(b),
            OpCode::Inherit => vec![],
            OpCode::GetSuper(b) => BE!(b),
            OpCode::GetSuperLong(b) => BE!(b),
            OpCode::SetField(b) => BE!(b),
            OpCode::SetFieldLong(b) => BE!(b),
            OpCode::GetField(b) => BE!(b),
            OpCode::GetFieldLong(b) => BE!(b),
            OpCode::SetItem => vec![],
            OpCode::GetItem => vec![],
            OpCode::MakeMethod => vec![],
            OpCode::InvokeMethod(b1, b2) => [BE!(b1), BE!(b2)].concat(),
            OpCode::InvokeMethodTCO(b1, b2) => [BE!(b1), BE!(b2)].concat(),
            OpCode::Nil => vec![],
            OpCode::True => vec![],
            OpCode::False => vec![],
            OpCode::One => vec![],
            OpCode::Zero => vec![],
            OpCode::Inc(b) => BE!(b),
            OpCode::Dec(b) => BE!(b),
            OpCode::IncLong(b) => BE!(b),
            OpCode::DecLong(b) => BE!(b),
            OpCode::GreaterEqual => vec![],
            OpCode::GreaterThan => vec![],
            OpCode::LessEqual => vec![],
            OpCode::LessThan => vec![],
            OpCode::Copy => vec![],
            OpCode::CopyN(b) => BE!(b),
            OpCode::IsNil => vec![],
            OpCode::IsNotNil => vec![],
            OpCode::IntoIter => vec![],
            OpCode::IterNext => vec![],
            OpCode::Unwrap => vec![],
            OpCode::GetFieldFast(b1, b2) => [BE!(b1), BE!(b2)].concat(),
            OpCode::SetFieldFast(b1, b2) => [BE!(b1), BE!(b2)].concat(),
            OpCode::MakeArr(b) => BE!(b),
            OpCode::MakeDict(b) => BE!(b),
            OpCode::Print => vec![],
            OpCode::PrintN(b) => BE!(b),
            OpCode::MulSub => vec![],
            OpCode::MulAdd => vec![],
            OpCode::MissingJumpLabel => vec![],
            OpCode::Error => vec![],
            OpCode::TimeoutCheck => vec![],
            OpCode::Label(_) => unreachable!(),
        };

        marker.extend(operands);
        marker
    }

    pub fn size(&self) -> usize {
        1 + match self {
            OpCode::Pop
            | OpCode::Negate
            | OpCode::Not
            | OpCode::Add
            | OpCode::Sub
            | OpCode::Div
            | OpCode::Mul
            | OpCode::Pow
            | OpCode::Mod
            | OpCode::Cmp
            | OpCode::CmpNot
            | OpCode::CmpType
            | OpCode::CmpTypeNot
            | OpCode::Return
            | OpCode::MatchPushBound
            | OpCode::MatchGetBound
            | OpCode::MatchPopBound
            | OpCode::Inherit
            | OpCode::SetItem
            | OpCode::GetItem
            | OpCode::MakeMethod
            | OpCode::Nil
            | OpCode::True
            | OpCode::False
            | OpCode::One
            | OpCode::Zero
            | OpCode::GreaterEqual
            | OpCode::GreaterThan
            | OpCode::LessEqual
            | OpCode::LessThan
            | OpCode::Copy
            | OpCode::IsNil
            | OpCode::IsNotNil
            | OpCode::IntoIter
            | OpCode::IterNext
            | OpCode::Unwrap
            | OpCode::Print
            | OpCode::MulSub
            | OpCode::MulAdd
            | OpCode::TimeoutCheck
            | OpCode::MissingJumpLabel => 0,
            OpCode::GetSuper(_)
            | OpCode::GetField(_)
            | OpCode::SetField(_)
            | OpCode::LoadConst(_)
            | OpCode::GetLocal(_)
            | OpCode::SetLocal(_)
            | OpCode::PopN(_)
            | OpCode::FnCall(_)
            | OpCode::TCOCall(_)
            | OpCode::FwdCall(_)
            | OpCode::SetLocalFast(_)
            | OpCode::SetUpvalue(_)
            | OpCode::GetUpvalue(_)
            | OpCode::LoadNativeFn(_)
            | OpCode::Interpolate(_)
            | OpCode::MakeClass(_)
            | OpCode::CopyN(_)
            | OpCode::Inc(_)
            | OpCode::Dec(_)
            | OpCode::MakeArr(_)
            | OpCode::MakeDict(_)
            | OpCode::PrintN(_) => 1,
            OpCode::GetLocalLong(_)
            | OpCode::SetLocalLong(_)
            | OpCode::LoadConstLong(_)
            | OpCode::PopNLong(_)
            | OpCode::InterpolateLong(_)
            | OpCode::MakeModule(_)
            | OpCode::Import(_)
            | OpCode::GetUpvalueLong(_)
            | OpCode::SetUpvalueLong(_)
            | OpCode::Jump(_)
            | OpCode::JumpBack(_)
            | OpCode::JumpIfFalse(_)
            | OpCode::MakeClassLong(_)
            | OpCode::GetSuperLong(_)
            | OpCode::SetFieldLong(_)
            | OpCode::GetFieldLong(_)
            | OpCode::IncLong(_)
            | OpCode::DecLong(_)
            | OpCode::InvokeMethod(_, _)
            | OpCode::InvokeMethodTCO(_, _)
            | OpCode::GetFieldFast(_, _)
            | OpCode::SetFieldFast(_, _) => 2,
            OpCode::MakeClosure(_, up) => 1 + up.len() * 2,
            OpCode::MakeClosureLong(_, up) => 1 + up.len() * 3,
            OpCode::Error => 0,
            OpCode::Label(_) => return 0,
        }
    }

    #[inline]
    pub fn is_jump_opcode(&self) -> bool {
        matches!(
            self,
            OpCode::Jump(_) | OpCode::JumpBack(_) | OpCode::JumpIfFalse(_)
        )
    }

    pub fn name(&self) -> &'static str {
        match self {
            Self::Label(_) => "Label",
            _ => match self.get_raw_opcode() {
                RawOpcode::LoadConst => "LoadConst",
                RawOpcode::LoadConstLong => "LoadConstLong",
                RawOpcode::GetLocal => "GetLocal",
                RawOpcode::SetLocal => "SetLocal",
                RawOpcode::GetLocalLong => "GetLocalLong",
                RawOpcode::SetLocalLong => "SetLocalLong",
                RawOpcode::Pop => "Pop",
                RawOpcode::PopN => "PopN",
                RawOpcode::PopNLong => "PopNLong",
                RawOpcode::Negate => "Negate",
                RawOpcode::Not => "Not",
                RawOpcode::Add => "Add",
                RawOpcode::Sub => "Sub",
                RawOpcode::Div => "Div",
                RawOpcode::Mul => "Mul",
                RawOpcode::Pow => "Pow",
                RawOpcode::Mod => "Mod",
                RawOpcode::Cmp => "Cmp",
                RawOpcode::CmpNot => "CmpNot",
                RawOpcode::CmpType => "CmpType",
                RawOpcode::CmpTypeNot => "CmpTypeNot",
                RawOpcode::Jump => "Jump",
                RawOpcode::JumpBack => "JumpBack",
                RawOpcode::JumpIfFalse => "JumpIfFalse",
                RawOpcode::FnCall => "FnCall",
                RawOpcode::TCOCall => "TCOCall",
                RawOpcode::FwdCall => "FwdCall",
                RawOpcode::SetLocalFast => "SetLocalFast",
                RawOpcode::Return => "Return",
                RawOpcode::GetUpvalue => "GetUpvalue",
                RawOpcode::GetUpvalueLong => "GetUpvalueLong",
                RawOpcode::SetUpvalue => "SetUpvalue",
                RawOpcode::SetUpvalueLong => "SetUpvalueLong",
                RawOpcode::MakeClosure => "MakeClosure",
                RawOpcode::MakeClosureLong => "MakeClosureLong",
                RawOpcode::LoadNativeFn => "LoadNativeFn",
                RawOpcode::Interpolate => "Interpolate",
                RawOpcode::InterpolateLong => "InterpolateLong",
                RawOpcode::MakeModule => "MakeModule",
                RawOpcode::Import => "Import",
                RawOpcode::MatchPushBound => "MatchPushBound",
                RawOpcode::MatchGetBound => "MatchGetBound",
                RawOpcode::MatchPopBound => "MatchPopBound",
                RawOpcode::MakeClass => "MakeClass",
                RawOpcode::MakeClassLong => "MakeClassLong",
                RawOpcode::Inherit => "Inherit",
                RawOpcode::GetSuper => "GetSuper",
                RawOpcode::GetSuperLong => "GetSuperLong",
                RawOpcode::SetField => "SetField",
                RawOpcode::SetFieldLong => "SetFieldLong",
                RawOpcode::GetField => "GetField",
                RawOpcode::GetFieldLong => "GetFieldLong",
                RawOpcode::SetItem => "SetItem",
                RawOpcode::GetItem => "GetItem",
                RawOpcode::MakeMethod => "MakeMethod",
                RawOpcode::InvokeMethod => "InvokeMethod",
                RawOpcode::InvokeMethodTCO => "InvokeMethodTCO",
                RawOpcode::Nil => "Nil",
                RawOpcode::True => "True",
                RawOpcode::False => "False",
                RawOpcode::One => "One",
                RawOpcode::Zero => "Zero",
                RawOpcode::Inc => "Inc",
                RawOpcode::Dec => "Dec",
                RawOpcode::IncLong => "IncLong",
                RawOpcode::DecLong => "DecLong",
                RawOpcode::GreaterEqual => "Greater",
                RawOpcode::GreaterThan => "GreaterThan",
                RawOpcode::LessEqual => "Less",
                RawOpcode::LessThan => "LessThan",
                RawOpcode::Copy => "Copy",
                RawOpcode::CopyN => "CopyN",
                RawOpcode::IsNil => "IsNil",
                RawOpcode::IsNotNil => "IsNotNil",
                RawOpcode::IntoIter => "IntoIter",
                RawOpcode::IterNext => "IterNext",
                RawOpcode::Unwrap => "Unwrap",
                RawOpcode::GetFieldFast => "GetFieldFast",
                RawOpcode::SetFieldFast => "SetFieldFast",
                RawOpcode::MakeArr => "MakeArr",
                RawOpcode::MakeDict => "MakeDict",
                RawOpcode::Print => "Print",
                RawOpcode::PrintN => "PrintN",
                RawOpcode::MulSub => "MulSub",
                RawOpcode::MulAdd => "MulAdd",
                RawOpcode::TimeoutCheck => "TimeoutCheck",
                RawOpcode::Unknown => "Unknown",
                RawOpcode::MissingJumpLabel => "MissingJumpLabel",
                RawOpcode::Error => "Error",
            },
        }
    }
}
