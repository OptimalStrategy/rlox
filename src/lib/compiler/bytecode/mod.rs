use crate::allocator::DeepSizeOf;

pub mod builder;
pub mod label;
pub mod opcode;
pub mod raw;

pub use self::builder::BytecodeBuilder;
pub use self::opcode::OpCode;
pub use self::raw::RawOpcode;

pub type Inst = u8;

#[non_exhaustive]
#[derive(Debug, Clone, PartialEq)]
pub enum Bytecode {
    Encoded(Vec<OpCode>),
    Decoded(Vec<Inst>),
    #[doc(hidden)]
    _Empty,
}

impl DeepSizeOf for Bytecode {
    #[inline]
    fn deep_size_of_children(&self) -> usize {
        match &self {
            Bytecode::Encoded(v) => crate::allocator::deep_size_of::deep_size_of_vec(v),
            Bytecode::Decoded(v) => crate::allocator::deep_size_of::deep_size_of_vec(v),
            Bytecode::_Empty => 0,
        }
    }
}

impl Bytecode {
    pub fn as_encoded(&self) -> Option<&Vec<OpCode>> {
        match self {
            Bytecode::Encoded(v) => Some(v),
            Bytecode::Decoded(_) => None,
            Bytecode::_Empty => None,
        }
    }

    pub fn as_raw(&self) -> Option<&Vec<Inst>> {
        match self {
            Bytecode::Decoded(v) => Some(v),
            Bytecode::Encoded(_) => None,
            Bytecode::_Empty => None,
        }
    }

    /// Returns the size of the bytecode in its raw form.
    /// O(1) for raw bytecode, O(N) for the symbolic representation.
    #[inline]
    pub fn raw_size_slow(&self) -> usize {
        match self {
            Bytecode::Encoded(v) => v.iter().fold(0, |acc, op| acc + op.size()),
            Bytecode::Decoded(v) => v.len(),
            Bytecode::_Empty => 0,
        }
    }
}
