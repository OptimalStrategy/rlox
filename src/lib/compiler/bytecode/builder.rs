use std::collections::HashMap;

use super::RawOpcode;
use crate::vm::object::Value;
use vm::immutable_string::hash;

#[derive(Debug, Clone)]
pub enum BuilderValue {
    Value(Value),
    Str(String),
}

impl From<String> for BuilderValue {
    fn from(v: String) -> BuilderValue {
        BuilderValue::Str(v)
    }
}

impl From<&str> for BuilderValue {
    fn from(v: &str) -> BuilderValue {
        BuilderValue::Str(v.to_owned())
    }
}

impl<V> From<V> for BuilderValue
where
    V: Into<Value>,
{
    fn from(v: V) -> BuilderValue {
        BuilderValue::Value(v.into())
    }
}

#[derive(Debug, Clone)]
pub struct BytecodeBuilder {
    bytecode: Vec<u8>,
    consts: HashMap<u64, usize>,
}

impl BytecodeBuilder {
    pub fn new() -> Self {
        Self {
            bytecode: vec![],
            consts: HashMap::new(),
        }
    }

    pub fn build_bytes(self) -> Vec<u8> {
        self.op_make_module().push(RawOpcode::Return).bytecode
    }

    pub fn build_bytes_with_timeout(self) -> Vec<u8> {
        self.op_timeout()
            .op_make_module()
            .push(RawOpcode::Return)
            .bytecode
    }

    pub fn push<B: Into<u8>>(mut self, byte: B) -> Self {
        self.bytecode.push(byte.into());
        self
    }

    pub fn add_const<V: Into<BuilderValue>>(&mut self, v: V) -> Option<usize> {
        let v = v.into();
        Self::hash_of(&v).map(|hash| {
            let len = self.consts.len();
            *self.consts.entry(hash).or_insert(len)
        })
    }

    pub fn add_local<V: Into<Value>>(self, _label: &str, v: V) -> Self {
        let v = v.into();
        if let Value::Nil = v {
            self.push(RawOpcode::Nil)
        } else {
            self.op_load_const(v)
        }
    }

    pub fn hash_of(v: &BuilderValue) -> Option<u64> {
        Some(match v {
            BuilderValue::Value(Value::Int(i)) => hash(i),
            BuilderValue::Value(Value::Float(f)) => hash(f.to_be_bytes()),
            BuilderValue::Value(Value::Bool(b)) => hash(b),
            BuilderValue::Str(s) => hash(s),
            _ => return None,
        })
    }

    pub fn op_load_const<V: Into<BuilderValue>>(mut self, v: V) -> Self {
        let id = self
            .add_const(v.into())
            .expect("Cannot store non-primitive values (note: use op_nil() for Nil values)")
            as u8;
        self.push(RawOpcode::LoadConst).push(id)
    }

    pub fn op_load_const_long<V: Into<BuilderValue>>(mut self, v: V) -> Self {
        let id = self
            .add_const(v.into())
            .expect("Cannot store non-primitive values (note: use op_nil() for Nil values)")
            as u16;
        self.push(RawOpcode::LoadConstLong).int_16(id)
    }

    pub fn op_get_local(self, at: u8) -> Self {
        self.push(RawOpcode::GetLocal).int_8(at)
    }

    pub fn op_set_local(self, at: u8) -> Self {
        self.push(RawOpcode::SetLocal).int_8(at)
    }

    pub fn op_get_local_long(self, at: u16) -> Self {
        self.push(RawOpcode::GetLocalLong).int_16(at)
    }

    pub fn op_set_local_long(self, at: u16) -> Self {
        self.push(RawOpcode::SetLocalLong).int_16(at)
    }

    pub fn op_pop(self) -> Self {
        self.push(RawOpcode::Pop)
    }

    pub fn op_pop_n(self, n: u8) -> Self {
        self.push(RawOpcode::PopN).int_8(n)
    }

    pub fn op_pop_n_long(self, n: u16) -> Self {
        self.push(RawOpcode::PopNLong).int_16(n)
    }

    pub fn op_timeout(self) -> Self {
        self.push(RawOpcode::TimeoutCheck)
    }

    pub fn op_negate(self) -> Self {
        self.push(RawOpcode::Negate)
    }

    pub fn op_not(self) -> Self {
        self.push(RawOpcode::Not)
    }

    pub fn op_add(self) -> Self {
        self.push(RawOpcode::Add)
    }

    pub fn op_sub(self) -> Self {
        self.push(RawOpcode::Sub)
    }

    pub fn op_mul(self) -> Self {
        self.push(RawOpcode::Mul)
    }

    pub fn op_div(self) -> Self {
        self.push(RawOpcode::Div)
    }

    pub fn op_pow(self) -> Self {
        self.push(RawOpcode::Pow)
    }

    pub fn op_mod(self) -> Self {
        self.push(RawOpcode::Mod)
    }

    pub fn op_cmp(self) -> Self {
        self.push(RawOpcode::Cmp)
    }

    pub fn op_cmp_not(self) -> Self {
        self.push(RawOpcode::CmpNot)
    }

    pub fn op_mul_add(self) -> Self {
        self.push(RawOpcode::MulAdd)
    }

    pub fn op_mul_sub(self) -> Self {
        self.push(RawOpcode::MulSub)
    }

    pub fn op_jump(self, n: u16) -> Self {
        self.push(RawOpcode::Jump).int_16(n)
    }

    pub fn op_jump_back(self, n: u16) -> Self {
        self.push(RawOpcode::JumpBack).int_16(n)
    }

    pub fn op_jump_zero(self, n: u16) -> Self {
        self.push(RawOpcode::JumpIfFalse).int_16(n)
    }

    pub fn op_jump_if_false(self, n: u16) -> Self {
        self.op_jump_zero(n)
    }

    pub fn op_break(self, n_locals: usize, offset: u16) -> Self {
        if n_locals == 0 {
            self
        } else if n_locals == 1 {
            self.push(RawOpcode::Pop)
        } else if n_locals < 256 {
            self.push(RawOpcode::PopN).int_8(n_locals as u8)
        } else if n_locals < 65536 {
            self.push(RawOpcode::PopNLong).int_16(n_locals as u16)
        } else {
            panic!("Invalid number of locals: {}", n_locals)
        }
        .op_jump(offset)
    }

    pub fn op_continue(self, n_locals: usize, offset: u16) -> Self {
        if n_locals == 0 {
            self
        } else if n_locals == 1 {
            self.push(RawOpcode::Pop)
        } else if n_locals < 256 {
            self.push(RawOpcode::PopN).int_8(n_locals as u8)
        } else if n_locals < 65536 {
            self.push(RawOpcode::PopNLong).int_16(n_locals as u16)
        } else {
            panic!("Invalid number of locals: {}", n_locals)
        }
        .op_jump_back(offset)
    }

    pub fn op_fn_call(self, n_args: u8) -> Self {
        self.push(RawOpcode::FnCall).int_8(n_args)
    }

    pub fn op_return(self) -> Self {
        self.push(RawOpcode::Return)
    }

    pub fn op_get_upvalue(self, addr: u8) -> Self {
        self.push(RawOpcode::GetUpvalue).int_8(addr)
    }

    pub fn op_get_upvalue_long(self, addr: u16) -> Self {
        self.push(RawOpcode::GetUpvalueLong).int_16(addr)
    }

    pub fn op_set_upvalue(self, addr: u8) -> Self {
        self.push(RawOpcode::GetUpvalue).int_8(addr)
    }

    pub fn op_set_upvalue_long(self, addr: u16) -> Self {
        self.push(RawOpcode::SetUpvalueLong).int_16(addr)
    }

    pub fn op_make_closure(self, addr: u8) -> Self {
        self.push(RawOpcode::MakeClosure).int_8(addr)
    }

    pub fn op_make_closure_long(self, addr: u16) -> Self {
        self.push(RawOpcode::MakeClosureLong).int_16(addr)
    }

    pub fn op_load_native_fn(self, addr: u8) -> Self {
        self.push(RawOpcode::LoadNativeFn).int_8(addr)
    }

    pub fn op_interpolate(self, n: u8) -> Self {
        self.push(RawOpcode::Interpolate).int_8(n)
    }

    pub fn op_interpolate_long(self, n: u16) -> Self {
        self.push(RawOpcode::InterpolateLong).int_16(n)
    }

    pub fn op_make_module(self) -> Self {
        let idx = self.consts.len();
        self.push(RawOpcode::MakeModule).int_16(idx as u16)
    }

    pub fn op_nil(self) -> Self {
        self.push(RawOpcode::Nil)
    }

    pub fn op_true(self) -> Self {
        self.push(RawOpcode::True)
    }

    pub fn op_false(self) -> Self {
        self.push(RawOpcode::False)
    }

    pub fn op_one(self) -> Self {
        self.push(RawOpcode::One)
    }

    pub fn op_zero(self) -> Self {
        self.push(RawOpcode::Zero)
    }

    pub fn op_copy(self) -> Self {
        self.push(RawOpcode::Copy)
    }

    pub fn op_match_push_bound(self) -> Self {
        self.push(RawOpcode::MatchPushBound)
    }

    pub fn op_match_get_bound(self) -> Self {
        self.push(RawOpcode::MatchGetBound)
    }

    pub fn op_match_pop_bound(self) -> Self {
        self.push(RawOpcode::MatchPopBound)
    }

    pub fn op_inc(self, slot: u8) -> Self {
        self.push(RawOpcode::Inc).int_8(slot)
    }

    pub fn op_dec(self, slot: u8) -> Self {
        self.push(RawOpcode::Dec).int_8(slot)
    }

    pub fn op_inc_long(self, slot: u16) -> Self {
        self.push(RawOpcode::IncLong).int_16(slot)
    }

    pub fn op_dec_long(self, slot: u16) -> Self {
        self.push(RawOpcode::DecLong).int_16(slot)
    }

    pub fn op_greater_equal(self) -> Self {
        self.push(RawOpcode::GreaterEqual)
    }

    pub fn op_greater_than(self) -> Self {
        self.push(RawOpcode::GreaterThan)
    }

    pub fn op_less_equal(self) -> Self {
        self.push(RawOpcode::LessEqual)
    }

    pub fn op_less_than(self) -> Self {
        self.push(RawOpcode::LessThan)
    }

    pub fn op_print(self) -> Self {
        self.push(RawOpcode::Print)
    }

    pub fn op_print_n(self, n: u8) -> Self {
        self.push(RawOpcode::PrintN).int_8(n)
    }

    pub fn upvalue(self, is_local: bool, index: u8) -> Self {
        self.int_8(is_local as u8).int_8(index)
    }

    pub fn upvalue_long(self, is_local: bool, index: u16) -> Self {
        self.int_8(is_local as u8).int_16(index)
    }

    pub fn int_8(self, i: u8) -> Self {
        self.push(i)
    }

    pub fn int_16(mut self, i: u16) -> Self {
        self.bytecode.extend(BE!(i));
        self
    }
}
