//! Defines the context for recording and reporting errors.
use crate::{
    errors::{report_error, LoxError},
    module::ModuleId,
    SharedCtx,
};

/// A context for reporting and recording errors.
#[derive(Debug, Default, Clone)]
pub struct ErrCtx {
    /// The list of errors or warnings recorded by this context.
    pub errors: Vec<LoxError>,
    /// Whether the context has recorded at least one error.
    pub had_error: bool,
    /// Whether the context has recorded at least one warning.
    pub had_warning: bool,
    /// The local, non-root module, that corresponds to this error context.
    local_module: Option<ModuleId>,
}

impl ErrCtx {
    /// Creates a new empty error context.
    pub fn new() -> Self {
        ErrCtx {
            errors: vec![],
            had_error: false,
            had_warning: false,
            local_module: None,
        }
    }

    /// Updates the local module of the context.
    pub fn with_local_module(module: ModuleId) -> Self {
        Self {
            local_module: Some(module),
            ..Self::new()
        }
    }

    /// Clears the context in-place.
    pub fn clear(&mut self) {
        *self = Self::new();
    }

    /// Creates a context and records the given error.
    pub fn with_error(e: LoxError) -> Self {
        let mut ex = Self::new();
        ex.record(e);
        ex
    }

    /// Records the given error in the context. If `local_module` is present,
    /// updates the error's module to that.
    pub fn record(&mut self, err: LoxError) {
        self.had_warning |= err.is_warning();
        self.had_error |= !err.is_warning();
        self.errors.push(if let Some(module) = self.local_module {
            err.with_module(module)
        } else {
            err
        })
    }

    /// Records the error if `res` is an Err and converts it into an [`Option`].
    pub fn maybe_record<T>(&mut self, res: Result<T, LoxError>) -> Option<T> {
        res.map_err(|e| self.record(e)).ok()
    }

    /// Records multiple errors.
    pub fn record_many(&mut self, errors: Vec<LoxError>) {
        errors.into_iter().for_each(|e| self.record(e));
    }

    /// Records the errors if `res` is an Err.
    pub fn maybe_record_many<T>(&mut self, res: Result<T, Vec<LoxError>>) -> Option<T> {
        res.map_err(|e| self.record_many(e)).ok()
    }

    /// Updates the current context with the errors from the given context.
    pub fn extend_from_another_ex(&mut self, ex: ErrCtx) {
        self.had_error |= ex.had_error;
        self.had_warning |= ex.had_warning;
        self.errors.extend(ex.errors);
    }

    /// Reports all errors without pretty-formatting them.
    pub fn report_all_fast(&self) {
        for e in &self.errors {
            eprintln!("{}\n", e)
        }
    }

    /// Reports all errors in a pretty way.
    pub fn report_all(&self, ctx: SharedCtx) {
        let guard = ctx.read_shared();
        let root = if guard.config.print_full_path {
            None
        } else {
            Some(guard.root.as_path())
        };
        for e in &self.errors {
            let info = guard
                .get_module_source_info(e.module).ok_or_else(|| {
                    eprintln!("Failed to retrieve the module {:?} from the registry while reporting the following error:\n{:#?}", e.module, e);
                }).unwrap();
            report_error(e, &info.source, &info.filename, root);
        }
    }

    /// Converts the context into an `Ok(ok)` if it's empty, Err(self.errors) otherwise.
    pub fn into_result<T>(self, ok: T) -> Result<T, Vec<LoxError>> {
        if self.errors.is_empty() {
            Ok(ok)
        } else {
            Err(self.errors)
        }
    }

    /// Converts the context into an `Ok(ok(self))` if it's empty, Err(self.errors) otherwise.
    pub fn into_result_map<T>(self, ok: impl Fn(ErrCtx) -> T) -> Result<T, Vec<LoxError>> {
        if self.errors.is_empty() {
            Ok(ok(self))
        } else {
            Err(self.errors)
        }
    }
}
