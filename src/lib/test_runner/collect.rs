use std::{
    collections::HashSet,
    error::Error,
    path::PathBuf,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc, Mutex,
    },
    thread::JoinHandle,
};

use crossbeam_channel::{unbounded, Receiver, Sender};

use super::TestDiscoveryArgs;
use crate::SharedCtx;

type SharedPaths = Arc<Mutex<Vec<PathBuf>>>;

struct WorkerHandle {
    is_idling: Arc<AtomicBool>,
    handle: JoinHandle<()>,
}

enum WorkerJob {
    Collect(PathBuf),
    Terminate,
}

pub fn collect_test_modules(
    ctx: SharedCtx,
    args: TestDiscoveryArgs<'_>,
) -> Result<Vec<PathBuf>, Box<dyn Error>> {
    thread_debug!(
        "Looking for test modules in {}",
        args.root_directory.display()
    );

    let n_threads = ctx.max_compiler_threads();
    let (tx, rx) = unbounded();
    let mut workers = Vec::with_capacity(n_threads);

    tx.send(WorkerJob::Collect(args.root_directory.clone()))
        .unwrap();

    let paths = Arc::new(Mutex::new(Vec::new()));

    for _ in 0..n_threads {
        workers.push(spawn_collector_thread(
            &args,
            paths.clone(),
            tx.clone(),
            rx.clone(),
        ));
    }

    loop {
        let mut all_idle = true;
        for worker in &workers {
            all_idle &= worker.is_idling.load(Ordering::Acquire);
        }
        if all_idle && rx.is_empty() {
            thread_debug!("All workers appear to be idle and the queue is empty. Sending the termination requests...");
            for _ in 0..n_threads {
                let _ = tx.send(WorkerJob::Terminate);
            }
            break;
        }
        std::thread::yield_now();
    }

    thread_debug!("... Requests sent. Waiting for the workers to terminate.");
    let mut had_error = false;
    for worker in workers {
        let id = worker.handle.thread().id().as_u64();
        match worker.handle.join() {
            Ok(_) => (),
            Err(e) => {
                eprintln!("Thread {} terminated with an error: {:?}", id, e);
                had_error = true;
            }
        }
    }

    if had_error {
        return Err(Box::from(
            "One or more errors have occurred while discovering test modules. Exiting.",
        ));
    }

    thread_debug!("Looks like all workers have exited successfully.");

    let mut paths = paths.lock().unwrap();
    Ok(std::mem::take(&mut paths))
}

fn spawn_collector_thread(
    args: &TestDiscoveryArgs<'_>,
    paths: SharedPaths,
    tx: Sender<WorkerJob>,
    rx: Receiver<WorkerJob>,
) -> WorkerHandle {
    let prefix = args.test_prefix.to_owned();
    let is_idling = Arc::new(AtomicBool::new(false));
    let ignored_dirs = Arc::clone(&args.ignored_dirs);
    WorkerHandle {
        is_idling: is_idling.clone(),
        handle: std::thread::spawn(move || {
            test_collector(prefix, paths, ignored_dirs, is_idling, tx, rx);
        }),
    }
}

fn test_collector(
    test_prefix: String,
    shared_paths: SharedPaths,
    ignored_dirs: Arc<HashSet<String>>,
    is_idling: Arc<AtomicBool>,
    queue_tx: Sender<WorkerJob>,
    queue_rx: Receiver<WorkerJob>,
) {
    thread_debug!("Spawned a test collector thread.");
    let mut local_paths = Vec::new();
    loop {
        is_idling.store(true, Ordering::Release);
        let path = match queue_rx.recv() {
            Ok(path) => match path {
                WorkerJob::Collect(path) => path,
                WorkerJob::Terminate => {
                    thread_debug!("Received a shutdown request. Terminating the thread...");
                    break;
                }
            },
            Err(_) => {
                thread_debug!("rx.recv() failed. Terminating the thread...");
                break;
            }
        };
        is_idling.store(false, Ordering::Release);

        // Let the thread panic
        std::fs::read_dir(path).unwrap().for_each(|entry| {
            if let Ok(e) = entry.map_err(|_e| {
                thread_error!("Couldn't read a file or directory: {}", _e);
            }) {
                let path = e.path();
                if path.is_file() {
                    // Ignore all non-lox files.
                    if !path.extension().map(|ext| ext == "lox").unwrap_or(false) {
                        return;
                    }

                    // Avoid collection the files that don't start with the prefix.
                    if !path
                        .file_name()
                        .map(|p| p.to_string_lossy().starts_with(&test_prefix))
                        .unwrap_or(false)
                    {
                        return;
                    }

                    thread_debug!("Collected a test module: {:?}", path.display());
                    local_paths.push(path);
                    return;
                } else if path
                    .file_name()
                    .map(|p| ignored_dirs.contains(p.to_string_lossy().as_ref()))
                    .unwrap_or(true)
                {
                    thread_debug!("Explicitly ignoring the path: {}", path.display());
                    return;
                }
                queue_tx
                    .send(WorkerJob::Collect(path))
                    .expect("Couldn't put a path in the queue.");
            };
        });
        continue;
    }

    thread_debug!("Thread has collected {} path(s)", local_paths.len());
    shared_paths.lock().unwrap().append(&mut local_paths);
}
