pub mod collect;

use std::{
    collections::HashSet,
    error::Error,
    io::Write,
    path::PathBuf,
    sync::Arc,
    time::{Duration, Instant},
};

use crossbeam_channel::{bounded, unbounded, Receiver, Sender};

use self::collect::collect_test_modules;
use crate::{
    allocator::Allocator,
    context::Context,
    module::ModuleId,
    vm::{traceback::TraceBack, Vm},
    SharedCtx,
};

pub static DEFAULT_IGNORED_DIRS: &[&str] = &[".git", "target", "loxbuild"];

#[derive(Debug)]
pub struct TestDiscoveryArgs<'a> {
    pub ignored_dirs: Arc<HashSet<String>>,
    pub root_directory: PathBuf,
    pub test_prefix: &'a str,
}

impl<'a> TestDiscoveryArgs<'a> {
    pub fn with_default_ignored_dirs(root_directory: PathBuf, test_prefix: &'a str) -> Self {
        Self {
            root_directory,
            test_prefix,
            ignored_dirs: Arc::new(
                DEFAULT_IGNORED_DIRS
                    .iter()
                    .copied()
                    .map(String::from)
                    .collect(),
            ),
        }
    }
}

pub fn run_lox_tests(ctx: SharedCtx, args: TestDiscoveryArgs<'_>) -> Result<(), Box<dyn Error>> {
    let test_start = Instant::now();
    let stdout = std::io::stdout();
    print!(":: Looking for lox tests ... ");
    stdout.lock().flush()?;

    let prefix = args.test_prefix.to_owned();
    let root = args.root_directory.clone();
    let modules = collect_test_modules(ctx.clone(), args)?;
    println!("Done.");
    println!(
        ":: Discovered {} test module(s) in {:.3} ms.",
        modules.len(),
        test_start.elapsed().as_secs_f64() * 1000.0,
    );

    let (module_tx, module_rx) = bounded(modules.len());
    for path in modules {
        let cloned = ctx.clone();
        let guard = ctx.write_shared().queue_rogue_module(cloned, path);
        module_tx.send(guard.get()).unwrap();
    }

    Context::<Allocator>::wait_for_workers(ctx.clone())?;
    if !Context::<Allocator>::check_worker_errors_and_import_graph(ctx.clone()) {
        std::process::exit(65);
    }

    let (result_tx, result_rx) = unbounded();
    let n_threads = if true {
        // TODO: run tests in parallel
        // XXX: How should the threads report test results if they all run in parallel?
        // XXX: How will the GC know about the object graphs belonging to other VMs?
        1
    } else {
        ctx.max_compiler_threads();
        unimplemented!("Parallel execution isn't supported.");
    };
    let mut handles = Vec::with_capacity(n_threads);
    for _ in 0..n_threads {
        let ctx = ctx.clone();
        let root = root.clone();
        let prefix = prefix.to_owned();
        let tx = result_tx.clone();
        let rx = module_rx.clone();
        handles.push(std::thread::spawn(move || {
            test_runner(ctx, root, prefix, tx, rx);
        }));
    }

    std::mem::drop(result_tx);

    let mut n_passed = 0;
    let mut n_total = 0;
    while let Ok(result) = result_rx.recv() {
        let (passed, total) = result.report_to_stdout(ctx.clone());
        n_passed += passed;
        n_total += total;
    }

    for handle in handles {
        handle.join().expect("A test runner thread has panicked.");
    }

    let runtime_msg = format!("{:.3} ms", test_start.elapsed().as_secs_f64() * 1000.0);
    let passed_msg = format!("{} / {}", n_passed, n_total);
    let mut pad_passed = 0;
    let mut pad_runtime = 0;
    if runtime_msg.len() + /* "Test suite completed in ".len() */ 24
        > passed_msg.len() + /* "Passed  Total".len() */ 13
    {
        pad_passed = runtime_msg.len() + 24 - 13;
    } else {
        pad_runtime = passed_msg.len() + 13 - runtime_msg.len() - 24;
    }
    if pad_passed != 0 && pad_passed % 2 == 0 {
        pad_passed += 1;
        pad_runtime += 2;
    }
    if pad_runtime != 0 && pad_runtime % 2 == 0 {
        pad_runtime += 1;
        pad_passed += 2;
    }
    println!(
        "::\n:: [ Passed {:^pass_width$} Total ]\n:: [ Test suite completed in {}{} ]",
        passed_msg,
        " ".repeat(pad_runtime),
        runtime_msg,
        pass_width = pad_passed,
    );

    Ok(())
}

fn test_runner(
    ctx: SharedCtx,
    root: PathBuf,
    prefix: String,
    tx: Sender<ModuleTestResults>,
    rx: Receiver<ModuleId>,
) {
    // TODO: introduce thread-local allocators
    while let Ok(id) = rx.try_recv() {
        let test_start = Instant::now();

        let module = ctx.read_shared().get_module(id).unwrap();
        let name = module
            .source_info
            .filename
            .replace(root.to_string_lossy().as_ref(), "")
            .trim_start_matches('/')
            .replace(['/', '\\'], ".")
            .trim_end_matches(".lox")
            .to_owned();
        let cco = module.cco_ptr().unwrap();

        let module_loading_time = Instant::now();
        if let Err(tb) = Vm::new(ctx.clone()).run(cco) {
            eprintln!(":: Failed to collect the tests in `{}.lox`:", name);
            tb.display(ctx.clone());
            continue;
        }

        let module = ctx.read_shared().get_module(id).unwrap();
        let mut results = ModuleTestResults {
            module: name,
            module_loading_time: module_loading_time.elapsed(),
            results: Vec::new(),
            runtime: Duration::from_millis(0),
        };
        let module = unsafe { module.lox_module_object() }.unwrap();

        let tests = module
            .exports()
            .values()
            .filter_map(|v| {
                v.as_heap_value().and_then(|h| h.as_cco()).and_then(|cco| {
                    if cco.name.starts_with(&prefix) {
                        Some(*v.as_ptr().unwrap())
                    } else {
                        None
                    }
                })
            })
            .collect::<Vec<_>>();

        results.results = Vec::with_capacity(tests.len());

        for test in tests {
            let start = Instant::now();
            let traceback = Vm::new(ctx.clone()).run(test).err();
            let test = test.as_ref().as_cco().name.clone().into_owned();
            results.results.push(TestResult {
                test,
                traceback,
                runtime: start.elapsed(),
            });
        }

        results.runtime = test_start.elapsed();

        tx.send(results).unwrap();
    }
}

#[derive(Debug)]
struct TestResult {
    test: String,
    runtime: Duration,
    traceback: Option<TraceBack>,
}

impl TestResult {
    pub fn is_success(&self) -> bool {
        self.traceback.is_none()
    }
}

#[derive(Debug)]
struct ModuleTestResults {
    module: String,
    runtime: Duration,
    module_loading_time: Duration,
    results: Vec<TestResult>,
}

impl ModuleTestResults {
    pub fn report_to_stdout(&self, ctx: SharedCtx) -> (usize, usize) {
        let stderr = std::io::stderr();
        let mut out = stderr.lock();

        writeln!(
            out,
            ":: Collected {} test(s) in {}.lox in {:.3} ms",
            self.results.len(),
            self.module,
            self.module_loading_time.as_secs_f64() * 1000.0
        )
        .unwrap();

        if self.results.is_empty() {
            return (0, 0);
        }

        let n_tests = self.results.len();
        let n_spaces = n_tests.to_string().len(); // ugh
        let n_failed = self.results.iter().map(|r| !r.is_success() as usize).sum();
        let largest_magnitude = ((self.runtime.as_secs_f64() * 1000.0).trunc() as u64)
            .to_string()
            .len();

        writeln!(
            out,
            "::\n:: Report for `{}.lox` [{} / {}]",
            self.module,
            n_tests - n_failed,
            n_tests
        )
        .unwrap();

        let longest_name = self.results.iter().map(|r| r.test.len()).max().unwrap();
        self.results.iter().enumerate().for_each(|(i, r)| {
            let runtime = r.runtime.as_secs_f64() * 1000.0;
            let magnitude = (runtime.trunc() as u64).to_string().len();
            let runtime = format!(
                "{}{:.3}",
                " ".repeat(largest_magnitude - magnitude),
                runtime
            );
            writeln!(
                out,
                "[{:>width$} \\ {}] Executing `{}` {} {} {} ms",
                i + 1,
                n_tests,
                r.test,
                ".".repeat(3 + longest_name - r.test.len()),
                if r.is_success() { "OK   " } else { "ERROR" },
                runtime,
                width = n_spaces
            )
            .unwrap();
            if let Some(tb) = &r.traceback {
                tb.display(ctx.clone());
            }
        });

        let status_message = match n_failed {
            0 => "SUCCESS",
            _ if n_failed == n_tests => "ALL TESTS FAILED",
            _ => "SOME TESTS FAILED",
        };
        let spaces = " ".repeat(n_spaces * 2 + 24 + longest_name - status_message.len());
        writeln!(
            out,
            ":: {} {} {:.3} ms\n::",
            status_message,
            spaces,
            self.runtime.as_secs_f64() * 1000.0
        )
        .unwrap();

        (n_tests - n_failed, n_tests)
    }
}
