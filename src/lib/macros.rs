#[macro_export]
macro_rules! consume_if {
    ($self:ident, $lexeme:ident, $otherwise:path, $lex:tt => $if_match:path) => {{
        let start = $self.current - 1;
        let mut kind = $otherwise;
        if $self.r#match($lex) {
            $lexeme = unsafe {
                $crate::frontend::scanner::concat_slices(&$self.source[start..$self.current])
            };
            kind = $if_match;
        }
        token!($self, $lexeme, kind)
    }};
    ($self:ident, $lexeme:ident, $otherwise:path, $lex_1:tt => $if_match_1:path, $lex_2:tt => $if_match_2:path) => {{
        let start = $self.current - 1;
        let mut kind = $otherwise;
        if $self.r#match($lex_1) {
            $lexeme = unsafe {
                $crate::frontend::scanner::concat_slices(&$self.source[start..$self.current])
            };
            kind = $if_match_1;
        } else if $self.r#match($lex_2) {
            $lexeme = unsafe {
                $crate::frontend::scanner::concat_slices(&$self.source[start..$self.current])
            };
            kind = $if_match_2;
        }
        token!($self, $lexeme, kind)
    }};
}

#[macro_export]
macro_rules! token {
    ($self: ident, $lexeme: expr, $kind: expr) => {
        Token::with_fixed_column($kind, $lexeme, $self.line, $self.column)
    };
}

#[macro_export]
macro_rules! compare_discriminants {
    ($left:expr, $right: expr) => {
        std::mem::discriminant($left) == std::mem::discriminant($right)
    };
}

#[macro_export]
macro_rules! capture_span {
    ($self:ident, $span:ident, $code:block) => {{
        let span_start = $self.previous().span;
        let result = $code;
        let span_end = $self.previous().span;
        $span = $crate::parser::combine_spans(span_start, span_end);
        result
    }};
    ($self:ident, $code:block) => {{
        let span_start = $self.previous().span;
        let result = $code;
        let span_end = $self.previous().span;
        let span = $crate::parser::combine_spans(span_start, span_end);
        (span, result)
    }};
    ($self:ident, $code:expr) => {
        capture_span!($self, { $code })
    };
}

#[macro_export]
macro_rules! stmt {
    ($stmt:expr, $span:expr) => {
        Stmt {
            kind: $stmt,
            span: $span,
        }
    };
}

#[macro_export]
macro_rules! stmt_ok {
    ($stmt:expr, $span:expr) => {
        Ok(stmt!($stmt, $span))
    };
}

#[macro_export]
macro_rules! expr {
    ($expr:expr, $span:expr) => {
        Expr {
            kind: $expr,
            span: $span,
        }
    };
}

#[macro_export]
macro_rules! expr_ok {
    ($expr:expr, $span:expr) => {
        Ok(expr!($expr, $span))
    };
}

#[macro_export]
macro_rules! combspan {
    ($node_left:expr, $node_right:expr) => {
        combspan!(span => $node_left.span, span => $node_right.span)
    };
    (span => $node_left:expr, $node_right:expr) => {
        combspan!(span => $node_left, span => $node_right.span)
    };
    ($node_left:expr, span => $node_right:expr) => {
        combspan!(span => $node_left.span, span => $node_right)
    };
    (span => $node_left:expr, span => $node_right:expr) => {
        $crate::parser::combine_spans($node_left, $node_right).unwrap()
    };
}

#[macro_export]
macro_rules! parse_rhs_operators {
    ($self:ident, $expr:ident, $right:expr) => {{
        let op = $self.previous().clone();
        let kind: Option<BinOpKind> = op.kind.into();
        let right = $right?;
        let span = combspan!($expr, right);
        let binop = BinOp {
            node: kind.unwrap(),
            span: op.span,
        };
        expr!(
            ExprKind::Binary(binop, Box::new($expr), Box::new(right)),
            span
        )
    }};
}

#[macro_export]
macro_rules! gen_from_for_enum {
    ($T:ty, $Enum:path, $variant:ident) => {
        impl From<$T> for $Enum {
            fn from(v: $T) -> Self {
                Self::$variant(v)
            }
        }
    };
}

#[macro_export]
macro_rules! unary_num_map {
    ($lit:expr, $op:tt) => {{
        use $crate::ast::LitValue;
        match $lit {
            LitValue::Integer(i) => LitValue::from($op i),
            LitValue::Float(n) => LitValue::from($op n),
            rest => rest,
        }
    }};
    ($lit:expr, $f:expr) => {{
        use $crate::ast::LitValue;
        match $lit {
            LitValue::Integer(i) => LitValue::from(($f)(i)),
            LitValue::Float(n) => LitValue::from(($f)(n)),
            rest => rest,
        }
    }};
}

#[macro_export]
macro_rules! binary_num_map {
    ($left:expr, $right:expr, +) => {{
        use $crate::ast::LitValue;
        match (&$left, &$right) {
            (LitValue::Integer(l), LitValue::Integer(r)) => Some(LitValue::from((std::num::Wrapping(*l) + std::num::Wrapping(*r)).0)),
            (LitValue::Float(l), LitValue::Float(r)) => Some(LitValue::from(l + r)),
            (LitValue::Integer(l), LitValue::Float(r)) => Some(LitValue::from((*l as $crate::types::LoxFloat) + *r)),
            (LitValue::Float(l), LitValue::Integer(r)) => Some(LitValue::from(*l + (*r as $crate::types::LoxFloat))),
            (LitValue::Str(l), LitValue::Str(r)) => Some(LitValue::from(l.to_owned() + &r)),
            _ => None,
        }
    }};
    ($left:expr, $right:expr, $op:tt) => {{
        use $crate::ast::LitValue;
        match (&$left, &$right) {
            (LitValue::Integer(l), LitValue::Integer(r)) => Some(LitValue::from((std::num::Wrapping(*l) $op std::num::Wrapping(*r)).0)),
            (LitValue::Float(l), LitValue::Float(r)) => Some(LitValue::from(*l $op *r)),
            (LitValue::Integer(l), LitValue::Float(r)) => Some(LitValue::from(*l as $crate::types::LoxFloat $op *r)),
            (LitValue::Float(l), LitValue::Integer(r)) => Some(LitValue::from(*l $op *r as $crate::types::LoxFloat)),
            _ => None,
        }
    }};
}

#[macro_export]
macro_rules! binary_ord_map {
    ($left:expr, $right:expr, $op:tt) => {{
        use $crate::ast::LitValue;
        match (&$left, &$right) {
            (LitValue::Integer(l), LitValue::Integer(r)) => Some(LitValue::from(l $op r)),
            (LitValue::Float(l), LitValue::Float(r)) => Some(LitValue::from(l $op r)),
            (LitValue::Integer(l), LitValue::Float(r)) => Some(LitValue::from((*l as $crate::types::LoxFloat) $op *r)),
            (LitValue::Float(l), LitValue::Integer(r)) => Some(LitValue::from(*l $op (*r as $crate::types::LoxFloat))),
            _ => None,
        }
    }};
}

#[macro_export]
macro_rules! fold_binary_op {
    ($expr:ident, $left:expr, $right:expr, $op:tt) => {
        assign_lit_node_if_some!(
            $expr,
            $left,
            $right,
            binary_num_map!($left.value, $right.value, $op)
        )
    };
    (ord => $expr:ident, $left:expr, $right:expr, $op:tt) => {
        assign_lit_node_if_some!(
            $expr,
            $left,
            $right,
            binary_ord_map!($left.value, $right.value, $op)
        )
    };
}

#[macro_export]
macro_rules! assign_lit_node_if_some {
    ($expr:expr, $left:expr, $right:expr, $value:expr) => {
        if let Some(value) = $value {
            $expr.kind = $crate::ast::ExprKind::Lit(Box::new($crate::ast::Lit {
                token: $crate::token::Token::eof_from_span(
                    combspan!(span => $left.token.span, span => $right.token.span),
                ),
                value,
            }))
        }
    };
    ($expr:expr, $operand:expr, $value:expr) => {
        if let Some(value) = $value {
            $expr.kind = $crate::ast::ExprKind::Lit(Box::new($crate::ast::Lit {
                token: $operand.token.clone(),
                value,
            }))
        }
    };
}

#[macro_export]
macro_rules! BE {
    ($b:expr) => {
        $b.to_be_bytes().to_vec()
    };
}

#[macro_export]
macro_rules! BE_from {
    ($b:expr) => {{
        use std::convert::TryInto;
        u16::from_be_bytes(
            (&$b)
                .try_into()
                .expect("Expected the slice to have at least 2 bytes"),
        )
    }};
}

#[macro_export]
macro_rules! impl_vtable_deepsize {
    ($Ty:ty, $field:ident, $ValueTy:ty) => {
        impl $crate::allocator::DeepSizeOf for $Ty {
            #[inline(always)]
            fn deep_size_of_children(&self) -> usize {
                $crate::allocator::deep_size_of::deep_size_of_map::<&'static str, $ValueTy>(
                    self.$field.capacity(),
                )
            }
        }
    };
}

#[macro_export]
macro_rules! known_deep_size_fast {
    ($size:expr, $($type:ty),+) => (
        $(
            impl $crate::allocator::DeepSizeOf for $type {
                #[inline(always)]
                fn deep_size_of_children(&self) -> usize {
                    $size
                }
            }
        )+
    );
}

#[macro_export]
macro_rules! thread_debug {
    ($($arg:tt)*) => {
        #[cfg(feature = "worker-debug-info")]
        {

            let thread_id = u64::from(std::thread::current().id().as_u64()) - 1u64;
            eprintln!(
                "[{}] {}",
                if thread_id == 0 {
                     "Main    ".to_string()
                } else {
                    format!("Worker {}", thread_id)
                },
                format!($($arg)*)
            );
        }
    };
}

#[macro_export]
macro_rules! thread_error {
    ($($arg:tt)*) => {
        {
            let thread_id = u64::from(std::thread::current().id().as_u64()) - 1u64;
            eprintln!(
                "[{}] {}",
                if thread_id == 0 {
                     "Main    ".to_string()
                } else {
                    format!("Worker {}", thread_id)
                },
                format!($($arg)*)
            );
        }
    };
}
