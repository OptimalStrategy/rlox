//! Contains the implementation of the pipeline worker and the definitions of
//! of the channel and message types.
use std::path::{Path, PathBuf};

use crossbeam_channel::{Receiver, Sender};

use crate::{
    err_context::ErrCtx, make_pipeline, module::ModuleId, ByteCodeSerializer, IRPass,
    ObjectPointer, SharedCtx, SourceInfo,
};

/// A message received by a pipeline worker.
pub type PipelineMessage = PathBuf;
/// A Sender that sends [`PipelineMessage`]s.
pub type PipelineTx = Sender<PipelineMessage>;
/// A Receiver that receives [`PipelineMessage`]s.
pub type PipelineRx = Receiver<PipelineMessage>;

/// The error returned by the worker control functions if something happens
/// to one or more workers.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct WorkerError;

pub(crate) fn create_module_from_path(
    ctx: SharedCtx,
    path: &Path,
) -> Result<ModuleId, &'static str> {
    // Modules are queued exclusively by Resolvers, BytecodeParsers, and test_collectors that check
    // that each module exists and is a file.
    let module_name = path
        .file_name()
        .and_then(|f| f.to_str())
        .ok_or("Expected the file name to be a valid unicode string.")?
        .trim_end_matches(".lox")
        .to_owned();
    let filename = path.display().to_string();
    let source = std::fs::read_to_string(path).expect("Failed to read the script file.");
    let info = SourceInfo::new(filename, source, module_name);
    Ok(ctx.write_shared().set_module_info(info))
}

pub(crate) fn spawn_worker(ctx: SharedCtx, notifier: Sender<()>) -> Result<(), &'static str> {
    thread_debug!("Spawned a worker thread, attempting to acquire the queue");
    let rx = { ctx.read_shared().get_queue().unwrap() };
    // Notify the main thread that the queue has been acquired successfully.
    notifier.send(()).unwrap();
    thread_debug!("Queue acquired, waiting for modules");

    let mut token = None;
    while let Ok(path) = rx.recv() {
        if token.is_none() {
            token = Some(ctx.write_shared().take_token());
        }
        thread_debug!("Compiling a new module: {}", path.display());

        let module_id = create_module_from_path(ctx.clone(), &path)?;

        #[cfg(feature = "worker-debug-info")]
        let module_name = ctx
            .read_shared()
            .get_module_source_info(module_id)
            .unwrap()
            .module
            .clone();

        let pipeline = make_pipeline(ctx.clone());
        match pipeline.apply(module_id) {
            Ok(_) => {
                thread_debug!("Successfully compiled `{}`", module_name);
                ctx.read_shared().ex.report_all(ctx.clone());
            }
            Err(mut ex) => {
                thread_debug!("Failed to compile `{}`", module_name);
                ex.extend_from_another_ex({
                    let mut guard = ctx.write_shared();
                    let mut ex = ErrCtx::new();
                    ex.had_error = true;
                    std::mem::replace(&mut guard.ex, ex)
                });
                ex.report_all(ctx.clone());
            }
        };

        // Return the token
        if rx.is_empty() {
            std::mem::drop(token.take());
        }
    }
    thread_debug!("Terminating the thread");
    Ok(())
}

// Called by the cache writer pass
pub(crate) fn cache_cco(ctx: SharedCtx, cco: ObjectPointer) {
    let module_id = cco.as_ref().as_cco().module;

    let mut guard = ctx.write_shared();
    let caching = guard.config.caching;

    let module = guard.get_module_mut(module_id).unwrap();
    module.set_cco(cco);

    let path = module.bin_cache();
    let loaded_from_cache = module.loaded_from_cache;
    #[cfg(feature = "worker-debug-info")]
    let module_name = module.name().to_owned();

    std::mem::drop(guard);

    thread_debug!(
        "Updated the CCO pointer for `{0}`; `{0}` is now ready",
        module_name
    );
    if caching && !loaded_from_cache {
        thread_debug!("Attempting to cache `{}` as a .rloxb ...", module_name);

        let blob = ctx.serialize(module_id);
        match std::fs::write(path, blob) {
            Ok(_) => {
                thread_debug!("Successfully cached `{}`: {}", module_name, path.display());
            }
            Err(_e) => {
                thread_debug!("Failed to cache `{}`: {}", module_name, _e);
            }
        }
    }
}

impl std::fmt::Display for WorkerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "One or more workers have failed")
    }
}
impl std::error::Error for WorkerError {}
