use std::convert::TryFrom;

use crate::{compiler::locations::EncodedLocation, vm::Value};

use super::{bytecode_writer::PtrTable, TypeMarker};

pub trait BinarySerializer {
    fn write_bytes(&self, table: &mut PtrTable, buf: &mut Vec<u8>);
    fn type_marker(&self) -> TypeMarker;
}

impl BinarySerializer for EncodedLocation {
    fn write_bytes(&self, _table: &mut PtrTable, buf: &mut Vec<u8>) {
        buf.extend(&self.pattern.to_be_bytes());
        buf.extend(&self.reps.to_be_bytes());
    }

    fn type_marker(&self) -> TypeMarker {
        TypeMarker::Location
    }
}

impl BinarySerializer for Value {
    fn write_bytes(&self, table: &mut PtrTable, buf: &mut Vec<u8>) {
        match self {
            Value::Int(i) => {
                i.write_bytes(table, buf);
            }
            Value::Float(f) => {
                f.write_bytes(table, buf);
            }
            Value::Bool(b) => {
                b.write_bytes(table, buf);
            }
            Value::Nil => {
                // Nil has only one value, hence the type marker is enough
            }
            Value::Ptr(ptr) => match ptr.as_ref().as_heap_value() {
                crate::vm::HeapValue::Str(_) | crate::vm::HeapValue::CCO(_) => {
                    let id = table.add_pointer(*ptr);
                    buf.extend(
                        &u32::try_from(id)
                            .expect("Too many strings or CCOs to fit into u32")
                            .to_be_bytes(),
                    );
                }
                crate::vm::HeapValue::ObjNative(obj) => {
                    // We don't need to serialize the module objects since this information
                    // is in the beginning of the blob.
                    debug_assert!(obj.lox_doc().starts_with("module"));
                }
                crate::vm::HeapValue::Obj(_) => unreachable!(),
                crate::vm::HeapValue::Cls(_) => unreachable!(),
                crate::vm::HeapValue::Closure { .. } => unreachable!(),
                crate::vm::HeapValue::BoundMethod { .. } => unreachable!(),
                crate::vm::HeapValue::UpValue(_) => unreachable!(),
            },
        }
    }

    fn type_marker(&self) -> TypeMarker {
        match self {
            Value::Int(i) => i.type_marker(),
            Value::Float(f) => f.type_marker(),
            Value::Bool(b) => b.type_marker(),
            Value::Nil => TypeMarker::Nil,
            Value::Ptr(ptr) => match ptr.as_ref().as_heap_value() {
                crate::vm::HeapValue::ObjNative(obj) => {
                    debug_assert!(obj.lox_doc().starts_with("module"));
                    TypeMarker::ModulePtr
                }
                crate::vm::HeapValue::Str(_) => TypeMarker::StrPtr,
                crate::vm::HeapValue::CCO(_) => TypeMarker::CCOPtr,
                crate::vm::HeapValue::Cls(_) => unreachable!(),
                crate::vm::HeapValue::Obj(_) => unreachable!(),
                crate::vm::HeapValue::Closure { .. } => unreachable!(),
                crate::vm::HeapValue::BoundMethod { .. } => unreachable!(),
                crate::vm::HeapValue::UpValue(_) => unreachable!(),
            },
        }
    }
}

macro_rules! impl_bsr {
    ($Ty:ty, $marker:path) => {
        impl BinarySerializer for $Ty {
            #[inline]
            fn write_bytes(&self, _: &mut PtrTable, buf: &mut Vec<u8>) {
                buf.extend(&self.to_be_bytes());
            }

            #[inline]
            fn type_marker(&self) -> TypeMarker {
                $marker
            }
        }
    };
    (byte $Ty:ty, $marker:path) => {
        impl BinarySerializer for $Ty {
            #[inline]
            fn write_bytes(&self, _: &mut PtrTable, buf: &mut Vec<u8>) {
                buf.push(*self as u8);
            }

            #[inline]
            fn type_marker(&self) -> TypeMarker {
                $marker
            }
        }
    };
}

impl_bsr!(i64, TypeMarker::Int64);
impl_bsr!(i128, TypeMarker::Int128);
impl_bsr!(f64, TypeMarker::Float64);
impl_bsr!(byte bool, TypeMarker::Bool);
