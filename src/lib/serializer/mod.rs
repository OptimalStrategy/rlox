//! # The rlox Binary Serialization Format
//! This document describes the rlox binary serialization format (RBSF).
//!
//! ## Acknowledgements
//! The RBSF is heavily inspired by the [`Inko bytecode format`].
//!
//! ## High-Level Description
//! The RBSF, as well as rlox itself, is based around modules. A module usually corresponds to a lox script,
//! containing some additional information such as the token hash of the source code and applied optimizations.
//! The table below documents the general module structure:
//!
//! | Name       | Description |
//! |------------|-------------|
//! | Module ID  | Module ID as a u64, with the u64 being a Sip13 hash of the filename. |
//! | Token Hash | A Sip13 token-level hash of the source code. Token-level means that only tokens are hashed, thus it will not be affected by changes to whitespace or comments. |
//! | Opt Config | Four booleans, each corresponding to an optimization from the `OptConfig`. |
//! | Imports    | The module's imports as pairs of (bool is_root, string name). |
//! | Exports    | The module's exports (globals) as pairs of (string name, u16 stack_pos) |
//! | CCO + Data | The module's source code as a Compiled Code Object (CCO) + serialization data. |
//!
//! ## CCO Layout
//! A [`CCO`] contains the following elements:
//!
//! | Name             | Description |
//! |------------------|-------------|
//! | CCO Name         | CCO's name as a utf-8 string. |
//! | CCO Module       | The Module ID of the module the CCO originates from as a u64. |
//! | Arity            | The CCO's arity, serialized as (tag, payload), 3 bytes total. See [`Arity`]  for possible arity values. |
//! | N Captured       | The number of variables the CCO captures as a u32. |
//! | Bytecode version | The version of CCO's bytecode as a single u32, prefixed with the magic bytes `2E 76` (`.v`). |
//! | Locations        | A typed array of 6-byte code locations. The locations are RLE encoded. |
//! | Bytecode         | The CCO's bytecode as a byte array. |      
//! | Constants        | An untyped array of [`Value`]s. |
//! | Strings          | A typed array of UTF-8 strings. |
//! | Code Objects     | The CCO's child code objects as a typed array of CCOs.          |
//!
//! # Types
//! | Name             | Layout |
//! |------------------|-------------|
//! | String           | A string marker followed by a u32 length and N bytes with the contents of the string. |
//! | TypedArray       | A typed array marker followed by the type of the elements, a u32 length, and N elements serialized according to their spec but without the markers. |
//! | UntypedArray     | An untyped array marker followed by a u32 length and N elements serialized according to their spec. |
//! | Int64            | An int64 marker followed by the 8 bytes of the integer (BE). |
//! | Int128           | An int128 marker followed by the 16 bytes of the integer (BE). |
//! | Float64          | A float64 marker followed by the 8 bytes of the float (BE). |
//! | Bool             | A bool marker followed by a single byte with the bool's value. |
//! | Nil              | A single Nil marker. |
//! | ModulePtr        | A single ModulePtr marker indicating that a pointer to the current module should be returned. |
//! | CCOPtr           | A CCOPtr marker followed by a u32 location in the Code Objects array of the current CCO indicating that a pointer to the CCO at the location should be returned. |      
//! | StrPtr           | A StrPtr marker followed by a u32 location in the Strings array of the current CCO indicating that a pointer to that string should be returned. |      
//! | CCO              | See the CCO spec above. |
//! | Location         | A location marker followed by two u32 values: 'pattern' and 'reps'. |
//! | OpCode           | An opcode marker followed by a single byte that must be a valid opcode. |
//! | ImportArray      | An ImportArray marker followed by an array of (bool is_root, string name) pairs. |
//! | ExportMap        | An ExportMap marker followed by an array of (string name, u16 stack_pos) pairs. |
//!
//!
//!
//! # RBSF Layout
//! RBSF blobs are stored in `.rloxb` files. Such files must follow the following format:
//!
//! 1. The 4-byte header `rlox` (`72 6C 6F 78`)
//! 2. The module meta data as describe above.
//! 3. The CCO data as described above.
//!
//! [`Inko bytecode format`]: https://inko-lang.org/manual/virtual-machine/bytecode/#header-compiled-code
//!
//! [`CCO`]: crate::compiler::cco::CompiledCodeObject
//! [`Arity`]: crate::compiler::cco::Arity
//! [`Value`]: crate::vm::object::value::Value
use crate::{module::ModuleId, SharedCtx};
use ObjectPointer;

use self::bytecode_parser::ByteCodeParserError;

pub mod binary_serializer;
pub mod bytecode_parser;
pub mod bytecode_writer;

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum TypeMarker {
    String = 0,
    TypedArray = 1,
    UntypedArray = 2,
    Int64 = 3,
    Int128 = 4,
    Float64 = 5,
    Bool = 6,
    Nil = 7,
    ModulePtr = 8,
    CCOPtr = 9,
    StrPtr = 10,
    CCO = 11,
    Location = 13,
    OpCode = 15,
    ImportArray = 16,
    ExportMap = 17,
}

impl Into<u8> for TypeMarker {
    fn into(self) -> u8 {
        self as _
    }
}

impl TypeMarker {
    fn from_u8(byte: u8) -> Option<Self> {
        if byte > Self::ImportArray as u8 {
            None
        } else {
            Some(unsafe { std::mem::transmute::<u8, TypeMarker>(byte) })
        }
    }
}

pub trait ByteCodeSerializer {
    fn serialize(&self, module_id: ModuleId) -> Vec<u8>;
    fn deserialize(
        &self,
        module_id: ModuleId,
        raw: &[u8],
    ) -> Result<ObjectPointer, ByteCodeParserError>;
}

impl ByteCodeSerializer for SharedCtx {
    fn serialize(&self, module_id: ModuleId) -> Vec<u8> {
        bytecode_writer::ByteCodeWriter::new(self.clone(), module_id).encode()
    }

    fn deserialize(
        &self,
        module_id: ModuleId,
        raw: &[u8],
    ) -> Result<ObjectPointer, ByteCodeParserError> {
        bytecode_parser::ByteCodeParser::new(self.clone(), module_id, raw).parse()
    }
}
