use serializer::TypeMarker;
use std::{collections::HashMap, convert::TryInto, path::PathBuf};

use crate::{
    compiler::bytecode::Bytecode,
    compiler::cco::Arity,
    compiler::cco::CompiledCodeObject,
    compiler::chunk::Chunk,
    compiler::locations::EncodedLocation,
    compiler::version::BytecodeVersion,
    compiler::version::CURRENT_VERSION,
    module::ModuleId,
    vm::HeapValue,
    vm::LoxObject,
    vm::{ImmutableString, Value},
    ObjectPointer, SharedCtx,
};

#[derive(Debug, Clone)]
pub(crate) struct ByteStream<'a> {
    ptr: usize,
    bytes: &'a [u8],
}

#[derive(Debug, Clone, PartialEq)]
pub enum ByteCodeParserError {
    InvalidHeader,
    ModuleIdMismatch {
        expected: ModuleId,
        actual: ModuleId,
    },
    // TODO: this should contain the missing path
    MissingImport,
    SourceHashMismatch,
    ImportIsNotFile,
    OptimizationsMismatch,
    IntegerSizeMismatch,
    UnexpectedByte {
        marker: TypeMarker,
        byte: u8,
        pos: usize,
    },
    InvalidTypeMarker,
    InvalidUTF8String,
    InvalidBoolValue,
    BytecodeVersionMismatch,
    UnexpectedEndOfStream,
}

pub struct ByteCodeParser<'a> {
    ctx: SharedCtx,
    buf: ByteStream<'a>,
    module_id: ModuleId,
    strings: HashMap<usize, ObjectPointer>,
    code_objects: HashMap<usize, ObjectPointer>,
}

impl<'a> ByteCodeParser<'a> {
    pub fn new(ctx: SharedCtx, module_id: ModuleId, raw: &'a [u8]) -> Self {
        ByteCodeParser {
            ctx,
            module_id,
            buf: ByteStream::new(raw),
            strings: HashMap::new(),
            code_objects: HashMap::new(),
        }
    }

    pub fn parse(mut self) -> Result<ObjectPointer, ByteCodeParserError> {
        self.parse_header()?;
        self.parse_module_metadata()?;
        match self.parse_cco() {
            Ok(ok) => {
                self.ctx
                    .write_shared()
                    .get_module_mut(self.module_id)
                    .unwrap()
                    .loaded_from_cache = true;
                Ok(ok)
            }
            Err(e) => Err(e),
        }
    }

    fn parse_header(&mut self) -> Result<(), ByteCodeParserError> {
        match self.read(4)? {
            b"rlox" => Ok(()),
            _ => Err(ByteCodeParserError::InvalidHeader),
        }
    }

    fn parse_module_metadata(&mut self) -> Result<(), ByteCodeParserError> {
        let ctx = self.ctx.clone();
        let (module, opt_config) = {
            let guard = ctx.read_shared();
            (
                guard.get_module(self.module_id).unwrap(),
                guard.config.opt_config.clone(),
            )
        };
        let id = self.read_u64()?;
        if id != self.module_id.0 {
            return Err(ByteCodeParserError::ModuleIdMismatch {
                expected: self.module_id,
                actual: ModuleId(id),
            });
        }
        let hash = self.read_u64()?;
        if hash != module.source_info.source_hash && module.source_info.source_hash != 0 {
            return Err(ByteCodeParserError::SourceHashMismatch);
        }
        let optimizations = self.read(4)?;
        if optimizations != opt_config.as_u8_array() {
            return Err(ByteCodeParserError::OptimizationsMismatch);
        }

        // Imports
        self.read_marker(TypeMarker::ImportArray)?;
        let n_imports = self.read_u16()?;

        for _ in 0..n_imports {
            let was_root_imported = self.read_u8()? == 1;
            let path = PathBuf::from(self.read_string()?);
            let filename =
                std::fs::canonicalize(path).map_err(|_| ByteCodeParserError::MissingImport)?;

            if !filename.is_file() {
                return Err(ByteCodeParserError::ImportIsNotFile);
            }

            let guard = self.ctx.write_shared().queue_module(
                self.ctx.clone(),
                self.module_id,
                filename,
                was_root_imported,
            );
            let _ = guard.get(); // Wait for the import to get queued.
        }

        // Exports
        self.read_marker(TypeMarker::ExportMap)?;
        let n_exports = self.read_u16()?;

        let mut indices = HashMap::new();
        for _ in 0..n_exports {
            let name = self.read_string()?;
            let stack_pos = self.read_u16()?;
            indices.insert(name, Value::Int(stack_pos as _));
        }

        unsafe {
            self.ctx
                .write_shared()
                .get_module_mut(self.module_id)
                .unwrap()
                .lox_module_object_mut()
                .unwrap()
        }
        .set_export_indices(indices);

        Ok(())
    }

    fn parse_cco(&mut self) -> Result<ObjectPointer, ByteCodeParserError> {
        self.read_marker(TypeMarker::CCO)?;
        self.parse_cco_contents()
    }

    fn parse_cco_contents(&mut self) -> Result<ObjectPointer, ByteCodeParserError> {
        let name = self.read_string()?;
        let module = ModuleId(self.read_u64()?);
        if module != self.module_id {
            return Err(ByteCodeParserError::ModuleIdMismatch {
                expected: self.module_id,
                actual: module,
            });
        }

        // TODO: enforce the tag values somehow
        let [tag, op1, op2]: [u8; 3] = self.read(3)?.try_into().unwrap();
        let arity = match tag {
            0 => Arity::Fixed(op1),
            1 => Arity::Variable,
            2 => Arity::LessEqual(op1),
            3 => Arity::Optional,
            4 => Arity::InRange(op1, op2),
            rest => unreachable!("Found an unexpected tag while parsing the CCO arity: {} (expected a value from 0 to 4)", rest),
        };
        let n_captured = self.read_u32()? as usize;
        let version = BytecodeVersion::from(self.read_u32()?);
        if version != CURRENT_VERSION {
            return Err(ByteCodeParserError::BytecodeVersionMismatch);
        }
        // Locations
        let locations = self.read_typed_array(TypeMarker::Location, |buf| {
            let pattern = buf.read_u32()?;
            let reps = buf.read_u16()?;
            Ok(EncodedLocation { pattern, reps })
        })?;
        // Opcodes
        self.read_marker(TypeMarker::TypedArray)?;
        self.read_marker(TypeMarker::OpCode)?;
        let len = self.read_u32()? as usize;
        let bytecode = Bytecode::Decoded(self.read(len)?.to_vec());
        // Constants
        let constants = self.read_untyped_array()?;
        // Strings
        self.read_string_ptr_array()?;

        let ctx = self.ctx.clone();
        let module_id = self.module_id;
        let mut code_objects = self.read_typed_array(TypeMarker::CCO, |buf| {
            let mut parser = ByteCodeParser {
                ctx: ctx.clone(),
                buf: buf.clone(),
                module_id,
                strings: HashMap::new(),
                code_objects: HashMap::new(),
            };
            let result = parser.parse_cco_contents();
            buf.ptr = parser.buf.ptr;
            result
        })?;

        // Swap the CCOs that are referenced by pointers into their pre-allocated slots.
        for (i, cco) in code_objects.iter_mut().enumerate() {
            if let Some(ptr) = self.code_objects.get_mut(&i) {
                std::mem::swap(cco.as_mut(), ptr.as_mut());
                *cco = *ptr;
            }
        }

        let cco = CompiledCodeObject::new(
            name,
            Chunk::new(bytecode, locations, constants, code_objects),
            arity,
            n_captured,
            module,
        );

        let ptr = self
            .ctx
            .write_shared()
            .alloc_without_gc_trigger(LoxObject::new(HeapValue::CCO(Box::new(cco))));

        Ok(ptr)
    }

    fn read_string(&mut self) -> Result<String, ByteCodeParserError> {
        self.read_marker(TypeMarker::String)?;
        let length = self.read_u32()?;
        let bytes = self.read(length as usize)?;
        String::from_utf8(bytes.to_vec()).map_err(|_| ByteCodeParserError::InvalidUTF8String)
    }

    fn read_untyped_array(&mut self) -> Result<Vec<Value>, ByteCodeParserError> {
        self.read_marker(TypeMarker::UntypedArray)?;
        let len = self.read_u32()? as usize;
        let mut array = Vec::with_capacity(len);
        for _ in 0..len {
            let marker = self.read_any_marker()?;
            let value = self.read_value(marker)?;
            array.push(value);
        }
        Ok(array)
    }

    fn read_typed_array<T, F>(
        &mut self,
        marker: TypeMarker,
        reader: F,
    ) -> Result<Vec<T>, ByteCodeParserError>
    where
        F: Fn(&mut ByteStream<'a>) -> Result<T, ByteCodeParserError>,
    {
        self.read_marker(TypeMarker::TypedArray)?;
        self.read_marker(marker)?;
        let len = self.read_u32()? as usize;
        let mut array = Vec::with_capacity(len);
        for _ in 0..len {
            let value = reader(&mut self.buf)?;
            array.push(value);
        }
        Ok(array)
    }

    fn read_string_ptr_array(&mut self) -> Result<(), ByteCodeParserError> {
        self.read_marker(TypeMarker::TypedArray)?;
        self.read_marker(TypeMarker::String)?;
        let len = self.read_u32()? as usize;
        for i in 0..len {
            let length = self.read_u32()?;
            let bytes = self.read(length as usize)?;
            let value = String::from_utf8(bytes.to_vec())
                .map_err(|_| ByteCodeParserError::InvalidUTF8String)?;
            let s = LoxObject::new(HeapValue::Str(
                Box::new(ImmutableString::new_with_hash(value).and_indices()),
            ));
            *self.strings.get_mut(&i).unwrap().as_mut() = s;
        }

        Ok(())
    }

    fn read_value(&mut self, marker: TypeMarker) -> Result<Value, ByteCodeParserError> {
        let result = match marker {
            TypeMarker::String => {
                unimplemented!();
                // self.read_string()
            }
            TypeMarker::TypedArray => {
                unimplemented!();
                // self.read_typed_array();
            }
            TypeMarker::UntypedArray => unimplemented!(),
            TypeMarker::Int64 => Value::Int(self.read_i64()? as _),
            TypeMarker::Int128 => {
                #[cfg(feature = "lox-64bit-ints")]
                {
                    let int = self
                        .read_i128()?
                        .try_into()
                        .map_err(|_| ByteCodeParserError::IntegerSizeMismatch)?;
                    Value::Int(int)
                }
                #[cfg(not(feature = "lox-64bit-ints"))]
                {
                    Value::Int(self.read_i128()?)
                }
            }
            TypeMarker::Float64 => Value::Float(self.read_f64()?),
            TypeMarker::Bool => Value::Bool(self.read_bool()?),
            TypeMarker::Nil => Value::Nil,
            TypeMarker::ModulePtr => Value::Ptr(
                self.ctx
                    .read_shared()
                    .get_module(self.module_id)
                    .unwrap()
                    .lox_module()
                    .unwrap(),
            ),
            TypeMarker::CCO => {
                unimplemented!();
            }
            TypeMarker::CCOPtr => {
                let id = self.read_u32()?;
                let ctx = self.ctx.clone();
                // Pre-allocate the object for this CCO, the actual value will be loaded later.
                let ptr = *self.code_objects.entry(id as usize).or_insert_with(|| {
                    ctx.write_shared()
                        .alloc_without_gc_trigger(LoxObject::new(HeapValue::UpValue(Value::Nil)))
                });
                Value::Ptr(ptr)
            }
            TypeMarker::StrPtr => {
                let id = self.read_u32()?;
                let ctx = self.ctx.clone();
                // Pre-allocate the object for this string, the actual value will be loaded later.
                let ptr = *self.strings.entry(id as usize).or_insert_with(|| {
                    ctx.write_shared()
                        .alloc_without_gc_trigger(LoxObject::new(HeapValue::UpValue(Value::Nil)))
                });
                Value::Ptr(ptr)
            }
            TypeMarker::Location => unreachable!(),
            TypeMarker::OpCode => unreachable!(),
            TypeMarker::ImportArray => unreachable!(),
            TypeMarker::ExportMap => unreachable!(),
        };
        Ok(result)
    }

    #[inline]
    fn read(&mut self, n: usize) -> Result<&[u8], ByteCodeParserError> {
        self.buf.try_read(n)
    }
}

impl<'a> BinaryReader for ByteCodeParser<'a> {
    #[inline]
    fn try_read(&mut self, n: usize) -> Result<&[u8], ByteCodeParserError> {
        self.buf.try_read(n)
    }

    #[inline]
    fn pos(&self) -> usize {
        self.buf.pos()
    }
}

impl<'a> ByteStream<'a> {
    pub fn new(bytes: &'a [u8]) -> Self {
        Self { ptr: 0, bytes }
    }

    #[inline]
    pub fn read(&mut self, n: usize) -> &[u8] {
        self.ptr += n;
        &self.bytes[self.ptr - n..self.ptr]
    }

    #[allow(unused)]
    #[inline]
    pub fn byte(&mut self) -> u8 {
        self.ptr += 1;
        self.bytes[self.ptr - 1]
    }
}

impl<'a> BinaryReader for ByteStream<'a> {
    #[inline]
    fn try_read(&mut self, n: usize) -> Result<&[u8], ByteCodeParserError> {
        if self.ptr + n > self.bytes.len() {
            Err(ByteCodeParserError::UnexpectedEndOfStream)
        } else {
            Ok(self.read(n))
        }
    }

    #[inline]
    fn pos(&self) -> usize {
        self.ptr
    }
}

pub(crate) trait BinaryReader {
    fn try_read(&mut self, n: usize) -> Result<&[u8], ByteCodeParserError>;
    fn pos(&self) -> usize;

    #[inline]
    fn read_u8(&mut self) -> Result<u8, ByteCodeParserError> {
        self.try_read(1)
            .map(|bytes| u8::from_be_bytes(bytes.try_into().unwrap()))
    }

    #[inline]
    fn read_bool(&mut self) -> Result<bool, ByteCodeParserError> {
        self.read_u8().and_then(|byte| {
            if byte > 1 {
                Err(ByteCodeParserError::InvalidBoolValue)
            } else {
                Ok(byte != 0)
            }
        })
    }

    #[inline]
    fn read_u16(&mut self) -> Result<u16, ByteCodeParserError> {
        self.try_read(2)
            .map(|bytes| u16::from_be_bytes(bytes.try_into().unwrap()))
    }

    #[inline]
    fn read_u32(&mut self) -> Result<u32, ByteCodeParserError> {
        self.try_read(4)
            .map(|bytes| u32::from_be_bytes(bytes.try_into().unwrap()))
    }

    #[inline]
    fn read_u64(&mut self) -> Result<u64, ByteCodeParserError> {
        self.try_read(8)
            .map(|bytes| u64::from_be_bytes(bytes.try_into().unwrap()))
    }

    #[inline]
    fn read_i64(&mut self) -> Result<i64, ByteCodeParserError> {
        self.try_read(8)
            .map(|bytes| i64::from_be_bytes(bytes.try_into().unwrap()))
    }

    #[inline]
    fn read_i128(&mut self) -> Result<i128, ByteCodeParserError> {
        self.try_read(16)
            .map(|bytes| i128::from_be_bytes(bytes.try_into().unwrap()))
    }

    #[inline]
    fn read_f64(&mut self) -> Result<f64, ByteCodeParserError> {
        self.try_read(8)
            .map(|bytes| f64::from_be_bytes(bytes.try_into().unwrap()))
    }

    fn read_marker(&mut self, marker: TypeMarker) -> Result<(), ByteCodeParserError> {
        let pos = self.pos();
        self.read_u8().and_then(|b| {
            if b != marker as u8 {
                Err(ByteCodeParserError::UnexpectedByte {
                    marker,
                    byte: b,
                    pos,
                })
            } else {
                Ok(())
            }
        })
    }

    #[inline]
    fn read_any_marker(&mut self) -> Result<TypeMarker, ByteCodeParserError> {
        self.read_u8()
            .and_then(|b| TypeMarker::from_u8(b).ok_or(ByteCodeParserError::InvalidTypeMarker))
    }
}

#[cfg(test)]
mod tests {
    #[allow(unused)]
    use crate::{
        compiler::{
            bytecode::{Bytecode, BytecodeBuilder},
            cco::Arity,
            dis::Disassemble,
            locations::{EncodedLocation, Location},
        },
        vm::Value,
    };
    #[allow(unused)]
    use crate::{
        compiler::{locations::Locations, version::CURRENT_VERSION},
        serializer::{bytecode_parser::ByteStream, ByteCodeSerializer, TypeMarker},
    };
    use crate::{make_pipeline, IRPass};
    use crate::{Context, SourceInfo};

    #[test]
    fn test_bytecode_parser() {
        let ctx = Context::default().into_shared();
        let (id, _config) = {
            let mut guard = ctx.write_shared();
            guard.config.opt_config.constant_folding = false;
            (
                guard.add_module(SourceInfo::test_info("print 1 + 2; \"abc\";")),
                guard.config.clone(),
            )
        };
        let _ = make_pipeline(ctx.clone()).apply(id).unwrap();
        let bytes = ctx.serialize(id);
        let cco = ctx.deserialize(id, &bytes).unwrap();
        let cco = cco.as_ref().as_cco();
        assert_eq!(cco.name, "<stream>.lox");
        assert_eq!(cco.version, CURRENT_VERSION);
        assert_eq!(cco.arity, Arity::Fixed(0));
        assert_eq!(cco.n_captured, 0);
        assert_eq!(
            cco.chunk.locations,
            Locations::Encoded(vec![
                EncodedLocation {
                    pattern: 65543,
                    reps: 1
                },
                EncodedLocation {
                    pattern: 65547,
                    reps: 2
                },
                EncodedLocation {
                    pattern: 65545,
                    reps: 1
                },
                EncodedLocation {
                    pattern: 65537,
                    reps: 1
                },
                EncodedLocation {
                    pattern: 65550,
                    reps: 3
                },
                EncodedLocation {
                    pattern: 65548,
                    reps: 4
                }
            ])
        );
        assert_eq!(
            cco.chunk.bytecode,
            Bytecode::Decoded(
                BytecodeBuilder::new()
                    .op_one()
                    .op_load_const(2)
                    .op_add()
                    .op_print()
                    .op_load_const("abc")
                    .op_pop()
                    .build_bytes()
            )
        );
        assert_eq!(&cco.chunk.constants[0], &Value::Int(2));
        assert_eq!(cco.chunk.constants[1].as_string().unwrap().as_ref(), "abc");

        eprintln!("{:x?}", bytes);
    }
}
