use std::convert::TryFrom;
use std::{collections::HashMap, convert::TryInto};

use crate::{
    compiler::cco::Arity, compiler::cco::CompiledCodeObject, module::ModuleId, vm::HeapValue,
    ObjectPointer, SharedCtx,
};

use super::{binary_serializer::BinarySerializer, TypeMarker};

pub struct PtrTable {
    strings: HashMap<ObjectPointer, usize>,
    code_objects: HashMap<ObjectPointer, usize>,
}

impl PtrTable {
    pub fn new() -> Self {
        Self {
            strings: HashMap::new(),
            code_objects: HashMap::new(),
        }
    }

    pub fn add_pointer(&mut self, ptr: ObjectPointer) -> usize {
        match ptr.as_ref().value {
            HeapValue::Str(_) => {
                let len = self.strings.len();
                *self.strings.entry(ptr).or_insert_with(|| len)
            }
            HeapValue::CCO(_) => {
                let len = self.code_objects.len();
                *self.code_objects.entry(ptr).or_insert_with(|| len)
            }
            _ => unreachable!(),
        }
    }

    #[inline(always)]
    pub fn n_strings(&self) -> usize {
        self.strings.len()
    }

    pub fn strings(&self) -> impl Iterator<Item = &str> {
        let mut strings = self.strings.iter().collect::<Vec<_>>();
        strings.sort_by_key(|s| s.1);
        strings
            .into_iter()
            .map(|s| s.0.as_ref().as_immutable_string().as_ref())
    }

    pub fn code_objects(&self) -> impl Iterator<Item = &CompiledCodeObject> {
        let mut cco_s = self.code_objects.iter().collect::<Vec<_>>();
        cco_s.sort_by_key(|s| s.1);
        cco_s.into_iter().map(|s| s.0.as_ref().as_cco())
    }

    #[inline]
    pub fn has_cco(&self, cco: &ObjectPointer) -> bool {
        self.code_objects.contains_key(cco)
    }
}

pub struct ByteCodeWriter {
    ctx: SharedCtx,
    module: ModuleId,
    buf: Vec<u8>,
}

impl ByteCodeWriter {
    pub fn new(ctx: SharedCtx, module: ModuleId) -> Self {
        Self {
            ctx,
            module,
            buf: Vec::new(),
        }
    }

    pub fn encode(mut self) -> Vec<u8> {
        self.write_header();
        let cco = self.write_module_metadata();
        self.write_u8(TypeMarker::CCO);
        self.write_cco(cco.as_ref().as_cco());
        self.buf
    }

    fn write_header(&mut self) {
        self.write(b"rlox");
    }

    fn write_module_metadata(&mut self) -> ObjectPointer {
        let ctx = self.ctx.clone();
        let (module, opts, imports) = {
            let guard = ctx.read_shared();
            (
                guard.get_module(self.module).unwrap(),
                guard.config.opt_config.clone(),
                guard
                    .import_graph()
                    .adjacent(guard.import_graph().get_node_id(&self.module).unwrap())
                    .map(|node| *node.data())
                    .collect::<Vec<_>>(),
            )
        };
        self.write_u64(self.module.0);
        self.write_u64(module.source_info.source_hash);
        self.write(&opts.as_u8_array());
        self.write_u8(TypeMarker::ImportArray);
        self.write_u16(
            imports
                .len()
                .try_into()
                .expect("Too many imports to serialize as a u16"),
        );
        for id in imports {
            let module = self.ctx.read_shared().get_module(id).unwrap();
            self.write_u8(module.was_root_imported);
            self.write_string(&module.source_info.filename);
        }

        let exports = unsafe {
            self.ctx
                .read_shared()
                .get_module(self.module)
                .unwrap()
                .lox_module_object()
        }
        .unwrap()
        .indices();

        self.write_u8(TypeMarker::ExportMap);
        self.write_u16(
            exports
                .len()
                .try_into()
                .expect("Too many exports to serialize"),
        );
        for (name, stack_pos) in exports {
            self.write_string(name);
            self.write_u16(stack_pos.as_int().unwrap() as _);
        }

        module.cco_ptr().unwrap()
    }

    #[inline]
    fn write(&mut self, bytes: &[u8]) {
        self.buf.extend(bytes);
    }

    fn write_cco(&mut self, cco: &CompiledCodeObject) {
        let mut table = PtrTable::new();

        self.write_string(&cco.name);
        self.write_u64(cco.module.0);
        // Arity as (tag, payload1, payload2)
        self.write(&match cco.arity {
            Arity::Fixed(v) => [0, v, 0],
            Arity::Variable => [1, 0, 0],
            Arity::LessEqual(v) => [2, v, 0],
            Arity::Optional => [3, 0, 0],
            Arity::InRange(lo, hi) => [4, lo, hi],
        });
        self.write_u32(
            cco.n_captured
                .try_into()
                .expect("Too many captured variables"),
        );
        // Bytecode version
        self.write_u32(cco.version.into());
        // Locations
        if let Some(encoded) = cco.chunk.locations.as_encoded() {
            self.write_typed_array(&mut table, encoded, TypeMarker::Location);
        } else {
            self.write_typed_array(
                &mut table,
                cco.chunk.locations.clone().encode().as_encoded().unwrap(),
                TypeMarker::Location,
            );
        }
        // Bytecode
        self.write_u8(TypeMarker::TypedArray);
        self.write_u8(TypeMarker::OpCode);
        match cco.chunk.bytecode.as_raw() {
            Some(raw) => {
                self.write_u32(
                    raw.len()
                        .try_into()
                        .expect("Too many opcode to fit into 32 bits."),
                );
                self.write(raw);
            }
            None => unreachable!(),
        }
        // Constants
        self.write_untyped_array(&mut table, &cco.chunk.constants);
        // Strings in the order they were serialized elsewhere
        self.write_u8(TypeMarker::TypedArray);
        self.write_u8(TypeMarker::String);
        self.write_u32(table.n_strings() as u32);
        for s in table.strings() {
            self.buf
                .extend(&u32::try_from(s.len()).unwrap().to_be_bytes());
            self.buf.extend(s.as_bytes());
        }
        // CCOs
        self.write_u8(TypeMarker::TypedArray);
        self.write_u8(TypeMarker::CCO);
        self.write_u32(cco.chunk.code_objects.len() as u32);
        // Serialize the CCOs that have pointers to them first
        // in the right order
        for cco in table.code_objects() {
            self.write_cco(cco);
        }
        // Then the rest of the CCOs
        for cco in &cco.chunk.code_objects {
            if !table.has_cco(cco) {
                self.write_cco(cco.as_ref().as_cco());
            }
        }
    }

    fn write_untyped_array<T: BinarySerializer>(&mut self, table: &mut PtrTable, array: &[T]) {
        self.write_u8(TypeMarker::UntypedArray);
        self.write_u32(array.len() as u32);
        for element in array {
            self.write_u8(element.type_marker());
            element.write_bytes(table, &mut self.buf);
        }
    }

    fn write_typed_array<T: BinarySerializer>(
        &mut self,
        table: &mut PtrTable,
        array: &[T],
        marker: TypeMarker,
    ) {
        self.write_u8(TypeMarker::TypedArray);
        self.write_u8(marker);
        self.write_u32(array.len() as u32);
        for element in array {
            element.write_bytes(table, &mut self.buf);
        }
    }

    fn write_string(&mut self, s: &str) {
        self.write_u8(TypeMarker::String);
        self.write_u32(s.len() as u32);
        self.write(s.as_bytes());
    }

    #[inline]
    fn write_u8<B: Into<u8>>(&mut self, byte: B) {
        self.buf.push(byte.into())
    }

    fn write_u32(&mut self, n: u32) {
        self.write(&n.to_be_bytes());
    }

    fn write_u16(&mut self, n: u16) {
        self.write(&n.to_be_bytes());
    }

    fn write_u64(&mut self, n: u64) {
        self.write(&n.to_be_bytes());
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        compiler::version::CURRENT_VERSION,
        serializer::{bytecode_parser::ByteStream, ByteCodeSerializer, TypeMarker},
    };
    use crate::{make_pipeline, IRPass};
    use crate::{Context, SourceInfo};

    #[test]
    fn test_bytecode_writer() {
        let ctx = Context::default().into_shared();
        let (id, config) = {
            let mut guard = ctx.write_shared();
            guard.config.opt_config.constant_folding = false;
            (
                guard.add_module(SourceInfo::test_info("print 1 + 2;")),
                guard.config.clone(),
            )
        };
        let _ = make_pipeline(ctx.clone()).apply(id).unwrap();

        let bytes = ctx.serialize(id);
        let mut bytes = ByteStream::new(&bytes);
        eprintln!("{:?}", bytes);

        // Header
        assert_eq!(bytes.read(4), b"rlox");
        // Module ID
        assert_eq!(bytes.read(8), id.0.to_be_bytes());
        // Source Hash
        assert_eq!(bytes.read(8), b"\xb3sf\x8c\xdat\x84\xf3");
        // Optimizations
        assert_eq!(bytes.byte(), config.opt_config.constant_folding as u8);
        assert_eq!(bytes.byte(), config.opt_config.peephole_optimizations as u8);
        assert_eq!(bytes.byte(), config.opt_config.tail_call_optimization as u8);
        assert_eq!(bytes.byte(), config.opt_config.dead_code_elimination as u8);
        // Imports
        assert_eq!(bytes.byte(), TypeMarker::ImportArray as u8);
        assert_eq!(bytes.read(2), 0u16.to_be_bytes());
        // Exports
        assert_eq!(bytes.byte(), TypeMarker::ExportMap as _);
        assert_eq!(bytes.read(2), 0u16.to_be_bytes());

        // Module CCO name
        assert_eq!(bytes.byte(), TypeMarker::CCO as _);
        assert_eq!(bytes.byte(), TypeMarker::String as _);
        assert_eq!(bytes.read(4), ("<stream>.lox".len() as u32).to_be_bytes());
        assert_eq!(bytes.read(12), b"<stream>.lox");

        // Module CCO module Id
        assert_eq!(bytes.read(8), id.0.to_be_bytes());

        // Module CCO arity
        assert_eq!(bytes.read(3), &[0, 0, 0]);

        // Module CCO n_captured
        assert_eq!(bytes.read(4), 0u32.to_be_bytes());

        // Bytecode version
        assert_eq!(bytes.read(4), CURRENT_VERSION.into_u32().to_be_bytes());

        // Locations
        assert_eq!(bytes.byte(), TypeMarker::TypedArray as _);
        assert_eq!(bytes.byte(), TypeMarker::Location as _);
        assert_eq!(bytes.read(4), &4u32.to_be_bytes());

        #[rustfmt::skip]
        assert_eq!(
            bytes.read(4 * 6),
            &[
                // Line 1, column 7
                0, 1, 0, 7,
                // Reps = 1
                0, 1,
                // Line 1, column 11
                0, 1, 0, 11,
                // Reps = 2
                0, 2,
                // Line 1, column 9
                0, 1, 0, 9,
                // Reps = 1
                0, 1,
                // Line 1, column 1
                0, 1, 0, 1,
                // Reps = 5
                0, 5
            ]
        );

        // Bytecode
        assert_eq!(bytes.byte(), TypeMarker::TypedArray as _);
        assert_eq!(bytes.byte(), TypeMarker::OpCode as _);
        assert_eq!(bytes.read(4), &9u32.to_be_bytes());
        assert_eq!(bytes.read(9), &[51, 0, 0, 11, 69, 34, 0, 1, 24]);

        // Constants
        assert_eq!(bytes.byte(), TypeMarker::UntypedArray as _);
        assert_eq!(bytes.read(4), &2u32.to_be_bytes());
        // 2
        if cfg!(feature = "lox-64bit-ints") {
            assert_eq!(bytes.byte(), TypeMarker::Int64 as _);
            assert_eq!(bytes.read(8), &2i64.to_be_bytes());
        } else {
            assert_eq!(bytes.byte(), TypeMarker::Int128 as _);
            assert_eq!(bytes.read(16), &2i128.to_be_bytes());
        }

        // LoxModule object
        assert_eq!(bytes.byte(), TypeMarker::ModulePtr as _);

        // Strings
        assert_eq!(bytes.byte(), TypeMarker::TypedArray as _);
        assert_eq!(bytes.byte(), TypeMarker::String as _);
        assert_eq!(bytes.read(4), &0u32.to_be_bytes());

        // Nested code objects
        assert_eq!(bytes.byte(), TypeMarker::TypedArray as _);
        assert_eq!(bytes.byte(), TypeMarker::CCO as _);
        assert_eq!(bytes.read(4), &0u32.to_be_bytes());
    }
}
