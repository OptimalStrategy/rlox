use crate::*;
use vm::builtin::Builtins;

pub trait BenchSource {
    fn source(&self) -> &str;
    fn module(&self) -> &str {
        "<bench source>"
    }
}

impl<'a> BenchSource for &'a String {
    fn source(&self) -> &str {
        self
    }
}

impl<'a> BenchSource for &'a str {
    fn source(&self) -> &str {
        self
    }
}

impl<'a> BenchSource for (&'a str, &'a str) {
    fn source(&self) -> &str {
        self.0
    }

    fn module(&self) -> &str {
        self.1
    }
}

pub fn compile<S: BenchSource>(source: S, ctx: SharedCtx) -> Result<ObjectPointer, String> {
    let id = ctx.write_shared().add_module(SourceInfo::new(
        source.module().to_string(),
        source.source().to_string(),
        source.module().to_string(),
    ));

    let builtins = Builtins::default(ctx.clone());
    let fold = ctx.read_shared().config.opt_config.constant_folding;
    ctx.write_shared().set_builtins(builtins);

    let pipeline = Pipeline::with_pass(Scanner::new(ctx.clone()))
        .add_pass(BinaryCacheLoaderPass::new(
            ctx.clone(),
            Pipeline::with_pass(Parser::new(ctx.clone()))
                .add_pass(Resolver::new(ctx.clone()))
                .add_pass(ConstantFolder::new(fold, 20))
                .add_pass(Compiler::new(ctx.clone()))
                .stage(),
        ))
        .map(|cco| {
            pipeline_worker::cache_cco(ctx.clone(), cco);
            cco
        });

    let result = match pipeline.apply(id) {
        Ok(res) => {
            let shared = ctx.read_shared();
            shared.ex.report_all(ctx.clone());
            Ok(res)
        }
        Err(ex) => {
            ctx.write_shared().ex.extend_from_another_ex(ex);
            ctx.read_shared().ex.report_all(ctx.clone());
            Err(String::from("Failed to compile the bench"))
        }
    }?;

    // Wait for the workers to finish
    Context::terminate_workers(ctx.clone()).map_err(|e| e.to_string())?;

    if Context::check_worker_errors_and_import_graph(ctx.clone()) {
        Ok(result)
    } else {
        Err(String::from("A worker had an error or the IG is bad."))
    }
}
