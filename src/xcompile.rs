extern crate clap;
extern crate rlox;

use std::io::Write;
use std::path::PathBuf;

use clap::{App, Arg};

use rlox::vm::builtin::Builtins;
use rlox::{pipeline::IRPass, *};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = App::new("rlox-xcompile")
        .version(env!("CARGO_PKG_VERSION"))
        .about("Compile a script X times.")
        .arg(
            Arg::with_name("script")
                .required(true)
                .takes_value(true)
                .value_name("FILE")
                .help("Path to a .lox script."),
        )
        .arg(
            Arg::with_name("times")
                .short("x")
                .long("times")
                .takes_value(true)
                .default_value("100")
                .help("Specify the number of times to compile the script"),
        )
        .arg(
            Arg::with_name("optimize")
                .short("O")
                .takes_value(true)
                .default_value("1")
                .possible_values(&["0", "1"])
                .help("Enable or disable all optimizations"),
        )
        .arg(
            Arg::with_name("cache")
                .short("C")
                .takes_value(true)
                .default_value("1")
                .possible_values(&["0", "1"])
                .help("Enable or disables caching"),
        )
        .get_matches();

    let mut config = CompilerConfig::default();

    if matches.value_of("optimize").unwrap() == "0" {
        config.opt_config = OptConfig::all_disabled()
    }

    config.caching = matches.value_of("cache").unwrap() == "1";

    let ctx = Context::default().set_config(config).into_shared();
    let builtins = Builtins::default(ctx.clone());
    ctx.write_shared().set_builtins(builtins);
    let (filename, source) = {
        let path = std::fs::canonicalize(PathBuf::from(matches.value_of("script").unwrap()))?;
        if !path.is_file() {
            return Err(Box::from(format!(
                "Given script is not a file: `{}`",
                path.to_string_lossy()
            )));
        }
        let filename = path
            .file_name()
            .unwrap()
            .to_str()
            .expect("Expected the filename to be a valid unicode string.")
            .to_owned();
        let source = std::fs::read_to_string(path)?;
        (filename, source)
    };
    let times = match matches.value_of("times").unwrap().parse::<usize>() {
        Ok(0) | Err(_) => return Err(Box::from("times must be a positive integer".to_owned())),
        Ok(rest) => rest,
    };

    let mut total_time = 0.0;
    println!("Compiling the file '{}' {} time(s)", filename, times);
    for i in 0..times {
        print!(
            "\r {:04} / {:04} ... {:.20}s",
            i + 1,
            times,
            total_time / (i + 1) as f64
        );
        std::io::stdout().flush().unwrap();

        let module = ctx
            .write_shared()
            .add_module(SourceInfo::test_info(source.clone()));

        let pipeline = make_pipeline(ctx.clone());
        let instant = std::time::Instant::now();
        let result = pipeline.apply(module);
        Context::terminate_workers(ctx.clone())?;
        total_time += instant.elapsed().as_secs_f64();

        match result {
            Ok(_) => {
                ctx.read_shared().ex.report_all(ctx.clone());
                ctx.write_shared().ex.clear();
            }
            Err(ex) => {
                ctx.write_shared().ex.extend_from_another_ex(ex);
                ctx.read_shared().ex.report_all(ctx.clone());
                ctx.write_shared().ex.clear();
            }
        };

        // Remove the module information before the next compile.
        ctx.write_shared().drop_modules();
    }
    println!(
        "\r{} / {} ... DONE ({}s per run)",
        times,
        times,
        total_time / times as f64
    );

    Ok(())
}
