//! This crate provides a simple Directed Graph (DiGraph) implementation with
//! a variety of helper methods.
//!
//! The implementation uses HashMaps, Vecs, and BTreeSets
//! to make the insertion, lookup, and removal operations more efficient at the cost of extra space used for bookkeeping.
//!
//! All single-node insertions and lookups are `O(1)`, while removal is `O(N * log2 E_avg)`
//! (where N is the number of nodes and E_avg is the average number of edges a node has).
//! The space used for bookkeeping is as follows:
//! ```ignore
//! O(
//!     Occupied(data)      // The number of nodes with data * size_of(data)
//!     + Vacant(data)      // The number of nodes without data * size_of(data)
//!     + 2 * Occupied(id)  // The number of nodes with data * size_of(usize) * 2
//!     + Vacant(id)        // The number of nodes without data * size_of(usize)
//!     + Edges(id)         // The number of edges in the graph * size_of(usize)
//! )
//!
//! // For an 8-byte node data type on a 64-bit machine, this corresponds to
//! O(3 * Occupied + 2 * Vacant + Edges) = O(O + V + E)
//!
//! // Or the following, given that no removals have been performed
//! O(3 * Nodes + 2 * 0 + Edges) = O(N + E)
//! ```
//!
//! See the definition of [`DiGraph`] for more info.
use std::{
    collections::hash_map::DefaultHasher,
    hash::{Hash, Hasher},
};

pub mod adjacency_view;
pub mod digraph;
pub mod graphviz;
pub mod node;
pub mod tarjans_scc;

pub use digraph::DiGraph;
pub use node::{Node, NodeId};

/// An internal helper for hashing nodes' data.
fn hash<T: Hash>(data: &T) -> u64 {
    let mut hasher = DefaultHasher::new();
    data.hash(&mut hasher);
    hasher.finish()
}
