//! A view over the nodes adjacent to a given node.
//! This iterator view is returned by the [`DiGraph::adjacent`] method.
//!
//! ```rust
//! # extern crate rlox_graph;
//! use rlox_graph::{DiGraph, Node};
//!
//! let mut g = DiGraph::new();
//! let ids = g.add_nodes(vec![1, 2, 3, 4]);
//! g.add_edges(ids[0], &[ids[1], ids[2]]);
//!
//! let mut adj = g.adjacent(ids[0]);
//! assert_eq!(adj.next().map(Node::data), Some(&2));
//! assert_eq!(adj.next().map(Node::data), Some(&3));
//! assert_eq!(adj.next().map(Node::data), None);
//! ```
use crate::{digraph::EdgeSetIter, DiGraph, Node, NodeId};

/// A view over the nodes adjacent to a given node. See the module-level documentation
/// and [`DiGraph::adjacent`] for more info.
#[derive(Debug)]
pub struct AdjacencyView<'a, T> {
    pub(crate) g: &'a DiGraph<T>,
    pub(crate) edges: EdgeSetIter<'a, NodeId>,
}

impl<'a, T> Iterator for AdjacencyView<'a, T>
where
    T: std::hash::Hash,
{
    type Item = &'a Node<T>;

    fn next(&mut self) -> Option<Self::Item> {
        self.edges.next().map(|id| self.g.get_node(*id).unwrap())
    }
}
