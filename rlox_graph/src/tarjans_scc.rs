//! Implements the Tarjan's strongly connected components algorithm
//! modified to not detected dead-ends as SCCs.
//! For reference, see [`this Rosetta Code example`] and [`the Wikipedia entry`].
//!
//! [`this Rosetta Code example`]: https://rosettacode.org/wiki/Tarjan#C.23
//! [`the Wikipedia Entry`]: https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm
use crate::{DiGraph, Node, NodeId};
use std::{collections::HashMap, hash::Hash};

/// A node wrapper that stores the additional information
/// needed for the Tarjan's SCC algorithm.
#[derive(Debug, Clone)]
struct NodeWrapper {
    /// The id of the node represented by the wrapper.
    node: NodeId,
    /// The smallest index of any node known to be reachable
    /// from v through v's DFS subtree, including v itself.
    low_link: isize,
    /// The consecutive number of the node in the order it was discovered in.
    /// This index may not match the order this node is in in the graph.
    index: isize,
    /// Set `true` if the node is already on the recursive stack.
    on_stack: bool,
}

impl NodeWrapper {
    /// Creates a new [`NodeWrapper`] with `low_link` and `index` set to -1.
    fn new(node: NodeId) -> Self {
        Self {
            node,
            low_link: -1,
            index: -1,
            on_stack: false,
        }
    }
}

/// Implements the Tarjan's strongly connected components algorithm
/// modified to not detected dead-ends as SCCs.
///
/// For reference, see [`this Rosetta Code example`] and [`the Wikipedia entry`].
///
/// [`this Rosetta Code example`]: https://rosettacode.org/wiki/Tarjan#C.23
/// [`the Wikipedia Entry`]: https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm
#[derive(Debug)]
pub struct TarjansSCC<'a, T> {
    /// A reference to the graph we're searching.
    graph: &'a DiGraph<T>,
    /// The consecutive number of the last discovered node.
    index: isize,
    /// The node
    nodes: HashMap<NodeId, NodeWrapper>,
    stack: Vec<NodeId>,
    components: Vec<Vec<NodeId>>,
}

impl<'a, T> TarjansSCC<'a, T>
where
    T: Hash,
{
    /// Creates a new [`TarjansSCC`] instance.
    pub fn new(graph: &'a DiGraph<T>) -> Self {
        let mut nodes = HashMap::with_capacity(graph.node_count());
        for node in graph.nodes() {
            nodes.insert(node.id(), NodeWrapper::new(node.id()));
        }
        Self {
            graph,
            index: 0,
            nodes,
            stack: Vec::new(),
            components: Vec::new(),
        }
    }

    /// Finds the strongly connected components in the graph and returns their count.
    pub fn find_sccs(&mut self) -> usize {
        // XXX: Cannot do this in a single loop due to lifetime issues
        let nodes = self.nodes.keys().copied().collect::<Vec<_>>();
        for v in nodes {
            // Skip already seen nodes
            if self.node(v).index >= 0 {
                continue;
            }
            self.strongconnect(v);
        }

        self.components.len()
    }

    /// Consumes the struct and returns an iterator over the SCCs.
    /// The iterator will be empty if [`TarjansSCC::find_sccs`] was never called.
    pub fn components(self) -> Components<'a, T> {
        Components {
            graph: self.graph,
            components: self.components,
        }
    }

    /// Returns a reference to the internal vector of component nodes' ids.
    #[inline]
    pub fn component_ids(&self) -> &Vec<Vec<NodeId>> {
        &self.components
    }

    /// A helper function to modify multiple fields of a node.
    #[inline]
    fn node_with<F, U>(&mut self, id: NodeId, f: F) -> U
    where
        F: Fn(&mut NodeWrapper) -> U,
    {
        f(self.node_mut(id))
    }

    /// Returns a reference to the node's wrapper.
    #[inline]
    fn node(&mut self, id: NodeId) -> &NodeWrapper {
        self.nodes.get(&id).unwrap()
    }

    /// Returns a mutable reference to the node's wrapper.
    #[inline]
    fn node_mut(&mut self, id: NodeId) -> &mut NodeWrapper {
        self.nodes.get_mut(&id).unwrap()
    }

    /// Performs a single depth-first search of the graph, finding all successors from the node v,
    /// and storing all strongly connected components of that subgraph.
    fn strongconnect(&mut self, v: NodeId) {
        let index = self.index;
        self.node_with(v, |n| {
            n.index = index;
            n.low_link = index;
            n.on_stack = true;
        });

        self.index += 1;
        self.stack.push(v);

        for w in self.graph.adjacent_ids(v) {
            if self.node(w).index < 0 {
                // Successor w has not been visited yet; recurse on it
                self.strongconnect(w);
                let w_link = self.node(w).low_link;
                self.node_with(v, |n| n.low_link = n.low_link.min(w_link))
            } else if self.node(w).on_stack {
                // Successor w is on the stack and hence in the current strongly connected component (SCC)
                let w_index = self.node(w).index;
                self.node_with(v, |n| n.low_link = n.low_link.min(w_index))
            }
        }

        // If v is a root node, pop a node off the stack and generate an SCC
        if self.node_with(v, |n| n.low_link == n.index) {
            let mut scc = Vec::new();

            loop {
                let w = self.stack.pop().unwrap();
                self.node_mut(w).on_stack = false;
                scc.push(w);

                if w == v {
                    break;
                }
            }

            // The original implementation treats dead-ends as self-referential nodes, i.e.
            // both `A -> [A]` and `A -> []` produce `[[A]]`, while `A -> []` does not create a cycle.
            // In order to not detect dead-ends as self-referential, we ignore any single-component connections
            if scc.len() > 1 || self.graph.adjacent_set(scc[0]).contains(&scc[0]) {
                self.components.push(scc)
            }
        }
    }
}

// An iterator over the SCCs in a graph.
#[derive(Debug)]
pub struct Components<'a, T>
where
    T: Hash,
{
    /// The graph the components are from.
    graph: &'a DiGraph<T>,
    /// The components' nodes' ids.
    components: Vec<Vec<NodeId>>,
}

impl<'a, T> Iterator for Components<'a, T>
where
    T: Hash,
{
    type Item = Box<dyn Iterator<Item = &'a Node<T>> + 'a>;

    #[allow(clippy::manual_map)] // type inference issues
    fn next(&mut self) -> Option<Self::Item> {
        let g = self.graph;
        match self.components.pop() {
            Some(sccs) => Some(Box::new(
                sccs.into_iter().map(move |id| g.get_node(id).unwrap()),
            )),
            None => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cycle_detection() {
        let mut g = DiGraph::new();
        let n0 = g.add_node(0u32);
        let n1 = g.add_node(1);
        let n2 = g.add_node(2);
        let n3 = g.add_node(3);
        let n4 = g.add_node(4);
        let n5 = g.add_node(5);
        let n6 = g.add_node(6);
        let n7 = g.add_node(7);

        g.add_edge(n0, n1);
        g.add_edge(n1, n2);
        g.add_edge(n2, n0);
        g.add_edges(n3, &[n1, n2, n4]);
        g.add_edges(n4, &[n5, n3]);
        g.add_edges(n5, &[n2, n6]);
        g.add_edge(n6, n5);
        g.add_edges(n7, &[n4, n7, n6]);

        let expected = vec![vec![0u32, 1, 2], vec![5, 6], vec![3, 4], vec![7]];

        let mut cycles = g.find_cycles().collect::<Vec<_>>();
        cycles.reverse();
        assert_eq!(cycles.len(), expected.len());

        for (scc, exp) in cycles.into_iter().zip(expected.into_iter()) {
            let mut scc = scc.map(|n| n.data).collect::<Vec<_>>();
            scc.sort();
            assert_eq!(scc, exp);
        }
    }
}
