//! This module defines the [`Node`] and [`NodeId`] types.
use std::hash::Hash;

pub type NodeDataAccessor<T> = for<'r> fn(&'r Node<T>) -> &'r T;

/// The id of a node.
///
/// Note: Mixing nodes from different graphs may lead to a runtime panic.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct NodeId(pub(crate) usize);

/// A graph node identified by a [`NodeId`].
#[derive(Debug, Clone)]
pub struct Node<T> {
    /// The data associated with the node.
    pub(crate) data: T,
    /// THe node's id.
    id: NodeId,
}

impl<T> Node<T> {
    /// Creates a new node with the given node id.
    pub(crate) const fn new(data: T, id: NodeId) -> Self {
        Self { data, id }
    }

    // Returns a reference to the data associated with the node.
    #[inline]
    pub const fn data(&self) -> &T {
        &self.data
    }

    /// Consumes the node and returns its data.
    #[inline]
    pub fn into_data(self) -> T {
        self.data
    }

    /// Returns the id of the node.
    #[inline]
    pub const fn id(&self) -> NodeId {
        self.id
    }
}

impl<T> std::hash::Hash for Node<T>
where
    T: std::hash::Hash,
{
    #[inline]
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.data.hash(state);
    }
}
