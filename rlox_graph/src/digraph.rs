//! An insertion- and lookup-optimized DiGraph implementation.
use core::borrow::Borrow;
use std::{
    collections::{BTreeSet, HashMap, HashSet, VecDeque},
    hash::Hash,
    iter::FromIterator,
};

use crate::{
    adjacency_view::AdjacencyView,
    graphviz::NodeAttributes,
    node::*,
    tarjans_scc::{Components, TarjansSCC},
};

pub type EdgeSet<T> = BTreeSet<T>;
pub type EdgeSetIter<'a, T> = std::collections::btree_set::Iter<'a, T>;

/// A tombstone-like enum for interleaving occupied and vacant node slots
/// in the graph's baking vec.
#[derive(Debug, Clone)]
enum NodeEntry<T> {
    /// An occupied entry that stores a node.
    Occupied(Node<T>),
    /// A vacant entry that may be written over.
    Vacant,
}

impl<T> NodeEntry<T> {
    /// Updates the entry node's data.
    /// Panics if the entry is not occupied.
    #[inline]
    fn update_data(&mut self, data: T) {
        match self {
            NodeEntry::Occupied(node) => {
                node.data = data;
            }
            NodeEntry::Vacant => unreachable!(),
        }
    }

    /// Returns the node stored in the entry or None if the entry is vacant.
    #[inline]
    fn as_node(&self) -> Option<&Node<T>> {
        match self {
            NodeEntry::Occupied(node) => Some(node),
            NodeEntry::Vacant => None,
        }
    }

    /// Returns `true` if the entry is occupied.
    #[inline]
    pub fn is_occupied(&self) -> bool {
        matches!(self, NodeEntry::Occupied(_))
    }

    /// Returns `Ok(node)` if the entry is occupied, `Err(self)` otherwise.
    #[inline]
    pub fn into_node(self) -> Result<Node<T>, Self> {
        match self {
            NodeEntry::Occupied(n) => Ok(n),
            NodeEntry::Vacant => Err(self),
        }
    }
}

/// A directed graph. Does not preserve multiple edges between the same nodes.
#[derive(Debug, Clone, Default)]
pub struct DiGraph<T> {
    /// The nodes in the graph. This vec doesn't shrink when a node is removed; instead,
    /// the node is replaced with a Vacant entry to be later reused to store a new node.
    nodes: Vec<NodeEntry<T>>,
    /// The ids of the vacant nodes.
    ids: Vec<NodeId>,
    /// This field maps nodes' data hashes to NodeIds. This allows us to quickly retrieve a node
    /// by the hash of its data.
    existing: HashMap<u64, NodeId>,
    /// An adjacency map for the nodes in the graph. The edges reachable from each node
    /// are stored in a BTreeSet to ensure a deterministic order.
    edges: HashMap<NodeId, EdgeSet<NodeId>>,
}

// TODO: make this ID-safe
impl<T> DiGraph<T>
where
    T: Hash,
{
    /// Creates a new empty [`DiGraph`].
    pub fn new() -> Self {
        Self {
            nodes: Vec::new(),
            ids: Vec::new(),
            existing: HashMap::new(),
            edges: HashMap::new(),
        }
    }

    /// Creates a new [`DiGraph`] initialized with the nodes in the given iterator.
    /// Consider using [`DiGraph::from_set`] if your collection is guaranteed to contain only unique elements.
    pub fn from_iterator<I: Iterator<Item = T>>(iter: I) -> Self {
        let data = iter.collect::<Vec<_>>();
        let mut graph = Self::new();
        graph.add_nodes(data);
        graph
    }

    /// Creates a new [`DiGraph`] initialized with the nodes in the given set.
    pub fn from_set<S: DiGraphSet<T>>(set: S) -> Self {
        let mut g = Self::new();
        unsafe { g.add_nodes_fast(set.into_iter().collect()) };
        g
    }

    /// Clears all baking collections, removing all elements.
    /// Keeps the allocated memory for reuse
    pub fn clear(&mut self) {
        self.nodes.clear();
        self.ids.clear();
        self.existing.clear();
        self.edges.clear();
    }

    /// Re-creates the baking collections, removing all elements
    /// and returning the memory to the allocator.
    pub fn clear_and_drop(&mut self) {
        *self = Self::new();
    }

    /// Returns the existing edges.
    #[inline]
    pub fn edges(&self) -> &HashMap<NodeId, EdgeSet<NodeId>> {
        &self.edges
    }

    /// Returns an iterator over the graph's nodes.
    /// (The iterator skips over the vacant entries).
    #[inline]
    pub fn nodes(&self) -> NodeIterator<'_, T> {
        NodeIterator {
            g: self,
            current: 0,
        }
    }

    /// Returns the values of the graph's nodes.
    /// This function uses [`DiGraph::nodes`] internally.
    #[inline]
    pub fn node_values(&self) -> std::iter::Map<NodeIterator<'_, T>, NodeDataAccessor<T>> {
        self.nodes().map(Node::data)
    }

    /// Returns the number of nodes in the graph.
    #[inline]
    pub fn node_count(&self) -> usize {
        self.nodes.len() - self.ids.len()
    }

    /// Consumes the graph and returns a list of the stored nodes' data.
    pub fn into_data(self) -> Vec<T> {
        let mut result = Vec::with_capacity(self.node_count());
        self.nodes.into_iter().for_each(|entry| match entry {
            NodeEntry::Occupied(node) => result.push(node.into_data()),
            NodeEntry::Vacant => {}
        });
        result
    }

    /// Adds a new node to the graph. Returns the id of the added node.
    /// All operations on the graph generally require one or more ids.
    /// It is possible to obtain the id of a node by its value,
    /// see [`DiGraph::get_node_id`] for more info.
    pub fn add_node(&mut self, data: T) -> NodeId {
        let hash = crate::hash(&data);
        if let Some(id) = self.existing.get(&hash) {
            self.nodes.get_mut(id.0).unwrap().update_data(data);
            *id
        } else {
            let id = if let Some(id) = self.ids.pop() {
                *self.nodes.get_mut(id.0).unwrap() = NodeEntry::Occupied(Node::new(data, id));
                id
            } else {
                let id = NodeId(self.nodes.len());
                self.nodes.push(NodeEntry::Occupied(Node::new(data, id)));
                id
            };
            self.existing.insert(hash, id);
            self.edges.insert(id, EdgeSet::new());
            id
        }
    }

    /// Adds multiple nodes to the graph. Returns the ids of the added nodes.
    /// See [`DiGraph::add_node`] for more info.
    pub fn add_nodes(&mut self, data: Vec<T>) -> Vec<NodeId> {
        let mut ids = Vec::with_capacity(data.len());
        for node in data {
            ids.push(self.add_node(node));
        }
        ids
    }

    /// Approximately 4x faster than [`DiGraph::add_nodes`] with `u32` node data.
    /// This function is unsafe! Check out [`DiGraph::from_set`] for a safe alternative
    /// if you wish to construct a graph with a pre-defined set of nodes.
    ///
    /// # Safety
    /// The caller must ensure that the nodes being added are (a) unique and
    /// (b) not present in the graph already.
    pub unsafe fn add_nodes_fast(&mut self, data: Vec<T>) -> Vec<NodeId> {
        self.nodes.reserve(data.len());
        self.existing.reserve(data.len());
        self.edges.reserve(data.len());
        let mut ids = Vec::with_capacity(data.len());

        // TODO: Bench vs nodes.len()
        for (i, data) in data.into_iter().enumerate() {
            let id = NodeId(i);
            let hash = crate::hash(&data);
            self.nodes.push(NodeEntry::Occupied(Node::new(data, id)));
            self.existing.insert(hash, id);
            self.edges.insert(id, EdgeSet::new());
            ids.push(id);
        }

        ids
    }

    /// Adds the nodes from another graph to this graph.
    /// Returns the id of the first node in the graph.
    pub fn graft(&mut self, another: DiGraph<T>) -> Option<NodeId> {
        if another.node_count() == 0 {
            return None;
        }

        let mut id0 = None;
        let mut prev_to_new = HashMap::with_capacity(another.node_count());

        for node in another
            .nodes
            .into_iter()
            .filter(NodeEntry::is_occupied)
            .collect::<Vec<_>>()
        {
            match node.into_node() {
                Ok(ok) => {
                    let prev_id = ok.id();
                    let id = self.add_node(ok.into_data());
                    prev_to_new.insert(prev_id, id);
                    id0.get_or_insert(id);
                }
                Err(_) => unreachable!(),
            }
        }
        for (id, edges) in another.edges {
            let new_id = prev_to_new.get(&id).unwrap();
            let new_edges = edges
                .into_iter()
                .map(|id| prev_to_new.get(&id).unwrap())
                .copied()
                .collect::<Vec<_>>();
            self.add_edges(*new_id, &new_edges)
        }

        id0
    }

    /// Returns `true` if a node with the given data exists in the graph.
    #[inline]
    pub fn has_node<N: Borrow<T>>(&self, node: &N) -> bool {
        let hash = crate::hash(node.borrow());
        self.existing.contains_key(&hash)
    }

    /// Returns the id of the node with the given data if it exists in the graph.
    #[inline]
    pub fn get_node_id<N: Borrow<T>>(&self, data: &N) -> Option<NodeId> {
        self.existing.get(&crate::hash(data.borrow())).copied()
    }

    /// Returns the node with the given id.
    #[inline]
    pub fn get_node(&self, id: NodeId) -> Option<&Node<T>> {
        self.nodes.get(id.0).and_then(NodeEntry::as_node)
    }

    /// Adds a new directed edge to the graph.
    #[inline]
    pub fn add_edge(&mut self, from: NodeId, to: NodeId) {
        self.edges.get_mut(&from).map(|edges| edges.insert(to));
    }

    /// Adds multiple directed edges to the graph.
    #[inline]
    pub fn add_edges(&mut self, from: NodeId, to: &[NodeId]) {
        if let Some(edges) = self.edges.get_mut(&from) {
            to.iter().for_each(|to| {
                edges.insert(*to);
            })
        }
    }

    /// Returns `true` if there's an edge from `from` to `to`.
    #[inline]
    pub fn has_edge(&self, from: NodeId, to: NodeId) -> bool {
        self.edges
            .get(&from)
            .map(|edges| edges.contains(&to))
            .unwrap_or(false)
    }

    /// Returns `true` if there's an edge from `first` to `second`  OR `second` to `first`.
    pub fn connected(&self, first: NodeId, second: NodeId) -> bool {
        self.has_edge(first, second) || self.has_edge(second, first)
    }

    /// Returns `true` if there's an edge from `first` to `second` AND from `second` to `first`.
    pub fn interconnected(&self, first: NodeId, second: NodeId) -> bool {
        self.has_edge(first, second) && self.has_edge(second, first)
    }

    /// Returns an iterator over the nodes adjacent to the given node.
    ///
    /// ```rust
    /// # extern crate rlox_graph;
    /// use rlox_graph::{DiGraph, Node};
    ///
    /// let mut g = DiGraph::new();
    /// let ids = g.add_nodes(vec![1, 2, 3, 4]);
    /// g.add_edges(ids[0], &[ids[1], ids[2]]);
    ///
    /// let mut adj = g.adjacent(ids[0]);
    /// assert_eq!(adj.next().map(Node::data), Some(&2));
    /// assert_eq!(adj.next().map(Node::data), Some(&3));
    /// assert_eq!(adj.next().map(Node::data), None);
    /// ```
    pub fn adjacent(&self, id: NodeId) -> AdjacencyView<'_, T> {
        let edges = self.edges.get(&id).unwrap().iter();
        AdjacencyView { g: self, edges }
    }

    /// Returns an iterator over the ids of the nodes adjacent to the given node.
    ///
    /// ```rust
    /// # extern crate rlox_graph;
    /// use rlox_graph::{DiGraph, Node};
    ///
    /// let mut g = DiGraph::new();
    /// let ids = g.add_nodes(vec![1, 2, 3, 4]);
    /// g.add_edges(ids[0], &[ids[1], ids[2]]);
    ///
    /// let mut adj = g.adjacent_ids(ids[0]);
    /// assert_eq!(adj.next(), Some(ids[1]));
    /// assert_eq!(adj.next(), Some(ids[2]));
    /// assert_eq!(adj.next(), None);
    /// ```
    pub fn adjacent_ids(&self, id: NodeId) -> impl Iterator<Item = NodeId> + '_ {
        self.adjacent(id).map(|n| n.id())
    }

    /// Returns the set of the nodes adjacent to the given node.
    ///
    ///
    /// ```rust
    /// # extern crate rlox_graph;
    /// use rlox_graph::{DiGraph, Node};
    ///
    /// let mut g = DiGraph::new();
    /// let ids = g.add_nodes(vec![1, 2, 3, 4]);
    /// g.add_edges(ids[0], &[ids[1], ids[2]]);
    ///
    /// let adj = g.adjacent_set(ids[0]);
    /// assert_eq!(adj.len(), 2);
    /// assert!(adj.contains(&ids[1]));
    /// assert!(adj.contains(&ids[2]));
    /// assert!(!adj.contains(&ids[3]));
    /// ```
    #[inline]
    pub fn adjacent_set(&self, id: NodeId) -> &EdgeSet<NodeId> {
        self.edges.get(&id).unwrap()
    }

    // TODO: make this more efficient?
    /// Removes a node and its edges from the graph. This function also removes the node from all other edges.
    pub fn remove_node(&mut self, id: NodeId) -> Option<Node<T>> {
        match self.nodes.get_mut(id.0) {
            Some(entry) => {
                let entry = std::mem::replace(entry, NodeEntry::Vacant);
                match entry {
                    NodeEntry::Occupied(node) => {
                        let hash = crate::hash(&node);
                        self.edges.remove(&id);
                        self.existing.remove(&hash);
                        self.ids.push(node.id());
                        self.edges.values_mut().for_each(|edge| {
                            edge.remove(&id);
                        });
                        Some(node)
                    }
                    NodeEntry::Vacant => None,
                }
            }
            _ => None,
        }
    }

    /// Removes multiples nodes and their edges from the graph. This function also removes the nodes from all other edges.
    pub fn remove_nodes(&mut self, to_be_removed: Vec<NodeId>) -> Vec<Node<T>> {
        let mut removed = Vec::with_capacity(to_be_removed.len());
        for id in to_be_removed {
            if let Some(node) = self.remove_node(id) {
                removed.push(node);
            }
        }
        removed
    }

    // TODO: make an iterator for this
    /// Returns a list of all nodes reachable from the given root node using a variation of DFS.
    /// The root node is not considered reachable if it doesn't have an edge to itself or is not reachable from its child nodes.
    /// (i.e. A -> B produces \[B\], while A -> B, A -> A produces \[A, B\], and A -> B -> C -> A produces [A, B, C]).
    pub fn reachable(&self, root: NodeId) -> Vec<&Node<T>> {
        // Add the root node's adjacent vertices to the stack right away.
        // We want to do this to avoid having the root node in the reachable set by default
        // (since it may be adjacent to self).
        let mut stack: Vec<NodeId> = self.adjacent_ids(root).collect();
        // TODO: [bench] Compare with a single hashset + an extra loop
        let mut reachable = Vec::with_capacity(stack.len());
        let mut visited = HashSet::new();

        while let Some(id) = stack.pop() {
            let node = self.get_node(id).unwrap();
            reachable.push(node);
            visited.insert(id);

            stack.extend(self.adjacent_ids(id).filter(|id| !visited.contains(id)));
        }

        reachable
    }

    /// Returns a list of all nodes unreachable from the given root node using a variation of DFS.
    /// The root node is considered reachable even if it doesn't haven an edge to itself or isn't reachable from its child nodes.
    pub fn unreachable(&self, root: NodeId) -> Vec<&Node<T>> {
        let mut reachable: HashSet<NodeId> =
            self.reachable(root).into_iter().map(|n| n.id()).collect();
        // Mark the root as reachable for the purposes on this function.
        reachable.insert(root);

        let mut unreachable = vec![];
        for node in self.nodes() {
            if !reachable.contains(&node.id()) {
                unreachable.push(node)
            }
        }
        unreachable
    }

    /// Removes the nodes unreachable from the given root node from the graph.
    /// The root node is considered reachable even if it doesn't haven an edge to itself or isn't reachable from its child nodes.
    pub fn remove_unreachable(&mut self, root: NodeId) -> Vec<Node<T>> {
        let mut reachable: HashSet<NodeId> =
            self.reachable(root).into_iter().map(|n| n.id()).collect();
        // Mark the root as reachable for the purposes on this function.
        reachable.insert(root);

        let mut unreachable = vec![];

        for node in self.nodes() {
            if !reachable.contains(&node.id()) {
                unreachable.push(node.id())
            }
        }

        self.remove_nodes(unreachable)
    }

    /// Returns `true` if the graph has at least one cycle.
    pub fn has_cycles(&self) -> bool {
        let mut stack = Vec::new();
        let mut visited = HashSet::new();

        for n in self.nodes() {
            if visited.contains(&n.id()) {
                continue;
            }

            visited.insert(n.id());
            stack.extend(self.adjacent_ids(n.id()));
            while let Some(id) = stack.pop() {
                if visited.contains(&id) {
                    return true;
                }
                visited.insert(id);
                stack.extend(self.adjacent_ids(id));
            }
        }

        false
    }

    /// Implements the [`Kahn’s topological sorting algorithm`]. This function will return [`None`] if the graph contains a cycle.
    /// The relative order of different but topologically equal nodes is not guaranteed.
    ///
    /// [`Kahn’s topological sorting algorithm`]: https://en.wikipedia.org/wiki/Topological_sorting#Kahn's_algorithm
    pub fn toposort(&self) -> Option<Vec<&Node<T>>> {
        let mut incoming_edge_count = vec![0; self.node_count()];

        for incoming_to in self.edges().values() {
            for to in incoming_to {
                incoming_edge_count[to.0] += 1;
            }
        }

        let mut q = incoming_edge_count
            .iter()
            .enumerate()
            .filter(|(_, count)| **count == 0)
            .map(|(i, _)| NodeId(i))
            .collect::<VecDeque<_>>();
        let mut top_order = Vec::new();
        let mut n_visited = 0;

        while !q.is_empty() {
            let id = q.pop_front().unwrap();
            top_order.push(self.get_node(id).unwrap());

            for to in self.adjacent_ids(id) {
                incoming_edge_count[to.0] -= 1;
                if incoming_edge_count[to.0] == 0 {
                    q.push_back(to);
                }
            }

            n_visited += 1;
        }

        // There was a cycle in the graph
        if n_visited != self.node_count() {
            None
        } else {
            Some(top_order)
        }
    }

    /// Finds all cycles in the graph using the [`Tarjan's Strongly Connected Components`] algorithm.
    /// This function returns an iterator over the sets of nodes that form cycles. The sets are not guaranteed to be in order
    /// because the implementation uses a hashmap internally.
    ///
    /// [`Tarjan's Strongly Connected Components`]: crate::tarjans_scc::TarjansSCC
    pub fn find_cycles(&self) -> Components<'_, T> {
        let mut tj = TarjansSCC::new(self);
        tj.find_sccs();
        tj.components()
    }

    /// Exports the graph into the .dot Graphviz format. This function ensures
    /// that all nodes and edges in the resulting file are ordered by their NodeId.
    ///
    /// ```
    /// # extern crate rlox_graph;
    /// use rlox_graph::{DiGraph, graphviz::NodeShape};
    ///
    /// let mut g = DiGraph::new();
    /// let ids = g.add_nodes(vec![1, 2, 3, 4]);
    /// g.add_edges(ids[0], &[ids[1], ids[2]]);
    /// g.add_edge(ids[2], ids[3]);
    ///
    /// let dot = g.export_dot(|node, attrs| {
    ///     attrs.shape(if node.data() % 2 == 0 {
    ///         NodeShape::Box
    ///     } else {
    ///         NodeShape::Circle
    ///     });
    ///     format!("{}", node.data())
    /// });
    ///
    /// assert_eq!(dot, r#"strict digraph {
    /// 1 [shape=circle];
    /// 2 [shape=box];
    /// 3 [shape=circle];
    /// 4 [shape=box];
    /// 1 -> 2 ;
    /// 1 -> 3 ;
    /// 3 -> 4 ;
    /// }"#);
    /// ```
    pub fn export_dot<F>(&self, node_formatter: F) -> String
    where
        F: for<'a, 'b> Fn(&'a Node<T>, &'b mut NodeAttributes) -> String,
    {
        self.export_dot_with_edge_formatter(node_formatter, |_, _, _| ())
    }

    /// Exports the graph into the .dot Graphviz format. This function ensures
    /// that all nodes and edges in the resulting file are ordered by their NodeId.
    ///
    /// ```
    /// # extern crate rlox_graph;
    /// use rlox_graph::{
    ///     graphviz::{AttrStyle, NodeShape},
    ///     DiGraph,
    /// };
    ///
    /// let mut g = DiGraph::new();
    /// let ids = g.add_nodes(vec![1, 2, 3, 4]);
    /// g.add_edges(ids[0], &[ids[1], ids[2]]);
    /// g.add_edge(ids[2], ids[3]);
    ///
    /// let dot = g.export_dot_with_edge_formatter(
    ///     |node, attrs| {
    ///         attrs.shape(if node.data() % 2 == 0 {
    ///             NodeShape::Box
    ///         } else {
    ///             NodeShape::Circle
    ///         });
    ///         format!("{}", node.data())
    ///     },
    ///     |from, to, attrs| {
    ///         attrs.style(if (from.data() + to.data()) % 2 == 0 {
    ///             AttrStyle::Dashed
    ///         } else {
    ///             AttrStyle::Dotted
    ///         });
    ///     },
    /// );
    ///
    /// assert_eq!(
    ///     dot,
    ///     r#"strict digraph {
    /// 1 [shape=circle];
    /// 2 [shape=box];
    /// 3 [shape=circle];
    /// 4 [shape=box];
    /// 1 -> 2 [style=dotted];
    /// 1 -> 3 [style=dashed];
    /// 3 -> 4 [style=dotted];
    /// }"#
    /// );
    /// ```
    pub fn export_dot_with_edge_formatter<F, E>(
        &self,
        node_formatter: F,
        edge_formatter: E,
    ) -> String
    where
        F: for<'a, 'b> Fn(&'a Node<T>, &'b mut NodeAttributes) -> String,
        E: for<'a, 'b> Fn(&'a Node<T>, &'a Node<T>, &'b mut NodeAttributes),
    {
        let mut dot = String::from("strict digraph {\n");
        let mut node_map = HashMap::new();

        for node in self.nodes() {
            let mut attrs = NodeAttributes::new();
            let body = node_formatter(node, &mut attrs);
            node_map.insert(node.id(), (body, attrs));
        }

        let mut values = node_map.iter().collect::<Vec<_>>();
        values.sort_by_key(|(id, _)| id.0);
        for (_, (node, attrs)) in values {
            dot.push_str(&format!("{} {};\n", node, attrs));
        }

        let mut all_edges = self.edges().iter().collect::<Vec<_>>();
        all_edges.sort_by_key(|(id, _)| id.0);
        for (id, edges) in all_edges {
            let mut edges = edges.iter().collect::<Vec<_>>();
            edges.sort_by_key(|id| id.0);
            for edge_id in edges {
                let from = &node_map.get(id).unwrap().0;
                let to = &node_map.get(edge_id).unwrap().0;
                let mut attrs = NodeAttributes::new();
                edge_formatter(
                    self.get_node(*id).unwrap(),
                    self.get_node(*edge_id).unwrap(),
                    &mut attrs,
                );
                dot.push_str(&format!("{} -> {} {};\n", from, to, attrs));
            }
        }

        dot.push('}');
        dot
    }
}

impl<T> FromIterator<T> for DiGraph<T>
where
    T: Hash,
{
    /// An alias for [`DiGraph::from_iterator`]
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        Self::from_iterator(iter.into_iter())
    }
}

/// An iterator over the nodes in a directed graph.
pub struct NodeIterator<'a, T> {
    g: &'a DiGraph<T>,
    current: usize,
}

impl<'a, T> Iterator for NodeIterator<'a, T> {
    type Item = &'a Node<T>;

    fn next(&mut self) -> Option<Self::Item> {
        while let Some(NodeEntry::Vacant) = self.g.nodes.get(self.current) {
            self.current += 1;
        }

        self.current += 1;
        self.g.nodes.get(self.current - 1).map(|entry| match entry {
            NodeEntry::Occupied(node) => node,
            NodeEntry::Vacant => unreachable!(),
        })
    }
}

/// This trait ensures that only sets may be passed to the [`DiGraph::from_set`] associated function.
/// To create nodes from an arbitrary collection that upholds the required invariants, see the [`DiGraph::add_nodes_fast`] method.
pub trait DiGraphSet<T>: Sized + IntoIterator<Item = T> + private::Sealed {}

impl<T> DiGraphSet<T> for HashSet<T> {}
impl<T> DiGraphSet<T> for BTreeSet<T> {}

mod private {
    use super::*;
    pub trait Sealed {}

    impl<T> Sealed for HashSet<T> {}
    impl<T> Sealed for BTreeSet<T> {}
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::graphviz::NodeShape;

    /// Makes the following graph:
    ///
    /// ```ignore
    ///  a --> c --> d
    ///  `---> b
    /// ```
    fn make_graph() -> (DiGraph<&'static str>, NodeId, NodeId, NodeId, NodeId) {
        let mut g = DiGraph::new();
        let a = g.add_node("a");
        let b = g.add_node("b");
        let c = g.add_node("c");
        let d = g.add_node("d");
        g.add_edge(a, b);
        g.add_edge(a, c);
        g.add_edge(c, d);
        (g, a, b, c, d)
    }

    #[test]
    fn test_nodes_and_edges() {
        let (g, a, b, c, d) = make_graph();

        assert!(g.has_node(&"a"));
        assert!(g.has_node(&"b"));
        assert!(g.has_node(&"c"));
        assert!(g.has_node(&"d"));

        assert!(!g.has_node(&"e"));
        assert!(!g.has_node(&"f"));

        // Check that the right vertices are connected by directed edges
        assert!(g.has_edge(a, b));
        assert!(g.has_edge(a, c));
        assert!(g.has_edge(c, d));

        // Check that the wrong vertices are not connected by directed edges
        assert!(!g.has_edge(b, a));
        assert!(!g.has_edge(c, a));
        assert!(!g.has_edge(d, c));
        assert!(!g.has_edge(b, c));
        assert!(!g.has_edge(c, b));
        assert!(!g.has_edge(d, b));

        // Check that some vertices are connected (have an edge in any way)
        assert!(g.connected(b, a));
        assert!(g.connected(c, a));
        assert!(g.connected(d, c));
    }

    #[test]
    fn test_node_removal() {
        let (mut g, a, b, c, d) = make_graph();
        assert_eq!(
            g.node_values().collect::<Vec<_>>(),
            &[&"a", &"b", &"c", &"d"]
        );

        g.remove_node(b);
        assert!(!g.has_node(&"b"));
        assert!(!g.has_edge(a, b));
        assert!(g.has_edge(a, c));
        assert_eq!(g.node_values().collect::<Vec<_>>(), &[&"a", &"c", &"d"]);

        g.remove_nodes(vec![a, d]);
        assert!(!g.has_node(&"a"));
        assert!(!g.has_node(&"d"));
        assert!(!g.has_edge(a, c));
        assert!(!g.has_edge(c, d));
        assert_eq!(g.node_values().collect::<Vec<_>>(), &[&"c"]);
    }

    #[test]
    fn test_adjacency_view() {
        let (g, a, _, _, _) = make_graph();

        let mut adj = g.adjacent(a).map(|n| n.data()).collect::<Vec<_>>();
        adj.sort();
        assert_eq!(adj, [&"b", &"c"]);
    }

    #[test]
    fn test_graph_reachability_and_cyclicity() {
        let (mut g, a, _b, c, d) = make_graph();
        assert!(!g.has_cycles());

        // Create a cycle (c -> d -> e -> c)
        let e = g.add_node("e");
        g.add_edge(d, e);
        g.add_edge(e, c);
        assert!(g.has_cycles());

        let mut reachable = g
            .reachable(a)
            .into_iter()
            .map(|n| n.data())
            .collect::<Vec<_>>();
        reachable.sort();
        assert_eq!(reachable, [&"b", &"c", &"d", &"e"]);

        let mut reachable = g
            .reachable(d)
            .into_iter()
            .map(|n| n.data())
            .collect::<Vec<_>>();
        reachable.sort();
        assert_eq!(reachable, [&"c", &"d", &"e"]);
    }

    #[test]
    fn test_graph_unreachable_node_removal() {
        let (mut g, a, _, c, _) = make_graph();
        // Add a disconnected cluster to the graph
        let e = g.add_node("e");
        let f = g.add_node("f");
        g.add_edge(e, f);

        // The graphs looks like this at this point.
        // After finding the unreachable nodes, e and f should removed from the graph.
        //
        //     a          e
        //    / \         |
        //   b   c        f
        //       |
        //       d
        //
        assert_eq!(
            g.remove_unreachable(a)
                .into_iter()
                .map(|n| n.into_data())
                .collect::<Vec<_>>(),
            &["e", "f"]
        );
        assert_eq!(
            g.node_values().collect::<Vec<_>>(),
            &[&"a", &"b", &"c", &"d"]
        );

        // Now remove the nodes that are unreachable from c, a and b.
        assert_eq!(
            g.remove_unreachable(c)
                .into_iter()
                .map(|n| n.into_data())
                .collect::<Vec<_>>(),
            &["a", "b"]
        );
        assert_eq!(g.node_values().collect::<Vec<_>>(), &[&"c", &"d"]);
    }

    #[test]
    fn test_dot_export() {
        let (g, ..) = make_graph();
        let dot = g.export_dot(|node, attrs| {
            attrs.shape(NodeShape::Box);
            attrs.set("graph_id", format!("\"{:?}\"", node.id()));
            format!("{}", node.data())
        });
        assert_eq!(
            dot,
            "strict digraph {\n\
            a [graph_id=\"NodeId(0)\", shape=box];\n\
            b [graph_id=\"NodeId(1)\", shape=box];\n\
            c [graph_id=\"NodeId(2)\", shape=box];\n\
            d [graph_id=\"NodeId(3)\", shape=box];\n\
            a -> b ;\n\
            a -> c ;\n\
            c -> d ;\n\
            }"
        );
    }

    #[test]
    fn test_toposort() {
        let mut g = DiGraph::new();
        let n0 = g.add_node(0);
        let n1 = g.add_node(1);
        let n2 = g.add_node(2);
        let n3 = g.add_node(3);
        let n4 = g.add_node(4);
        let n5 = g.add_node(5);

        g.add_edges(n5, &[n0, n2]);
        g.add_edges(n4, &[n0, n1]);
        g.add_edge(n2, n3);
        g.add_edge(n3, n1);

        let result = g
            .toposort()
            .unwrap()
            .into_iter()
            .map(Node::data)
            .copied()
            .collect::<Vec<_>>();
        assert!(result == [4, 5, 0, 2, 3, 1] || result == [4, 5, 2, 0, 3, 1])
    }

    #[test]
    fn test_toposort_returns_none_in_graphs_with_cycles() {
        let (mut g, .., c, d) = make_graph();
        assert!(!g.has_cycles());

        let result = g
            .toposort()
            .map(|v| v.into_iter().map(Node::data).copied().collect::<Vec<_>>());
        assert!(
            result == Some(vec!["a", "c", "b", "d"]) || result == Some(vec!["a", "b", "c", "d"])
        );

        // Create a cycle (c -> d -> e -> c)
        let e = g.add_node("e");
        g.add_edge(d, e);
        g.add_edge(e, c);
        assert!(g.has_cycles());
        assert_eq!(
            g.toposort()
                .map(|v| v.into_iter().map(Node::data).copied().collect::<Vec<_>>()),
            None
        );
    }

    #[test]
    fn test_graph_grafting() {
        let (mut g, a, ..) = make_graph();
        let mut sub_g = DiGraph::from_iter(vec!["e", "f", "g"]);
        sub_g.add_edge(
            sub_g.get_node_id(&"e").unwrap(),
            sub_g.get_node_id(&"f").unwrap(),
        );
        sub_g.add_edge(
            sub_g.get_node_id(&"e").unwrap(),
            sub_g.get_node_id(&"g").unwrap(),
        );

        let first = g.graft(sub_g).unwrap();
        assert_eq!(g.node_count(), 7);
        assert_eq!(
            g.unreachable(a)
                .into_iter()
                .map(Node::data)
                .collect::<Vec<_>>(),
            &[&"e", &"f", &"g"]
        );

        g.add_edge(a, first);
        assert_eq!(
            g.unreachable(a)
                .into_iter()
                .map(Node::data)
                .collect::<Vec<_>>(),
            Vec::<&&str>::new(),
        );
    }
}
