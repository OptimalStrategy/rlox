//! Structs and Enums for Graphviz export.
use std::collections::HashMap;

/// The graphviz attributes of a node.
#[derive(Debug, Default)]
pub struct NodeAttributes {
    attrs: HashMap<String, String>,
}

impl NodeAttributes {
    /// Create a new empty [`NodeAttributes`] instance.
    pub fn new() -> Self {
        Self {
            attrs: HashMap::new(),
        }
    }

    /// Returns `true` if no attributes have been added.
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.attrs.is_empty()
    }

    /// Sets the shape attribute to the given value.
    #[inline]
    pub fn shape(&mut self, shape: NodeShape) -> &mut Self {
        self.set("shape", format!("{}", shape))
    }

    /// Sets the style attribute to the given value.
    #[inline]
    pub fn style(&mut self, style: AttrStyle) -> &mut Self {
        self.set("style", format!("{}", style))
    }

    /// Sets the color attribute to the given hex color value.
    /// The color is not verified to be a valid hex color string.
    #[inline]
    pub fn hex_color<S: Into<String>>(&mut self, color: S) -> &mut Self {
        self.set("color", format!("\"{}\"", color.into()))
    }

    /// Sets the color attribute to the given color value.
    /// The color is not verified to be a valid color.
    #[inline]
    pub fn color<S: Into<String>>(&mut self, color: S) -> &mut Self {
        self.set("color", color)
    }

    /// Sets the given attribute to the given value.
    #[inline]
    pub fn set<A, V>(&mut self, attr: A, value: V) -> &mut Self
    where
        A: Into<String>,
        V: Into<String>,
    {
        self.attrs.insert(attr.into(), value.into());
        self
    }
}

impl std::fmt::Display for NodeAttributes {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.attrs.is_empty() {
            write!(f, "")
        } else {
            write!(f, "[{}]", {
                let mut attrs = self
                    .attrs
                    .iter()
                    .map(|(k, v)| format!("{}={}", k, v))
                    .collect::<Vec<_>>();
                attrs.sort();
                attrs.join(", ")
            })
        }
    }
}

/// Graphviz's node shapes.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum NodeShape {
    Box,
    Polygon,
    Ellipse,
    Oval,
    Circle,
    Point,
    Egg,
    Triangle,
    Plaintext,
    Plain,
    Diamond,
    Trapezium,
    Parallelogram,
    House,
    Pentagon,
    Hexagon,
    Septagon,
    Octagon,
    Doublecircle,
    Doubleoctagon,
    Tripleoctagon,
    Invtriangle,
    Invtrapezium,
    Invhouse,
    Mdiamond,
    Msquare,
    Mcircle,
    Rect,
    Rectangle,
    Square,
    Star,
    None,
    Underline,
    Cylinder,
    Note,
    Tab,
    Folder,
    Box3d,
    Component,
    Promoter,
    Cds,
    Terminator,
    Utr,
    Primersite,
    Restrictionsite,
    Fivepoverhang,
    Threepoverhang,
    Noverhang,
    Assembly,
    Signature,
    Insulator,
    Ribosite,
    Rnastab,
    Proteasesite,
    Proteinstab,
    Rpromoter,
    Rarrow,
    Larrow,
    Lpromoter,
}

/// Graphviz's node and connection styles.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum AttrStyle {
    Dashed,
    Dotted,
    Solid,
    Invis,
    Bold,
    Tapered,
    Filled,
    Striped,
    Wedged,
    Diagonals,
    Rounded,
    Radial,
}

impl std::fmt::Display for NodeShape {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", format!("{:?}", self).to_lowercase())
    }
}

impl std::fmt::Display for AttrStyle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", format!("{:?}", self).to_lowercase())
    }
}
