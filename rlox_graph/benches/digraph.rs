#[macro_use]
extern crate criterion;
extern crate rand;
extern crate rlox_graph;

use criterion::{Benchmark, Criterion};
use rand::{thread_rng, RngCore};
use rlox_graph::DiGraph;
use std::collections::HashSet;
use std::iter::FromIterator;

fn digraph_add_nodes(c: &mut Criterion) {
    let mut rng = thread_rng();
    let mut nodes = (0..10_000).collect::<Vec<_>>();
    nodes.sort_by_key(|_| rng.next_u32());

    // Add 10k nodes
    let bench_nodes = nodes.clone();
    let bench = Benchmark::new("<DiGraph::add_nodes (u32, 10k)>", move |b| {
        let inner_nodes = bench_nodes.clone();
        b.iter(move || {
            let mut g = DiGraph::new();
            g.add_nodes(inner_nodes.clone());
        });
    });

    // Add 10k nodes (fast)
    let bench_nodes = nodes.clone();
    let bench = bench.with_function("<DiGraph::add_nodes_fast (u32, 10k)>", move |b| {
        let inner_nodes = bench_nodes.clone();
        b.iter(move || {
            let mut g = DiGraph::new();
            unsafe { g.add_nodes_fast(inner_nodes.clone()) };
        });
    });

    // Add 10k nodes from a set
    let bench_nodes = nodes.clone();
    let bench = bench.with_function("<DiGraph::from_set (u32, 10k)>", move |b| {
        let inner_set: HashSet<u32> = HashSet::from_iter(bench_nodes.clone());
        b.iter(move || DiGraph::from_set(inner_set.clone()));
    });

    c.bench("add_nodes", bench);
}

criterion_group!(graph_benches, digraph_add_nodes);
criterion_main!(graph_benches);
