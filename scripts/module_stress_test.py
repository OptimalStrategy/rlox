import os
import random
import string
from pathlib import Path
from typing import Optional, List, Tuple
from dataclasses import dataclass
from collections import namedtuple

MAX_DEPTH = 7
MAX_WIDTH = 6
DEPTH_P = 0.6
WIDTH_P = 0.7
ROOT_P = 0.1
CYCLES = True
RANDOM_CODE = True

# Random code parameters
N_VARS = 12
N_STATEMENTS = 5
MAX_STRING_LENGTH = 23
ASSIGNMENT_P = 0.2
KNOWN_COND_P = 0.5
ELSE_BLOCK_P = 0.5
CODE_MAX_DEPTH = 3

Var = namedtuple("Var", ["name", "val"])
Env = namedtuple("Env", ["num", "str", "const", "gen"])

def random_env(env, const: bool = True):
    if const:
        w = [0.3, 0.4, 0.3]
    else:
        w = [0.5, 0.5, 0.0]
    return random.choices([env.str, env.num, env.const], weights=w, k=1)[0]


def generate_import(package: str, root_relative: bool = True):
    s = "import "
    if root_relative:
        s += "root :: "
    return f"{s}{package};"


def generate_expr(env, pool=None) -> str:
    kind = random.choice(["+", "-", "*", "**", "/", "==", "!="])
    pool = pool or random_env(env)
    left, right = random.choice(pool), random.choice(pool)
    return f"{left.name} {kind} {right.name}"


def generate_statement(env, depth: int = 0) -> str:
    if depth > CODE_MAX_DEPTH:
        kind = "expr"
    else:
        kind = random.choice(["if", "expr", "for"])

    if kind == "if":
        return generate_if(env, depth + 1)
    elif kind == "for":
        return generate_for(env, depth + 1)
    elif kind == "expr":
        pool = random_env(env, const=False)
        var = random.choice(pool).name
        expr = generate_expr(env, pool)
        return f"{var} = {expr};"


def generate_block(env, depth: int) -> str:
    s = "{\n"
    for _ in range(random.randint(1, N_STATEMENTS)):
        stmt = generate_statement(env, depth + 1).split("\n")
        s += "\n".join(" " * 4 + line for line in stmt)
        s += "\n"

    s += "}"
    return s


def generate_if(env, depth: int) -> str:
    s = "if ("

    if random.random() < KNOWN_COND_P:
        cond = str(random.random() < 5).lower()
    else:
        cond = generate_expr(env)

    s += cond
    s += ") "
    s += generate_block(env, depth)

    if random.random() < ELSE_BLOCK_P:
        s += " else "
        s += generate_block(env, depth)
    s += "\n"
    return s


def generate_for(env, depth: int) -> str:
    loop_var = f"loop{next(env.gen)}"

    if random.random() < KNOWN_COND_P:
        start, end, inc  = 0, random.randint(10, 100), random.randint(1, 3)
    else:
        start, end, inc = random.choice(env.num).name, random.choice(env.num).name, 1

    s = f"for (var {loop_var} = {start}; {loop_var} < {end}; {loop_var} += {inc})"
    env.num.append(Var(loop_var, loop_var))

    s += generate_block(env, depth)
    s += "\n"

    env.num.pop()
    return s



def generate_random_code() -> str:
    gen = variable_name_generator()

    num = [
        Var(
            next(gen),
            random.randint(-10_000, 10_000)
            if random.random() < 0.7
            else '{0:.10f}'.format(random.random()),
        )
        for _ in range(N_VARS // 3)
    ]
    const = [
        Var(
            next(gen).upper(),
            random.randint(-10_000, 10_000)
            if random.random() < 0.7
            else '{0:.10f}'.format(random.random()),
        )
        for _ in range(N_VARS // 3)
    ]
    strs = [
        Var(
            next(gen),
            '"'
            + repr(
                "".join(
                    random.choice(string.ascii_letters + string.digits)
                    for _ in range(random.randint(0, MAX_STRING_LENGTH))
                )
            )[1:-1]
            + '"',
        )
        for _ in range(N_VARS // 3)
    ]

    env = Env(num, strs, const, generator())

    s = ""
    for v in env.const:
        s += f"const {v.name} = {v.val};\n"

    for v in env.num + env.str:
        s += f"var {v.name} = {v.val};\n"

    for _ in range(N_STATEMENTS):
        s += generate_statement(env, 0) + "\n"

    return s


def generator() -> Optional[str]:
    n = 0
    while True:
        yield n
        n += 1


def variable_name_generator() -> Optional[str]:
    for n in generator():
        yield f"var_{n}"


def module_name_generator() -> Optional[str]:
    for n in generator():
        yield f"module_{n}"


def package_name_generator() -> Optional[str]:
    for n in generator():
        yield f"package_{n}"


def generate_import_graph(ctx: "Context", path_so_far: List[str], depth: int):
    if depth <= 0:
        return

    for i in range(ctx.max_width):
        if random.random() < ctx.depth_prob:
            pkg = ctx.gen_package()
            generate_import_graph(ctx, path_so_far + [pkg], depth - 1)

        if random.random() < ctx.width_prob:
            name = ctx.gen_module()
            module = Module(name, path_so_far, ctx.sample_imports())
            ctx.add_module(module)
            if depth == ctx.max_depth:
                ctx.roots.append(module)

    if ctx.allow_cycles and ctx.modules:
        mod1 = ctx.modules[random.randint(0, len(ctx.modules) - 1)]
        mod2 = ctx.modules[random.randint(0, len(ctx.modules) - 1)]
        mod1.imports.append(mod2)
        mod2.imports.append(mod1)

        # if random.random() < ctx.depth_prob:
        #    pkg = ctx.gen_package()
        #    generate_import_graph(ctx, path_so_far + [pkg], depth - 1)


@dataclass
class Module:
    name: str
    package: List[str]
    imports: List["Module"]
    reachable: bool = False


@dataclass
class Context:
    max_depth: int = MAX_DEPTH
    max_width: int = MAX_WIDTH
    depth_prob: float = DEPTH_P
    width_prob: float = WIDTH_P
    root_prob: float = 0.1
    import_prob: float = 0.7
    n_imports: float = 4
    root_package = "mod_stress_test"
    allow_cycles: bool = CYCLES
    modules_: List[Module] = None
    roots_: List[Module] = None
    mod_gen = module_name_generator()
    pkg_gen = package_name_generator()

    def gen_module(self) -> str:
        return next(self.mod_gen)

    def gen_package(self) -> str:
        return next(self.pkg_gen)

    def add_module(self, name: str) -> str:
        if self.modules is None:
            self.modules = []
        self.modules.append(name)
        return name

    def sample_imports(self) -> List[Module]:
        if not self.modules:
            return []
        imports = []
        for _ in range(
            len(
                list(
                    filter(
                        lambda _: random.random() < self.import_prob,
                        range(self.n_imports),
                    )
                )
            )
        ):
            imports.append(self.modules[random.randint(0, len(self.modules) - 1)])
        return imports

    def generate(self):
        generate_import_graph(self, [self.root_package], self.max_depth)
        self.eliminate_orphans()

    def eliminate_orphans(self):
        roots = self.roots.copy()
        modules = self.modules.copy()
        while modules:
            stack = roots
            while stack:
                mod = stack.pop()
                if mod.reachable:
                    continue
                mod.reachable = True
                stack.extend(mod.imports)

            modules = [x for x in modules if not x.reachable]
            roots = []

            for i in range(len(modules) if len(modules) < 10 else len(modules) // 10):
                mod = modules[i]
                self.roots.append(mod)
                roots.append(mod)

    def make_dirs(self):
        for module in self.modules:
            path = "/".join(module.package)
            if not os.path.exists(path):
                os.makedirs(path)
            with open(os.path.join(path, f"{module.name}.lox"), "w") as f:
                source = ""
                for i in module.imports:
                    pkg = ".".join(i.package + [i.name])
                    source += f"{generate_import(pkg)}\n"
                if RANDOM_CODE:
                    source += f"{generate_random_code()}\n"
                f.write(source)

    @property
    def modules(self):
        if self.modules_ is None:
            self.modules_ = []
        return self.modules_

    @property
    def roots(self):
        if self.roots_ is None:
            self.roots_ = []
        return self.roots_


if __name__ == "__main__":
    ctx = Context()
    ctx.generate()
    ctx.make_dirs()

    print(f"Generated {len(ctx.modules)} modules (cycles: {ctx.allow_cycles}, code: {RANDOM_CODE})")
    with open("stress_test.lox", "w") as f:
        source = ""
        for i in ctx.roots:
            pkg = ".".join(i.package + [i.name])
            source += f"{generate_import(pkg)}\n"
        f.write(source)
