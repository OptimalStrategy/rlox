# A Rust implementation of the [Lox](http://craftinginterpreters.com) programming language

## Building
```bash
# A normal release build
$ cargo build --release

# An LTO-optimized release build targeting the current CPU + disabled bounds checks in the VM and smaller integers
$ CARGO_PROFILE_RELEASE_LTO=fat CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1 RUSTFLAGS='-Ctarget-cpu=native' cargo run --release --features=unchecked-vm,lox-64bit-int
```

## Testing
```bash
# Run all tests
$ cargo test --workspace

# Run all tests with safety checks disabled
$ cargo test --workspace --features=unchecked-vm

# Run all tests with Lox optimizations disabled
$ LOX_TEST_OPT_LEVEL=0 cargo test --workspace
```

## Benchmarking
```
cargo bench
```

## Features
| Name                 | Description                                                                                                                                            |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------       |
| debug-info           | Output additional debug information such as object reprs and VM execution logging.                                                                     |
| worker-debug-info    | Output additional debug information such as module queueing and worker thread startup/shutdown.                                                        |
| debug-stack-repr     | Use the `Debug` implementation for logging rather than `Display`. Useful when dealing with memory bugs or when a less verbose output is desired.       |
| ~~lox-test~~         | ~~Use relative paths rather than absolute paths when reporting errors.~~ Replaced with a CLI flag for absolute paths in in `v0.8.1`.                   |
| gc-debug             | Make the GC and allocator log allocations, object graph tracing, object collection, and object deallocation.                                           |
| gc-stats-only        | Only output the statistics of allocations and deallocations.                                                                                           |
| trace-lox-calls      | Log Lox `CallFrames`.                                                                                                                                  |
| profile-instructions | Record the frequency of every pair of instructions executed by the VM. Produces a CSV file and stores it in the `loxbuild` directory.                  |
| lox-64bit-ints       | Use 64-bit integers (`i64`) rather than the default 128-bit integers (`i128`). Can speed up certain programs by 4% to 7%.                              |
| unchecked-vm         | Disable some of the bounds and safety checks in the vm . Can speed up most programs programs by 5% to 15%.                                             |


## TODO
1. [x] String interpolation
2. [x] Maybe implement a Lox codegen pass
3. [x] `const` keyword
4. [x] `:?` operator
5. [x] `loop` statement
6. [x] `continue`/`break`.
7. [x] Add compiler tests for `if`, `match`, `for`, `while`, `loop`, and `for-in`.
8. [x] Port the rest of pylox's test suit
9. [ ] Better GC/Allocator, see the [Immix](https://www.cs.cornell.edu/courses/cs6120/2019fa/blog/immix/) paper for a possible implementation.
10. [x] Constant Folding
11. [x] Peephole optimizations
12. [x] Tail Call Optimization
    1. [x] Detect simple tail calls
    2. [ ] Detect tail calls in `:?` and similar situations
    3. [x] Introduce an InvokeMethodTCO instruction

13. [x] CFG Generation + Dead Code Elimination
14. [x] Module CCO caching
    1.  [ ] Make the binary parser faster
    2.  [ ] Test and document the implementation
15. [x] Module import support
    1. [x] Implement toposort
    2. [x] Find an efficient way to build and resolve the import graph
    3. [x] Come up with a neat way to parallelize module pipelining and compilation
    4. [x] Implement native `Module` objects and export of locals (Maybe introduce globals instead?)
    5. [x] Refactor the error API to support multiple modules
16. [x] Implement classes
17. [x] Implement inheritance
18. [ ] Profile a bunch of large Lox program for more peephole optimizations and a better dispatch loop branch order
    1. `donut.lox` - Done
19. [x] Do Constant Propagation on compile time `const` variables?
20. [x] Implement stack unwinding
21. [x] Implement the REPL
22. [ ] TUI debugger?
23. [ ] Implement the rest of pylox's standard library
24. [x] Switch the `DiGraph` implementation to `BTreeSet` to ensure deterministic order of edges
25. [ ] Document the code
26. [x] Support the main escape sequences
27. [ ] Add length to `Location`s (requires a better encoding). TODO: look at different encoding techniques and compare them to the current RLE. For example, we could encode different spans on the same line as (N, N common locations, K, K different lengths).
28. [x] ~~Rewrite the parser to use less recursion to avoid stackoverflows~~ Solved with `stacker`.
29. [x] Extra integer literals
    1. [x] Spaced integer literals, e.g. `1_000_000`
    1. [x] Hexadecimal integer literals, e.g. `0x8BADF00D`
    2. [x] Binary integer literals, e.g. `0b1111_1111`
30. [x] Scientific floating point literals, e.g. `10E5`, `3e-15`
31. [ ] Record syntax
    1. [x] Basic record syntax
    2. [ ] Method definitions
    3. [ ] Default arguments?
32. [x] Implement a native test runner (`rlox test`)
33. [ ] Make the VM thread-safe
    1.  [ ] Introduce a permanent allocator for interning, builtins, modules, etc.
    2.  [ ] Introduce a local allocator/gc that VMs would manage themselves.
    3.  [ ] Figure out how to share objects between VMs
