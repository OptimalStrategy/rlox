#![no_main]
extern crate rlox;

use libfuzzer_sys::fuzz_target;
use rlox::{make_pipeline, pipeline::IRPass, Context, SourceInfo};

fuzz_target!(|data: &[u8]| {
    // Skip anything that contains the word loop to prevent timeouts

    for win in data.windows(4) {
        if win == (b"loop") {
            return;
        }
    }

    if let Ok(source) = String::from_utf8(data.into()) {
        let ctx = Context::default().with_disabled_cache().into_shared();
        let module = ctx.write_shared().add_module(SourceInfo::new(
            String::from("<fuzz_from_raw_bytes>"),
            source,
            String::from("<fuzz-1>"),
        ));
        let _ = make_pipeline(ctx.clone()).apply(module).map(|ptr| {
            Context::terminate_workers(ctx.clone()).unwrap();
            if !Context::check_worker_errors_and_import_graph(ctx.clone()) {
                panic!();
            }

            let mut vm = rlox::vm::Vm::new(ctx.clone());
            vm.run(ptr)
        });
    }
});
