import root :: stdlib.array { Array };
import root :: stdlib.iterators { range };

import root :: stdlib.ml.reg { Regression };
import root :: stdlib.ml.metrics { r2score };


fun gen_data(n, f) {
    const x_train = [];
    const y_train = [];
    const x_test = [];
    const y_test = [];

    for (_ in range(n)) {
        var v = Math.random();
        x_train.push(Array([v, 1 / v]));
        y_train.push(f(v, 1 / v));

        v = Math.random();
        x_test.push(Array([v, 1 / v]));
        y_test.push(f(v, 1 / v));
    }

    return [x_train, y_train, x_test, y_test].map(Array);
}

// Optionally, pin the seed:
// Math.seed(0);
print "Seed: ${Math.seed()}";

const data = gen_data(100, fun(x1, x2) => 8 * x1 + x2);
const X_train = data .. 0;
const y_train = data .. 1;
const X_test = data .. 2;
const y_test = data .. 3;

const reg = Regression(2);
reg.log_training(true);
reg.fit(1000, X_train, y_train, 0.001);

print "Test Loss:  ${reg.loss(reg.predict(X_test), y_test)}";
print "R2 Score:   ${r2score(reg.predict(X_test), y_test)} (1.0 = perfect)";
print "Weights:    ${reg.W.arr}";
print "Bias:       ${reg.b}";
