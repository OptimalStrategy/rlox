const WIDTH = 48;
const HEIGHT = 24;
const HORIZONTAL_BORDER = (fun () {
    var s = "+";
    for (var i = 0; i < WIDTH; ++i) {
        s += "-";
    }
    s += "+";
    return s;
})();

// Constants
const APPLE_CHANCE = 0.05;

const EMPTY = 0;
const APPLE = -1;
const SNAKE_BODY = 1;
const SNAKE_HEAD = 2;

const DIR_STOP = 0;
const DIR_UP = 1;
const DIR_DOWN = 2;
const DIR_LEFT = 3;
const DIR_RIGHT = 4;


fun get_random_point() {
    return Math.random_in_range(0, HEIGHT) * WIDTH + Math.random_in_range(0, WIDTH);
}

class Snake {
    init() {
        this.map = [].fill(WIDTH * HEIGHT, EMPTY);
        this.head = get_random_point();
        this.map..this.head = SNAKE_HEAD;
        this.segments = [];
        this.direction = DIR_STOP;

        this.score = 0;
        this.spawn_apple();
    }

    spawn_apple() {
        const map = this.map;

        var point = get_random_point();
        while (map..point != EMPTY) {
            point = get_random_point();
        }

        map..point = APPLE;
    }

    render() {
        const map = this.map;
        print "Score: ${this.score}";
        print HORIZONTAL_BORDER;
        for (var y = 0; y < HEIGHT; ++y) {
            var row = y * WIDTH;
            Console.write("|", true);
            for (var x = 0; x < WIDTH; ++x) {
                const ch = match map..(row + x) as cell {
                     EMPTY => " ",
                     APPLE => "*",
                     SNAKE_BODY => "o",
                     SNAKE_HEAD => "@",
                    _ => panic("Unexpected cell: ${cell}"),
                };
                Console.write(ch, true);
            }
            print "|";
        }
        print HORIZONTAL_BORDER;
    }

    update() {
        const map = this.map;
        var prev_pos = this.head;

        const new_pos = this.move_in_direction(prev_pos);
        var new_pos_cell = map.get(new_pos, nil);

        if (new_pos_cell == nil) {
            return true;
        }

        match new_pos_cell {
            EMPTY => {}
            APPLE => {
                this.score += 1;
                this.segments.push(nil);
                this.spawn_apple();
            }
            SNAKE_BODY => return true;
            SNAKE_HEAD => return true;
        }

        map..prev_pos = EMPTY;
        const segments = this.segments;
        const last = segments.get(segments.length - 1, nil);
        for (var i = 0; i < segments.length; ++i) {
            var tmp = segments..i;
            segments..i = prev_pos;
            map..prev_pos = SNAKE_BODY;
            prev_pos = tmp;
        }

        if (last != nil) {
            map..last = EMPTY;
        }

        this.head = new_pos;
        map..new_pos = SNAKE_HEAD;


        if (Math.random() < APPLE_CHANCE) {
            this.spawn_apple();
        }

        return false;
    }

    move_in_direction(pos) {
        return match this.direction {
            DIR_UP => pos - WIDTH,
            DIR_DOWN => pos + WIDTH,
            DIR_LEFT => pos - 1,
            DIR_RIGHT => pos + 1,
            DIR_STOP => pos
        };
    }

    move_up() {
        this.direction = DIR_UP;
    }

    move_down() {
        this.direction = DIR_DOWN;
    }

    move_left()  {
        this.direction = DIR_LEFT;
    }

    move_right() {
        this.direction = DIR_RIGHT;
    }

    game_loop() {
        const keys = Console.keys(50);

        Console.hide_cursor();
        Console.clear();
        this.render();

        var run = true;
        while (run) {
            for (key in keys) {
                match key {
                    "up" => {
                        this.move_up();
                    }
                    "down" => {
                        this.move_down();
                    }
                    "left" => {
                        this.move_left();
                    }
                    "right" => {
                        this.move_right();
                    }
                    "q" => {
                        Console.show_cursor();
                        run = false;
                        break;
                    }
                }
            }

            if (this.direction != DIR_STOP) {
                Console.clear();
                if (this.update()) {
                    run = false;
                }
                this.render();
            }
        }

        Console.show_cursor();
    }
}

const snake = Snake();
snake.game_loop();
print("Your final score: ${snake.score}");
